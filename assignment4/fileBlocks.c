#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "mmu.h"
#include "param.h"

int main(int argc, char** argv){
  int fd;
  fd = open(argv[1],O_RDONLY);
  print_file_blocks(fd);
  close(fd);
  exit();
  return 0; 
}