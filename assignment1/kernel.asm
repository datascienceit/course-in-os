
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4 0f                	in     $0xf,%al

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 a0 10 00       	mov    $0x10a000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl $(stack + KSTACKSIZE), %esp
80100028:	bc 50 c6 10 80       	mov    $0x8010c650,%esp

  # Jump to main(), and switch to executing at
  # high addresses. The indirect call is needed because
  # the assembler produces a PC-relative instruction
  # for a direct jump.
  mov $main, %eax
8010002d:	b8 97 34 10 80       	mov    $0x80103497,%eax
  jmp *%eax
80100032:	ff e0                	jmp    *%eax

80100034 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100034:	55                   	push   %ebp
80100035:	89 e5                	mov    %esp,%ebp
80100037:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  initlock(&bcache.lock, "bcache");
8010003a:	c7 44 24 04 30 82 10 	movl   $0x80108230,0x4(%esp)
80100041:	80 
80100042:	c7 04 24 60 c6 10 80 	movl   $0x8010c660,(%esp)
80100049:	e8 c4 4b 00 00       	call   80104c12 <initlock>

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
8010004e:	c7 05 90 db 10 80 84 	movl   $0x8010db84,0x8010db90
80100055:	db 10 80 
  bcache.head.next = &bcache.head;
80100058:	c7 05 94 db 10 80 84 	movl   $0x8010db84,0x8010db94
8010005f:	db 10 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100062:	c7 45 f4 94 c6 10 80 	movl   $0x8010c694,-0xc(%ebp)
80100069:	eb 3a                	jmp    801000a5 <binit+0x71>
    b->next = bcache.head.next;
8010006b:	8b 15 94 db 10 80    	mov    0x8010db94,%edx
80100071:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100074:	89 50 10             	mov    %edx,0x10(%eax)
    b->prev = &bcache.head;
80100077:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010007a:	c7 40 0c 84 db 10 80 	movl   $0x8010db84,0xc(%eax)
    b->dev = -1;
80100081:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100084:	c7 40 04 ff ff ff ff 	movl   $0xffffffff,0x4(%eax)
    bcache.head.next->prev = b;
8010008b:	a1 94 db 10 80       	mov    0x8010db94,%eax
80100090:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100093:	89 50 0c             	mov    %edx,0xc(%eax)
    bcache.head.next = b;
80100096:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100099:	a3 94 db 10 80       	mov    %eax,0x8010db94

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010009e:	81 45 f4 18 02 00 00 	addl   $0x218,-0xc(%ebp)
801000a5:	81 7d f4 84 db 10 80 	cmpl   $0x8010db84,-0xc(%ebp)
801000ac:	72 bd                	jb     8010006b <binit+0x37>
    b->prev = &bcache.head;
    b->dev = -1;
    bcache.head.next->prev = b;
    bcache.head.next = b;
  }
}
801000ae:	c9                   	leave  
801000af:	c3                   	ret    

801000b0 <bget>:
// Look through buffer cache for sector on device dev.
// If not found, allocate fresh block.
// In either case, return B_BUSY buffer.
static struct buf*
bget(uint dev, uint sector)
{
801000b0:	55                   	push   %ebp
801000b1:	89 e5                	mov    %esp,%ebp
801000b3:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  acquire(&bcache.lock);
801000b6:	c7 04 24 60 c6 10 80 	movl   $0x8010c660,(%esp)
801000bd:	e8 71 4b 00 00       	call   80104c33 <acquire>

 loop:
  // Is the sector already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000c2:	a1 94 db 10 80       	mov    0x8010db94,%eax
801000c7:	89 45 f4             	mov    %eax,-0xc(%ebp)
801000ca:	eb 63                	jmp    8010012f <bget+0x7f>
    if(b->dev == dev && b->sector == sector){
801000cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000cf:	8b 40 04             	mov    0x4(%eax),%eax
801000d2:	3b 45 08             	cmp    0x8(%ebp),%eax
801000d5:	75 4f                	jne    80100126 <bget+0x76>
801000d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000da:	8b 40 08             	mov    0x8(%eax),%eax
801000dd:	3b 45 0c             	cmp    0xc(%ebp),%eax
801000e0:	75 44                	jne    80100126 <bget+0x76>
      if(!(b->flags & B_BUSY)){
801000e2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000e5:	8b 00                	mov    (%eax),%eax
801000e7:	83 e0 01             	and    $0x1,%eax
801000ea:	85 c0                	test   %eax,%eax
801000ec:	75 23                	jne    80100111 <bget+0x61>
        b->flags |= B_BUSY;
801000ee:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000f1:	8b 00                	mov    (%eax),%eax
801000f3:	89 c2                	mov    %eax,%edx
801000f5:	83 ca 01             	or     $0x1,%edx
801000f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801000fb:	89 10                	mov    %edx,(%eax)
        release(&bcache.lock);
801000fd:	c7 04 24 60 c6 10 80 	movl   $0x8010c660,(%esp)
80100104:	e8 8c 4b 00 00       	call   80104c95 <release>
        return b;
80100109:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010010c:	e9 93 00 00 00       	jmp    801001a4 <bget+0xf4>
      }
      sleep(b, &bcache.lock);
80100111:	c7 44 24 04 60 c6 10 	movl   $0x8010c660,0x4(%esp)
80100118:	80 
80100119:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010011c:	89 04 24             	mov    %eax,(%esp)
8010011f:	e8 31 48 00 00       	call   80104955 <sleep>
      goto loop;
80100124:	eb 9c                	jmp    801000c2 <bget+0x12>

  acquire(&bcache.lock);

 loop:
  // Is the sector already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
80100126:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100129:	8b 40 10             	mov    0x10(%eax),%eax
8010012c:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010012f:	81 7d f4 84 db 10 80 	cmpl   $0x8010db84,-0xc(%ebp)
80100136:	75 94                	jne    801000cc <bget+0x1c>
      goto loop;
    }
  }

  // Not cached; recycle some non-busy and clean buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100138:	a1 90 db 10 80       	mov    0x8010db90,%eax
8010013d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100140:	eb 4d                	jmp    8010018f <bget+0xdf>
    if((b->flags & B_BUSY) == 0 && (b->flags & B_DIRTY) == 0){
80100142:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100145:	8b 00                	mov    (%eax),%eax
80100147:	83 e0 01             	and    $0x1,%eax
8010014a:	85 c0                	test   %eax,%eax
8010014c:	75 38                	jne    80100186 <bget+0xd6>
8010014e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100151:	8b 00                	mov    (%eax),%eax
80100153:	83 e0 04             	and    $0x4,%eax
80100156:	85 c0                	test   %eax,%eax
80100158:	75 2c                	jne    80100186 <bget+0xd6>
      b->dev = dev;
8010015a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010015d:	8b 55 08             	mov    0x8(%ebp),%edx
80100160:	89 50 04             	mov    %edx,0x4(%eax)
      b->sector = sector;
80100163:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100166:	8b 55 0c             	mov    0xc(%ebp),%edx
80100169:	89 50 08             	mov    %edx,0x8(%eax)
      b->flags = B_BUSY;
8010016c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010016f:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
      release(&bcache.lock);
80100175:	c7 04 24 60 c6 10 80 	movl   $0x8010c660,(%esp)
8010017c:	e8 14 4b 00 00       	call   80104c95 <release>
      return b;
80100181:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100184:	eb 1e                	jmp    801001a4 <bget+0xf4>
      goto loop;
    }
  }

  // Not cached; recycle some non-busy and clean buffer.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100186:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100189:	8b 40 0c             	mov    0xc(%eax),%eax
8010018c:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010018f:	81 7d f4 84 db 10 80 	cmpl   $0x8010db84,-0xc(%ebp)
80100196:	75 aa                	jne    80100142 <bget+0x92>
      b->flags = B_BUSY;
      release(&bcache.lock);
      return b;
    }
  }
  panic("bget: no buffers");
80100198:	c7 04 24 37 82 10 80 	movl   $0x80108237,(%esp)
8010019f:	e8 99 03 00 00       	call   8010053d <panic>
}
801001a4:	c9                   	leave  
801001a5:	c3                   	ret    

801001a6 <bread>:

// Return a B_BUSY buf with the contents of the indicated disk sector.
struct buf*
bread(uint dev, uint sector)
{
801001a6:	55                   	push   %ebp
801001a7:	89 e5                	mov    %esp,%ebp
801001a9:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  b = bget(dev, sector);
801001ac:	8b 45 0c             	mov    0xc(%ebp),%eax
801001af:	89 44 24 04          	mov    %eax,0x4(%esp)
801001b3:	8b 45 08             	mov    0x8(%ebp),%eax
801001b6:	89 04 24             	mov    %eax,(%esp)
801001b9:	e8 f2 fe ff ff       	call   801000b0 <bget>
801001be:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(!(b->flags & B_VALID))
801001c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001c4:	8b 00                	mov    (%eax),%eax
801001c6:	83 e0 02             	and    $0x2,%eax
801001c9:	85 c0                	test   %eax,%eax
801001cb:	75 0b                	jne    801001d8 <bread+0x32>
    iderw(b);
801001cd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801001d0:	89 04 24             	mov    %eax,(%esp)
801001d3:	e8 6c 26 00 00       	call   80102844 <iderw>
  return b;
801001d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801001db:	c9                   	leave  
801001dc:	c3                   	ret    

801001dd <bwrite>:

// Write b's contents to disk.  Must be B_BUSY.
void
bwrite(struct buf *b)
{
801001dd:	55                   	push   %ebp
801001de:	89 e5                	mov    %esp,%ebp
801001e0:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
801001e3:	8b 45 08             	mov    0x8(%ebp),%eax
801001e6:	8b 00                	mov    (%eax),%eax
801001e8:	83 e0 01             	and    $0x1,%eax
801001eb:	85 c0                	test   %eax,%eax
801001ed:	75 0c                	jne    801001fb <bwrite+0x1e>
    panic("bwrite");
801001ef:	c7 04 24 48 82 10 80 	movl   $0x80108248,(%esp)
801001f6:	e8 42 03 00 00       	call   8010053d <panic>
  b->flags |= B_DIRTY;
801001fb:	8b 45 08             	mov    0x8(%ebp),%eax
801001fe:	8b 00                	mov    (%eax),%eax
80100200:	89 c2                	mov    %eax,%edx
80100202:	83 ca 04             	or     $0x4,%edx
80100205:	8b 45 08             	mov    0x8(%ebp),%eax
80100208:	89 10                	mov    %edx,(%eax)
  iderw(b);
8010020a:	8b 45 08             	mov    0x8(%ebp),%eax
8010020d:	89 04 24             	mov    %eax,(%esp)
80100210:	e8 2f 26 00 00       	call   80102844 <iderw>
}
80100215:	c9                   	leave  
80100216:	c3                   	ret    

80100217 <brelse>:

// Release a B_BUSY buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
80100217:	55                   	push   %ebp
80100218:	89 e5                	mov    %esp,%ebp
8010021a:	83 ec 18             	sub    $0x18,%esp
  if((b->flags & B_BUSY) == 0)
8010021d:	8b 45 08             	mov    0x8(%ebp),%eax
80100220:	8b 00                	mov    (%eax),%eax
80100222:	83 e0 01             	and    $0x1,%eax
80100225:	85 c0                	test   %eax,%eax
80100227:	75 0c                	jne    80100235 <brelse+0x1e>
    panic("brelse");
80100229:	c7 04 24 4f 82 10 80 	movl   $0x8010824f,(%esp)
80100230:	e8 08 03 00 00       	call   8010053d <panic>

  acquire(&bcache.lock);
80100235:	c7 04 24 60 c6 10 80 	movl   $0x8010c660,(%esp)
8010023c:	e8 f2 49 00 00       	call   80104c33 <acquire>

  b->next->prev = b->prev;
80100241:	8b 45 08             	mov    0x8(%ebp),%eax
80100244:	8b 40 10             	mov    0x10(%eax),%eax
80100247:	8b 55 08             	mov    0x8(%ebp),%edx
8010024a:	8b 52 0c             	mov    0xc(%edx),%edx
8010024d:	89 50 0c             	mov    %edx,0xc(%eax)
  b->prev->next = b->next;
80100250:	8b 45 08             	mov    0x8(%ebp),%eax
80100253:	8b 40 0c             	mov    0xc(%eax),%eax
80100256:	8b 55 08             	mov    0x8(%ebp),%edx
80100259:	8b 52 10             	mov    0x10(%edx),%edx
8010025c:	89 50 10             	mov    %edx,0x10(%eax)
  b->next = bcache.head.next;
8010025f:	8b 15 94 db 10 80    	mov    0x8010db94,%edx
80100265:	8b 45 08             	mov    0x8(%ebp),%eax
80100268:	89 50 10             	mov    %edx,0x10(%eax)
  b->prev = &bcache.head;
8010026b:	8b 45 08             	mov    0x8(%ebp),%eax
8010026e:	c7 40 0c 84 db 10 80 	movl   $0x8010db84,0xc(%eax)
  bcache.head.next->prev = b;
80100275:	a1 94 db 10 80       	mov    0x8010db94,%eax
8010027a:	8b 55 08             	mov    0x8(%ebp),%edx
8010027d:	89 50 0c             	mov    %edx,0xc(%eax)
  bcache.head.next = b;
80100280:	8b 45 08             	mov    0x8(%ebp),%eax
80100283:	a3 94 db 10 80       	mov    %eax,0x8010db94

  b->flags &= ~B_BUSY;
80100288:	8b 45 08             	mov    0x8(%ebp),%eax
8010028b:	8b 00                	mov    (%eax),%eax
8010028d:	89 c2                	mov    %eax,%edx
8010028f:	83 e2 fe             	and    $0xfffffffe,%edx
80100292:	8b 45 08             	mov    0x8(%ebp),%eax
80100295:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80100297:	8b 45 08             	mov    0x8(%ebp),%eax
8010029a:	89 04 24             	mov    %eax,(%esp)
8010029d:	e8 8c 47 00 00       	call   80104a2e <wakeup>

  release(&bcache.lock);
801002a2:	c7 04 24 60 c6 10 80 	movl   $0x8010c660,(%esp)
801002a9:	e8 e7 49 00 00       	call   80104c95 <release>
}
801002ae:	c9                   	leave  
801002af:	c3                   	ret    

801002b0 <inb>:
// Routines to let C code use special x86 instructions.

static inline uchar
inb(ushort port)
{
801002b0:	55                   	push   %ebp
801002b1:	89 e5                	mov    %esp,%ebp
801002b3:	53                   	push   %ebx
801002b4:	83 ec 14             	sub    $0x14,%esp
801002b7:	8b 45 08             	mov    0x8(%ebp),%eax
801002ba:	66 89 45 e8          	mov    %ax,-0x18(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801002be:	0f b7 55 e8          	movzwl -0x18(%ebp),%edx
801002c2:	66 89 55 ea          	mov    %dx,-0x16(%ebp)
801002c6:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
801002ca:	ec                   	in     (%dx),%al
801002cb:	89 c3                	mov    %eax,%ebx
801002cd:	88 5d fb             	mov    %bl,-0x5(%ebp)
  return data;
801002d0:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
}
801002d4:	83 c4 14             	add    $0x14,%esp
801002d7:	5b                   	pop    %ebx
801002d8:	5d                   	pop    %ebp
801002d9:	c3                   	ret    

801002da <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801002da:	55                   	push   %ebp
801002db:	89 e5                	mov    %esp,%ebp
801002dd:	83 ec 08             	sub    $0x8,%esp
801002e0:	8b 55 08             	mov    0x8(%ebp),%edx
801002e3:	8b 45 0c             	mov    0xc(%ebp),%eax
801002e6:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
801002ea:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801002ed:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
801002f1:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
801002f5:	ee                   	out    %al,(%dx)
}
801002f6:	c9                   	leave  
801002f7:	c3                   	ret    

801002f8 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
801002f8:	55                   	push   %ebp
801002f9:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
801002fb:	fa                   	cli    
}
801002fc:	5d                   	pop    %ebp
801002fd:	c3                   	ret    

801002fe <printint>:
  int locking;
} cons;

static void
printint(int xx, int base, int sign)
{
801002fe:	55                   	push   %ebp
801002ff:	89 e5                	mov    %esp,%ebp
80100301:	83 ec 48             	sub    $0x48,%esp
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
80100304:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100308:	74 19                	je     80100323 <printint+0x25>
8010030a:	8b 45 08             	mov    0x8(%ebp),%eax
8010030d:	c1 e8 1f             	shr    $0x1f,%eax
80100310:	89 45 10             	mov    %eax,0x10(%ebp)
80100313:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100317:	74 0a                	je     80100323 <printint+0x25>
    x = -xx;
80100319:	8b 45 08             	mov    0x8(%ebp),%eax
8010031c:	f7 d8                	neg    %eax
8010031e:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100321:	eb 06                	jmp    80100329 <printint+0x2b>
  else
    x = xx;
80100323:	8b 45 08             	mov    0x8(%ebp),%eax
80100326:	89 45 f0             	mov    %eax,-0x10(%ebp)

  i = 0;
80100329:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  do{
    buf[i++] = digits[x % base];
80100330:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80100333:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100336:	ba 00 00 00 00       	mov    $0x0,%edx
8010033b:	f7 f1                	div    %ecx
8010033d:	89 d0                	mov    %edx,%eax
8010033f:	0f b6 90 04 90 10 80 	movzbl -0x7fef6ffc(%eax),%edx
80100346:	8d 45 e0             	lea    -0x20(%ebp),%eax
80100349:	03 45 f4             	add    -0xc(%ebp),%eax
8010034c:	88 10                	mov    %dl,(%eax)
8010034e:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  }while((x /= base) != 0);
80100352:	8b 55 0c             	mov    0xc(%ebp),%edx
80100355:	89 55 d4             	mov    %edx,-0x2c(%ebp)
80100358:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010035b:	ba 00 00 00 00       	mov    $0x0,%edx
80100360:	f7 75 d4             	divl   -0x2c(%ebp)
80100363:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100366:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010036a:	75 c4                	jne    80100330 <printint+0x32>

  if(sign)
8010036c:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100370:	74 23                	je     80100395 <printint+0x97>
    buf[i++] = '-';
80100372:	8d 45 e0             	lea    -0x20(%ebp),%eax
80100375:	03 45 f4             	add    -0xc(%ebp),%eax
80100378:	c6 00 2d             	movb   $0x2d,(%eax)
8010037b:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)

  while(--i >= 0)
8010037f:	eb 14                	jmp    80100395 <printint+0x97>
    consputc(buf[i]);
80100381:	8d 45 e0             	lea    -0x20(%ebp),%eax
80100384:	03 45 f4             	add    -0xc(%ebp),%eax
80100387:	0f b6 00             	movzbl (%eax),%eax
8010038a:	0f be c0             	movsbl %al,%eax
8010038d:	89 04 24             	mov    %eax,(%esp)
80100390:	e8 ce 03 00 00       	call   80100763 <consputc>
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
80100395:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
80100399:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010039d:	79 e2                	jns    80100381 <printint+0x83>
    consputc(buf[i]);
}
8010039f:	c9                   	leave  
801003a0:	c3                   	ret    

801003a1 <cprintf>:
//PAGEBREAK: 50

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(char *fmt, ...)
{
801003a1:	55                   	push   %ebp
801003a2:	89 e5                	mov    %esp,%ebp
801003a4:	83 ec 38             	sub    $0x38,%esp
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
801003a7:	a1 f4 b5 10 80       	mov    0x8010b5f4,%eax
801003ac:	89 45 e8             	mov    %eax,-0x18(%ebp)
  if(locking)
801003af:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801003b3:	74 0c                	je     801003c1 <cprintf+0x20>
    acquire(&cons.lock);
801003b5:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
801003bc:	e8 72 48 00 00       	call   80104c33 <acquire>

  if (fmt == 0)
801003c1:	8b 45 08             	mov    0x8(%ebp),%eax
801003c4:	85 c0                	test   %eax,%eax
801003c6:	75 0c                	jne    801003d4 <cprintf+0x33>
    panic("null fmt");
801003c8:	c7 04 24 56 82 10 80 	movl   $0x80108256,(%esp)
801003cf:	e8 69 01 00 00       	call   8010053d <panic>

  argp = (uint*)(void*)(&fmt + 1);
801003d4:	8d 45 0c             	lea    0xc(%ebp),%eax
801003d7:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801003da:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801003e1:	e9 20 01 00 00       	jmp    80100506 <cprintf+0x165>
    if(c != '%'){
801003e6:	83 7d e4 25          	cmpl   $0x25,-0x1c(%ebp)
801003ea:	74 10                	je     801003fc <cprintf+0x5b>
      consputc(c);
801003ec:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801003ef:	89 04 24             	mov    %eax,(%esp)
801003f2:	e8 6c 03 00 00       	call   80100763 <consputc>
      continue;
801003f7:	e9 06 01 00 00       	jmp    80100502 <cprintf+0x161>
    }
    c = fmt[++i] & 0xff;
801003fc:	8b 55 08             	mov    0x8(%ebp),%edx
801003ff:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100403:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100406:	01 d0                	add    %edx,%eax
80100408:	0f b6 00             	movzbl (%eax),%eax
8010040b:	0f be c0             	movsbl %al,%eax
8010040e:	25 ff 00 00 00       	and    $0xff,%eax
80100413:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(c == 0)
80100416:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
8010041a:	0f 84 08 01 00 00    	je     80100528 <cprintf+0x187>
      break;
    switch(c){
80100420:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100423:	83 f8 70             	cmp    $0x70,%eax
80100426:	74 4d                	je     80100475 <cprintf+0xd4>
80100428:	83 f8 70             	cmp    $0x70,%eax
8010042b:	7f 13                	jg     80100440 <cprintf+0x9f>
8010042d:	83 f8 25             	cmp    $0x25,%eax
80100430:	0f 84 a6 00 00 00    	je     801004dc <cprintf+0x13b>
80100436:	83 f8 64             	cmp    $0x64,%eax
80100439:	74 14                	je     8010044f <cprintf+0xae>
8010043b:	e9 aa 00 00 00       	jmp    801004ea <cprintf+0x149>
80100440:	83 f8 73             	cmp    $0x73,%eax
80100443:	74 53                	je     80100498 <cprintf+0xf7>
80100445:	83 f8 78             	cmp    $0x78,%eax
80100448:	74 2b                	je     80100475 <cprintf+0xd4>
8010044a:	e9 9b 00 00 00       	jmp    801004ea <cprintf+0x149>
    case 'd':
      printint(*argp++, 10, 1);
8010044f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100452:	8b 00                	mov    (%eax),%eax
80100454:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
80100458:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
8010045f:	00 
80100460:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80100467:	00 
80100468:	89 04 24             	mov    %eax,(%esp)
8010046b:	e8 8e fe ff ff       	call   801002fe <printint>
      break;
80100470:	e9 8d 00 00 00       	jmp    80100502 <cprintf+0x161>
    case 'x':
    case 'p':
      printint(*argp++, 16, 0);
80100475:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100478:	8b 00                	mov    (%eax),%eax
8010047a:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
8010047e:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80100485:	00 
80100486:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
8010048d:	00 
8010048e:	89 04 24             	mov    %eax,(%esp)
80100491:	e8 68 fe ff ff       	call   801002fe <printint>
      break;
80100496:	eb 6a                	jmp    80100502 <cprintf+0x161>
    case 's':
      if((s = (char*)*argp++) == 0)
80100498:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010049b:	8b 00                	mov    (%eax),%eax
8010049d:	89 45 ec             	mov    %eax,-0x14(%ebp)
801004a0:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
801004a4:	0f 94 c0             	sete   %al
801004a7:	83 45 f0 04          	addl   $0x4,-0x10(%ebp)
801004ab:	84 c0                	test   %al,%al
801004ad:	74 20                	je     801004cf <cprintf+0x12e>
        s = "(null)";
801004af:	c7 45 ec 5f 82 10 80 	movl   $0x8010825f,-0x14(%ebp)
      for(; *s; s++)
801004b6:	eb 17                	jmp    801004cf <cprintf+0x12e>
        consputc(*s);
801004b8:	8b 45 ec             	mov    -0x14(%ebp),%eax
801004bb:	0f b6 00             	movzbl (%eax),%eax
801004be:	0f be c0             	movsbl %al,%eax
801004c1:	89 04 24             	mov    %eax,(%esp)
801004c4:	e8 9a 02 00 00       	call   80100763 <consputc>
      printint(*argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
801004c9:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
801004cd:	eb 01                	jmp    801004d0 <cprintf+0x12f>
801004cf:	90                   	nop
801004d0:	8b 45 ec             	mov    -0x14(%ebp),%eax
801004d3:	0f b6 00             	movzbl (%eax),%eax
801004d6:	84 c0                	test   %al,%al
801004d8:	75 de                	jne    801004b8 <cprintf+0x117>
        consputc(*s);
      break;
801004da:	eb 26                	jmp    80100502 <cprintf+0x161>
    case '%':
      consputc('%');
801004dc:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
801004e3:	e8 7b 02 00 00       	call   80100763 <consputc>
      break;
801004e8:	eb 18                	jmp    80100502 <cprintf+0x161>
    default:
      // Print unknown % sequence to draw attention.
      consputc('%');
801004ea:	c7 04 24 25 00 00 00 	movl   $0x25,(%esp)
801004f1:	e8 6d 02 00 00       	call   80100763 <consputc>
      consputc(c);
801004f6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801004f9:	89 04 24             	mov    %eax,(%esp)
801004fc:	e8 62 02 00 00       	call   80100763 <consputc>
      break;
80100501:	90                   	nop

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100502:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100506:	8b 55 08             	mov    0x8(%ebp),%edx
80100509:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010050c:	01 d0                	add    %edx,%eax
8010050e:	0f b6 00             	movzbl (%eax),%eax
80100511:	0f be c0             	movsbl %al,%eax
80100514:	25 ff 00 00 00       	and    $0xff,%eax
80100519:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010051c:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100520:	0f 85 c0 fe ff ff    	jne    801003e6 <cprintf+0x45>
80100526:	eb 01                	jmp    80100529 <cprintf+0x188>
      consputc(c);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
80100528:	90                   	nop
      consputc(c);
      break;
    }
  }

  if(locking)
80100529:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
8010052d:	74 0c                	je     8010053b <cprintf+0x19a>
    release(&cons.lock);
8010052f:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
80100536:	e8 5a 47 00 00       	call   80104c95 <release>
}
8010053b:	c9                   	leave  
8010053c:	c3                   	ret    

8010053d <panic>:

void
panic(char *s)
{
8010053d:	55                   	push   %ebp
8010053e:	89 e5                	mov    %esp,%ebp
80100540:	83 ec 48             	sub    $0x48,%esp
  int i;
  uint pcs[10];
  
  cli();
80100543:	e8 b0 fd ff ff       	call   801002f8 <cli>
  cons.locking = 0;
80100548:	c7 05 f4 b5 10 80 00 	movl   $0x0,0x8010b5f4
8010054f:	00 00 00 
  cprintf("cpu%d: panic: ", cpu->id);
80100552:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80100558:	0f b6 00             	movzbl (%eax),%eax
8010055b:	0f b6 c0             	movzbl %al,%eax
8010055e:	89 44 24 04          	mov    %eax,0x4(%esp)
80100562:	c7 04 24 66 82 10 80 	movl   $0x80108266,(%esp)
80100569:	e8 33 fe ff ff       	call   801003a1 <cprintf>
  cprintf(s);
8010056e:	8b 45 08             	mov    0x8(%ebp),%eax
80100571:	89 04 24             	mov    %eax,(%esp)
80100574:	e8 28 fe ff ff       	call   801003a1 <cprintf>
  cprintf("\n");
80100579:	c7 04 24 75 82 10 80 	movl   $0x80108275,(%esp)
80100580:	e8 1c fe ff ff       	call   801003a1 <cprintf>
  getcallerpcs(&s, pcs);
80100585:	8d 45 cc             	lea    -0x34(%ebp),%eax
80100588:	89 44 24 04          	mov    %eax,0x4(%esp)
8010058c:	8d 45 08             	lea    0x8(%ebp),%eax
8010058f:	89 04 24             	mov    %eax,(%esp)
80100592:	e8 4d 47 00 00       	call   80104ce4 <getcallerpcs>
  for(i=0; i<10; i++)
80100597:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010059e:	eb 1b                	jmp    801005bb <panic+0x7e>
    cprintf(" %p", pcs[i]);
801005a0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801005a3:	8b 44 85 cc          	mov    -0x34(%ebp,%eax,4),%eax
801005a7:	89 44 24 04          	mov    %eax,0x4(%esp)
801005ab:	c7 04 24 77 82 10 80 	movl   $0x80108277,(%esp)
801005b2:	e8 ea fd ff ff       	call   801003a1 <cprintf>
  cons.locking = 0;
  cprintf("cpu%d: panic: ", cpu->id);
  cprintf(s);
  cprintf("\n");
  getcallerpcs(&s, pcs);
  for(i=0; i<10; i++)
801005b7:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801005bb:	83 7d f4 09          	cmpl   $0x9,-0xc(%ebp)
801005bf:	7e df                	jle    801005a0 <panic+0x63>
    cprintf(" %p", pcs[i]);
  panicked = 1; // freeze other CPU
801005c1:	c7 05 a4 b5 10 80 01 	movl   $0x1,0x8010b5a4
801005c8:	00 00 00 
  for(;;)
    ;
801005cb:	eb fe                	jmp    801005cb <panic+0x8e>

801005cd <cgaputc>:
#define RIGHTARROW 0xE5
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
801005cd:	55                   	push   %ebp
801005ce:	89 e5                	mov    %esp,%ebp
801005d0:	83 ec 28             	sub    $0x28,%esp
  int pos;
  
  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
801005d3:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
801005da:	00 
801005db:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
801005e2:	e8 f3 fc ff ff       	call   801002da <outb>
  pos = inb(CRTPORT+1) << 8;
801005e7:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
801005ee:	e8 bd fc ff ff       	call   801002b0 <inb>
801005f3:	0f b6 c0             	movzbl %al,%eax
801005f6:	c1 e0 08             	shl    $0x8,%eax
801005f9:	89 45 f4             	mov    %eax,-0xc(%ebp)
  outb(CRTPORT, 15);
801005fc:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80100603:	00 
80100604:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
8010060b:	e8 ca fc ff ff       	call   801002da <outb>
  pos |= inb(CRTPORT+1);
80100610:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100617:	e8 94 fc ff ff       	call   801002b0 <inb>
8010061c:	0f b6 c0             	movzbl %al,%eax
8010061f:	09 45 f4             	or     %eax,-0xc(%ebp)

  if(c == '\n')
80100622:	83 7d 08 0a          	cmpl   $0xa,0x8(%ebp)
80100626:	75 30                	jne    80100658 <cgaputc+0x8b>
    pos += 80 - pos%80;
80100628:	8b 4d f4             	mov    -0xc(%ebp),%ecx
8010062b:	ba 67 66 66 66       	mov    $0x66666667,%edx
80100630:	89 c8                	mov    %ecx,%eax
80100632:	f7 ea                	imul   %edx
80100634:	c1 fa 05             	sar    $0x5,%edx
80100637:	89 c8                	mov    %ecx,%eax
80100639:	c1 f8 1f             	sar    $0x1f,%eax
8010063c:	29 c2                	sub    %eax,%edx
8010063e:	89 d0                	mov    %edx,%eax
80100640:	c1 e0 02             	shl    $0x2,%eax
80100643:	01 d0                	add    %edx,%eax
80100645:	c1 e0 04             	shl    $0x4,%eax
80100648:	89 ca                	mov    %ecx,%edx
8010064a:	29 c2                	sub    %eax,%edx
8010064c:	b8 50 00 00 00       	mov    $0x50,%eax
80100651:	29 d0                	sub    %edx,%eax
80100653:	01 45 f4             	add    %eax,-0xc(%ebp)
80100656:	eb 56                	jmp    801006ae <cgaputc+0xe1>
  else if(c == BACKSPACE){
80100658:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
8010065f:	75 0c                	jne    8010066d <cgaputc+0xa0>
    if(pos > 0) --pos;
80100661:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100665:	7e 47                	jle    801006ae <cgaputc+0xe1>
80100667:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
8010066b:	eb 41                	jmp    801006ae <cgaputc+0xe1>
  } else if(c == LEFTARROW){
8010066d:	81 7d 08 e4 00 00 00 	cmpl   $0xe4,0x8(%ebp)
80100674:	75 0c                	jne    80100682 <cgaputc+0xb5>
      if(pos > 0) {
80100676:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010067a:	7e 32                	jle    801006ae <cgaputc+0xe1>
	--pos;
8010067c:	83 6d f4 01          	subl   $0x1,-0xc(%ebp)
80100680:	eb 2c                	jmp    801006ae <cgaputc+0xe1>
      }
    }else if(c == RIGHTARROW){
80100682:	81 7d 08 e5 00 00 00 	cmpl   $0xe5,0x8(%ebp)
80100689:	75 06                	jne    80100691 <cgaputc+0xc4>
	++pos;
8010068b:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010068f:	eb 1d                	jmp    801006ae <cgaputc+0xe1>
      }else
      crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100691:	a1 00 90 10 80       	mov    0x80109000,%eax
80100696:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100699:	01 d2                	add    %edx,%edx
8010069b:	01 c2                	add    %eax,%edx
8010069d:	8b 45 08             	mov    0x8(%ebp),%eax
801006a0:	66 25 ff 00          	and    $0xff,%ax
801006a4:	80 cc 07             	or     $0x7,%ah
801006a7:	66 89 02             	mov    %ax,(%edx)
801006aa:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
  
  if((pos/80) >= 24){  // Scroll up.
801006ae:	81 7d f4 7f 07 00 00 	cmpl   $0x77f,-0xc(%ebp)
801006b5:	7e 53                	jle    8010070a <cgaputc+0x13d>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
801006b7:	a1 00 90 10 80       	mov    0x80109000,%eax
801006bc:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
801006c2:	a1 00 90 10 80       	mov    0x80109000,%eax
801006c7:	c7 44 24 08 60 0e 00 	movl   $0xe60,0x8(%esp)
801006ce:	00 
801006cf:	89 54 24 04          	mov    %edx,0x4(%esp)
801006d3:	89 04 24             	mov    %eax,(%esp)
801006d6:	e8 7a 48 00 00       	call   80104f55 <memmove>
    pos -= 80;
801006db:	83 6d f4 50          	subl   $0x50,-0xc(%ebp)
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
801006df:	b8 80 07 00 00       	mov    $0x780,%eax
801006e4:	2b 45 f4             	sub    -0xc(%ebp),%eax
801006e7:	01 c0                	add    %eax,%eax
801006e9:	8b 15 00 90 10 80    	mov    0x80109000,%edx
801006ef:	8b 4d f4             	mov    -0xc(%ebp),%ecx
801006f2:	01 c9                	add    %ecx,%ecx
801006f4:	01 ca                	add    %ecx,%edx
801006f6:	89 44 24 08          	mov    %eax,0x8(%esp)
801006fa:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100701:	00 
80100702:	89 14 24             	mov    %edx,(%esp)
80100705:	e8 78 47 00 00       	call   80104e82 <memset>
  }
  
  outb(CRTPORT, 14);
8010070a:	c7 44 24 04 0e 00 00 	movl   $0xe,0x4(%esp)
80100711:	00 
80100712:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100719:	e8 bc fb ff ff       	call   801002da <outb>
  outb(CRTPORT+1, pos>>8);
8010071e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100721:	c1 f8 08             	sar    $0x8,%eax
80100724:	0f b6 c0             	movzbl %al,%eax
80100727:	89 44 24 04          	mov    %eax,0x4(%esp)
8010072b:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
80100732:	e8 a3 fb ff ff       	call   801002da <outb>
  outb(CRTPORT, 15);
80100737:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
8010073e:	00 
8010073f:	c7 04 24 d4 03 00 00 	movl   $0x3d4,(%esp)
80100746:	e8 8f fb ff ff       	call   801002da <outb>
  outb(CRTPORT+1, pos);
8010074b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010074e:	0f b6 c0             	movzbl %al,%eax
80100751:	89 44 24 04          	mov    %eax,0x4(%esp)
80100755:	c7 04 24 d5 03 00 00 	movl   $0x3d5,(%esp)
8010075c:	e8 79 fb ff ff       	call   801002da <outb>
  
}
80100761:	c9                   	leave  
80100762:	c3                   	ret    

80100763 <consputc>:
int arrows_buf[128];
int buf_position = 0;

void
consputc(int c)
{
80100763:	55                   	push   %ebp
80100764:	89 e5                	mov    %esp,%ebp
80100766:	83 ec 18             	sub    $0x18,%esp
  if(panicked){
80100769:	a1 a4 b5 10 80       	mov    0x8010b5a4,%eax
8010076e:	85 c0                	test   %eax,%eax
80100770:	74 07                	je     80100779 <consputc+0x16>
    cli();
80100772:	e8 81 fb ff ff       	call   801002f8 <cli>
    for(;;)
      ;
80100777:	eb fe                	jmp    80100777 <consputc+0x14>
  }
  
  if(c == BACKSPACE){
80100779:	81 7d 08 00 01 00 00 	cmpl   $0x100,0x8(%ebp)
80100780:	75 33                	jne    801007b5 <consputc+0x52>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100782:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100789:	e8 07 61 00 00       	call   80106895 <uartputc>
8010078e:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100795:	e8 fb 60 00 00       	call   80106895 <uartputc>
8010079a:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
801007a1:	e8 ef 60 00 00       	call   80106895 <uartputc>
    buf_position--;
801007a6:	a1 a0 b5 10 80       	mov    0x8010b5a0,%eax
801007ab:	83 e8 01             	sub    $0x1,%eax
801007ae:	a3 a0 b5 10 80       	mov    %eax,0x8010b5a0
801007b3:	eb 7a                	jmp    8010082f <consputc+0xcc>
    }else if(c == LEFTARROW){
801007b5:	81 7d 08 e4 00 00 00 	cmpl   $0xe4,0x8(%ebp)
801007bc:	75 1b                	jne    801007d9 <consputc+0x76>
	uartputc('\b');
801007be:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
801007c5:	e8 cb 60 00 00       	call   80106895 <uartputc>
	buf_position--;
801007ca:	a1 a0 b5 10 80       	mov    0x8010b5a0,%eax
801007cf:	83 e8 01             	sub    $0x1,%eax
801007d2:	a3 a0 b5 10 80       	mov    %eax,0x8010b5a0
801007d7:	eb 56                	jmp    8010082f <consputc+0xcc>
      }else if(c == RIGHTARROW){
801007d9:	81 7d 08 e5 00 00 00 	cmpl   $0xe5,0x8(%ebp)
801007e0:	75 26                	jne    80100808 <consputc+0xa5>
	      uartputc(arrows_buf[buf_position-1]);
801007e2:	a1 a0 b5 10 80       	mov    0x8010b5a0,%eax
801007e7:	83 e8 01             	sub    $0x1,%eax
801007ea:	8b 04 85 60 de 10 80 	mov    -0x7fef21a0(,%eax,4),%eax
801007f1:	89 04 24             	mov    %eax,(%esp)
801007f4:	e8 9c 60 00 00       	call   80106895 <uartputc>
	      buf_position++;
801007f9:	a1 a0 b5 10 80       	mov    0x8010b5a0,%eax
801007fe:	83 c0 01             	add    $0x1,%eax
80100801:	a3 a0 b5 10 80       	mov    %eax,0x8010b5a0
80100806:	eb 27                	jmp    8010082f <consputc+0xcc>
	}else{
	  uartputc(c);
80100808:	8b 45 08             	mov    0x8(%ebp),%eax
8010080b:	89 04 24             	mov    %eax,(%esp)
8010080e:	e8 82 60 00 00       	call   80106895 <uartputc>
	  arrows_buf[buf_position] = c;
80100813:	a1 a0 b5 10 80       	mov    0x8010b5a0,%eax
80100818:	8b 55 08             	mov    0x8(%ebp),%edx
8010081b:	89 14 85 60 de 10 80 	mov    %edx,-0x7fef21a0(,%eax,4)
	  buf_position++;
80100822:	a1 a0 b5 10 80       	mov    0x8010b5a0,%eax
80100827:	83 c0 01             	add    $0x1,%eax
8010082a:	a3 a0 b5 10 80       	mov    %eax,0x8010b5a0
	}
  cgaputc(c);
8010082f:	8b 45 08             	mov    0x8(%ebp),%eax
80100832:	89 04 24             	mov    %eax,(%esp)
80100835:	e8 93 fd ff ff       	call   801005cd <cgaputc>
}
8010083a:	c9                   	leave  
8010083b:	c3                   	ret    

8010083c <consoleintr>:

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(void))
{
8010083c:	55                   	push   %ebp
8010083d:	89 e5                	mov    %esp,%ebp
8010083f:	83 ec 28             	sub    $0x28,%esp
  int c;

  acquire(&input.lock);
80100842:	c7 04 24 a0 dd 10 80 	movl   $0x8010dda0,(%esp)
80100849:	e8 e5 43 00 00       	call   80104c33 <acquire>
  while((c = getc()) >= 0){
8010084e:	e9 41 01 00 00       	jmp    80100994 <consoleintr+0x158>
    switch(c){
80100853:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100856:	83 f8 10             	cmp    $0x10,%eax
80100859:	74 1e                	je     80100879 <consoleintr+0x3d>
8010085b:	83 f8 10             	cmp    $0x10,%eax
8010085e:	7f 0a                	jg     8010086a <consoleintr+0x2e>
80100860:	83 f8 08             	cmp    $0x8,%eax
80100863:	74 68                	je     801008cd <consoleintr+0x91>
80100865:	e9 94 00 00 00       	jmp    801008fe <consoleintr+0xc2>
8010086a:	83 f8 15             	cmp    $0x15,%eax
8010086d:	74 2f                	je     8010089e <consoleintr+0x62>
8010086f:	83 f8 7f             	cmp    $0x7f,%eax
80100872:	74 59                	je     801008cd <consoleintr+0x91>
80100874:	e9 85 00 00 00       	jmp    801008fe <consoleintr+0xc2>
    case C('P'):  // Process listing.
      procdump();
80100879:	e8 53 42 00 00       	call   80104ad1 <procdump>
      break;
8010087e:	e9 11 01 00 00       	jmp    80100994 <consoleintr+0x158>
    case C('U'):  // Kill line.
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
80100883:	a1 5c de 10 80       	mov    0x8010de5c,%eax
80100888:	83 e8 01             	sub    $0x1,%eax
8010088b:	a3 5c de 10 80       	mov    %eax,0x8010de5c
        consputc(BACKSPACE);
80100890:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
80100897:	e8 c7 fe ff ff       	call   80100763 <consputc>
8010089c:	eb 01                	jmp    8010089f <consoleintr+0x63>
    switch(c){
    case C('P'):  // Process listing.
      procdump();
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
8010089e:	90                   	nop
8010089f:	8b 15 5c de 10 80    	mov    0x8010de5c,%edx
801008a5:	a1 58 de 10 80       	mov    0x8010de58,%eax
801008aa:	39 c2                	cmp    %eax,%edx
801008ac:	0f 84 db 00 00 00    	je     8010098d <consoleintr+0x151>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
801008b2:	a1 5c de 10 80       	mov    0x8010de5c,%eax
801008b7:	83 e8 01             	sub    $0x1,%eax
801008ba:	83 e0 7f             	and    $0x7f,%eax
801008bd:	0f b6 80 d4 dd 10 80 	movzbl -0x7fef222c(%eax),%eax
    switch(c){
    case C('P'):  // Process listing.
      procdump();
      break;
    case C('U'):  // Kill line.
      while(input.e != input.w &&
801008c4:	3c 0a                	cmp    $0xa,%al
801008c6:	75 bb                	jne    80100883 <consoleintr+0x47>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
801008c8:	e9 c0 00 00 00       	jmp    8010098d <consoleintr+0x151>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
801008cd:	8b 15 5c de 10 80    	mov    0x8010de5c,%edx
801008d3:	a1 58 de 10 80       	mov    0x8010de58,%eax
801008d8:	39 c2                	cmp    %eax,%edx
801008da:	0f 84 b0 00 00 00    	je     80100990 <consoleintr+0x154>
        input.e--;
801008e0:	a1 5c de 10 80       	mov    0x8010de5c,%eax
801008e5:	83 e8 01             	sub    $0x1,%eax
801008e8:	a3 5c de 10 80       	mov    %eax,0x8010de5c
        consputc(BACKSPACE);
801008ed:	c7 04 24 00 01 00 00 	movl   $0x100,(%esp)
801008f4:	e8 6a fe ff ff       	call   80100763 <consputc>
      }
      break;
801008f9:	e9 92 00 00 00       	jmp    80100990 <consoleintr+0x154>
    default:
      if(c != 0 && input.e-input.r < INPUT_BUF){
801008fe:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80100902:	0f 84 8b 00 00 00    	je     80100993 <consoleintr+0x157>
80100908:	8b 15 5c de 10 80    	mov    0x8010de5c,%edx
8010090e:	a1 54 de 10 80       	mov    0x8010de54,%eax
80100913:	89 d1                	mov    %edx,%ecx
80100915:	29 c1                	sub    %eax,%ecx
80100917:	89 c8                	mov    %ecx,%eax
80100919:	83 f8 7f             	cmp    $0x7f,%eax
8010091c:	77 75                	ja     80100993 <consoleintr+0x157>
        c = (c == '\r') ? '\n' : c;
8010091e:	83 7d f4 0d          	cmpl   $0xd,-0xc(%ebp)
80100922:	74 05                	je     80100929 <consoleintr+0xed>
80100924:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100927:	eb 05                	jmp    8010092e <consoleintr+0xf2>
80100929:	b8 0a 00 00 00       	mov    $0xa,%eax
8010092e:	89 45 f4             	mov    %eax,-0xc(%ebp)
        input.buf[input.e++ % INPUT_BUF] = c;
80100931:	a1 5c de 10 80       	mov    0x8010de5c,%eax
80100936:	89 c1                	mov    %eax,%ecx
80100938:	83 e1 7f             	and    $0x7f,%ecx
8010093b:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010093e:	88 91 d4 dd 10 80    	mov    %dl,-0x7fef222c(%ecx)
80100944:	83 c0 01             	add    $0x1,%eax
80100947:	a3 5c de 10 80       	mov    %eax,0x8010de5c
        consputc(c);
8010094c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010094f:	89 04 24             	mov    %eax,(%esp)
80100952:	e8 0c fe ff ff       	call   80100763 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100957:	83 7d f4 0a          	cmpl   $0xa,-0xc(%ebp)
8010095b:	74 18                	je     80100975 <consoleintr+0x139>
8010095d:	83 7d f4 04          	cmpl   $0x4,-0xc(%ebp)
80100961:	74 12                	je     80100975 <consoleintr+0x139>
80100963:	a1 5c de 10 80       	mov    0x8010de5c,%eax
80100968:	8b 15 54 de 10 80    	mov    0x8010de54,%edx
8010096e:	83 ea 80             	sub    $0xffffff80,%edx
80100971:	39 d0                	cmp    %edx,%eax
80100973:	75 1e                	jne    80100993 <consoleintr+0x157>
          input.w = input.e;
80100975:	a1 5c de 10 80       	mov    0x8010de5c,%eax
8010097a:	a3 58 de 10 80       	mov    %eax,0x8010de58
          wakeup(&input.r);
8010097f:	c7 04 24 54 de 10 80 	movl   $0x8010de54,(%esp)
80100986:	e8 a3 40 00 00       	call   80104a2e <wakeup>
        }
      }
      break;
8010098b:	eb 06                	jmp    80100993 <consoleintr+0x157>
      while(input.e != input.w &&
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
8010098d:	90                   	nop
8010098e:	eb 04                	jmp    80100994 <consoleintr+0x158>
    case C('H'): case '\x7f':  // Backspace
      if(input.e != input.w){
        input.e--;
        consputc(BACKSPACE);
      }
      break;
80100990:	90                   	nop
80100991:	eb 01                	jmp    80100994 <consoleintr+0x158>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
          input.w = input.e;
          wakeup(&input.r);
        }
      }
      break;
80100993:	90                   	nop
consoleintr(int (*getc)(void))
{
  int c;

  acquire(&input.lock);
  while((c = getc()) >= 0){
80100994:	8b 45 08             	mov    0x8(%ebp),%eax
80100997:	ff d0                	call   *%eax
80100999:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010099c:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801009a0:	0f 89 ad fe ff ff    	jns    80100853 <consoleintr+0x17>
        }
      }
      break;
    }
  }
  release(&input.lock);
801009a6:	c7 04 24 a0 dd 10 80 	movl   $0x8010dda0,(%esp)
801009ad:	e8 e3 42 00 00       	call   80104c95 <release>
}
801009b2:	c9                   	leave  
801009b3:	c3                   	ret    

801009b4 <consoleread>:

int
consoleread(struct inode *ip, char *dst, int n)
{
801009b4:	55                   	push   %ebp
801009b5:	89 e5                	mov    %esp,%ebp
801009b7:	83 ec 28             	sub    $0x28,%esp
  uint target;
  int c;

  iunlock(ip);
801009ba:	8b 45 08             	mov    0x8(%ebp),%eax
801009bd:	89 04 24             	mov    %eax,(%esp)
801009c0:	e8 81 10 00 00       	call   80101a46 <iunlock>
  target = n;
801009c5:	8b 45 10             	mov    0x10(%ebp),%eax
801009c8:	89 45 f4             	mov    %eax,-0xc(%ebp)
  acquire(&input.lock);
801009cb:	c7 04 24 a0 dd 10 80 	movl   $0x8010dda0,(%esp)
801009d2:	e8 5c 42 00 00       	call   80104c33 <acquire>
  while(n > 0){
801009d7:	e9 a8 00 00 00       	jmp    80100a84 <consoleread+0xd0>
    while(input.r == input.w){
      if(proc->killed){
801009dc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801009e2:	8b 40 24             	mov    0x24(%eax),%eax
801009e5:	85 c0                	test   %eax,%eax
801009e7:	74 21                	je     80100a0a <consoleread+0x56>
        release(&input.lock);
801009e9:	c7 04 24 a0 dd 10 80 	movl   $0x8010dda0,(%esp)
801009f0:	e8 a0 42 00 00       	call   80104c95 <release>
        ilock(ip);
801009f5:	8b 45 08             	mov    0x8(%ebp),%eax
801009f8:	89 04 24             	mov    %eax,(%esp)
801009fb:	e8 f8 0e 00 00       	call   801018f8 <ilock>
        return -1;
80100a00:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100a05:	e9 a9 00 00 00       	jmp    80100ab3 <consoleread+0xff>
      }
      sleep(&input.r, &input.lock);
80100a0a:	c7 44 24 04 a0 dd 10 	movl   $0x8010dda0,0x4(%esp)
80100a11:	80 
80100a12:	c7 04 24 54 de 10 80 	movl   $0x8010de54,(%esp)
80100a19:	e8 37 3f 00 00       	call   80104955 <sleep>
80100a1e:	eb 01                	jmp    80100a21 <consoleread+0x6d>

  iunlock(ip);
  target = n;
  acquire(&input.lock);
  while(n > 0){
    while(input.r == input.w){
80100a20:	90                   	nop
80100a21:	8b 15 54 de 10 80    	mov    0x8010de54,%edx
80100a27:	a1 58 de 10 80       	mov    0x8010de58,%eax
80100a2c:	39 c2                	cmp    %eax,%edx
80100a2e:	74 ac                	je     801009dc <consoleread+0x28>
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &input.lock);
    }
    c = input.buf[input.r++ % INPUT_BUF];
80100a30:	a1 54 de 10 80       	mov    0x8010de54,%eax
80100a35:	89 c2                	mov    %eax,%edx
80100a37:	83 e2 7f             	and    $0x7f,%edx
80100a3a:	0f b6 92 d4 dd 10 80 	movzbl -0x7fef222c(%edx),%edx
80100a41:	0f be d2             	movsbl %dl,%edx
80100a44:	89 55 f0             	mov    %edx,-0x10(%ebp)
80100a47:	83 c0 01             	add    $0x1,%eax
80100a4a:	a3 54 de 10 80       	mov    %eax,0x8010de54
    if(c == C('D')){  // EOF
80100a4f:	83 7d f0 04          	cmpl   $0x4,-0x10(%ebp)
80100a53:	75 17                	jne    80100a6c <consoleread+0xb8>
      if(n < target){
80100a55:	8b 45 10             	mov    0x10(%ebp),%eax
80100a58:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80100a5b:	73 2f                	jae    80100a8c <consoleread+0xd8>
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
80100a5d:	a1 54 de 10 80       	mov    0x8010de54,%eax
80100a62:	83 e8 01             	sub    $0x1,%eax
80100a65:	a3 54 de 10 80       	mov    %eax,0x8010de54
      }
      break;
80100a6a:	eb 20                	jmp    80100a8c <consoleread+0xd8>
    }
    *dst++ = c;
80100a6c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100a6f:	89 c2                	mov    %eax,%edx
80100a71:	8b 45 0c             	mov    0xc(%ebp),%eax
80100a74:	88 10                	mov    %dl,(%eax)
80100a76:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
    --n;
80100a7a:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
    if(c == '\n')
80100a7e:	83 7d f0 0a          	cmpl   $0xa,-0x10(%ebp)
80100a82:	74 0b                	je     80100a8f <consoleread+0xdb>
  int c;

  iunlock(ip);
  target = n;
  acquire(&input.lock);
  while(n > 0){
80100a84:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80100a88:	7f 96                	jg     80100a20 <consoleread+0x6c>
80100a8a:	eb 04                	jmp    80100a90 <consoleread+0xdc>
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
80100a8c:	90                   	nop
80100a8d:	eb 01                	jmp    80100a90 <consoleread+0xdc>
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
80100a8f:	90                   	nop
  }
  release(&input.lock);
80100a90:	c7 04 24 a0 dd 10 80 	movl   $0x8010dda0,(%esp)
80100a97:	e8 f9 41 00 00       	call   80104c95 <release>
  ilock(ip);
80100a9c:	8b 45 08             	mov    0x8(%ebp),%eax
80100a9f:	89 04 24             	mov    %eax,(%esp)
80100aa2:	e8 51 0e 00 00       	call   801018f8 <ilock>

  return target - n;
80100aa7:	8b 45 10             	mov    0x10(%ebp),%eax
80100aaa:	8b 55 f4             	mov    -0xc(%ebp),%edx
80100aad:	89 d1                	mov    %edx,%ecx
80100aaf:	29 c1                	sub    %eax,%ecx
80100ab1:	89 c8                	mov    %ecx,%eax
}
80100ab3:	c9                   	leave  
80100ab4:	c3                   	ret    

80100ab5 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100ab5:	55                   	push   %ebp
80100ab6:	89 e5                	mov    %esp,%ebp
80100ab8:	83 ec 28             	sub    $0x28,%esp
  int i;

  iunlock(ip);
80100abb:	8b 45 08             	mov    0x8(%ebp),%eax
80100abe:	89 04 24             	mov    %eax,(%esp)
80100ac1:	e8 80 0f 00 00       	call   80101a46 <iunlock>
  acquire(&cons.lock);
80100ac6:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
80100acd:	e8 61 41 00 00       	call   80104c33 <acquire>
  for(i = 0; i < n; i++)
80100ad2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80100ad9:	eb 1d                	jmp    80100af8 <consolewrite+0x43>
    consputc(buf[i] & 0xff);
80100adb:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100ade:	03 45 0c             	add    0xc(%ebp),%eax
80100ae1:	0f b6 00             	movzbl (%eax),%eax
80100ae4:	0f be c0             	movsbl %al,%eax
80100ae7:	25 ff 00 00 00       	and    $0xff,%eax
80100aec:	89 04 24             	mov    %eax,(%esp)
80100aef:	e8 6f fc ff ff       	call   80100763 <consputc>
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
80100af4:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100af8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100afb:	3b 45 10             	cmp    0x10(%ebp),%eax
80100afe:	7c db                	jl     80100adb <consolewrite+0x26>
    consputc(buf[i] & 0xff);
  release(&cons.lock);
80100b00:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
80100b07:	e8 89 41 00 00       	call   80104c95 <release>
  ilock(ip);
80100b0c:	8b 45 08             	mov    0x8(%ebp),%eax
80100b0f:	89 04 24             	mov    %eax,(%esp)
80100b12:	e8 e1 0d 00 00       	call   801018f8 <ilock>

  return n;
80100b17:	8b 45 10             	mov    0x10(%ebp),%eax
}
80100b1a:	c9                   	leave  
80100b1b:	c3                   	ret    

80100b1c <consoleinit>:

void
consoleinit(void)
{
80100b1c:	55                   	push   %ebp
80100b1d:	89 e5                	mov    %esp,%ebp
80100b1f:	83 ec 18             	sub    $0x18,%esp
  initlock(&cons.lock, "console");
80100b22:	c7 44 24 04 7b 82 10 	movl   $0x8010827b,0x4(%esp)
80100b29:	80 
80100b2a:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
80100b31:	e8 dc 40 00 00       	call   80104c12 <initlock>
  initlock(&input.lock, "input");
80100b36:	c7 44 24 04 83 82 10 	movl   $0x80108283,0x4(%esp)
80100b3d:	80 
80100b3e:	c7 04 24 a0 dd 10 80 	movl   $0x8010dda0,(%esp)
80100b45:	e8 c8 40 00 00       	call   80104c12 <initlock>

  devsw[CONSOLE].write = consolewrite;
80100b4a:	c7 05 0c ea 10 80 b5 	movl   $0x80100ab5,0x8010ea0c
80100b51:	0a 10 80 
  devsw[CONSOLE].read = consoleread;
80100b54:	c7 05 08 ea 10 80 b4 	movl   $0x801009b4,0x8010ea08
80100b5b:	09 10 80 
  cons.locking = 1;
80100b5e:	c7 05 f4 b5 10 80 01 	movl   $0x1,0x8010b5f4
80100b65:	00 00 00 

  picenable(IRQ_KBD);
80100b68:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100b6f:	e8 dd 2f 00 00       	call   80103b51 <picenable>
  ioapicenable(IRQ_KBD, 0);
80100b74:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80100b7b:	00 
80100b7c:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80100b83:	e8 7e 1e 00 00       	call   80102a06 <ioapicenable>
}
80100b88:	c9                   	leave  
80100b89:	c3                   	ret    
	...

80100b8c <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100b8c:	55                   	push   %ebp
80100b8d:	89 e5                	mov    %esp,%ebp
80100b8f:	81 ec 38 01 00 00    	sub    $0x138,%esp
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;

  if((ip = namei(path)) == 0)
80100b95:	8b 45 08             	mov    0x8(%ebp),%eax
80100b98:	89 04 24             	mov    %eax,(%esp)
80100b9b:	e8 fa 18 00 00       	call   8010249a <namei>
80100ba0:	89 45 d8             	mov    %eax,-0x28(%ebp)
80100ba3:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
80100ba7:	75 0a                	jne    80100bb3 <exec+0x27>
    return -1;
80100ba9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100bae:	e9 da 03 00 00       	jmp    80100f8d <exec+0x401>
  ilock(ip);
80100bb3:	8b 45 d8             	mov    -0x28(%ebp),%eax
80100bb6:	89 04 24             	mov    %eax,(%esp)
80100bb9:	e8 3a 0d 00 00       	call   801018f8 <ilock>
  pgdir = 0;
80100bbe:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
80100bc5:	c7 44 24 0c 34 00 00 	movl   $0x34,0xc(%esp)
80100bcc:	00 
80100bcd:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80100bd4:	00 
80100bd5:	8d 85 0c ff ff ff    	lea    -0xf4(%ebp),%eax
80100bdb:	89 44 24 04          	mov    %eax,0x4(%esp)
80100bdf:	8b 45 d8             	mov    -0x28(%ebp),%eax
80100be2:	89 04 24             	mov    %eax,(%esp)
80100be5:	e8 04 12 00 00       	call   80101dee <readi>
80100bea:	83 f8 33             	cmp    $0x33,%eax
80100bed:	0f 86 54 03 00 00    	jbe    80100f47 <exec+0x3bb>
    goto bad;
  if(elf.magic != ELF_MAGIC)
80100bf3:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100bf9:	3d 7f 45 4c 46       	cmp    $0x464c457f,%eax
80100bfe:	0f 85 46 03 00 00    	jne    80100f4a <exec+0x3be>
    goto bad;

  if((pgdir = setupkvm(kalloc)) == 0)
80100c04:	c7 04 24 8f 2b 10 80 	movl   $0x80102b8f,(%esp)
80100c0b:	e8 c9 6d 00 00       	call   801079d9 <setupkvm>
80100c10:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80100c13:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
80100c17:	0f 84 30 03 00 00    	je     80100f4d <exec+0x3c1>
    goto bad;

  // Load program into memory.
  sz = 0;
80100c1d:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100c24:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
80100c2b:	8b 85 28 ff ff ff    	mov    -0xd8(%ebp),%eax
80100c31:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100c34:	e9 c5 00 00 00       	jmp    80100cfe <exec+0x172>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100c39:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100c3c:	c7 44 24 0c 20 00 00 	movl   $0x20,0xc(%esp)
80100c43:	00 
80100c44:	89 44 24 08          	mov    %eax,0x8(%esp)
80100c48:	8d 85 ec fe ff ff    	lea    -0x114(%ebp),%eax
80100c4e:	89 44 24 04          	mov    %eax,0x4(%esp)
80100c52:	8b 45 d8             	mov    -0x28(%ebp),%eax
80100c55:	89 04 24             	mov    %eax,(%esp)
80100c58:	e8 91 11 00 00       	call   80101dee <readi>
80100c5d:	83 f8 20             	cmp    $0x20,%eax
80100c60:	0f 85 ea 02 00 00    	jne    80100f50 <exec+0x3c4>
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
80100c66:	8b 85 ec fe ff ff    	mov    -0x114(%ebp),%eax
80100c6c:	83 f8 01             	cmp    $0x1,%eax
80100c6f:	75 7f                	jne    80100cf0 <exec+0x164>
      continue;
    if(ph.memsz < ph.filesz)
80100c71:	8b 95 00 ff ff ff    	mov    -0x100(%ebp),%edx
80100c77:	8b 85 fc fe ff ff    	mov    -0x104(%ebp),%eax
80100c7d:	39 c2                	cmp    %eax,%edx
80100c7f:	0f 82 ce 02 00 00    	jb     80100f53 <exec+0x3c7>
      goto bad;
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100c85:	8b 95 f4 fe ff ff    	mov    -0x10c(%ebp),%edx
80100c8b:	8b 85 00 ff ff ff    	mov    -0x100(%ebp),%eax
80100c91:	01 d0                	add    %edx,%eax
80100c93:	89 44 24 08          	mov    %eax,0x8(%esp)
80100c97:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100c9a:	89 44 24 04          	mov    %eax,0x4(%esp)
80100c9e:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80100ca1:	89 04 24             	mov    %eax,(%esp)
80100ca4:	e8 02 71 00 00       	call   80107dab <allocuvm>
80100ca9:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100cac:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100cb0:	0f 84 a0 02 00 00    	je     80100f56 <exec+0x3ca>
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100cb6:	8b 8d fc fe ff ff    	mov    -0x104(%ebp),%ecx
80100cbc:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
80100cc2:	8b 85 f4 fe ff ff    	mov    -0x10c(%ebp),%eax
80100cc8:	89 4c 24 10          	mov    %ecx,0x10(%esp)
80100ccc:	89 54 24 0c          	mov    %edx,0xc(%esp)
80100cd0:	8b 55 d8             	mov    -0x28(%ebp),%edx
80100cd3:	89 54 24 08          	mov    %edx,0x8(%esp)
80100cd7:	89 44 24 04          	mov    %eax,0x4(%esp)
80100cdb:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80100cde:	89 04 24             	mov    %eax,(%esp)
80100ce1:	e8 d6 6f 00 00       	call   80107cbc <loaduvm>
80100ce6:	85 c0                	test   %eax,%eax
80100ce8:	0f 88 6b 02 00 00    	js     80100f59 <exec+0x3cd>
80100cee:	eb 01                	jmp    80100cf1 <exec+0x165>
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
80100cf0:	90                   	nop
  if((pgdir = setupkvm(kalloc)) == 0)
    goto bad;

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100cf1:	83 45 ec 01          	addl   $0x1,-0x14(%ebp)
80100cf5:	8b 45 e8             	mov    -0x18(%ebp),%eax
80100cf8:	83 c0 20             	add    $0x20,%eax
80100cfb:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100cfe:	0f b7 85 38 ff ff ff 	movzwl -0xc8(%ebp),%eax
80100d05:	0f b7 c0             	movzwl %ax,%eax
80100d08:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80100d0b:	0f 8f 28 ff ff ff    	jg     80100c39 <exec+0xad>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
80100d11:	8b 45 d8             	mov    -0x28(%ebp),%eax
80100d14:	89 04 24             	mov    %eax,(%esp)
80100d17:	e8 60 0e 00 00       	call   80101b7c <iunlockput>
  ip = 0;
80100d1c:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
80100d23:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d26:	05 ff 0f 00 00       	add    $0xfff,%eax
80100d2b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80100d30:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100d33:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d36:	05 00 20 00 00       	add    $0x2000,%eax
80100d3b:	89 44 24 08          	mov    %eax,0x8(%esp)
80100d3f:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d42:	89 44 24 04          	mov    %eax,0x4(%esp)
80100d46:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80100d49:	89 04 24             	mov    %eax,(%esp)
80100d4c:	e8 5a 70 00 00       	call   80107dab <allocuvm>
80100d51:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100d54:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80100d58:	0f 84 fe 01 00 00    	je     80100f5c <exec+0x3d0>
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100d5e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d61:	2d 00 20 00 00       	sub    $0x2000,%eax
80100d66:	89 44 24 04          	mov    %eax,0x4(%esp)
80100d6a:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80100d6d:	89 04 24             	mov    %eax,(%esp)
80100d70:	e8 5a 72 00 00       	call   80107fcf <clearpteu>
  sp = sz;
80100d75:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d78:	89 45 dc             	mov    %eax,-0x24(%ebp)

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80100d7b:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80100d82:	e9 81 00 00 00       	jmp    80100e08 <exec+0x27c>
    if(argc >= MAXARG)
80100d87:	83 7d e4 1f          	cmpl   $0x1f,-0x1c(%ebp)
80100d8b:	0f 87 ce 01 00 00    	ja     80100f5f <exec+0x3d3>
      goto bad;
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100d91:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100d94:	c1 e0 02             	shl    $0x2,%eax
80100d97:	03 45 0c             	add    0xc(%ebp),%eax
80100d9a:	8b 00                	mov    (%eax),%eax
80100d9c:	89 04 24             	mov    %eax,(%esp)
80100d9f:	e8 5c 43 00 00       	call   80105100 <strlen>
80100da4:	f7 d0                	not    %eax
80100da6:	03 45 dc             	add    -0x24(%ebp),%eax
80100da9:	83 e0 fc             	and    $0xfffffffc,%eax
80100dac:	89 45 dc             	mov    %eax,-0x24(%ebp)
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100daf:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100db2:	c1 e0 02             	shl    $0x2,%eax
80100db5:	03 45 0c             	add    0xc(%ebp),%eax
80100db8:	8b 00                	mov    (%eax),%eax
80100dba:	89 04 24             	mov    %eax,(%esp)
80100dbd:	e8 3e 43 00 00       	call   80105100 <strlen>
80100dc2:	83 c0 01             	add    $0x1,%eax
80100dc5:	89 c2                	mov    %eax,%edx
80100dc7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100dca:	c1 e0 02             	shl    $0x2,%eax
80100dcd:	03 45 0c             	add    0xc(%ebp),%eax
80100dd0:	8b 00                	mov    (%eax),%eax
80100dd2:	89 54 24 0c          	mov    %edx,0xc(%esp)
80100dd6:	89 44 24 08          	mov    %eax,0x8(%esp)
80100dda:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100ddd:	89 44 24 04          	mov    %eax,0x4(%esp)
80100de1:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80100de4:	89 04 24             	mov    %eax,(%esp)
80100de7:	e8 97 73 00 00       	call   80108183 <copyout>
80100dec:	85 c0                	test   %eax,%eax
80100dee:	0f 88 6e 01 00 00    	js     80100f62 <exec+0x3d6>
      goto bad;
    ustack[3+argc] = sp;
80100df4:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100df7:	8d 50 03             	lea    0x3(%eax),%edx
80100dfa:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100dfd:	89 84 95 40 ff ff ff 	mov    %eax,-0xc0(%ebp,%edx,4)
    goto bad;
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
80100e04:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80100e08:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e0b:	c1 e0 02             	shl    $0x2,%eax
80100e0e:	03 45 0c             	add    0xc(%ebp),%eax
80100e11:	8b 00                	mov    (%eax),%eax
80100e13:	85 c0                	test   %eax,%eax
80100e15:	0f 85 6c ff ff ff    	jne    80100d87 <exec+0x1fb>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[3+argc] = sp;
  }
  ustack[3+argc] = 0;
80100e1b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e1e:	83 c0 03             	add    $0x3,%eax
80100e21:	c7 84 85 40 ff ff ff 	movl   $0x0,-0xc0(%ebp,%eax,4)
80100e28:	00 00 00 00 

  ustack[0] = 0xffffffff;  // fake return PC
80100e2c:	c7 85 40 ff ff ff ff 	movl   $0xffffffff,-0xc0(%ebp)
80100e33:	ff ff ff 
  ustack[1] = argc;
80100e36:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e39:	89 85 44 ff ff ff    	mov    %eax,-0xbc(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100e3f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e42:	83 c0 01             	add    $0x1,%eax
80100e45:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80100e4c:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100e4f:	29 d0                	sub    %edx,%eax
80100e51:	89 85 48 ff ff ff    	mov    %eax,-0xb8(%ebp)

  sp -= (3+argc+1) * 4;
80100e57:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e5a:	83 c0 04             	add    $0x4,%eax
80100e5d:	c1 e0 02             	shl    $0x2,%eax
80100e60:	29 45 dc             	sub    %eax,-0x24(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100e63:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100e66:	83 c0 04             	add    $0x4,%eax
80100e69:	c1 e0 02             	shl    $0x2,%eax
80100e6c:	89 44 24 0c          	mov    %eax,0xc(%esp)
80100e70:	8d 85 40 ff ff ff    	lea    -0xc0(%ebp),%eax
80100e76:	89 44 24 08          	mov    %eax,0x8(%esp)
80100e7a:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100e7d:	89 44 24 04          	mov    %eax,0x4(%esp)
80100e81:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80100e84:	89 04 24             	mov    %eax,(%esp)
80100e87:	e8 f7 72 00 00       	call   80108183 <copyout>
80100e8c:	85 c0                	test   %eax,%eax
80100e8e:	0f 88 d1 00 00 00    	js     80100f65 <exec+0x3d9>
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80100e94:	8b 45 08             	mov    0x8(%ebp),%eax
80100e97:	89 45 f4             	mov    %eax,-0xc(%ebp)
80100e9a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100e9d:	89 45 f0             	mov    %eax,-0x10(%ebp)
80100ea0:	eb 17                	jmp    80100eb9 <exec+0x32d>
    if(*s == '/')
80100ea2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100ea5:	0f b6 00             	movzbl (%eax),%eax
80100ea8:	3c 2f                	cmp    $0x2f,%al
80100eaa:	75 09                	jne    80100eb5 <exec+0x329>
      last = s+1;
80100eac:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100eaf:	83 c0 01             	add    $0x1,%eax
80100eb2:	89 45 f0             	mov    %eax,-0x10(%ebp)
  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
80100eb5:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80100eb9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100ebc:	0f b6 00             	movzbl (%eax),%eax
80100ebf:	84 c0                	test   %al,%al
80100ec1:	75 df                	jne    80100ea2 <exec+0x316>
    if(*s == '/')
      last = s+1;
  safestrcpy(proc->name, last, sizeof(proc->name));
80100ec3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100ec9:	8d 50 6c             	lea    0x6c(%eax),%edx
80100ecc:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
80100ed3:	00 
80100ed4:	8b 45 f0             	mov    -0x10(%ebp),%eax
80100ed7:	89 44 24 04          	mov    %eax,0x4(%esp)
80100edb:	89 14 24             	mov    %edx,(%esp)
80100ede:	e8 cf 41 00 00       	call   801050b2 <safestrcpy>

  // Commit to the user image.
  oldpgdir = proc->pgdir;
80100ee3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100ee9:	8b 40 04             	mov    0x4(%eax),%eax
80100eec:	89 45 d0             	mov    %eax,-0x30(%ebp)
  proc->pgdir = pgdir;
80100eef:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100ef5:	8b 55 d4             	mov    -0x2c(%ebp),%edx
80100ef8:	89 50 04             	mov    %edx,0x4(%eax)
  proc->sz = sz;
80100efb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f01:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100f04:	89 10                	mov    %edx,(%eax)
  proc->tf->eip = elf.entry;  // main
80100f06:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f0c:	8b 40 18             	mov    0x18(%eax),%eax
80100f0f:	8b 95 24 ff ff ff    	mov    -0xdc(%ebp),%edx
80100f15:	89 50 38             	mov    %edx,0x38(%eax)
  proc->tf->esp = sp;
80100f18:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f1e:	8b 40 18             	mov    0x18(%eax),%eax
80100f21:	8b 55 dc             	mov    -0x24(%ebp),%edx
80100f24:	89 50 44             	mov    %edx,0x44(%eax)
  switchuvm(proc);
80100f27:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80100f2d:	89 04 24             	mov    %eax,(%esp)
80100f30:	e8 95 6b 00 00       	call   80107aca <switchuvm>
  freevm(oldpgdir);
80100f35:	8b 45 d0             	mov    -0x30(%ebp),%eax
80100f38:	89 04 24             	mov    %eax,(%esp)
80100f3b:	e8 01 70 00 00       	call   80107f41 <freevm>
  return 0;
80100f40:	b8 00 00 00 00       	mov    $0x0,%eax
80100f45:	eb 46                	jmp    80100f8d <exec+0x401>
  ilock(ip);
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) < sizeof(elf))
    goto bad;
80100f47:	90                   	nop
80100f48:	eb 1c                	jmp    80100f66 <exec+0x3da>
  if(elf.magic != ELF_MAGIC)
    goto bad;
80100f4a:	90                   	nop
80100f4b:	eb 19                	jmp    80100f66 <exec+0x3da>

  if((pgdir = setupkvm(kalloc)) == 0)
    goto bad;
80100f4d:	90                   	nop
80100f4e:	eb 16                	jmp    80100f66 <exec+0x3da>

  // Load program into memory.
  sz = 0;
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
80100f50:	90                   	nop
80100f51:	eb 13                	jmp    80100f66 <exec+0x3da>
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
80100f53:	90                   	nop
80100f54:	eb 10                	jmp    80100f66 <exec+0x3da>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
80100f56:	90                   	nop
80100f57:	eb 0d                	jmp    80100f66 <exec+0x3da>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
80100f59:	90                   	nop
80100f5a:	eb 0a                	jmp    80100f66 <exec+0x3da>

  // Allocate two pages at the next page boundary.
  // Make the first inaccessible.  Use the second as the user stack.
  sz = PGROUNDUP(sz);
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
80100f5c:	90                   	nop
80100f5d:	eb 07                	jmp    80100f66 <exec+0x3da>
  sp = sz;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
80100f5f:	90                   	nop
80100f60:	eb 04                	jmp    80100f66 <exec+0x3da>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
80100f62:	90                   	nop
80100f63:	eb 01                	jmp    80100f66 <exec+0x3da>
  ustack[1] = argc;
  ustack[2] = sp - (argc+1)*4;  // argv pointer

  sp -= (3+argc+1) * 4;
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
    goto bad;
80100f65:	90                   	nop
  switchuvm(proc);
  freevm(oldpgdir);
  return 0;

 bad:
  if(pgdir)
80100f66:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
80100f6a:	74 0b                	je     80100f77 <exec+0x3eb>
    freevm(pgdir);
80100f6c:	8b 45 d4             	mov    -0x2c(%ebp),%eax
80100f6f:	89 04 24             	mov    %eax,(%esp)
80100f72:	e8 ca 6f 00 00       	call   80107f41 <freevm>
  if(ip)
80100f77:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
80100f7b:	74 0b                	je     80100f88 <exec+0x3fc>
    iunlockput(ip);
80100f7d:	8b 45 d8             	mov    -0x28(%ebp),%eax
80100f80:	89 04 24             	mov    %eax,(%esp)
80100f83:	e8 f4 0b 00 00       	call   80101b7c <iunlockput>
  return -1;
80100f88:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100f8d:	c9                   	leave  
80100f8e:	c3                   	ret    
	...

80100f90 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80100f90:	55                   	push   %ebp
80100f91:	89 e5                	mov    %esp,%ebp
80100f93:	83 ec 18             	sub    $0x18,%esp
  initlock(&ftable.lock, "ftable");
80100f96:	c7 44 24 04 89 82 10 	movl   $0x80108289,0x4(%esp)
80100f9d:	80 
80100f9e:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
80100fa5:	e8 68 3c 00 00       	call   80104c12 <initlock>
}
80100faa:	c9                   	leave  
80100fab:	c3                   	ret    

80100fac <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80100fac:	55                   	push   %ebp
80100fad:	89 e5                	mov    %esp,%ebp
80100faf:	83 ec 28             	sub    $0x28,%esp
  struct file *f;

  acquire(&ftable.lock);
80100fb2:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
80100fb9:	e8 75 3c 00 00       	call   80104c33 <acquire>
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100fbe:	c7 45 f4 94 e0 10 80 	movl   $0x8010e094,-0xc(%ebp)
80100fc5:	eb 29                	jmp    80100ff0 <filealloc+0x44>
    if(f->ref == 0){
80100fc7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100fca:	8b 40 04             	mov    0x4(%eax),%eax
80100fcd:	85 c0                	test   %eax,%eax
80100fcf:	75 1b                	jne    80100fec <filealloc+0x40>
      f->ref = 1;
80100fd1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100fd4:	c7 40 04 01 00 00 00 	movl   $0x1,0x4(%eax)
      release(&ftable.lock);
80100fdb:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
80100fe2:	e8 ae 3c 00 00       	call   80104c95 <release>
      return f;
80100fe7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80100fea:	eb 1e                	jmp    8010100a <filealloc+0x5e>
filealloc(void)
{
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100fec:	83 45 f4 18          	addl   $0x18,-0xc(%ebp)
80100ff0:	81 7d f4 f4 e9 10 80 	cmpl   $0x8010e9f4,-0xc(%ebp)
80100ff7:	72 ce                	jb     80100fc7 <filealloc+0x1b>
      f->ref = 1;
      release(&ftable.lock);
      return f;
    }
  }
  release(&ftable.lock);
80100ff9:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
80101000:	e8 90 3c 00 00       	call   80104c95 <release>
  return 0;
80101005:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010100a:	c9                   	leave  
8010100b:	c3                   	ret    

8010100c <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
8010100c:	55                   	push   %ebp
8010100d:	89 e5                	mov    %esp,%ebp
8010100f:	83 ec 18             	sub    $0x18,%esp
  acquire(&ftable.lock);
80101012:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
80101019:	e8 15 3c 00 00       	call   80104c33 <acquire>
  if(f->ref < 1)
8010101e:	8b 45 08             	mov    0x8(%ebp),%eax
80101021:	8b 40 04             	mov    0x4(%eax),%eax
80101024:	85 c0                	test   %eax,%eax
80101026:	7f 0c                	jg     80101034 <filedup+0x28>
    panic("filedup");
80101028:	c7 04 24 90 82 10 80 	movl   $0x80108290,(%esp)
8010102f:	e8 09 f5 ff ff       	call   8010053d <panic>
  f->ref++;
80101034:	8b 45 08             	mov    0x8(%ebp),%eax
80101037:	8b 40 04             	mov    0x4(%eax),%eax
8010103a:	8d 50 01             	lea    0x1(%eax),%edx
8010103d:	8b 45 08             	mov    0x8(%ebp),%eax
80101040:	89 50 04             	mov    %edx,0x4(%eax)
  release(&ftable.lock);
80101043:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
8010104a:	e8 46 3c 00 00       	call   80104c95 <release>
  return f;
8010104f:	8b 45 08             	mov    0x8(%ebp),%eax
}
80101052:	c9                   	leave  
80101053:	c3                   	ret    

80101054 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80101054:	55                   	push   %ebp
80101055:	89 e5                	mov    %esp,%ebp
80101057:	83 ec 38             	sub    $0x38,%esp
  struct file ff;

  acquire(&ftable.lock);
8010105a:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
80101061:	e8 cd 3b 00 00       	call   80104c33 <acquire>
  if(f->ref < 1)
80101066:	8b 45 08             	mov    0x8(%ebp),%eax
80101069:	8b 40 04             	mov    0x4(%eax),%eax
8010106c:	85 c0                	test   %eax,%eax
8010106e:	7f 0c                	jg     8010107c <fileclose+0x28>
    panic("fileclose");
80101070:	c7 04 24 98 82 10 80 	movl   $0x80108298,(%esp)
80101077:	e8 c1 f4 ff ff       	call   8010053d <panic>
  if(--f->ref > 0){
8010107c:	8b 45 08             	mov    0x8(%ebp),%eax
8010107f:	8b 40 04             	mov    0x4(%eax),%eax
80101082:	8d 50 ff             	lea    -0x1(%eax),%edx
80101085:	8b 45 08             	mov    0x8(%ebp),%eax
80101088:	89 50 04             	mov    %edx,0x4(%eax)
8010108b:	8b 45 08             	mov    0x8(%ebp),%eax
8010108e:	8b 40 04             	mov    0x4(%eax),%eax
80101091:	85 c0                	test   %eax,%eax
80101093:	7e 11                	jle    801010a6 <fileclose+0x52>
    release(&ftable.lock);
80101095:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
8010109c:	e8 f4 3b 00 00       	call   80104c95 <release>
    return;
801010a1:	e9 82 00 00 00       	jmp    80101128 <fileclose+0xd4>
  }
  ff = *f;
801010a6:	8b 45 08             	mov    0x8(%ebp),%eax
801010a9:	8b 10                	mov    (%eax),%edx
801010ab:	89 55 e0             	mov    %edx,-0x20(%ebp)
801010ae:	8b 50 04             	mov    0x4(%eax),%edx
801010b1:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801010b4:	8b 50 08             	mov    0x8(%eax),%edx
801010b7:	89 55 e8             	mov    %edx,-0x18(%ebp)
801010ba:	8b 50 0c             	mov    0xc(%eax),%edx
801010bd:	89 55 ec             	mov    %edx,-0x14(%ebp)
801010c0:	8b 50 10             	mov    0x10(%eax),%edx
801010c3:	89 55 f0             	mov    %edx,-0x10(%ebp)
801010c6:	8b 40 14             	mov    0x14(%eax),%eax
801010c9:	89 45 f4             	mov    %eax,-0xc(%ebp)
  f->ref = 0;
801010cc:	8b 45 08             	mov    0x8(%ebp),%eax
801010cf:	c7 40 04 00 00 00 00 	movl   $0x0,0x4(%eax)
  f->type = FD_NONE;
801010d6:	8b 45 08             	mov    0x8(%ebp),%eax
801010d9:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  release(&ftable.lock);
801010df:	c7 04 24 60 e0 10 80 	movl   $0x8010e060,(%esp)
801010e6:	e8 aa 3b 00 00       	call   80104c95 <release>
  
  if(ff.type == FD_PIPE)
801010eb:	8b 45 e0             	mov    -0x20(%ebp),%eax
801010ee:	83 f8 01             	cmp    $0x1,%eax
801010f1:	75 18                	jne    8010110b <fileclose+0xb7>
    pipeclose(ff.pipe, ff.writable);
801010f3:	0f b6 45 e9          	movzbl -0x17(%ebp),%eax
801010f7:	0f be d0             	movsbl %al,%edx
801010fa:	8b 45 ec             	mov    -0x14(%ebp),%eax
801010fd:	89 54 24 04          	mov    %edx,0x4(%esp)
80101101:	89 04 24             	mov    %eax,(%esp)
80101104:	e8 02 2d 00 00       	call   80103e0b <pipeclose>
80101109:	eb 1d                	jmp    80101128 <fileclose+0xd4>
  else if(ff.type == FD_INODE){
8010110b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010110e:	83 f8 02             	cmp    $0x2,%eax
80101111:	75 15                	jne    80101128 <fileclose+0xd4>
    begin_trans();
80101113:	e8 95 21 00 00       	call   801032ad <begin_trans>
    iput(ff.ip);
80101118:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010111b:	89 04 24             	mov    %eax,(%esp)
8010111e:	e8 88 09 00 00       	call   80101aab <iput>
    commit_trans();
80101123:	e8 ce 21 00 00       	call   801032f6 <commit_trans>
  }
}
80101128:	c9                   	leave  
80101129:	c3                   	ret    

8010112a <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
8010112a:	55                   	push   %ebp
8010112b:	89 e5                	mov    %esp,%ebp
8010112d:	83 ec 18             	sub    $0x18,%esp
  if(f->type == FD_INODE){
80101130:	8b 45 08             	mov    0x8(%ebp),%eax
80101133:	8b 00                	mov    (%eax),%eax
80101135:	83 f8 02             	cmp    $0x2,%eax
80101138:	75 38                	jne    80101172 <filestat+0x48>
    ilock(f->ip);
8010113a:	8b 45 08             	mov    0x8(%ebp),%eax
8010113d:	8b 40 10             	mov    0x10(%eax),%eax
80101140:	89 04 24             	mov    %eax,(%esp)
80101143:	e8 b0 07 00 00       	call   801018f8 <ilock>
    stati(f->ip, st);
80101148:	8b 45 08             	mov    0x8(%ebp),%eax
8010114b:	8b 40 10             	mov    0x10(%eax),%eax
8010114e:	8b 55 0c             	mov    0xc(%ebp),%edx
80101151:	89 54 24 04          	mov    %edx,0x4(%esp)
80101155:	89 04 24             	mov    %eax,(%esp)
80101158:	e8 4c 0c 00 00       	call   80101da9 <stati>
    iunlock(f->ip);
8010115d:	8b 45 08             	mov    0x8(%ebp),%eax
80101160:	8b 40 10             	mov    0x10(%eax),%eax
80101163:	89 04 24             	mov    %eax,(%esp)
80101166:	e8 db 08 00 00       	call   80101a46 <iunlock>
    return 0;
8010116b:	b8 00 00 00 00       	mov    $0x0,%eax
80101170:	eb 05                	jmp    80101177 <filestat+0x4d>
  }
  return -1;
80101172:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101177:	c9                   	leave  
80101178:	c3                   	ret    

80101179 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80101179:	55                   	push   %ebp
8010117a:	89 e5                	mov    %esp,%ebp
8010117c:	83 ec 28             	sub    $0x28,%esp
  int r;

  if(f->readable == 0)
8010117f:	8b 45 08             	mov    0x8(%ebp),%eax
80101182:	0f b6 40 08          	movzbl 0x8(%eax),%eax
80101186:	84 c0                	test   %al,%al
80101188:	75 0a                	jne    80101194 <fileread+0x1b>
    return -1;
8010118a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010118f:	e9 9f 00 00 00       	jmp    80101233 <fileread+0xba>
  if(f->type == FD_PIPE)
80101194:	8b 45 08             	mov    0x8(%ebp),%eax
80101197:	8b 00                	mov    (%eax),%eax
80101199:	83 f8 01             	cmp    $0x1,%eax
8010119c:	75 1e                	jne    801011bc <fileread+0x43>
    return piperead(f->pipe, addr, n);
8010119e:	8b 45 08             	mov    0x8(%ebp),%eax
801011a1:	8b 40 0c             	mov    0xc(%eax),%eax
801011a4:	8b 55 10             	mov    0x10(%ebp),%edx
801011a7:	89 54 24 08          	mov    %edx,0x8(%esp)
801011ab:	8b 55 0c             	mov    0xc(%ebp),%edx
801011ae:	89 54 24 04          	mov    %edx,0x4(%esp)
801011b2:	89 04 24             	mov    %eax,(%esp)
801011b5:	e8 d3 2d 00 00       	call   80103f8d <piperead>
801011ba:	eb 77                	jmp    80101233 <fileread+0xba>
  if(f->type == FD_INODE){
801011bc:	8b 45 08             	mov    0x8(%ebp),%eax
801011bf:	8b 00                	mov    (%eax),%eax
801011c1:	83 f8 02             	cmp    $0x2,%eax
801011c4:	75 61                	jne    80101227 <fileread+0xae>
    ilock(f->ip);
801011c6:	8b 45 08             	mov    0x8(%ebp),%eax
801011c9:	8b 40 10             	mov    0x10(%eax),%eax
801011cc:	89 04 24             	mov    %eax,(%esp)
801011cf:	e8 24 07 00 00       	call   801018f8 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
801011d4:	8b 4d 10             	mov    0x10(%ebp),%ecx
801011d7:	8b 45 08             	mov    0x8(%ebp),%eax
801011da:	8b 50 14             	mov    0x14(%eax),%edx
801011dd:	8b 45 08             	mov    0x8(%ebp),%eax
801011e0:	8b 40 10             	mov    0x10(%eax),%eax
801011e3:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801011e7:	89 54 24 08          	mov    %edx,0x8(%esp)
801011eb:	8b 55 0c             	mov    0xc(%ebp),%edx
801011ee:	89 54 24 04          	mov    %edx,0x4(%esp)
801011f2:	89 04 24             	mov    %eax,(%esp)
801011f5:	e8 f4 0b 00 00       	call   80101dee <readi>
801011fa:	89 45 f4             	mov    %eax,-0xc(%ebp)
801011fd:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101201:	7e 11                	jle    80101214 <fileread+0x9b>
      f->off += r;
80101203:	8b 45 08             	mov    0x8(%ebp),%eax
80101206:	8b 50 14             	mov    0x14(%eax),%edx
80101209:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010120c:	01 c2                	add    %eax,%edx
8010120e:	8b 45 08             	mov    0x8(%ebp),%eax
80101211:	89 50 14             	mov    %edx,0x14(%eax)
    iunlock(f->ip);
80101214:	8b 45 08             	mov    0x8(%ebp),%eax
80101217:	8b 40 10             	mov    0x10(%eax),%eax
8010121a:	89 04 24             	mov    %eax,(%esp)
8010121d:	e8 24 08 00 00       	call   80101a46 <iunlock>
    return r;
80101222:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101225:	eb 0c                	jmp    80101233 <fileread+0xba>
  }
  panic("fileread");
80101227:	c7 04 24 a2 82 10 80 	movl   $0x801082a2,(%esp)
8010122e:	e8 0a f3 ff ff       	call   8010053d <panic>
}
80101233:	c9                   	leave  
80101234:	c3                   	ret    

80101235 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80101235:	55                   	push   %ebp
80101236:	89 e5                	mov    %esp,%ebp
80101238:	53                   	push   %ebx
80101239:	83 ec 24             	sub    $0x24,%esp
  int r;

  if(f->writable == 0)
8010123c:	8b 45 08             	mov    0x8(%ebp),%eax
8010123f:	0f b6 40 09          	movzbl 0x9(%eax),%eax
80101243:	84 c0                	test   %al,%al
80101245:	75 0a                	jne    80101251 <filewrite+0x1c>
    return -1;
80101247:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010124c:	e9 23 01 00 00       	jmp    80101374 <filewrite+0x13f>
  if(f->type == FD_PIPE)
80101251:	8b 45 08             	mov    0x8(%ebp),%eax
80101254:	8b 00                	mov    (%eax),%eax
80101256:	83 f8 01             	cmp    $0x1,%eax
80101259:	75 21                	jne    8010127c <filewrite+0x47>
    return pipewrite(f->pipe, addr, n);
8010125b:	8b 45 08             	mov    0x8(%ebp),%eax
8010125e:	8b 40 0c             	mov    0xc(%eax),%eax
80101261:	8b 55 10             	mov    0x10(%ebp),%edx
80101264:	89 54 24 08          	mov    %edx,0x8(%esp)
80101268:	8b 55 0c             	mov    0xc(%ebp),%edx
8010126b:	89 54 24 04          	mov    %edx,0x4(%esp)
8010126f:	89 04 24             	mov    %eax,(%esp)
80101272:	e8 26 2c 00 00       	call   80103e9d <pipewrite>
80101277:	e9 f8 00 00 00       	jmp    80101374 <filewrite+0x13f>
  if(f->type == FD_INODE){
8010127c:	8b 45 08             	mov    0x8(%ebp),%eax
8010127f:	8b 00                	mov    (%eax),%eax
80101281:	83 f8 02             	cmp    $0x2,%eax
80101284:	0f 85 de 00 00 00    	jne    80101368 <filewrite+0x133>
    // the maximum log transaction size, including
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * 512;
8010128a:	c7 45 ec 00 06 00 00 	movl   $0x600,-0x14(%ebp)
    int i = 0;
80101291:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    while(i < n){
80101298:	e9 a8 00 00 00       	jmp    80101345 <filewrite+0x110>
      int n1 = n - i;
8010129d:	8b 45 f4             	mov    -0xc(%ebp),%eax
801012a0:	8b 55 10             	mov    0x10(%ebp),%edx
801012a3:	89 d1                	mov    %edx,%ecx
801012a5:	29 c1                	sub    %eax,%ecx
801012a7:	89 c8                	mov    %ecx,%eax
801012a9:	89 45 f0             	mov    %eax,-0x10(%ebp)
      if(n1 > max)
801012ac:	8b 45 f0             	mov    -0x10(%ebp),%eax
801012af:	3b 45 ec             	cmp    -0x14(%ebp),%eax
801012b2:	7e 06                	jle    801012ba <filewrite+0x85>
        n1 = max;
801012b4:	8b 45 ec             	mov    -0x14(%ebp),%eax
801012b7:	89 45 f0             	mov    %eax,-0x10(%ebp)

      begin_trans();
801012ba:	e8 ee 1f 00 00       	call   801032ad <begin_trans>
      ilock(f->ip);
801012bf:	8b 45 08             	mov    0x8(%ebp),%eax
801012c2:	8b 40 10             	mov    0x10(%eax),%eax
801012c5:	89 04 24             	mov    %eax,(%esp)
801012c8:	e8 2b 06 00 00       	call   801018f8 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
801012cd:	8b 5d f0             	mov    -0x10(%ebp),%ebx
801012d0:	8b 45 08             	mov    0x8(%ebp),%eax
801012d3:	8b 48 14             	mov    0x14(%eax),%ecx
801012d6:	8b 45 f4             	mov    -0xc(%ebp),%eax
801012d9:	89 c2                	mov    %eax,%edx
801012db:	03 55 0c             	add    0xc(%ebp),%edx
801012de:	8b 45 08             	mov    0x8(%ebp),%eax
801012e1:	8b 40 10             	mov    0x10(%eax),%eax
801012e4:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
801012e8:	89 4c 24 08          	mov    %ecx,0x8(%esp)
801012ec:	89 54 24 04          	mov    %edx,0x4(%esp)
801012f0:	89 04 24             	mov    %eax,(%esp)
801012f3:	e8 61 0c 00 00       	call   80101f59 <writei>
801012f8:	89 45 e8             	mov    %eax,-0x18(%ebp)
801012fb:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801012ff:	7e 11                	jle    80101312 <filewrite+0xdd>
        f->off += r;
80101301:	8b 45 08             	mov    0x8(%ebp),%eax
80101304:	8b 50 14             	mov    0x14(%eax),%edx
80101307:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010130a:	01 c2                	add    %eax,%edx
8010130c:	8b 45 08             	mov    0x8(%ebp),%eax
8010130f:	89 50 14             	mov    %edx,0x14(%eax)
      iunlock(f->ip);
80101312:	8b 45 08             	mov    0x8(%ebp),%eax
80101315:	8b 40 10             	mov    0x10(%eax),%eax
80101318:	89 04 24             	mov    %eax,(%esp)
8010131b:	e8 26 07 00 00       	call   80101a46 <iunlock>
      commit_trans();
80101320:	e8 d1 1f 00 00       	call   801032f6 <commit_trans>

      if(r < 0)
80101325:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
80101329:	78 28                	js     80101353 <filewrite+0x11e>
        break;
      if(r != n1)
8010132b:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010132e:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80101331:	74 0c                	je     8010133f <filewrite+0x10a>
        panic("short filewrite");
80101333:	c7 04 24 ab 82 10 80 	movl   $0x801082ab,(%esp)
8010133a:	e8 fe f1 ff ff       	call   8010053d <panic>
      i += r;
8010133f:	8b 45 e8             	mov    -0x18(%ebp),%eax
80101342:	01 45 f4             	add    %eax,-0xc(%ebp)
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((LOGSIZE-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
80101345:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101348:	3b 45 10             	cmp    0x10(%ebp),%eax
8010134b:	0f 8c 4c ff ff ff    	jl     8010129d <filewrite+0x68>
80101351:	eb 01                	jmp    80101354 <filewrite+0x11f>
        f->off += r;
      iunlock(f->ip);
      commit_trans();

      if(r < 0)
        break;
80101353:	90                   	nop
      if(r != n1)
        panic("short filewrite");
      i += r;
    }
    return i == n ? n : -1;
80101354:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101357:	3b 45 10             	cmp    0x10(%ebp),%eax
8010135a:	75 05                	jne    80101361 <filewrite+0x12c>
8010135c:	8b 45 10             	mov    0x10(%ebp),%eax
8010135f:	eb 05                	jmp    80101366 <filewrite+0x131>
80101361:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101366:	eb 0c                	jmp    80101374 <filewrite+0x13f>
  }
  panic("filewrite");
80101368:	c7 04 24 bb 82 10 80 	movl   $0x801082bb,(%esp)
8010136f:	e8 c9 f1 ff ff       	call   8010053d <panic>
}
80101374:	83 c4 24             	add    $0x24,%esp
80101377:	5b                   	pop    %ebx
80101378:	5d                   	pop    %ebp
80101379:	c3                   	ret    
	...

8010137c <readsb>:
static void itrunc(struct inode*);

// Read the super block.
void
readsb(int dev, struct superblock *sb)
{
8010137c:	55                   	push   %ebp
8010137d:	89 e5                	mov    %esp,%ebp
8010137f:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  
  bp = bread(dev, 1);
80101382:	8b 45 08             	mov    0x8(%ebp),%eax
80101385:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010138c:	00 
8010138d:	89 04 24             	mov    %eax,(%esp)
80101390:	e8 11 ee ff ff       	call   801001a6 <bread>
80101395:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memmove(sb, bp->data, sizeof(*sb));
80101398:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010139b:	83 c0 18             	add    $0x18,%eax
8010139e:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801013a5:	00 
801013a6:	89 44 24 04          	mov    %eax,0x4(%esp)
801013aa:	8b 45 0c             	mov    0xc(%ebp),%eax
801013ad:	89 04 24             	mov    %eax,(%esp)
801013b0:	e8 a0 3b 00 00       	call   80104f55 <memmove>
  brelse(bp);
801013b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801013b8:	89 04 24             	mov    %eax,(%esp)
801013bb:	e8 57 ee ff ff       	call   80100217 <brelse>
}
801013c0:	c9                   	leave  
801013c1:	c3                   	ret    

801013c2 <bzero>:

// Zero a block.
static void
bzero(int dev, int bno)
{
801013c2:	55                   	push   %ebp
801013c3:	89 e5                	mov    %esp,%ebp
801013c5:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  
  bp = bread(dev, bno);
801013c8:	8b 55 0c             	mov    0xc(%ebp),%edx
801013cb:	8b 45 08             	mov    0x8(%ebp),%eax
801013ce:	89 54 24 04          	mov    %edx,0x4(%esp)
801013d2:	89 04 24             	mov    %eax,(%esp)
801013d5:	e8 cc ed ff ff       	call   801001a6 <bread>
801013da:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(bp->data, 0, BSIZE);
801013dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801013e0:	83 c0 18             	add    $0x18,%eax
801013e3:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
801013ea:	00 
801013eb:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801013f2:	00 
801013f3:	89 04 24             	mov    %eax,(%esp)
801013f6:	e8 87 3a 00 00       	call   80104e82 <memset>
  log_write(bp);
801013fb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801013fe:	89 04 24             	mov    %eax,(%esp)
80101401:	e8 48 1f 00 00       	call   8010334e <log_write>
  brelse(bp);
80101406:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101409:	89 04 24             	mov    %eax,(%esp)
8010140c:	e8 06 ee ff ff       	call   80100217 <brelse>
}
80101411:	c9                   	leave  
80101412:	c3                   	ret    

80101413 <balloc>:
// Blocks. 

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
80101413:	55                   	push   %ebp
80101414:	89 e5                	mov    %esp,%ebp
80101416:	53                   	push   %ebx
80101417:	83 ec 34             	sub    $0x34,%esp
  int b, bi, m;
  struct buf *bp;
  struct superblock sb;

  bp = 0;
8010141a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
  readsb(dev, &sb);
80101421:	8b 45 08             	mov    0x8(%ebp),%eax
80101424:	8d 55 d8             	lea    -0x28(%ebp),%edx
80101427:	89 54 24 04          	mov    %edx,0x4(%esp)
8010142b:	89 04 24             	mov    %eax,(%esp)
8010142e:	e8 49 ff ff ff       	call   8010137c <readsb>
  for(b = 0; b < sb.size; b += BPB){
80101433:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010143a:	e9 11 01 00 00       	jmp    80101550 <balloc+0x13d>
    bp = bread(dev, BBLOCK(b, sb.ninodes));
8010143f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101442:	8d 90 ff 0f 00 00    	lea    0xfff(%eax),%edx
80101448:	85 c0                	test   %eax,%eax
8010144a:	0f 48 c2             	cmovs  %edx,%eax
8010144d:	c1 f8 0c             	sar    $0xc,%eax
80101450:	8b 55 e0             	mov    -0x20(%ebp),%edx
80101453:	c1 ea 03             	shr    $0x3,%edx
80101456:	01 d0                	add    %edx,%eax
80101458:	83 c0 03             	add    $0x3,%eax
8010145b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010145f:	8b 45 08             	mov    0x8(%ebp),%eax
80101462:	89 04 24             	mov    %eax,(%esp)
80101465:	e8 3c ed ff ff       	call   801001a6 <bread>
8010146a:	89 45 ec             	mov    %eax,-0x14(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010146d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80101474:	e9 a7 00 00 00       	jmp    80101520 <balloc+0x10d>
      m = 1 << (bi % 8);
80101479:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010147c:	89 c2                	mov    %eax,%edx
8010147e:	c1 fa 1f             	sar    $0x1f,%edx
80101481:	c1 ea 1d             	shr    $0x1d,%edx
80101484:	01 d0                	add    %edx,%eax
80101486:	83 e0 07             	and    $0x7,%eax
80101489:	29 d0                	sub    %edx,%eax
8010148b:	ba 01 00 00 00       	mov    $0x1,%edx
80101490:	89 d3                	mov    %edx,%ebx
80101492:	89 c1                	mov    %eax,%ecx
80101494:	d3 e3                	shl    %cl,%ebx
80101496:	89 d8                	mov    %ebx,%eax
80101498:	89 45 e8             	mov    %eax,-0x18(%ebp)
      if((bp->data[bi/8] & m) == 0){  // Is block free?
8010149b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010149e:	8d 50 07             	lea    0x7(%eax),%edx
801014a1:	85 c0                	test   %eax,%eax
801014a3:	0f 48 c2             	cmovs  %edx,%eax
801014a6:	c1 f8 03             	sar    $0x3,%eax
801014a9:	8b 55 ec             	mov    -0x14(%ebp),%edx
801014ac:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
801014b1:	0f b6 c0             	movzbl %al,%eax
801014b4:	23 45 e8             	and    -0x18(%ebp),%eax
801014b7:	85 c0                	test   %eax,%eax
801014b9:	75 61                	jne    8010151c <balloc+0x109>
        bp->data[bi/8] |= m;  // Mark block in use.
801014bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
801014be:	8d 50 07             	lea    0x7(%eax),%edx
801014c1:	85 c0                	test   %eax,%eax
801014c3:	0f 48 c2             	cmovs  %edx,%eax
801014c6:	c1 f8 03             	sar    $0x3,%eax
801014c9:	8b 55 ec             	mov    -0x14(%ebp),%edx
801014cc:	0f b6 54 02 18       	movzbl 0x18(%edx,%eax,1),%edx
801014d1:	89 d1                	mov    %edx,%ecx
801014d3:	8b 55 e8             	mov    -0x18(%ebp),%edx
801014d6:	09 ca                	or     %ecx,%edx
801014d8:	89 d1                	mov    %edx,%ecx
801014da:	8b 55 ec             	mov    -0x14(%ebp),%edx
801014dd:	88 4c 02 18          	mov    %cl,0x18(%edx,%eax,1)
        log_write(bp);
801014e1:	8b 45 ec             	mov    -0x14(%ebp),%eax
801014e4:	89 04 24             	mov    %eax,(%esp)
801014e7:	e8 62 1e 00 00       	call   8010334e <log_write>
        brelse(bp);
801014ec:	8b 45 ec             	mov    -0x14(%ebp),%eax
801014ef:	89 04 24             	mov    %eax,(%esp)
801014f2:	e8 20 ed ff ff       	call   80100217 <brelse>
        bzero(dev, b + bi);
801014f7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801014fa:	8b 55 f4             	mov    -0xc(%ebp),%edx
801014fd:	01 c2                	add    %eax,%edx
801014ff:	8b 45 08             	mov    0x8(%ebp),%eax
80101502:	89 54 24 04          	mov    %edx,0x4(%esp)
80101506:	89 04 24             	mov    %eax,(%esp)
80101509:	e8 b4 fe ff ff       	call   801013c2 <bzero>
        return b + bi;
8010150e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101511:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101514:	01 d0                	add    %edx,%eax
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
}
80101516:	83 c4 34             	add    $0x34,%esp
80101519:	5b                   	pop    %ebx
8010151a:	5d                   	pop    %ebp
8010151b:	c3                   	ret    

  bp = 0;
  readsb(dev, &sb);
  for(b = 0; b < sb.size; b += BPB){
    bp = bread(dev, BBLOCK(b, sb.ninodes));
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010151c:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80101520:	81 7d f0 ff 0f 00 00 	cmpl   $0xfff,-0x10(%ebp)
80101527:	7f 15                	jg     8010153e <balloc+0x12b>
80101529:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010152c:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010152f:	01 d0                	add    %edx,%eax
80101531:	89 c2                	mov    %eax,%edx
80101533:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101536:	39 c2                	cmp    %eax,%edx
80101538:	0f 82 3b ff ff ff    	jb     80101479 <balloc+0x66>
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
8010153e:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101541:	89 04 24             	mov    %eax,(%esp)
80101544:	e8 ce ec ff ff       	call   80100217 <brelse>
  struct buf *bp;
  struct superblock sb;

  bp = 0;
  readsb(dev, &sb);
  for(b = 0; b < sb.size; b += BPB){
80101549:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80101550:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101553:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101556:	39 c2                	cmp    %eax,%edx
80101558:	0f 82 e1 fe ff ff    	jb     8010143f <balloc+0x2c>
        return b + bi;
      }
    }
    brelse(bp);
  }
  panic("balloc: out of blocks");
8010155e:	c7 04 24 c5 82 10 80 	movl   $0x801082c5,(%esp)
80101565:	e8 d3 ef ff ff       	call   8010053d <panic>

8010156a <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
8010156a:	55                   	push   %ebp
8010156b:	89 e5                	mov    %esp,%ebp
8010156d:	53                   	push   %ebx
8010156e:	83 ec 34             	sub    $0x34,%esp
  struct buf *bp;
  struct superblock sb;
  int bi, m;

  readsb(dev, &sb);
80101571:	8d 45 dc             	lea    -0x24(%ebp),%eax
80101574:	89 44 24 04          	mov    %eax,0x4(%esp)
80101578:	8b 45 08             	mov    0x8(%ebp),%eax
8010157b:	89 04 24             	mov    %eax,(%esp)
8010157e:	e8 f9 fd ff ff       	call   8010137c <readsb>
  bp = bread(dev, BBLOCK(b, sb.ninodes));
80101583:	8b 45 0c             	mov    0xc(%ebp),%eax
80101586:	89 c2                	mov    %eax,%edx
80101588:	c1 ea 0c             	shr    $0xc,%edx
8010158b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010158e:	c1 e8 03             	shr    $0x3,%eax
80101591:	01 d0                	add    %edx,%eax
80101593:	8d 50 03             	lea    0x3(%eax),%edx
80101596:	8b 45 08             	mov    0x8(%ebp),%eax
80101599:	89 54 24 04          	mov    %edx,0x4(%esp)
8010159d:	89 04 24             	mov    %eax,(%esp)
801015a0:	e8 01 ec ff ff       	call   801001a6 <bread>
801015a5:	89 45 f4             	mov    %eax,-0xc(%ebp)
  bi = b % BPB;
801015a8:	8b 45 0c             	mov    0xc(%ebp),%eax
801015ab:	25 ff 0f 00 00       	and    $0xfff,%eax
801015b0:	89 45 f0             	mov    %eax,-0x10(%ebp)
  m = 1 << (bi % 8);
801015b3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801015b6:	89 c2                	mov    %eax,%edx
801015b8:	c1 fa 1f             	sar    $0x1f,%edx
801015bb:	c1 ea 1d             	shr    $0x1d,%edx
801015be:	01 d0                	add    %edx,%eax
801015c0:	83 e0 07             	and    $0x7,%eax
801015c3:	29 d0                	sub    %edx,%eax
801015c5:	ba 01 00 00 00       	mov    $0x1,%edx
801015ca:	89 d3                	mov    %edx,%ebx
801015cc:	89 c1                	mov    %eax,%ecx
801015ce:	d3 e3                	shl    %cl,%ebx
801015d0:	89 d8                	mov    %ebx,%eax
801015d2:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if((bp->data[bi/8] & m) == 0)
801015d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801015d8:	8d 50 07             	lea    0x7(%eax),%edx
801015db:	85 c0                	test   %eax,%eax
801015dd:	0f 48 c2             	cmovs  %edx,%eax
801015e0:	c1 f8 03             	sar    $0x3,%eax
801015e3:	8b 55 f4             	mov    -0xc(%ebp),%edx
801015e6:	0f b6 44 02 18       	movzbl 0x18(%edx,%eax,1),%eax
801015eb:	0f b6 c0             	movzbl %al,%eax
801015ee:	23 45 ec             	and    -0x14(%ebp),%eax
801015f1:	85 c0                	test   %eax,%eax
801015f3:	75 0c                	jne    80101601 <bfree+0x97>
    panic("freeing free block");
801015f5:	c7 04 24 db 82 10 80 	movl   $0x801082db,(%esp)
801015fc:	e8 3c ef ff ff       	call   8010053d <panic>
  bp->data[bi/8] &= ~m;
80101601:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101604:	8d 50 07             	lea    0x7(%eax),%edx
80101607:	85 c0                	test   %eax,%eax
80101609:	0f 48 c2             	cmovs  %edx,%eax
8010160c:	c1 f8 03             	sar    $0x3,%eax
8010160f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101612:	0f b6 54 02 18       	movzbl 0x18(%edx,%eax,1),%edx
80101617:	8b 4d ec             	mov    -0x14(%ebp),%ecx
8010161a:	f7 d1                	not    %ecx
8010161c:	21 ca                	and    %ecx,%edx
8010161e:	89 d1                	mov    %edx,%ecx
80101620:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101623:	88 4c 02 18          	mov    %cl,0x18(%edx,%eax,1)
  log_write(bp);
80101627:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010162a:	89 04 24             	mov    %eax,(%esp)
8010162d:	e8 1c 1d 00 00       	call   8010334e <log_write>
  brelse(bp);
80101632:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101635:	89 04 24             	mov    %eax,(%esp)
80101638:	e8 da eb ff ff       	call   80100217 <brelse>
}
8010163d:	83 c4 34             	add    $0x34,%esp
80101640:	5b                   	pop    %ebx
80101641:	5d                   	pop    %ebp
80101642:	c3                   	ret    

80101643 <iinit>:
  struct inode inode[NINODE];
} icache;

void
iinit(void)
{
80101643:	55                   	push   %ebp
80101644:	89 e5                	mov    %esp,%ebp
80101646:	83 ec 18             	sub    $0x18,%esp
  initlock(&icache.lock, "icache");
80101649:	c7 44 24 04 ee 82 10 	movl   $0x801082ee,0x4(%esp)
80101650:	80 
80101651:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101658:	e8 b5 35 00 00       	call   80104c12 <initlock>
}
8010165d:	c9                   	leave  
8010165e:	c3                   	ret    

8010165f <ialloc>:
//PAGEBREAK!
// Allocate a new inode with the given type on device dev.
// A free inode has a type of zero.
struct inode*
ialloc(uint dev, short type)
{
8010165f:	55                   	push   %ebp
80101660:	89 e5                	mov    %esp,%ebp
80101662:	83 ec 48             	sub    $0x48,%esp
80101665:	8b 45 0c             	mov    0xc(%ebp),%eax
80101668:	66 89 45 d4          	mov    %ax,-0x2c(%ebp)
  int inum;
  struct buf *bp;
  struct dinode *dip;
  struct superblock sb;

  readsb(dev, &sb);
8010166c:	8b 45 08             	mov    0x8(%ebp),%eax
8010166f:	8d 55 dc             	lea    -0x24(%ebp),%edx
80101672:	89 54 24 04          	mov    %edx,0x4(%esp)
80101676:	89 04 24             	mov    %eax,(%esp)
80101679:	e8 fe fc ff ff       	call   8010137c <readsb>

  for(inum = 1; inum < sb.ninodes; inum++){
8010167e:	c7 45 f4 01 00 00 00 	movl   $0x1,-0xc(%ebp)
80101685:	e9 98 00 00 00       	jmp    80101722 <ialloc+0xc3>
    bp = bread(dev, IBLOCK(inum));
8010168a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010168d:	c1 e8 03             	shr    $0x3,%eax
80101690:	83 c0 02             	add    $0x2,%eax
80101693:	89 44 24 04          	mov    %eax,0x4(%esp)
80101697:	8b 45 08             	mov    0x8(%ebp),%eax
8010169a:	89 04 24             	mov    %eax,(%esp)
8010169d:	e8 04 eb ff ff       	call   801001a6 <bread>
801016a2:	89 45 f0             	mov    %eax,-0x10(%ebp)
    dip = (struct dinode*)bp->data + inum%IPB;
801016a5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801016a8:	8d 50 18             	lea    0x18(%eax),%edx
801016ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
801016ae:	83 e0 07             	and    $0x7,%eax
801016b1:	c1 e0 06             	shl    $0x6,%eax
801016b4:	01 d0                	add    %edx,%eax
801016b6:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if(dip->type == 0){  // a free inode
801016b9:	8b 45 ec             	mov    -0x14(%ebp),%eax
801016bc:	0f b7 00             	movzwl (%eax),%eax
801016bf:	66 85 c0             	test   %ax,%ax
801016c2:	75 4f                	jne    80101713 <ialloc+0xb4>
      memset(dip, 0, sizeof(*dip));
801016c4:	c7 44 24 08 40 00 00 	movl   $0x40,0x8(%esp)
801016cb:	00 
801016cc:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801016d3:	00 
801016d4:	8b 45 ec             	mov    -0x14(%ebp),%eax
801016d7:	89 04 24             	mov    %eax,(%esp)
801016da:	e8 a3 37 00 00       	call   80104e82 <memset>
      dip->type = type;
801016df:	8b 45 ec             	mov    -0x14(%ebp),%eax
801016e2:	0f b7 55 d4          	movzwl -0x2c(%ebp),%edx
801016e6:	66 89 10             	mov    %dx,(%eax)
      log_write(bp);   // mark it allocated on the disk
801016e9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801016ec:	89 04 24             	mov    %eax,(%esp)
801016ef:	e8 5a 1c 00 00       	call   8010334e <log_write>
      brelse(bp);
801016f4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801016f7:	89 04 24             	mov    %eax,(%esp)
801016fa:	e8 18 eb ff ff       	call   80100217 <brelse>
      return iget(dev, inum);
801016ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101702:	89 44 24 04          	mov    %eax,0x4(%esp)
80101706:	8b 45 08             	mov    0x8(%ebp),%eax
80101709:	89 04 24             	mov    %eax,(%esp)
8010170c:	e8 e3 00 00 00       	call   801017f4 <iget>
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
}
80101711:	c9                   	leave  
80101712:	c3                   	ret    
      dip->type = type;
      log_write(bp);   // mark it allocated on the disk
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
80101713:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101716:	89 04 24             	mov    %eax,(%esp)
80101719:	e8 f9 ea ff ff       	call   80100217 <brelse>
  struct dinode *dip;
  struct superblock sb;

  readsb(dev, &sb);

  for(inum = 1; inum < sb.ninodes; inum++){
8010171e:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80101722:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101725:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101728:	39 c2                	cmp    %eax,%edx
8010172a:	0f 82 5a ff ff ff    	jb     8010168a <ialloc+0x2b>
      brelse(bp);
      return iget(dev, inum);
    }
    brelse(bp);
  }
  panic("ialloc: no inodes");
80101730:	c7 04 24 f5 82 10 80 	movl   $0x801082f5,(%esp)
80101737:	e8 01 ee ff ff       	call   8010053d <panic>

8010173c <iupdate>:
}

// Copy a modified in-memory inode to disk.
void
iupdate(struct inode *ip)
{
8010173c:	55                   	push   %ebp
8010173d:	89 e5                	mov    %esp,%ebp
8010173f:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  bp = bread(ip->dev, IBLOCK(ip->inum));
80101742:	8b 45 08             	mov    0x8(%ebp),%eax
80101745:	8b 40 04             	mov    0x4(%eax),%eax
80101748:	c1 e8 03             	shr    $0x3,%eax
8010174b:	8d 50 02             	lea    0x2(%eax),%edx
8010174e:	8b 45 08             	mov    0x8(%ebp),%eax
80101751:	8b 00                	mov    (%eax),%eax
80101753:	89 54 24 04          	mov    %edx,0x4(%esp)
80101757:	89 04 24             	mov    %eax,(%esp)
8010175a:	e8 47 ea ff ff       	call   801001a6 <bread>
8010175f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  dip = (struct dinode*)bp->data + ip->inum%IPB;
80101762:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101765:	8d 50 18             	lea    0x18(%eax),%edx
80101768:	8b 45 08             	mov    0x8(%ebp),%eax
8010176b:	8b 40 04             	mov    0x4(%eax),%eax
8010176e:	83 e0 07             	and    $0x7,%eax
80101771:	c1 e0 06             	shl    $0x6,%eax
80101774:	01 d0                	add    %edx,%eax
80101776:	89 45 f0             	mov    %eax,-0x10(%ebp)
  dip->type = ip->type;
80101779:	8b 45 08             	mov    0x8(%ebp),%eax
8010177c:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101780:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101783:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
80101786:	8b 45 08             	mov    0x8(%ebp),%eax
80101789:	0f b7 50 12          	movzwl 0x12(%eax),%edx
8010178d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101790:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
80101794:	8b 45 08             	mov    0x8(%ebp),%eax
80101797:	0f b7 50 14          	movzwl 0x14(%eax),%edx
8010179b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010179e:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
801017a2:	8b 45 08             	mov    0x8(%ebp),%eax
801017a5:	0f b7 50 16          	movzwl 0x16(%eax),%edx
801017a9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801017ac:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
801017b0:	8b 45 08             	mov    0x8(%ebp),%eax
801017b3:	8b 50 18             	mov    0x18(%eax),%edx
801017b6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801017b9:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801017bc:	8b 45 08             	mov    0x8(%ebp),%eax
801017bf:	8d 50 1c             	lea    0x1c(%eax),%edx
801017c2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801017c5:	83 c0 0c             	add    $0xc,%eax
801017c8:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
801017cf:	00 
801017d0:	89 54 24 04          	mov    %edx,0x4(%esp)
801017d4:	89 04 24             	mov    %eax,(%esp)
801017d7:	e8 79 37 00 00       	call   80104f55 <memmove>
  log_write(bp);
801017dc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801017df:	89 04 24             	mov    %eax,(%esp)
801017e2:	e8 67 1b 00 00       	call   8010334e <log_write>
  brelse(bp);
801017e7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801017ea:	89 04 24             	mov    %eax,(%esp)
801017ed:	e8 25 ea ff ff       	call   80100217 <brelse>
}
801017f2:	c9                   	leave  
801017f3:	c3                   	ret    

801017f4 <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
801017f4:	55                   	push   %ebp
801017f5:	89 e5                	mov    %esp,%ebp
801017f7:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *empty;

  acquire(&icache.lock);
801017fa:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101801:	e8 2d 34 00 00       	call   80104c33 <acquire>

  // Is the inode already cached?
  empty = 0;
80101806:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010180d:	c7 45 f4 94 ea 10 80 	movl   $0x8010ea94,-0xc(%ebp)
80101814:	eb 59                	jmp    8010186f <iget+0x7b>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101816:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101819:	8b 40 08             	mov    0x8(%eax),%eax
8010181c:	85 c0                	test   %eax,%eax
8010181e:	7e 35                	jle    80101855 <iget+0x61>
80101820:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101823:	8b 00                	mov    (%eax),%eax
80101825:	3b 45 08             	cmp    0x8(%ebp),%eax
80101828:	75 2b                	jne    80101855 <iget+0x61>
8010182a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010182d:	8b 40 04             	mov    0x4(%eax),%eax
80101830:	3b 45 0c             	cmp    0xc(%ebp),%eax
80101833:	75 20                	jne    80101855 <iget+0x61>
      ip->ref++;
80101835:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101838:	8b 40 08             	mov    0x8(%eax),%eax
8010183b:	8d 50 01             	lea    0x1(%eax),%edx
8010183e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101841:	89 50 08             	mov    %edx,0x8(%eax)
      release(&icache.lock);
80101844:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
8010184b:	e8 45 34 00 00       	call   80104c95 <release>
      return ip;
80101850:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101853:	eb 6f                	jmp    801018c4 <iget+0xd0>
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101855:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80101859:	75 10                	jne    8010186b <iget+0x77>
8010185b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010185e:	8b 40 08             	mov    0x8(%eax),%eax
80101861:	85 c0                	test   %eax,%eax
80101863:	75 06                	jne    8010186b <iget+0x77>
      empty = ip;
80101865:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101868:	89 45 f0             	mov    %eax,-0x10(%ebp)

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010186b:	83 45 f4 50          	addl   $0x50,-0xc(%ebp)
8010186f:	81 7d f4 34 fa 10 80 	cmpl   $0x8010fa34,-0xc(%ebp)
80101876:	72 9e                	jb     80101816 <iget+0x22>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
80101878:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
8010187c:	75 0c                	jne    8010188a <iget+0x96>
    panic("iget: no inodes");
8010187e:	c7 04 24 07 83 10 80 	movl   $0x80108307,(%esp)
80101885:	e8 b3 ec ff ff       	call   8010053d <panic>

  ip = empty;
8010188a:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010188d:	89 45 f4             	mov    %eax,-0xc(%ebp)
  ip->dev = dev;
80101890:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101893:	8b 55 08             	mov    0x8(%ebp),%edx
80101896:	89 10                	mov    %edx,(%eax)
  ip->inum = inum;
80101898:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010189b:	8b 55 0c             	mov    0xc(%ebp),%edx
8010189e:	89 50 04             	mov    %edx,0x4(%eax)
  ip->ref = 1;
801018a1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801018a4:	c7 40 08 01 00 00 00 	movl   $0x1,0x8(%eax)
  ip->flags = 0;
801018ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
801018ae:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  release(&icache.lock);
801018b5:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
801018bc:	e8 d4 33 00 00       	call   80104c95 <release>

  return ip;
801018c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801018c4:	c9                   	leave  
801018c5:	c3                   	ret    

801018c6 <idup>:

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode*
idup(struct inode *ip)
{
801018c6:	55                   	push   %ebp
801018c7:	89 e5                	mov    %esp,%ebp
801018c9:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
801018cc:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
801018d3:	e8 5b 33 00 00       	call   80104c33 <acquire>
  ip->ref++;
801018d8:	8b 45 08             	mov    0x8(%ebp),%eax
801018db:	8b 40 08             	mov    0x8(%eax),%eax
801018de:	8d 50 01             	lea    0x1(%eax),%edx
801018e1:	8b 45 08             	mov    0x8(%ebp),%eax
801018e4:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
801018e7:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
801018ee:	e8 a2 33 00 00       	call   80104c95 <release>
  return ip;
801018f3:	8b 45 08             	mov    0x8(%ebp),%eax
}
801018f6:	c9                   	leave  
801018f7:	c3                   	ret    

801018f8 <ilock>:

// Lock the given inode.
// Reads the inode from disk if necessary.
void
ilock(struct inode *ip)
{
801018f8:	55                   	push   %ebp
801018f9:	89 e5                	mov    %esp,%ebp
801018fb:	83 ec 28             	sub    $0x28,%esp
  struct buf *bp;
  struct dinode *dip;

  if(ip == 0 || ip->ref < 1)
801018fe:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80101902:	74 0a                	je     8010190e <ilock+0x16>
80101904:	8b 45 08             	mov    0x8(%ebp),%eax
80101907:	8b 40 08             	mov    0x8(%eax),%eax
8010190a:	85 c0                	test   %eax,%eax
8010190c:	7f 0c                	jg     8010191a <ilock+0x22>
    panic("ilock");
8010190e:	c7 04 24 17 83 10 80 	movl   $0x80108317,(%esp)
80101915:	e8 23 ec ff ff       	call   8010053d <panic>

  acquire(&icache.lock);
8010191a:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101921:	e8 0d 33 00 00       	call   80104c33 <acquire>
  while(ip->flags & I_BUSY)
80101926:	eb 13                	jmp    8010193b <ilock+0x43>
    sleep(ip, &icache.lock);
80101928:	c7 44 24 04 60 ea 10 	movl   $0x8010ea60,0x4(%esp)
8010192f:	80 
80101930:	8b 45 08             	mov    0x8(%ebp),%eax
80101933:	89 04 24             	mov    %eax,(%esp)
80101936:	e8 1a 30 00 00       	call   80104955 <sleep>

  if(ip == 0 || ip->ref < 1)
    panic("ilock");

  acquire(&icache.lock);
  while(ip->flags & I_BUSY)
8010193b:	8b 45 08             	mov    0x8(%ebp),%eax
8010193e:	8b 40 0c             	mov    0xc(%eax),%eax
80101941:	83 e0 01             	and    $0x1,%eax
80101944:	84 c0                	test   %al,%al
80101946:	75 e0                	jne    80101928 <ilock+0x30>
    sleep(ip, &icache.lock);
  ip->flags |= I_BUSY;
80101948:	8b 45 08             	mov    0x8(%ebp),%eax
8010194b:	8b 40 0c             	mov    0xc(%eax),%eax
8010194e:	89 c2                	mov    %eax,%edx
80101950:	83 ca 01             	or     $0x1,%edx
80101953:	8b 45 08             	mov    0x8(%ebp),%eax
80101956:	89 50 0c             	mov    %edx,0xc(%eax)
  release(&icache.lock);
80101959:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101960:	e8 30 33 00 00       	call   80104c95 <release>

  if(!(ip->flags & I_VALID)){
80101965:	8b 45 08             	mov    0x8(%ebp),%eax
80101968:	8b 40 0c             	mov    0xc(%eax),%eax
8010196b:	83 e0 02             	and    $0x2,%eax
8010196e:	85 c0                	test   %eax,%eax
80101970:	0f 85 ce 00 00 00    	jne    80101a44 <ilock+0x14c>
    bp = bread(ip->dev, IBLOCK(ip->inum));
80101976:	8b 45 08             	mov    0x8(%ebp),%eax
80101979:	8b 40 04             	mov    0x4(%eax),%eax
8010197c:	c1 e8 03             	shr    $0x3,%eax
8010197f:	8d 50 02             	lea    0x2(%eax),%edx
80101982:	8b 45 08             	mov    0x8(%ebp),%eax
80101985:	8b 00                	mov    (%eax),%eax
80101987:	89 54 24 04          	mov    %edx,0x4(%esp)
8010198b:	89 04 24             	mov    %eax,(%esp)
8010198e:	e8 13 e8 ff ff       	call   801001a6 <bread>
80101993:	89 45 f4             	mov    %eax,-0xc(%ebp)
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80101996:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101999:	8d 50 18             	lea    0x18(%eax),%edx
8010199c:	8b 45 08             	mov    0x8(%ebp),%eax
8010199f:	8b 40 04             	mov    0x4(%eax),%eax
801019a2:	83 e0 07             	and    $0x7,%eax
801019a5:	c1 e0 06             	shl    $0x6,%eax
801019a8:	01 d0                	add    %edx,%eax
801019aa:	89 45 f0             	mov    %eax,-0x10(%ebp)
    ip->type = dip->type;
801019ad:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019b0:	0f b7 10             	movzwl (%eax),%edx
801019b3:	8b 45 08             	mov    0x8(%ebp),%eax
801019b6:	66 89 50 10          	mov    %dx,0x10(%eax)
    ip->major = dip->major;
801019ba:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019bd:	0f b7 50 02          	movzwl 0x2(%eax),%edx
801019c1:	8b 45 08             	mov    0x8(%ebp),%eax
801019c4:	66 89 50 12          	mov    %dx,0x12(%eax)
    ip->minor = dip->minor;
801019c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019cb:	0f b7 50 04          	movzwl 0x4(%eax),%edx
801019cf:	8b 45 08             	mov    0x8(%ebp),%eax
801019d2:	66 89 50 14          	mov    %dx,0x14(%eax)
    ip->nlink = dip->nlink;
801019d6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019d9:	0f b7 50 06          	movzwl 0x6(%eax),%edx
801019dd:	8b 45 08             	mov    0x8(%ebp),%eax
801019e0:	66 89 50 16          	mov    %dx,0x16(%eax)
    ip->size = dip->size;
801019e4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019e7:	8b 50 08             	mov    0x8(%eax),%edx
801019ea:	8b 45 08             	mov    0x8(%ebp),%eax
801019ed:	89 50 18             	mov    %edx,0x18(%eax)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801019f0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801019f3:	8d 50 0c             	lea    0xc(%eax),%edx
801019f6:	8b 45 08             	mov    0x8(%ebp),%eax
801019f9:	83 c0 1c             	add    $0x1c,%eax
801019fc:	c7 44 24 08 34 00 00 	movl   $0x34,0x8(%esp)
80101a03:	00 
80101a04:	89 54 24 04          	mov    %edx,0x4(%esp)
80101a08:	89 04 24             	mov    %eax,(%esp)
80101a0b:	e8 45 35 00 00       	call   80104f55 <memmove>
    brelse(bp);
80101a10:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101a13:	89 04 24             	mov    %eax,(%esp)
80101a16:	e8 fc e7 ff ff       	call   80100217 <brelse>
    ip->flags |= I_VALID;
80101a1b:	8b 45 08             	mov    0x8(%ebp),%eax
80101a1e:	8b 40 0c             	mov    0xc(%eax),%eax
80101a21:	89 c2                	mov    %eax,%edx
80101a23:	83 ca 02             	or     $0x2,%edx
80101a26:	8b 45 08             	mov    0x8(%ebp),%eax
80101a29:	89 50 0c             	mov    %edx,0xc(%eax)
    if(ip->type == 0)
80101a2c:	8b 45 08             	mov    0x8(%ebp),%eax
80101a2f:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80101a33:	66 85 c0             	test   %ax,%ax
80101a36:	75 0c                	jne    80101a44 <ilock+0x14c>
      panic("ilock: no type");
80101a38:	c7 04 24 1d 83 10 80 	movl   $0x8010831d,(%esp)
80101a3f:	e8 f9 ea ff ff       	call   8010053d <panic>
  }
}
80101a44:	c9                   	leave  
80101a45:	c3                   	ret    

80101a46 <iunlock>:

// Unlock the given inode.
void
iunlock(struct inode *ip)
{
80101a46:	55                   	push   %ebp
80101a47:	89 e5                	mov    %esp,%ebp
80101a49:	83 ec 18             	sub    $0x18,%esp
  if(ip == 0 || !(ip->flags & I_BUSY) || ip->ref < 1)
80101a4c:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80101a50:	74 17                	je     80101a69 <iunlock+0x23>
80101a52:	8b 45 08             	mov    0x8(%ebp),%eax
80101a55:	8b 40 0c             	mov    0xc(%eax),%eax
80101a58:	83 e0 01             	and    $0x1,%eax
80101a5b:	85 c0                	test   %eax,%eax
80101a5d:	74 0a                	je     80101a69 <iunlock+0x23>
80101a5f:	8b 45 08             	mov    0x8(%ebp),%eax
80101a62:	8b 40 08             	mov    0x8(%eax),%eax
80101a65:	85 c0                	test   %eax,%eax
80101a67:	7f 0c                	jg     80101a75 <iunlock+0x2f>
    panic("iunlock");
80101a69:	c7 04 24 2c 83 10 80 	movl   $0x8010832c,(%esp)
80101a70:	e8 c8 ea ff ff       	call   8010053d <panic>

  acquire(&icache.lock);
80101a75:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101a7c:	e8 b2 31 00 00       	call   80104c33 <acquire>
  ip->flags &= ~I_BUSY;
80101a81:	8b 45 08             	mov    0x8(%ebp),%eax
80101a84:	8b 40 0c             	mov    0xc(%eax),%eax
80101a87:	89 c2                	mov    %eax,%edx
80101a89:	83 e2 fe             	and    $0xfffffffe,%edx
80101a8c:	8b 45 08             	mov    0x8(%ebp),%eax
80101a8f:	89 50 0c             	mov    %edx,0xc(%eax)
  wakeup(ip);
80101a92:	8b 45 08             	mov    0x8(%ebp),%eax
80101a95:	89 04 24             	mov    %eax,(%esp)
80101a98:	e8 91 2f 00 00       	call   80104a2e <wakeup>
  release(&icache.lock);
80101a9d:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101aa4:	e8 ec 31 00 00       	call   80104c95 <release>
}
80101aa9:	c9                   	leave  
80101aaa:	c3                   	ret    

80101aab <iput>:
// be recycled.
// If that was the last reference and the inode has no links
// to it, free the inode (and its content) on disk.
void
iput(struct inode *ip)
{
80101aab:	55                   	push   %ebp
80101aac:	89 e5                	mov    %esp,%ebp
80101aae:	83 ec 18             	sub    $0x18,%esp
  acquire(&icache.lock);
80101ab1:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101ab8:	e8 76 31 00 00       	call   80104c33 <acquire>
  if(ip->ref == 1 && (ip->flags & I_VALID) && ip->nlink == 0){
80101abd:	8b 45 08             	mov    0x8(%ebp),%eax
80101ac0:	8b 40 08             	mov    0x8(%eax),%eax
80101ac3:	83 f8 01             	cmp    $0x1,%eax
80101ac6:	0f 85 93 00 00 00    	jne    80101b5f <iput+0xb4>
80101acc:	8b 45 08             	mov    0x8(%ebp),%eax
80101acf:	8b 40 0c             	mov    0xc(%eax),%eax
80101ad2:	83 e0 02             	and    $0x2,%eax
80101ad5:	85 c0                	test   %eax,%eax
80101ad7:	0f 84 82 00 00 00    	je     80101b5f <iput+0xb4>
80101add:	8b 45 08             	mov    0x8(%ebp),%eax
80101ae0:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80101ae4:	66 85 c0             	test   %ax,%ax
80101ae7:	75 76                	jne    80101b5f <iput+0xb4>
    // inode has no links: truncate and free inode.
    if(ip->flags & I_BUSY)
80101ae9:	8b 45 08             	mov    0x8(%ebp),%eax
80101aec:	8b 40 0c             	mov    0xc(%eax),%eax
80101aef:	83 e0 01             	and    $0x1,%eax
80101af2:	84 c0                	test   %al,%al
80101af4:	74 0c                	je     80101b02 <iput+0x57>
      panic("iput busy");
80101af6:	c7 04 24 34 83 10 80 	movl   $0x80108334,(%esp)
80101afd:	e8 3b ea ff ff       	call   8010053d <panic>
    ip->flags |= I_BUSY;
80101b02:	8b 45 08             	mov    0x8(%ebp),%eax
80101b05:	8b 40 0c             	mov    0xc(%eax),%eax
80101b08:	89 c2                	mov    %eax,%edx
80101b0a:	83 ca 01             	or     $0x1,%edx
80101b0d:	8b 45 08             	mov    0x8(%ebp),%eax
80101b10:	89 50 0c             	mov    %edx,0xc(%eax)
    release(&icache.lock);
80101b13:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101b1a:	e8 76 31 00 00       	call   80104c95 <release>
    itrunc(ip);
80101b1f:	8b 45 08             	mov    0x8(%ebp),%eax
80101b22:	89 04 24             	mov    %eax,(%esp)
80101b25:	e8 72 01 00 00       	call   80101c9c <itrunc>
    ip->type = 0;
80101b2a:	8b 45 08             	mov    0x8(%ebp),%eax
80101b2d:	66 c7 40 10 00 00    	movw   $0x0,0x10(%eax)
    iupdate(ip);
80101b33:	8b 45 08             	mov    0x8(%ebp),%eax
80101b36:	89 04 24             	mov    %eax,(%esp)
80101b39:	e8 fe fb ff ff       	call   8010173c <iupdate>
    acquire(&icache.lock);
80101b3e:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101b45:	e8 e9 30 00 00       	call   80104c33 <acquire>
    ip->flags = 0;
80101b4a:	8b 45 08             	mov    0x8(%ebp),%eax
80101b4d:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    wakeup(ip);
80101b54:	8b 45 08             	mov    0x8(%ebp),%eax
80101b57:	89 04 24             	mov    %eax,(%esp)
80101b5a:	e8 cf 2e 00 00       	call   80104a2e <wakeup>
  }
  ip->ref--;
80101b5f:	8b 45 08             	mov    0x8(%ebp),%eax
80101b62:	8b 40 08             	mov    0x8(%eax),%eax
80101b65:	8d 50 ff             	lea    -0x1(%eax),%edx
80101b68:	8b 45 08             	mov    0x8(%ebp),%eax
80101b6b:	89 50 08             	mov    %edx,0x8(%eax)
  release(&icache.lock);
80101b6e:	c7 04 24 60 ea 10 80 	movl   $0x8010ea60,(%esp)
80101b75:	e8 1b 31 00 00       	call   80104c95 <release>
}
80101b7a:	c9                   	leave  
80101b7b:	c3                   	ret    

80101b7c <iunlockput>:

// Common idiom: unlock, then put.
void
iunlockput(struct inode *ip)
{
80101b7c:	55                   	push   %ebp
80101b7d:	89 e5                	mov    %esp,%ebp
80101b7f:	83 ec 18             	sub    $0x18,%esp
  iunlock(ip);
80101b82:	8b 45 08             	mov    0x8(%ebp),%eax
80101b85:	89 04 24             	mov    %eax,(%esp)
80101b88:	e8 b9 fe ff ff       	call   80101a46 <iunlock>
  iput(ip);
80101b8d:	8b 45 08             	mov    0x8(%ebp),%eax
80101b90:	89 04 24             	mov    %eax,(%esp)
80101b93:	e8 13 ff ff ff       	call   80101aab <iput>
}
80101b98:	c9                   	leave  
80101b99:	c3                   	ret    

80101b9a <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
80101b9a:	55                   	push   %ebp
80101b9b:	89 e5                	mov    %esp,%ebp
80101b9d:	53                   	push   %ebx
80101b9e:	83 ec 24             	sub    $0x24,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
80101ba1:	83 7d 0c 0b          	cmpl   $0xb,0xc(%ebp)
80101ba5:	77 3e                	ja     80101be5 <bmap+0x4b>
    if((addr = ip->addrs[bn]) == 0)
80101ba7:	8b 45 08             	mov    0x8(%ebp),%eax
80101baa:	8b 55 0c             	mov    0xc(%ebp),%edx
80101bad:	83 c2 04             	add    $0x4,%edx
80101bb0:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101bb4:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101bb7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101bbb:	75 20                	jne    80101bdd <bmap+0x43>
      ip->addrs[bn] = addr = balloc(ip->dev);
80101bbd:	8b 45 08             	mov    0x8(%ebp),%eax
80101bc0:	8b 00                	mov    (%eax),%eax
80101bc2:	89 04 24             	mov    %eax,(%esp)
80101bc5:	e8 49 f8 ff ff       	call   80101413 <balloc>
80101bca:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101bcd:	8b 45 08             	mov    0x8(%ebp),%eax
80101bd0:	8b 55 0c             	mov    0xc(%ebp),%edx
80101bd3:	8d 4a 04             	lea    0x4(%edx),%ecx
80101bd6:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101bd9:	89 54 88 0c          	mov    %edx,0xc(%eax,%ecx,4)
    return addr;
80101bdd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101be0:	e9 b1 00 00 00       	jmp    80101c96 <bmap+0xfc>
  }
  bn -= NDIRECT;
80101be5:	83 6d 0c 0c          	subl   $0xc,0xc(%ebp)

  if(bn < NINDIRECT){
80101be9:	83 7d 0c 7f          	cmpl   $0x7f,0xc(%ebp)
80101bed:	0f 87 97 00 00 00    	ja     80101c8a <bmap+0xf0>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
80101bf3:	8b 45 08             	mov    0x8(%ebp),%eax
80101bf6:	8b 40 4c             	mov    0x4c(%eax),%eax
80101bf9:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101bfc:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101c00:	75 19                	jne    80101c1b <bmap+0x81>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80101c02:	8b 45 08             	mov    0x8(%ebp),%eax
80101c05:	8b 00                	mov    (%eax),%eax
80101c07:	89 04 24             	mov    %eax,(%esp)
80101c0a:	e8 04 f8 ff ff       	call   80101413 <balloc>
80101c0f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101c12:	8b 45 08             	mov    0x8(%ebp),%eax
80101c15:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101c18:	89 50 4c             	mov    %edx,0x4c(%eax)
    bp = bread(ip->dev, addr);
80101c1b:	8b 45 08             	mov    0x8(%ebp),%eax
80101c1e:	8b 00                	mov    (%eax),%eax
80101c20:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101c23:	89 54 24 04          	mov    %edx,0x4(%esp)
80101c27:	89 04 24             	mov    %eax,(%esp)
80101c2a:	e8 77 e5 ff ff       	call   801001a6 <bread>
80101c2f:	89 45 f0             	mov    %eax,-0x10(%ebp)
    a = (uint*)bp->data;
80101c32:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101c35:	83 c0 18             	add    $0x18,%eax
80101c38:	89 45 ec             	mov    %eax,-0x14(%ebp)
    if((addr = a[bn]) == 0){
80101c3b:	8b 45 0c             	mov    0xc(%ebp),%eax
80101c3e:	c1 e0 02             	shl    $0x2,%eax
80101c41:	03 45 ec             	add    -0x14(%ebp),%eax
80101c44:	8b 00                	mov    (%eax),%eax
80101c46:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101c49:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80101c4d:	75 2b                	jne    80101c7a <bmap+0xe0>
      a[bn] = addr = balloc(ip->dev);
80101c4f:	8b 45 0c             	mov    0xc(%ebp),%eax
80101c52:	c1 e0 02             	shl    $0x2,%eax
80101c55:	89 c3                	mov    %eax,%ebx
80101c57:	03 5d ec             	add    -0x14(%ebp),%ebx
80101c5a:	8b 45 08             	mov    0x8(%ebp),%eax
80101c5d:	8b 00                	mov    (%eax),%eax
80101c5f:	89 04 24             	mov    %eax,(%esp)
80101c62:	e8 ac f7 ff ff       	call   80101413 <balloc>
80101c67:	89 45 f4             	mov    %eax,-0xc(%ebp)
80101c6a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101c6d:	89 03                	mov    %eax,(%ebx)
      log_write(bp);
80101c6f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101c72:	89 04 24             	mov    %eax,(%esp)
80101c75:	e8 d4 16 00 00       	call   8010334e <log_write>
    }
    brelse(bp);
80101c7a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101c7d:	89 04 24             	mov    %eax,(%esp)
80101c80:	e8 92 e5 ff ff       	call   80100217 <brelse>
    return addr;
80101c85:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101c88:	eb 0c                	jmp    80101c96 <bmap+0xfc>
  }

  panic("bmap: out of range");
80101c8a:	c7 04 24 3e 83 10 80 	movl   $0x8010833e,(%esp)
80101c91:	e8 a7 e8 ff ff       	call   8010053d <panic>
}
80101c96:	83 c4 24             	add    $0x24,%esp
80101c99:	5b                   	pop    %ebx
80101c9a:	5d                   	pop    %ebp
80101c9b:	c3                   	ret    

80101c9c <itrunc>:
// to it (no directory entries referring to it)
// and has no in-memory reference to it (is
// not an open file or current directory).
static void
itrunc(struct inode *ip)
{
80101c9c:	55                   	push   %ebp
80101c9d:	89 e5                	mov    %esp,%ebp
80101c9f:	83 ec 28             	sub    $0x28,%esp
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101ca2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80101ca9:	eb 44                	jmp    80101cef <itrunc+0x53>
    if(ip->addrs[i]){
80101cab:	8b 45 08             	mov    0x8(%ebp),%eax
80101cae:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101cb1:	83 c2 04             	add    $0x4,%edx
80101cb4:	8b 44 90 0c          	mov    0xc(%eax,%edx,4),%eax
80101cb8:	85 c0                	test   %eax,%eax
80101cba:	74 2f                	je     80101ceb <itrunc+0x4f>
      bfree(ip->dev, ip->addrs[i]);
80101cbc:	8b 45 08             	mov    0x8(%ebp),%eax
80101cbf:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101cc2:	83 c2 04             	add    $0x4,%edx
80101cc5:	8b 54 90 0c          	mov    0xc(%eax,%edx,4),%edx
80101cc9:	8b 45 08             	mov    0x8(%ebp),%eax
80101ccc:	8b 00                	mov    (%eax),%eax
80101cce:	89 54 24 04          	mov    %edx,0x4(%esp)
80101cd2:	89 04 24             	mov    %eax,(%esp)
80101cd5:	e8 90 f8 ff ff       	call   8010156a <bfree>
      ip->addrs[i] = 0;
80101cda:	8b 45 08             	mov    0x8(%ebp),%eax
80101cdd:	8b 55 f4             	mov    -0xc(%ebp),%edx
80101ce0:	83 c2 04             	add    $0x4,%edx
80101ce3:	c7 44 90 0c 00 00 00 	movl   $0x0,0xc(%eax,%edx,4)
80101cea:	00 
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101ceb:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80101cef:	83 7d f4 0b          	cmpl   $0xb,-0xc(%ebp)
80101cf3:	7e b6                	jle    80101cab <itrunc+0xf>
      bfree(ip->dev, ip->addrs[i]);
      ip->addrs[i] = 0;
    }
  }
  
  if(ip->addrs[NDIRECT]){
80101cf5:	8b 45 08             	mov    0x8(%ebp),%eax
80101cf8:	8b 40 4c             	mov    0x4c(%eax),%eax
80101cfb:	85 c0                	test   %eax,%eax
80101cfd:	0f 84 8f 00 00 00    	je     80101d92 <itrunc+0xf6>
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80101d03:	8b 45 08             	mov    0x8(%ebp),%eax
80101d06:	8b 50 4c             	mov    0x4c(%eax),%edx
80101d09:	8b 45 08             	mov    0x8(%ebp),%eax
80101d0c:	8b 00                	mov    (%eax),%eax
80101d0e:	89 54 24 04          	mov    %edx,0x4(%esp)
80101d12:	89 04 24             	mov    %eax,(%esp)
80101d15:	e8 8c e4 ff ff       	call   801001a6 <bread>
80101d1a:	89 45 ec             	mov    %eax,-0x14(%ebp)
    a = (uint*)bp->data;
80101d1d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d20:	83 c0 18             	add    $0x18,%eax
80101d23:	89 45 e8             	mov    %eax,-0x18(%ebp)
    for(j = 0; j < NINDIRECT; j++){
80101d26:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
80101d2d:	eb 2f                	jmp    80101d5e <itrunc+0xc2>
      if(a[j])
80101d2f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d32:	c1 e0 02             	shl    $0x2,%eax
80101d35:	03 45 e8             	add    -0x18(%ebp),%eax
80101d38:	8b 00                	mov    (%eax),%eax
80101d3a:	85 c0                	test   %eax,%eax
80101d3c:	74 1c                	je     80101d5a <itrunc+0xbe>
        bfree(ip->dev, a[j]);
80101d3e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d41:	c1 e0 02             	shl    $0x2,%eax
80101d44:	03 45 e8             	add    -0x18(%ebp),%eax
80101d47:	8b 10                	mov    (%eax),%edx
80101d49:	8b 45 08             	mov    0x8(%ebp),%eax
80101d4c:	8b 00                	mov    (%eax),%eax
80101d4e:	89 54 24 04          	mov    %edx,0x4(%esp)
80101d52:	89 04 24             	mov    %eax,(%esp)
80101d55:	e8 10 f8 ff ff       	call   8010156a <bfree>
  }
  
  if(ip->addrs[NDIRECT]){
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
    a = (uint*)bp->data;
    for(j = 0; j < NINDIRECT; j++){
80101d5a:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80101d5e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101d61:	83 f8 7f             	cmp    $0x7f,%eax
80101d64:	76 c9                	jbe    80101d2f <itrunc+0x93>
      if(a[j])
        bfree(ip->dev, a[j]);
    }
    brelse(bp);
80101d66:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101d69:	89 04 24             	mov    %eax,(%esp)
80101d6c:	e8 a6 e4 ff ff       	call   80100217 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
80101d71:	8b 45 08             	mov    0x8(%ebp),%eax
80101d74:	8b 50 4c             	mov    0x4c(%eax),%edx
80101d77:	8b 45 08             	mov    0x8(%ebp),%eax
80101d7a:	8b 00                	mov    (%eax),%eax
80101d7c:	89 54 24 04          	mov    %edx,0x4(%esp)
80101d80:	89 04 24             	mov    %eax,(%esp)
80101d83:	e8 e2 f7 ff ff       	call   8010156a <bfree>
    ip->addrs[NDIRECT] = 0;
80101d88:	8b 45 08             	mov    0x8(%ebp),%eax
80101d8b:	c7 40 4c 00 00 00 00 	movl   $0x0,0x4c(%eax)
  }

  ip->size = 0;
80101d92:	8b 45 08             	mov    0x8(%ebp),%eax
80101d95:	c7 40 18 00 00 00 00 	movl   $0x0,0x18(%eax)
  iupdate(ip);
80101d9c:	8b 45 08             	mov    0x8(%ebp),%eax
80101d9f:	89 04 24             	mov    %eax,(%esp)
80101da2:	e8 95 f9 ff ff       	call   8010173c <iupdate>
}
80101da7:	c9                   	leave  
80101da8:	c3                   	ret    

80101da9 <stati>:

// Copy stat information from inode.
void
stati(struct inode *ip, struct stat *st)
{
80101da9:	55                   	push   %ebp
80101daa:	89 e5                	mov    %esp,%ebp
  st->dev = ip->dev;
80101dac:	8b 45 08             	mov    0x8(%ebp),%eax
80101daf:	8b 00                	mov    (%eax),%eax
80101db1:	89 c2                	mov    %eax,%edx
80101db3:	8b 45 0c             	mov    0xc(%ebp),%eax
80101db6:	89 50 04             	mov    %edx,0x4(%eax)
  st->ino = ip->inum;
80101db9:	8b 45 08             	mov    0x8(%ebp),%eax
80101dbc:	8b 50 04             	mov    0x4(%eax),%edx
80101dbf:	8b 45 0c             	mov    0xc(%ebp),%eax
80101dc2:	89 50 08             	mov    %edx,0x8(%eax)
  st->type = ip->type;
80101dc5:	8b 45 08             	mov    0x8(%ebp),%eax
80101dc8:	0f b7 50 10          	movzwl 0x10(%eax),%edx
80101dcc:	8b 45 0c             	mov    0xc(%ebp),%eax
80101dcf:	66 89 10             	mov    %dx,(%eax)
  st->nlink = ip->nlink;
80101dd2:	8b 45 08             	mov    0x8(%ebp),%eax
80101dd5:	0f b7 50 16          	movzwl 0x16(%eax),%edx
80101dd9:	8b 45 0c             	mov    0xc(%ebp),%eax
80101ddc:	66 89 50 0c          	mov    %dx,0xc(%eax)
  st->size = ip->size;
80101de0:	8b 45 08             	mov    0x8(%ebp),%eax
80101de3:	8b 50 18             	mov    0x18(%eax),%edx
80101de6:	8b 45 0c             	mov    0xc(%ebp),%eax
80101de9:	89 50 10             	mov    %edx,0x10(%eax)
}
80101dec:	5d                   	pop    %ebp
80101ded:	c3                   	ret    

80101dee <readi>:

//PAGEBREAK!
// Read data from inode.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101dee:	55                   	push   %ebp
80101def:	89 e5                	mov    %esp,%ebp
80101df1:	53                   	push   %ebx
80101df2:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101df5:	8b 45 08             	mov    0x8(%ebp),%eax
80101df8:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80101dfc:	66 83 f8 03          	cmp    $0x3,%ax
80101e00:	75 60                	jne    80101e62 <readi+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101e02:	8b 45 08             	mov    0x8(%ebp),%eax
80101e05:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101e09:	66 85 c0             	test   %ax,%ax
80101e0c:	78 20                	js     80101e2e <readi+0x40>
80101e0e:	8b 45 08             	mov    0x8(%ebp),%eax
80101e11:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101e15:	66 83 f8 09          	cmp    $0x9,%ax
80101e19:	7f 13                	jg     80101e2e <readi+0x40>
80101e1b:	8b 45 08             	mov    0x8(%ebp),%eax
80101e1e:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101e22:	98                   	cwtl   
80101e23:	8b 04 c5 00 ea 10 80 	mov    -0x7fef1600(,%eax,8),%eax
80101e2a:	85 c0                	test   %eax,%eax
80101e2c:	75 0a                	jne    80101e38 <readi+0x4a>
      return -1;
80101e2e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101e33:	e9 1b 01 00 00       	jmp    80101f53 <readi+0x165>
    return devsw[ip->major].read(ip, dst, n);
80101e38:	8b 45 08             	mov    0x8(%ebp),%eax
80101e3b:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101e3f:	98                   	cwtl   
80101e40:	8b 14 c5 00 ea 10 80 	mov    -0x7fef1600(,%eax,8),%edx
80101e47:	8b 45 14             	mov    0x14(%ebp),%eax
80101e4a:	89 44 24 08          	mov    %eax,0x8(%esp)
80101e4e:	8b 45 0c             	mov    0xc(%ebp),%eax
80101e51:	89 44 24 04          	mov    %eax,0x4(%esp)
80101e55:	8b 45 08             	mov    0x8(%ebp),%eax
80101e58:	89 04 24             	mov    %eax,(%esp)
80101e5b:	ff d2                	call   *%edx
80101e5d:	e9 f1 00 00 00       	jmp    80101f53 <readi+0x165>
  }

  if(off > ip->size || off + n < off)
80101e62:	8b 45 08             	mov    0x8(%ebp),%eax
80101e65:	8b 40 18             	mov    0x18(%eax),%eax
80101e68:	3b 45 10             	cmp    0x10(%ebp),%eax
80101e6b:	72 0d                	jb     80101e7a <readi+0x8c>
80101e6d:	8b 45 14             	mov    0x14(%ebp),%eax
80101e70:	8b 55 10             	mov    0x10(%ebp),%edx
80101e73:	01 d0                	add    %edx,%eax
80101e75:	3b 45 10             	cmp    0x10(%ebp),%eax
80101e78:	73 0a                	jae    80101e84 <readi+0x96>
    return -1;
80101e7a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101e7f:	e9 cf 00 00 00       	jmp    80101f53 <readi+0x165>
  if(off + n > ip->size)
80101e84:	8b 45 14             	mov    0x14(%ebp),%eax
80101e87:	8b 55 10             	mov    0x10(%ebp),%edx
80101e8a:	01 c2                	add    %eax,%edx
80101e8c:	8b 45 08             	mov    0x8(%ebp),%eax
80101e8f:	8b 40 18             	mov    0x18(%eax),%eax
80101e92:	39 c2                	cmp    %eax,%edx
80101e94:	76 0c                	jbe    80101ea2 <readi+0xb4>
    n = ip->size - off;
80101e96:	8b 45 08             	mov    0x8(%ebp),%eax
80101e99:	8b 40 18             	mov    0x18(%eax),%eax
80101e9c:	2b 45 10             	sub    0x10(%ebp),%eax
80101e9f:	89 45 14             	mov    %eax,0x14(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101ea2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80101ea9:	e9 96 00 00 00       	jmp    80101f44 <readi+0x156>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101eae:	8b 45 10             	mov    0x10(%ebp),%eax
80101eb1:	c1 e8 09             	shr    $0x9,%eax
80101eb4:	89 44 24 04          	mov    %eax,0x4(%esp)
80101eb8:	8b 45 08             	mov    0x8(%ebp),%eax
80101ebb:	89 04 24             	mov    %eax,(%esp)
80101ebe:	e8 d7 fc ff ff       	call   80101b9a <bmap>
80101ec3:	8b 55 08             	mov    0x8(%ebp),%edx
80101ec6:	8b 12                	mov    (%edx),%edx
80101ec8:	89 44 24 04          	mov    %eax,0x4(%esp)
80101ecc:	89 14 24             	mov    %edx,(%esp)
80101ecf:	e8 d2 e2 ff ff       	call   801001a6 <bread>
80101ed4:	89 45 f0             	mov    %eax,-0x10(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
80101ed7:	8b 45 10             	mov    0x10(%ebp),%eax
80101eda:	89 c2                	mov    %eax,%edx
80101edc:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80101ee2:	b8 00 02 00 00       	mov    $0x200,%eax
80101ee7:	89 c1                	mov    %eax,%ecx
80101ee9:	29 d1                	sub    %edx,%ecx
80101eeb:	89 ca                	mov    %ecx,%edx
80101eed:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101ef0:	8b 4d 14             	mov    0x14(%ebp),%ecx
80101ef3:	89 cb                	mov    %ecx,%ebx
80101ef5:	29 c3                	sub    %eax,%ebx
80101ef7:	89 d8                	mov    %ebx,%eax
80101ef9:	39 c2                	cmp    %eax,%edx
80101efb:	0f 46 c2             	cmovbe %edx,%eax
80101efe:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(dst, bp->data + off%BSIZE, m);
80101f01:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f04:	8d 50 18             	lea    0x18(%eax),%edx
80101f07:	8b 45 10             	mov    0x10(%ebp),%eax
80101f0a:	25 ff 01 00 00       	and    $0x1ff,%eax
80101f0f:	01 c2                	add    %eax,%edx
80101f11:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101f14:	89 44 24 08          	mov    %eax,0x8(%esp)
80101f18:	89 54 24 04          	mov    %edx,0x4(%esp)
80101f1c:	8b 45 0c             	mov    0xc(%ebp),%eax
80101f1f:	89 04 24             	mov    %eax,(%esp)
80101f22:	e8 2e 30 00 00       	call   80104f55 <memmove>
    brelse(bp);
80101f27:	8b 45 f0             	mov    -0x10(%ebp),%eax
80101f2a:	89 04 24             	mov    %eax,(%esp)
80101f2d:	e8 e5 e2 ff ff       	call   80100217 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101f32:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101f35:	01 45 f4             	add    %eax,-0xc(%ebp)
80101f38:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101f3b:	01 45 10             	add    %eax,0x10(%ebp)
80101f3e:	8b 45 ec             	mov    -0x14(%ebp),%eax
80101f41:	01 45 0c             	add    %eax,0xc(%ebp)
80101f44:	8b 45 f4             	mov    -0xc(%ebp),%eax
80101f47:	3b 45 14             	cmp    0x14(%ebp),%eax
80101f4a:	0f 82 5e ff ff ff    	jb     80101eae <readi+0xc0>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
    m = min(n - tot, BSIZE - off%BSIZE);
    memmove(dst, bp->data + off%BSIZE, m);
    brelse(bp);
  }
  return n;
80101f50:	8b 45 14             	mov    0x14(%ebp),%eax
}
80101f53:	83 c4 24             	add    $0x24,%esp
80101f56:	5b                   	pop    %ebx
80101f57:	5d                   	pop    %ebp
80101f58:	c3                   	ret    

80101f59 <writei>:

// PAGEBREAK!
// Write data to inode.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
80101f59:	55                   	push   %ebp
80101f5a:	89 e5                	mov    %esp,%ebp
80101f5c:	53                   	push   %ebx
80101f5d:	83 ec 24             	sub    $0x24,%esp
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101f60:	8b 45 08             	mov    0x8(%ebp),%eax
80101f63:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80101f67:	66 83 f8 03          	cmp    $0x3,%ax
80101f6b:	75 60                	jne    80101fcd <writei+0x74>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
80101f6d:	8b 45 08             	mov    0x8(%ebp),%eax
80101f70:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101f74:	66 85 c0             	test   %ax,%ax
80101f77:	78 20                	js     80101f99 <writei+0x40>
80101f79:	8b 45 08             	mov    0x8(%ebp),%eax
80101f7c:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101f80:	66 83 f8 09          	cmp    $0x9,%ax
80101f84:	7f 13                	jg     80101f99 <writei+0x40>
80101f86:	8b 45 08             	mov    0x8(%ebp),%eax
80101f89:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101f8d:	98                   	cwtl   
80101f8e:	8b 04 c5 04 ea 10 80 	mov    -0x7fef15fc(,%eax,8),%eax
80101f95:	85 c0                	test   %eax,%eax
80101f97:	75 0a                	jne    80101fa3 <writei+0x4a>
      return -1;
80101f99:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101f9e:	e9 46 01 00 00       	jmp    801020e9 <writei+0x190>
    return devsw[ip->major].write(ip, src, n);
80101fa3:	8b 45 08             	mov    0x8(%ebp),%eax
80101fa6:	0f b7 40 12          	movzwl 0x12(%eax),%eax
80101faa:	98                   	cwtl   
80101fab:	8b 14 c5 04 ea 10 80 	mov    -0x7fef15fc(,%eax,8),%edx
80101fb2:	8b 45 14             	mov    0x14(%ebp),%eax
80101fb5:	89 44 24 08          	mov    %eax,0x8(%esp)
80101fb9:	8b 45 0c             	mov    0xc(%ebp),%eax
80101fbc:	89 44 24 04          	mov    %eax,0x4(%esp)
80101fc0:	8b 45 08             	mov    0x8(%ebp),%eax
80101fc3:	89 04 24             	mov    %eax,(%esp)
80101fc6:	ff d2                	call   *%edx
80101fc8:	e9 1c 01 00 00       	jmp    801020e9 <writei+0x190>
  }

  if(off > ip->size || off + n < off)
80101fcd:	8b 45 08             	mov    0x8(%ebp),%eax
80101fd0:	8b 40 18             	mov    0x18(%eax),%eax
80101fd3:	3b 45 10             	cmp    0x10(%ebp),%eax
80101fd6:	72 0d                	jb     80101fe5 <writei+0x8c>
80101fd8:	8b 45 14             	mov    0x14(%ebp),%eax
80101fdb:	8b 55 10             	mov    0x10(%ebp),%edx
80101fde:	01 d0                	add    %edx,%eax
80101fe0:	3b 45 10             	cmp    0x10(%ebp),%eax
80101fe3:	73 0a                	jae    80101fef <writei+0x96>
    return -1;
80101fe5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101fea:	e9 fa 00 00 00       	jmp    801020e9 <writei+0x190>
  if(off + n > MAXFILE*BSIZE)
80101fef:	8b 45 14             	mov    0x14(%ebp),%eax
80101ff2:	8b 55 10             	mov    0x10(%ebp),%edx
80101ff5:	01 d0                	add    %edx,%eax
80101ff7:	3d 00 18 01 00       	cmp    $0x11800,%eax
80101ffc:	76 0a                	jbe    80102008 <writei+0xaf>
    return -1;
80101ffe:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102003:	e9 e1 00 00 00       	jmp    801020e9 <writei+0x190>

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80102008:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010200f:	e9 a1 00 00 00       	jmp    801020b5 <writei+0x15c>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80102014:	8b 45 10             	mov    0x10(%ebp),%eax
80102017:	c1 e8 09             	shr    $0x9,%eax
8010201a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010201e:	8b 45 08             	mov    0x8(%ebp),%eax
80102021:	89 04 24             	mov    %eax,(%esp)
80102024:	e8 71 fb ff ff       	call   80101b9a <bmap>
80102029:	8b 55 08             	mov    0x8(%ebp),%edx
8010202c:	8b 12                	mov    (%edx),%edx
8010202e:	89 44 24 04          	mov    %eax,0x4(%esp)
80102032:	89 14 24             	mov    %edx,(%esp)
80102035:	e8 6c e1 ff ff       	call   801001a6 <bread>
8010203a:	89 45 f0             	mov    %eax,-0x10(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
8010203d:	8b 45 10             	mov    0x10(%ebp),%eax
80102040:	89 c2                	mov    %eax,%edx
80102042:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80102048:	b8 00 02 00 00       	mov    $0x200,%eax
8010204d:	89 c1                	mov    %eax,%ecx
8010204f:	29 d1                	sub    %edx,%ecx
80102051:	89 ca                	mov    %ecx,%edx
80102053:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102056:	8b 4d 14             	mov    0x14(%ebp),%ecx
80102059:	89 cb                	mov    %ecx,%ebx
8010205b:	29 c3                	sub    %eax,%ebx
8010205d:	89 d8                	mov    %ebx,%eax
8010205f:	39 c2                	cmp    %eax,%edx
80102061:	0f 46 c2             	cmovbe %edx,%eax
80102064:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(bp->data + off%BSIZE, src, m);
80102067:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010206a:	8d 50 18             	lea    0x18(%eax),%edx
8010206d:	8b 45 10             	mov    0x10(%ebp),%eax
80102070:	25 ff 01 00 00       	and    $0x1ff,%eax
80102075:	01 c2                	add    %eax,%edx
80102077:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010207a:	89 44 24 08          	mov    %eax,0x8(%esp)
8010207e:	8b 45 0c             	mov    0xc(%ebp),%eax
80102081:	89 44 24 04          	mov    %eax,0x4(%esp)
80102085:	89 14 24             	mov    %edx,(%esp)
80102088:	e8 c8 2e 00 00       	call   80104f55 <memmove>
    log_write(bp);
8010208d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102090:	89 04 24             	mov    %eax,(%esp)
80102093:	e8 b6 12 00 00       	call   8010334e <log_write>
    brelse(bp);
80102098:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010209b:	89 04 24             	mov    %eax,(%esp)
8010209e:	e8 74 e1 ff ff       	call   80100217 <brelse>
  if(off > ip->size || off + n < off)
    return -1;
  if(off + n > MAXFILE*BSIZE)
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
801020a3:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020a6:	01 45 f4             	add    %eax,-0xc(%ebp)
801020a9:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020ac:	01 45 10             	add    %eax,0x10(%ebp)
801020af:	8b 45 ec             	mov    -0x14(%ebp),%eax
801020b2:	01 45 0c             	add    %eax,0xc(%ebp)
801020b5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801020b8:	3b 45 14             	cmp    0x14(%ebp),%eax
801020bb:	0f 82 53 ff ff ff    	jb     80102014 <writei+0xbb>
    memmove(bp->data + off%BSIZE, src, m);
    log_write(bp);
    brelse(bp);
  }

  if(n > 0 && off > ip->size){
801020c1:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
801020c5:	74 1f                	je     801020e6 <writei+0x18d>
801020c7:	8b 45 08             	mov    0x8(%ebp),%eax
801020ca:	8b 40 18             	mov    0x18(%eax),%eax
801020cd:	3b 45 10             	cmp    0x10(%ebp),%eax
801020d0:	73 14                	jae    801020e6 <writei+0x18d>
    ip->size = off;
801020d2:	8b 45 08             	mov    0x8(%ebp),%eax
801020d5:	8b 55 10             	mov    0x10(%ebp),%edx
801020d8:	89 50 18             	mov    %edx,0x18(%eax)
    iupdate(ip);
801020db:	8b 45 08             	mov    0x8(%ebp),%eax
801020de:	89 04 24             	mov    %eax,(%esp)
801020e1:	e8 56 f6 ff ff       	call   8010173c <iupdate>
  }
  return n;
801020e6:	8b 45 14             	mov    0x14(%ebp),%eax
}
801020e9:	83 c4 24             	add    $0x24,%esp
801020ec:	5b                   	pop    %ebx
801020ed:	5d                   	pop    %ebp
801020ee:	c3                   	ret    

801020ef <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
801020ef:	55                   	push   %ebp
801020f0:	89 e5                	mov    %esp,%ebp
801020f2:	83 ec 18             	sub    $0x18,%esp
  return strncmp(s, t, DIRSIZ);
801020f5:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
801020fc:	00 
801020fd:	8b 45 0c             	mov    0xc(%ebp),%eax
80102100:	89 44 24 04          	mov    %eax,0x4(%esp)
80102104:	8b 45 08             	mov    0x8(%ebp),%eax
80102107:	89 04 24             	mov    %eax,(%esp)
8010210a:	e8 ea 2e 00 00       	call   80104ff9 <strncmp>
}
8010210f:	c9                   	leave  
80102110:	c3                   	ret    

80102111 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80102111:	55                   	push   %ebp
80102112:	89 e5                	mov    %esp,%ebp
80102114:	83 ec 38             	sub    $0x38,%esp
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80102117:	8b 45 08             	mov    0x8(%ebp),%eax
8010211a:	0f b7 40 10          	movzwl 0x10(%eax),%eax
8010211e:	66 83 f8 01          	cmp    $0x1,%ax
80102122:	74 0c                	je     80102130 <dirlookup+0x1f>
    panic("dirlookup not DIR");
80102124:	c7 04 24 51 83 10 80 	movl   $0x80108351,(%esp)
8010212b:	e8 0d e4 ff ff       	call   8010053d <panic>

  for(off = 0; off < dp->size; off += sizeof(de)){
80102130:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80102137:	e9 87 00 00 00       	jmp    801021c3 <dirlookup+0xb2>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010213c:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
80102143:	00 
80102144:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102147:	89 44 24 08          	mov    %eax,0x8(%esp)
8010214b:	8d 45 e0             	lea    -0x20(%ebp),%eax
8010214e:	89 44 24 04          	mov    %eax,0x4(%esp)
80102152:	8b 45 08             	mov    0x8(%ebp),%eax
80102155:	89 04 24             	mov    %eax,(%esp)
80102158:	e8 91 fc ff ff       	call   80101dee <readi>
8010215d:	83 f8 10             	cmp    $0x10,%eax
80102160:	74 0c                	je     8010216e <dirlookup+0x5d>
      panic("dirlink read");
80102162:	c7 04 24 63 83 10 80 	movl   $0x80108363,(%esp)
80102169:	e8 cf e3 ff ff       	call   8010053d <panic>
    if(de.inum == 0)
8010216e:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
80102172:	66 85 c0             	test   %ax,%ax
80102175:	74 47                	je     801021be <dirlookup+0xad>
      continue;
    if(namecmp(name, de.name) == 0){
80102177:	8d 45 e0             	lea    -0x20(%ebp),%eax
8010217a:	83 c0 02             	add    $0x2,%eax
8010217d:	89 44 24 04          	mov    %eax,0x4(%esp)
80102181:	8b 45 0c             	mov    0xc(%ebp),%eax
80102184:	89 04 24             	mov    %eax,(%esp)
80102187:	e8 63 ff ff ff       	call   801020ef <namecmp>
8010218c:	85 c0                	test   %eax,%eax
8010218e:	75 2f                	jne    801021bf <dirlookup+0xae>
      // entry matches path element
      if(poff)
80102190:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80102194:	74 08                	je     8010219e <dirlookup+0x8d>
        *poff = off;
80102196:	8b 45 10             	mov    0x10(%ebp),%eax
80102199:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010219c:	89 10                	mov    %edx,(%eax)
      inum = de.inum;
8010219e:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801021a2:	0f b7 c0             	movzwl %ax,%eax
801021a5:	89 45 f0             	mov    %eax,-0x10(%ebp)
      return iget(dp->dev, inum);
801021a8:	8b 45 08             	mov    0x8(%ebp),%eax
801021ab:	8b 00                	mov    (%eax),%eax
801021ad:	8b 55 f0             	mov    -0x10(%ebp),%edx
801021b0:	89 54 24 04          	mov    %edx,0x4(%esp)
801021b4:	89 04 24             	mov    %eax,(%esp)
801021b7:	e8 38 f6 ff ff       	call   801017f4 <iget>
801021bc:	eb 19                	jmp    801021d7 <dirlookup+0xc6>

  for(off = 0; off < dp->size; off += sizeof(de)){
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      continue;
801021be:	90                   	nop
  struct dirent de;

  if(dp->type != T_DIR)
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
801021bf:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
801021c3:	8b 45 08             	mov    0x8(%ebp),%eax
801021c6:	8b 40 18             	mov    0x18(%eax),%eax
801021c9:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801021cc:	0f 87 6a ff ff ff    	ja     8010213c <dirlookup+0x2b>
      inum = de.inum;
      return iget(dp->dev, inum);
    }
  }

  return 0;
801021d2:	b8 00 00 00 00       	mov    $0x0,%eax
}
801021d7:	c9                   	leave  
801021d8:	c3                   	ret    

801021d9 <dirlink>:

// Write a new directory entry (name, inum) into the directory dp.
int
dirlink(struct inode *dp, char *name, uint inum)
{
801021d9:	55                   	push   %ebp
801021da:	89 e5                	mov    %esp,%ebp
801021dc:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;
  struct inode *ip;

  // Check that name is not present.
  if((ip = dirlookup(dp, name, 0)) != 0){
801021df:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
801021e6:	00 
801021e7:	8b 45 0c             	mov    0xc(%ebp),%eax
801021ea:	89 44 24 04          	mov    %eax,0x4(%esp)
801021ee:	8b 45 08             	mov    0x8(%ebp),%eax
801021f1:	89 04 24             	mov    %eax,(%esp)
801021f4:	e8 18 ff ff ff       	call   80102111 <dirlookup>
801021f9:	89 45 f0             	mov    %eax,-0x10(%ebp)
801021fc:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80102200:	74 15                	je     80102217 <dirlink+0x3e>
    iput(ip);
80102202:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102205:	89 04 24             	mov    %eax,(%esp)
80102208:	e8 9e f8 ff ff       	call   80101aab <iput>
    return -1;
8010220d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102212:	e9 b8 00 00 00       	jmp    801022cf <dirlink+0xf6>
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
80102217:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010221e:	eb 44                	jmp    80102264 <dirlink+0x8b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102220:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102223:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
8010222a:	00 
8010222b:	89 44 24 08          	mov    %eax,0x8(%esp)
8010222f:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102232:	89 44 24 04          	mov    %eax,0x4(%esp)
80102236:	8b 45 08             	mov    0x8(%ebp),%eax
80102239:	89 04 24             	mov    %eax,(%esp)
8010223c:	e8 ad fb ff ff       	call   80101dee <readi>
80102241:	83 f8 10             	cmp    $0x10,%eax
80102244:	74 0c                	je     80102252 <dirlink+0x79>
      panic("dirlink read");
80102246:	c7 04 24 63 83 10 80 	movl   $0x80108363,(%esp)
8010224d:	e8 eb e2 ff ff       	call   8010053d <panic>
    if(de.inum == 0)
80102252:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
80102256:	66 85 c0             	test   %ax,%ax
80102259:	74 18                	je     80102273 <dirlink+0x9a>
    iput(ip);
    return -1;
  }

  // Look for an empty dirent.
  for(off = 0; off < dp->size; off += sizeof(de)){
8010225b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010225e:	83 c0 10             	add    $0x10,%eax
80102261:	89 45 f4             	mov    %eax,-0xc(%ebp)
80102264:	8b 55 f4             	mov    -0xc(%ebp),%edx
80102267:	8b 45 08             	mov    0x8(%ebp),%eax
8010226a:	8b 40 18             	mov    0x18(%eax),%eax
8010226d:	39 c2                	cmp    %eax,%edx
8010226f:	72 af                	jb     80102220 <dirlink+0x47>
80102271:	eb 01                	jmp    80102274 <dirlink+0x9b>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if(de.inum == 0)
      break;
80102273:	90                   	nop
  }

  strncpy(de.name, name, DIRSIZ);
80102274:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
8010227b:	00 
8010227c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010227f:	89 44 24 04          	mov    %eax,0x4(%esp)
80102283:	8d 45 e0             	lea    -0x20(%ebp),%eax
80102286:	83 c0 02             	add    $0x2,%eax
80102289:	89 04 24             	mov    %eax,(%esp)
8010228c:	e8 c0 2d 00 00       	call   80105051 <strncpy>
  de.inum = inum;
80102291:	8b 45 10             	mov    0x10(%ebp),%eax
80102294:	66 89 45 e0          	mov    %ax,-0x20(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102298:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010229b:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801022a2:	00 
801022a3:	89 44 24 08          	mov    %eax,0x8(%esp)
801022a7:	8d 45 e0             	lea    -0x20(%ebp),%eax
801022aa:	89 44 24 04          	mov    %eax,0x4(%esp)
801022ae:	8b 45 08             	mov    0x8(%ebp),%eax
801022b1:	89 04 24             	mov    %eax,(%esp)
801022b4:	e8 a0 fc ff ff       	call   80101f59 <writei>
801022b9:	83 f8 10             	cmp    $0x10,%eax
801022bc:	74 0c                	je     801022ca <dirlink+0xf1>
    panic("dirlink");
801022be:	c7 04 24 70 83 10 80 	movl   $0x80108370,(%esp)
801022c5:	e8 73 e2 ff ff       	call   8010053d <panic>
  
  return 0;
801022ca:	b8 00 00 00 00       	mov    $0x0,%eax
}
801022cf:	c9                   	leave  
801022d0:	c3                   	ret    

801022d1 <skipelem>:
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name)
{
801022d1:	55                   	push   %ebp
801022d2:	89 e5                	mov    %esp,%ebp
801022d4:	83 ec 28             	sub    $0x28,%esp
  char *s;
  int len;

  while(*path == '/')
801022d7:	eb 04                	jmp    801022dd <skipelem+0xc>
    path++;
801022d9:	83 45 08 01          	addl   $0x1,0x8(%ebp)
skipelem(char *path, char *name)
{
  char *s;
  int len;

  while(*path == '/')
801022dd:	8b 45 08             	mov    0x8(%ebp),%eax
801022e0:	0f b6 00             	movzbl (%eax),%eax
801022e3:	3c 2f                	cmp    $0x2f,%al
801022e5:	74 f2                	je     801022d9 <skipelem+0x8>
    path++;
  if(*path == 0)
801022e7:	8b 45 08             	mov    0x8(%ebp),%eax
801022ea:	0f b6 00             	movzbl (%eax),%eax
801022ed:	84 c0                	test   %al,%al
801022ef:	75 0a                	jne    801022fb <skipelem+0x2a>
    return 0;
801022f1:	b8 00 00 00 00       	mov    $0x0,%eax
801022f6:	e9 86 00 00 00       	jmp    80102381 <skipelem+0xb0>
  s = path;
801022fb:	8b 45 08             	mov    0x8(%ebp),%eax
801022fe:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(*path != '/' && *path != 0)
80102301:	eb 04                	jmp    80102307 <skipelem+0x36>
    path++;
80102303:	83 45 08 01          	addl   $0x1,0x8(%ebp)
  while(*path == '/')
    path++;
  if(*path == 0)
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
80102307:	8b 45 08             	mov    0x8(%ebp),%eax
8010230a:	0f b6 00             	movzbl (%eax),%eax
8010230d:	3c 2f                	cmp    $0x2f,%al
8010230f:	74 0a                	je     8010231b <skipelem+0x4a>
80102311:	8b 45 08             	mov    0x8(%ebp),%eax
80102314:	0f b6 00             	movzbl (%eax),%eax
80102317:	84 c0                	test   %al,%al
80102319:	75 e8                	jne    80102303 <skipelem+0x32>
    path++;
  len = path - s;
8010231b:	8b 55 08             	mov    0x8(%ebp),%edx
8010231e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102321:	89 d1                	mov    %edx,%ecx
80102323:	29 c1                	sub    %eax,%ecx
80102325:	89 c8                	mov    %ecx,%eax
80102327:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(len >= DIRSIZ)
8010232a:	83 7d f0 0d          	cmpl   $0xd,-0x10(%ebp)
8010232e:	7e 1c                	jle    8010234c <skipelem+0x7b>
    memmove(name, s, DIRSIZ);
80102330:	c7 44 24 08 0e 00 00 	movl   $0xe,0x8(%esp)
80102337:	00 
80102338:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010233b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010233f:	8b 45 0c             	mov    0xc(%ebp),%eax
80102342:	89 04 24             	mov    %eax,(%esp)
80102345:	e8 0b 2c 00 00       	call   80104f55 <memmove>
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
8010234a:	eb 28                	jmp    80102374 <skipelem+0xa3>
    path++;
  len = path - s;
  if(len >= DIRSIZ)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
8010234c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010234f:	89 44 24 08          	mov    %eax,0x8(%esp)
80102353:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102356:	89 44 24 04          	mov    %eax,0x4(%esp)
8010235a:	8b 45 0c             	mov    0xc(%ebp),%eax
8010235d:	89 04 24             	mov    %eax,(%esp)
80102360:	e8 f0 2b 00 00       	call   80104f55 <memmove>
    name[len] = 0;
80102365:	8b 45 f0             	mov    -0x10(%ebp),%eax
80102368:	03 45 0c             	add    0xc(%ebp),%eax
8010236b:	c6 00 00             	movb   $0x0,(%eax)
  }
  while(*path == '/')
8010236e:	eb 04                	jmp    80102374 <skipelem+0xa3>
    path++;
80102370:	83 45 08 01          	addl   $0x1,0x8(%ebp)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while(*path == '/')
80102374:	8b 45 08             	mov    0x8(%ebp),%eax
80102377:	0f b6 00             	movzbl (%eax),%eax
8010237a:	3c 2f                	cmp    $0x2f,%al
8010237c:	74 f2                	je     80102370 <skipelem+0x9f>
    path++;
  return path;
8010237e:	8b 45 08             	mov    0x8(%ebp),%eax
}
80102381:	c9                   	leave  
80102382:	c3                   	ret    

80102383 <namex>:
// Look up and return the inode for a path name.
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80102383:	55                   	push   %ebp
80102384:	89 e5                	mov    %esp,%ebp
80102386:	83 ec 28             	sub    $0x28,%esp
  struct inode *ip, *next;

  if(*path == '/')
80102389:	8b 45 08             	mov    0x8(%ebp),%eax
8010238c:	0f b6 00             	movzbl (%eax),%eax
8010238f:	3c 2f                	cmp    $0x2f,%al
80102391:	75 1c                	jne    801023af <namex+0x2c>
    ip = iget(ROOTDEV, ROOTINO);
80102393:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010239a:	00 
8010239b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801023a2:	e8 4d f4 ff ff       	call   801017f4 <iget>
801023a7:	89 45 f4             	mov    %eax,-0xc(%ebp)
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
801023aa:	e9 af 00 00 00       	jmp    8010245e <namex+0xdb>
  struct inode *ip, *next;

  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);
801023af:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801023b5:	8b 40 68             	mov    0x68(%eax),%eax
801023b8:	89 04 24             	mov    %eax,(%esp)
801023bb:	e8 06 f5 ff ff       	call   801018c6 <idup>
801023c0:	89 45 f4             	mov    %eax,-0xc(%ebp)

  while((path = skipelem(path, name)) != 0){
801023c3:	e9 96 00 00 00       	jmp    8010245e <namex+0xdb>
    ilock(ip);
801023c8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023cb:	89 04 24             	mov    %eax,(%esp)
801023ce:	e8 25 f5 ff ff       	call   801018f8 <ilock>
    if(ip->type != T_DIR){
801023d3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023d6:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801023da:	66 83 f8 01          	cmp    $0x1,%ax
801023de:	74 15                	je     801023f5 <namex+0x72>
      iunlockput(ip);
801023e0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801023e3:	89 04 24             	mov    %eax,(%esp)
801023e6:	e8 91 f7 ff ff       	call   80101b7c <iunlockput>
      return 0;
801023eb:	b8 00 00 00 00       	mov    $0x0,%eax
801023f0:	e9 a3 00 00 00       	jmp    80102498 <namex+0x115>
    }
    if(nameiparent && *path == '\0'){
801023f5:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
801023f9:	74 1d                	je     80102418 <namex+0x95>
801023fb:	8b 45 08             	mov    0x8(%ebp),%eax
801023fe:	0f b6 00             	movzbl (%eax),%eax
80102401:	84 c0                	test   %al,%al
80102403:	75 13                	jne    80102418 <namex+0x95>
      // Stop one level early.
      iunlock(ip);
80102405:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102408:	89 04 24             	mov    %eax,(%esp)
8010240b:	e8 36 f6 ff ff       	call   80101a46 <iunlock>
      return ip;
80102410:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102413:	e9 80 00 00 00       	jmp    80102498 <namex+0x115>
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80102418:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
8010241f:	00 
80102420:	8b 45 10             	mov    0x10(%ebp),%eax
80102423:	89 44 24 04          	mov    %eax,0x4(%esp)
80102427:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010242a:	89 04 24             	mov    %eax,(%esp)
8010242d:	e8 df fc ff ff       	call   80102111 <dirlookup>
80102432:	89 45 f0             	mov    %eax,-0x10(%ebp)
80102435:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80102439:	75 12                	jne    8010244d <namex+0xca>
      iunlockput(ip);
8010243b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010243e:	89 04 24             	mov    %eax,(%esp)
80102441:	e8 36 f7 ff ff       	call   80101b7c <iunlockput>
      return 0;
80102446:	b8 00 00 00 00       	mov    $0x0,%eax
8010244b:	eb 4b                	jmp    80102498 <namex+0x115>
    }
    iunlockput(ip);
8010244d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102450:	89 04 24             	mov    %eax,(%esp)
80102453:	e8 24 f7 ff ff       	call   80101b7c <iunlockput>
    ip = next;
80102458:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010245b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(proc->cwd);

  while((path = skipelem(path, name)) != 0){
8010245e:	8b 45 10             	mov    0x10(%ebp),%eax
80102461:	89 44 24 04          	mov    %eax,0x4(%esp)
80102465:	8b 45 08             	mov    0x8(%ebp),%eax
80102468:	89 04 24             	mov    %eax,(%esp)
8010246b:	e8 61 fe ff ff       	call   801022d1 <skipelem>
80102470:	89 45 08             	mov    %eax,0x8(%ebp)
80102473:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102477:	0f 85 4b ff ff ff    	jne    801023c8 <namex+0x45>
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
8010247d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102481:	74 12                	je     80102495 <namex+0x112>
    iput(ip);
80102483:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102486:	89 04 24             	mov    %eax,(%esp)
80102489:	e8 1d f6 ff ff       	call   80101aab <iput>
    return 0;
8010248e:	b8 00 00 00 00       	mov    $0x0,%eax
80102493:	eb 03                	jmp    80102498 <namex+0x115>
  }
  return ip;
80102495:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80102498:	c9                   	leave  
80102499:	c3                   	ret    

8010249a <namei>:

struct inode*
namei(char *path)
{
8010249a:	55                   	push   %ebp
8010249b:	89 e5                	mov    %esp,%ebp
8010249d:	83 ec 28             	sub    $0x28,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
801024a0:	8d 45 ea             	lea    -0x16(%ebp),%eax
801024a3:	89 44 24 08          	mov    %eax,0x8(%esp)
801024a7:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801024ae:	00 
801024af:	8b 45 08             	mov    0x8(%ebp),%eax
801024b2:	89 04 24             	mov    %eax,(%esp)
801024b5:	e8 c9 fe ff ff       	call   80102383 <namex>
}
801024ba:	c9                   	leave  
801024bb:	c3                   	ret    

801024bc <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
801024bc:	55                   	push   %ebp
801024bd:	89 e5                	mov    %esp,%ebp
801024bf:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 1, name);
801024c2:	8b 45 0c             	mov    0xc(%ebp),%eax
801024c5:	89 44 24 08          	mov    %eax,0x8(%esp)
801024c9:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
801024d0:	00 
801024d1:	8b 45 08             	mov    0x8(%ebp),%eax
801024d4:	89 04 24             	mov    %eax,(%esp)
801024d7:	e8 a7 fe ff ff       	call   80102383 <namex>
}
801024dc:	c9                   	leave  
801024dd:	c3                   	ret    
	...

801024e0 <inb>:
// Routines to let C code use special x86 instructions.

static inline uchar
inb(ushort port)
{
801024e0:	55                   	push   %ebp
801024e1:	89 e5                	mov    %esp,%ebp
801024e3:	53                   	push   %ebx
801024e4:	83 ec 14             	sub    $0x14,%esp
801024e7:	8b 45 08             	mov    0x8(%ebp),%eax
801024ea:	66 89 45 e8          	mov    %ax,-0x18(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801024ee:	0f b7 55 e8          	movzwl -0x18(%ebp),%edx
801024f2:	66 89 55 ea          	mov    %dx,-0x16(%ebp)
801024f6:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
801024fa:	ec                   	in     (%dx),%al
801024fb:	89 c3                	mov    %eax,%ebx
801024fd:	88 5d fb             	mov    %bl,-0x5(%ebp)
  return data;
80102500:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
}
80102504:	83 c4 14             	add    $0x14,%esp
80102507:	5b                   	pop    %ebx
80102508:	5d                   	pop    %ebp
80102509:	c3                   	ret    

8010250a <insl>:

static inline void
insl(int port, void *addr, int cnt)
{
8010250a:	55                   	push   %ebp
8010250b:	89 e5                	mov    %esp,%ebp
8010250d:	57                   	push   %edi
8010250e:	53                   	push   %ebx
  asm volatile("cld; rep insl" :
8010250f:	8b 55 08             	mov    0x8(%ebp),%edx
80102512:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102515:	8b 45 10             	mov    0x10(%ebp),%eax
80102518:	89 cb                	mov    %ecx,%ebx
8010251a:	89 df                	mov    %ebx,%edi
8010251c:	89 c1                	mov    %eax,%ecx
8010251e:	fc                   	cld    
8010251f:	f3 6d                	rep insl (%dx),%es:(%edi)
80102521:	89 c8                	mov    %ecx,%eax
80102523:	89 fb                	mov    %edi,%ebx
80102525:	89 5d 0c             	mov    %ebx,0xc(%ebp)
80102528:	89 45 10             	mov    %eax,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "memory", "cc");
}
8010252b:	5b                   	pop    %ebx
8010252c:	5f                   	pop    %edi
8010252d:	5d                   	pop    %ebp
8010252e:	c3                   	ret    

8010252f <outb>:

static inline void
outb(ushort port, uchar data)
{
8010252f:	55                   	push   %ebp
80102530:	89 e5                	mov    %esp,%ebp
80102532:	83 ec 08             	sub    $0x8,%esp
80102535:	8b 55 08             	mov    0x8(%ebp),%edx
80102538:	8b 45 0c             	mov    0xc(%ebp),%eax
8010253b:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010253f:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102542:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102546:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
8010254a:	ee                   	out    %al,(%dx)
}
8010254b:	c9                   	leave  
8010254c:	c3                   	ret    

8010254d <outsl>:
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
}

static inline void
outsl(int port, const void *addr, int cnt)
{
8010254d:	55                   	push   %ebp
8010254e:	89 e5                	mov    %esp,%ebp
80102550:	56                   	push   %esi
80102551:	53                   	push   %ebx
  asm volatile("cld; rep outsl" :
80102552:	8b 55 08             	mov    0x8(%ebp),%edx
80102555:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102558:	8b 45 10             	mov    0x10(%ebp),%eax
8010255b:	89 cb                	mov    %ecx,%ebx
8010255d:	89 de                	mov    %ebx,%esi
8010255f:	89 c1                	mov    %eax,%ecx
80102561:	fc                   	cld    
80102562:	f3 6f                	rep outsl %ds:(%esi),(%dx)
80102564:	89 c8                	mov    %ecx,%eax
80102566:	89 f3                	mov    %esi,%ebx
80102568:	89 5d 0c             	mov    %ebx,0xc(%ebp)
8010256b:	89 45 10             	mov    %eax,0x10(%ebp)
               "=S" (addr), "=c" (cnt) :
               "d" (port), "0" (addr), "1" (cnt) :
               "cc");
}
8010256e:	5b                   	pop    %ebx
8010256f:	5e                   	pop    %esi
80102570:	5d                   	pop    %ebp
80102571:	c3                   	ret    

80102572 <idewait>:
static void idestart(struct buf*);

// Wait for IDE disk to become ready.
static int
idewait(int checkerr)
{
80102572:	55                   	push   %ebp
80102573:	89 e5                	mov    %esp,%ebp
80102575:	83 ec 14             	sub    $0x14,%esp
  int r;

  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY) 
80102578:	90                   	nop
80102579:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102580:	e8 5b ff ff ff       	call   801024e0 <inb>
80102585:	0f b6 c0             	movzbl %al,%eax
80102588:	89 45 fc             	mov    %eax,-0x4(%ebp)
8010258b:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010258e:	25 c0 00 00 00       	and    $0xc0,%eax
80102593:	83 f8 40             	cmp    $0x40,%eax
80102596:	75 e1                	jne    80102579 <idewait+0x7>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
80102598:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010259c:	74 11                	je     801025af <idewait+0x3d>
8010259e:	8b 45 fc             	mov    -0x4(%ebp),%eax
801025a1:	83 e0 21             	and    $0x21,%eax
801025a4:	85 c0                	test   %eax,%eax
801025a6:	74 07                	je     801025af <idewait+0x3d>
    return -1;
801025a8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801025ad:	eb 05                	jmp    801025b4 <idewait+0x42>
  return 0;
801025af:	b8 00 00 00 00       	mov    $0x0,%eax
}
801025b4:	c9                   	leave  
801025b5:	c3                   	ret    

801025b6 <ideinit>:

void
ideinit(void)
{
801025b6:	55                   	push   %ebp
801025b7:	89 e5                	mov    %esp,%ebp
801025b9:	83 ec 28             	sub    $0x28,%esp
  int i;

  initlock(&idelock, "ide");
801025bc:	c7 44 24 04 78 83 10 	movl   $0x80108378,0x4(%esp)
801025c3:	80 
801025c4:	c7 04 24 00 b6 10 80 	movl   $0x8010b600,(%esp)
801025cb:	e8 42 26 00 00       	call   80104c12 <initlock>
  picenable(IRQ_IDE);
801025d0:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
801025d7:	e8 75 15 00 00       	call   80103b51 <picenable>
  ioapicenable(IRQ_IDE, ncpu - 1);
801025dc:	a1 00 01 11 80       	mov    0x80110100,%eax
801025e1:	83 e8 01             	sub    $0x1,%eax
801025e4:	89 44 24 04          	mov    %eax,0x4(%esp)
801025e8:	c7 04 24 0e 00 00 00 	movl   $0xe,(%esp)
801025ef:	e8 12 04 00 00       	call   80102a06 <ioapicenable>
  idewait(0);
801025f4:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801025fb:	e8 72 ff ff ff       	call   80102572 <idewait>
  
  // Check if disk 1 is present
  outb(0x1f6, 0xe0 | (1<<4));
80102600:	c7 44 24 04 f0 00 00 	movl   $0xf0,0x4(%esp)
80102607:	00 
80102608:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
8010260f:	e8 1b ff ff ff       	call   8010252f <outb>
  for(i=0; i<1000; i++){
80102614:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010261b:	eb 20                	jmp    8010263d <ideinit+0x87>
    if(inb(0x1f7) != 0){
8010261d:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102624:	e8 b7 fe ff ff       	call   801024e0 <inb>
80102629:	84 c0                	test   %al,%al
8010262b:	74 0c                	je     80102639 <ideinit+0x83>
      havedisk1 = 1;
8010262d:	c7 05 38 b6 10 80 01 	movl   $0x1,0x8010b638
80102634:	00 00 00 
      break;
80102637:	eb 0d                	jmp    80102646 <ideinit+0x90>
  ioapicenable(IRQ_IDE, ncpu - 1);
  idewait(0);
  
  // Check if disk 1 is present
  outb(0x1f6, 0xe0 | (1<<4));
  for(i=0; i<1000; i++){
80102639:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010263d:	81 7d f4 e7 03 00 00 	cmpl   $0x3e7,-0xc(%ebp)
80102644:	7e d7                	jle    8010261d <ideinit+0x67>
      break;
    }
  }
  
  // Switch back to disk 0.
  outb(0x1f6, 0xe0 | (0<<4));
80102646:	c7 44 24 04 e0 00 00 	movl   $0xe0,0x4(%esp)
8010264d:	00 
8010264e:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102655:	e8 d5 fe ff ff       	call   8010252f <outb>
}
8010265a:	c9                   	leave  
8010265b:	c3                   	ret    

8010265c <idestart>:

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
8010265c:	55                   	push   %ebp
8010265d:	89 e5                	mov    %esp,%ebp
8010265f:	83 ec 18             	sub    $0x18,%esp
  if(b == 0)
80102662:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80102666:	75 0c                	jne    80102674 <idestart+0x18>
    panic("idestart");
80102668:	c7 04 24 7c 83 10 80 	movl   $0x8010837c,(%esp)
8010266f:	e8 c9 de ff ff       	call   8010053d <panic>

  idewait(0);
80102674:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010267b:	e8 f2 fe ff ff       	call   80102572 <idewait>
  outb(0x3f6, 0);  // generate interrupt
80102680:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102687:	00 
80102688:	c7 04 24 f6 03 00 00 	movl   $0x3f6,(%esp)
8010268f:	e8 9b fe ff ff       	call   8010252f <outb>
  outb(0x1f2, 1);  // number of sectors
80102694:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
8010269b:	00 
8010269c:	c7 04 24 f2 01 00 00 	movl   $0x1f2,(%esp)
801026a3:	e8 87 fe ff ff       	call   8010252f <outb>
  outb(0x1f3, b->sector & 0xff);
801026a8:	8b 45 08             	mov    0x8(%ebp),%eax
801026ab:	8b 40 08             	mov    0x8(%eax),%eax
801026ae:	0f b6 c0             	movzbl %al,%eax
801026b1:	89 44 24 04          	mov    %eax,0x4(%esp)
801026b5:	c7 04 24 f3 01 00 00 	movl   $0x1f3,(%esp)
801026bc:	e8 6e fe ff ff       	call   8010252f <outb>
  outb(0x1f4, (b->sector >> 8) & 0xff);
801026c1:	8b 45 08             	mov    0x8(%ebp),%eax
801026c4:	8b 40 08             	mov    0x8(%eax),%eax
801026c7:	c1 e8 08             	shr    $0x8,%eax
801026ca:	0f b6 c0             	movzbl %al,%eax
801026cd:	89 44 24 04          	mov    %eax,0x4(%esp)
801026d1:	c7 04 24 f4 01 00 00 	movl   $0x1f4,(%esp)
801026d8:	e8 52 fe ff ff       	call   8010252f <outb>
  outb(0x1f5, (b->sector >> 16) & 0xff);
801026dd:	8b 45 08             	mov    0x8(%ebp),%eax
801026e0:	8b 40 08             	mov    0x8(%eax),%eax
801026e3:	c1 e8 10             	shr    $0x10,%eax
801026e6:	0f b6 c0             	movzbl %al,%eax
801026e9:	89 44 24 04          	mov    %eax,0x4(%esp)
801026ed:	c7 04 24 f5 01 00 00 	movl   $0x1f5,(%esp)
801026f4:	e8 36 fe ff ff       	call   8010252f <outb>
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((b->sector>>24)&0x0f));
801026f9:	8b 45 08             	mov    0x8(%ebp),%eax
801026fc:	8b 40 04             	mov    0x4(%eax),%eax
801026ff:	83 e0 01             	and    $0x1,%eax
80102702:	89 c2                	mov    %eax,%edx
80102704:	c1 e2 04             	shl    $0x4,%edx
80102707:	8b 45 08             	mov    0x8(%ebp),%eax
8010270a:	8b 40 08             	mov    0x8(%eax),%eax
8010270d:	c1 e8 18             	shr    $0x18,%eax
80102710:	83 e0 0f             	and    $0xf,%eax
80102713:	09 d0                	or     %edx,%eax
80102715:	83 c8 e0             	or     $0xffffffe0,%eax
80102718:	0f b6 c0             	movzbl %al,%eax
8010271b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010271f:	c7 04 24 f6 01 00 00 	movl   $0x1f6,(%esp)
80102726:	e8 04 fe ff ff       	call   8010252f <outb>
  if(b->flags & B_DIRTY){
8010272b:	8b 45 08             	mov    0x8(%ebp),%eax
8010272e:	8b 00                	mov    (%eax),%eax
80102730:	83 e0 04             	and    $0x4,%eax
80102733:	85 c0                	test   %eax,%eax
80102735:	74 34                	je     8010276b <idestart+0x10f>
    outb(0x1f7, IDE_CMD_WRITE);
80102737:	c7 44 24 04 30 00 00 	movl   $0x30,0x4(%esp)
8010273e:	00 
8010273f:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
80102746:	e8 e4 fd ff ff       	call   8010252f <outb>
    outsl(0x1f0, b->data, 512/4);
8010274b:	8b 45 08             	mov    0x8(%ebp),%eax
8010274e:	83 c0 18             	add    $0x18,%eax
80102751:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80102758:	00 
80102759:	89 44 24 04          	mov    %eax,0x4(%esp)
8010275d:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
80102764:	e8 e4 fd ff ff       	call   8010254d <outsl>
80102769:	eb 14                	jmp    8010277f <idestart+0x123>
  } else {
    outb(0x1f7, IDE_CMD_READ);
8010276b:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
80102772:	00 
80102773:	c7 04 24 f7 01 00 00 	movl   $0x1f7,(%esp)
8010277a:	e8 b0 fd ff ff       	call   8010252f <outb>
  }
}
8010277f:	c9                   	leave  
80102780:	c3                   	ret    

80102781 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102781:	55                   	push   %ebp
80102782:	89 e5                	mov    %esp,%ebp
80102784:	83 ec 28             	sub    $0x28,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102787:	c7 04 24 00 b6 10 80 	movl   $0x8010b600,(%esp)
8010278e:	e8 a0 24 00 00       	call   80104c33 <acquire>
  if((b = idequeue) == 0){
80102793:	a1 34 b6 10 80       	mov    0x8010b634,%eax
80102798:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010279b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010279f:	75 11                	jne    801027b2 <ideintr+0x31>
    release(&idelock);
801027a1:	c7 04 24 00 b6 10 80 	movl   $0x8010b600,(%esp)
801027a8:	e8 e8 24 00 00       	call   80104c95 <release>
    // cprintf("spurious IDE interrupt\n");
    return;
801027ad:	e9 90 00 00 00       	jmp    80102842 <ideintr+0xc1>
  }
  idequeue = b->qnext;
801027b2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027b5:	8b 40 14             	mov    0x14(%eax),%eax
801027b8:	a3 34 b6 10 80       	mov    %eax,0x8010b634

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
801027bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027c0:	8b 00                	mov    (%eax),%eax
801027c2:	83 e0 04             	and    $0x4,%eax
801027c5:	85 c0                	test   %eax,%eax
801027c7:	75 2e                	jne    801027f7 <ideintr+0x76>
801027c9:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801027d0:	e8 9d fd ff ff       	call   80102572 <idewait>
801027d5:	85 c0                	test   %eax,%eax
801027d7:	78 1e                	js     801027f7 <ideintr+0x76>
    insl(0x1f0, b->data, 512/4);
801027d9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027dc:	83 c0 18             	add    $0x18,%eax
801027df:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
801027e6:	00 
801027e7:	89 44 24 04          	mov    %eax,0x4(%esp)
801027eb:	c7 04 24 f0 01 00 00 	movl   $0x1f0,(%esp)
801027f2:	e8 13 fd ff ff       	call   8010250a <insl>
  
  // Wake process waiting for this buf.
  b->flags |= B_VALID;
801027f7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801027fa:	8b 00                	mov    (%eax),%eax
801027fc:	89 c2                	mov    %eax,%edx
801027fe:	83 ca 02             	or     $0x2,%edx
80102801:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102804:	89 10                	mov    %edx,(%eax)
  b->flags &= ~B_DIRTY;
80102806:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102809:	8b 00                	mov    (%eax),%eax
8010280b:	89 c2                	mov    %eax,%edx
8010280d:	83 e2 fb             	and    $0xfffffffb,%edx
80102810:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102813:	89 10                	mov    %edx,(%eax)
  wakeup(b);
80102815:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102818:	89 04 24             	mov    %eax,(%esp)
8010281b:	e8 0e 22 00 00       	call   80104a2e <wakeup>
  
  // Start disk on next buf in queue.
  if(idequeue != 0)
80102820:	a1 34 b6 10 80       	mov    0x8010b634,%eax
80102825:	85 c0                	test   %eax,%eax
80102827:	74 0d                	je     80102836 <ideintr+0xb5>
    idestart(idequeue);
80102829:	a1 34 b6 10 80       	mov    0x8010b634,%eax
8010282e:	89 04 24             	mov    %eax,(%esp)
80102831:	e8 26 fe ff ff       	call   8010265c <idestart>

  release(&idelock);
80102836:	c7 04 24 00 b6 10 80 	movl   $0x8010b600,(%esp)
8010283d:	e8 53 24 00 00       	call   80104c95 <release>
}
80102842:	c9                   	leave  
80102843:	c3                   	ret    

80102844 <iderw>:
// Sync buf with disk. 
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
80102844:	55                   	push   %ebp
80102845:	89 e5                	mov    %esp,%ebp
80102847:	83 ec 28             	sub    $0x28,%esp
  struct buf **pp;

  if(!(b->flags & B_BUSY))
8010284a:	8b 45 08             	mov    0x8(%ebp),%eax
8010284d:	8b 00                	mov    (%eax),%eax
8010284f:	83 e0 01             	and    $0x1,%eax
80102852:	85 c0                	test   %eax,%eax
80102854:	75 0c                	jne    80102862 <iderw+0x1e>
    panic("iderw: buf not busy");
80102856:	c7 04 24 85 83 10 80 	movl   $0x80108385,(%esp)
8010285d:	e8 db dc ff ff       	call   8010053d <panic>
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
80102862:	8b 45 08             	mov    0x8(%ebp),%eax
80102865:	8b 00                	mov    (%eax),%eax
80102867:	83 e0 06             	and    $0x6,%eax
8010286a:	83 f8 02             	cmp    $0x2,%eax
8010286d:	75 0c                	jne    8010287b <iderw+0x37>
    panic("iderw: nothing to do");
8010286f:	c7 04 24 99 83 10 80 	movl   $0x80108399,(%esp)
80102876:	e8 c2 dc ff ff       	call   8010053d <panic>
  if(b->dev != 0 && !havedisk1)
8010287b:	8b 45 08             	mov    0x8(%ebp),%eax
8010287e:	8b 40 04             	mov    0x4(%eax),%eax
80102881:	85 c0                	test   %eax,%eax
80102883:	74 15                	je     8010289a <iderw+0x56>
80102885:	a1 38 b6 10 80       	mov    0x8010b638,%eax
8010288a:	85 c0                	test   %eax,%eax
8010288c:	75 0c                	jne    8010289a <iderw+0x56>
    panic("iderw: ide disk 1 not present");
8010288e:	c7 04 24 ae 83 10 80 	movl   $0x801083ae,(%esp)
80102895:	e8 a3 dc ff ff       	call   8010053d <panic>

  acquire(&idelock);  //DOC: acquire-lock
8010289a:	c7 04 24 00 b6 10 80 	movl   $0x8010b600,(%esp)
801028a1:	e8 8d 23 00 00       	call   80104c33 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
801028a6:	8b 45 08             	mov    0x8(%ebp),%eax
801028a9:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC: insert-queue
801028b0:	c7 45 f4 34 b6 10 80 	movl   $0x8010b634,-0xc(%ebp)
801028b7:	eb 0b                	jmp    801028c4 <iderw+0x80>
801028b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801028bc:	8b 00                	mov    (%eax),%eax
801028be:	83 c0 14             	add    $0x14,%eax
801028c1:	89 45 f4             	mov    %eax,-0xc(%ebp)
801028c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801028c7:	8b 00                	mov    (%eax),%eax
801028c9:	85 c0                	test   %eax,%eax
801028cb:	75 ec                	jne    801028b9 <iderw+0x75>
    ;
  *pp = b;
801028cd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801028d0:	8b 55 08             	mov    0x8(%ebp),%edx
801028d3:	89 10                	mov    %edx,(%eax)
  
  // Start disk if necessary.
  if(idequeue == b)
801028d5:	a1 34 b6 10 80       	mov    0x8010b634,%eax
801028da:	3b 45 08             	cmp    0x8(%ebp),%eax
801028dd:	75 22                	jne    80102901 <iderw+0xbd>
    idestart(b);
801028df:	8b 45 08             	mov    0x8(%ebp),%eax
801028e2:	89 04 24             	mov    %eax,(%esp)
801028e5:	e8 72 fd ff ff       	call   8010265c <idestart>
  
  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801028ea:	eb 15                	jmp    80102901 <iderw+0xbd>
    sleep(b, &idelock);
801028ec:	c7 44 24 04 00 b6 10 	movl   $0x8010b600,0x4(%esp)
801028f3:	80 
801028f4:	8b 45 08             	mov    0x8(%ebp),%eax
801028f7:	89 04 24             	mov    %eax,(%esp)
801028fa:	e8 56 20 00 00       	call   80104955 <sleep>
801028ff:	eb 01                	jmp    80102902 <iderw+0xbe>
  // Start disk if necessary.
  if(idequeue == b)
    idestart(b);
  
  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80102901:	90                   	nop
80102902:	8b 45 08             	mov    0x8(%ebp),%eax
80102905:	8b 00                	mov    (%eax),%eax
80102907:	83 e0 06             	and    $0x6,%eax
8010290a:	83 f8 02             	cmp    $0x2,%eax
8010290d:	75 dd                	jne    801028ec <iderw+0xa8>
    sleep(b, &idelock);
  }

  release(&idelock);
8010290f:	c7 04 24 00 b6 10 80 	movl   $0x8010b600,(%esp)
80102916:	e8 7a 23 00 00       	call   80104c95 <release>
}
8010291b:	c9                   	leave  
8010291c:	c3                   	ret    
8010291d:	00 00                	add    %al,(%eax)
	...

80102920 <ioapicread>:
  uint data;
};

static uint
ioapicread(int reg)
{
80102920:	55                   	push   %ebp
80102921:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
80102923:	a1 34 fa 10 80       	mov    0x8010fa34,%eax
80102928:	8b 55 08             	mov    0x8(%ebp),%edx
8010292b:	89 10                	mov    %edx,(%eax)
  return ioapic->data;
8010292d:	a1 34 fa 10 80       	mov    0x8010fa34,%eax
80102932:	8b 40 10             	mov    0x10(%eax),%eax
}
80102935:	5d                   	pop    %ebp
80102936:	c3                   	ret    

80102937 <ioapicwrite>:

static void
ioapicwrite(int reg, uint data)
{
80102937:	55                   	push   %ebp
80102938:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
8010293a:	a1 34 fa 10 80       	mov    0x8010fa34,%eax
8010293f:	8b 55 08             	mov    0x8(%ebp),%edx
80102942:	89 10                	mov    %edx,(%eax)
  ioapic->data = data;
80102944:	a1 34 fa 10 80       	mov    0x8010fa34,%eax
80102949:	8b 55 0c             	mov    0xc(%ebp),%edx
8010294c:	89 50 10             	mov    %edx,0x10(%eax)
}
8010294f:	5d                   	pop    %ebp
80102950:	c3                   	ret    

80102951 <ioapicinit>:

void
ioapicinit(void)
{
80102951:	55                   	push   %ebp
80102952:	89 e5                	mov    %esp,%ebp
80102954:	83 ec 28             	sub    $0x28,%esp
  int i, id, maxintr;

  if(!ismp)
80102957:	a1 04 fb 10 80       	mov    0x8010fb04,%eax
8010295c:	85 c0                	test   %eax,%eax
8010295e:	0f 84 9f 00 00 00    	je     80102a03 <ioapicinit+0xb2>
    return;

  ioapic = (volatile struct ioapic*)IOAPIC;
80102964:	c7 05 34 fa 10 80 00 	movl   $0xfec00000,0x8010fa34
8010296b:	00 c0 fe 
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
8010296e:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80102975:	e8 a6 ff ff ff       	call   80102920 <ioapicread>
8010297a:	c1 e8 10             	shr    $0x10,%eax
8010297d:	25 ff 00 00 00       	and    $0xff,%eax
80102982:	89 45 f0             	mov    %eax,-0x10(%ebp)
  id = ioapicread(REG_ID) >> 24;
80102985:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010298c:	e8 8f ff ff ff       	call   80102920 <ioapicread>
80102991:	c1 e8 18             	shr    $0x18,%eax
80102994:	89 45 ec             	mov    %eax,-0x14(%ebp)
  if(id != ioapicid)
80102997:	0f b6 05 00 fb 10 80 	movzbl 0x8010fb00,%eax
8010299e:	0f b6 c0             	movzbl %al,%eax
801029a1:	3b 45 ec             	cmp    -0x14(%ebp),%eax
801029a4:	74 0c                	je     801029b2 <ioapicinit+0x61>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
801029a6:	c7 04 24 cc 83 10 80 	movl   $0x801083cc,(%esp)
801029ad:	e8 ef d9 ff ff       	call   801003a1 <cprintf>

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801029b2:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801029b9:	eb 3e                	jmp    801029f9 <ioapicinit+0xa8>
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
801029bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801029be:	83 c0 20             	add    $0x20,%eax
801029c1:	0d 00 00 01 00       	or     $0x10000,%eax
801029c6:	8b 55 f4             	mov    -0xc(%ebp),%edx
801029c9:	83 c2 08             	add    $0x8,%edx
801029cc:	01 d2                	add    %edx,%edx
801029ce:	89 44 24 04          	mov    %eax,0x4(%esp)
801029d2:	89 14 24             	mov    %edx,(%esp)
801029d5:	e8 5d ff ff ff       	call   80102937 <ioapicwrite>
    ioapicwrite(REG_TABLE+2*i+1, 0);
801029da:	8b 45 f4             	mov    -0xc(%ebp),%eax
801029dd:	83 c0 08             	add    $0x8,%eax
801029e0:	01 c0                	add    %eax,%eax
801029e2:	83 c0 01             	add    $0x1,%eax
801029e5:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801029ec:	00 
801029ed:	89 04 24             	mov    %eax,(%esp)
801029f0:	e8 42 ff ff ff       	call   80102937 <ioapicwrite>
  if(id != ioapicid)
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
801029f5:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801029f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801029fc:	3b 45 f0             	cmp    -0x10(%ebp),%eax
801029ff:	7e ba                	jle    801029bb <ioapicinit+0x6a>
80102a01:	eb 01                	jmp    80102a04 <ioapicinit+0xb3>
ioapicinit(void)
{
  int i, id, maxintr;

  if(!ismp)
    return;
80102a03:	90                   	nop
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
80102a04:	c9                   	leave  
80102a05:	c3                   	ret    

80102a06 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80102a06:	55                   	push   %ebp
80102a07:	89 e5                	mov    %esp,%ebp
80102a09:	83 ec 08             	sub    $0x8,%esp
  if(!ismp)
80102a0c:	a1 04 fb 10 80       	mov    0x8010fb04,%eax
80102a11:	85 c0                	test   %eax,%eax
80102a13:	74 39                	je     80102a4e <ioapicenable+0x48>
    return;

  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80102a15:	8b 45 08             	mov    0x8(%ebp),%eax
80102a18:	83 c0 20             	add    $0x20,%eax
80102a1b:	8b 55 08             	mov    0x8(%ebp),%edx
80102a1e:	83 c2 08             	add    $0x8,%edx
80102a21:	01 d2                	add    %edx,%edx
80102a23:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a27:	89 14 24             	mov    %edx,(%esp)
80102a2a:	e8 08 ff ff ff       	call   80102937 <ioapicwrite>
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102a2f:	8b 45 0c             	mov    0xc(%ebp),%eax
80102a32:	c1 e0 18             	shl    $0x18,%eax
80102a35:	8b 55 08             	mov    0x8(%ebp),%edx
80102a38:	83 c2 08             	add    $0x8,%edx
80102a3b:	01 d2                	add    %edx,%edx
80102a3d:	83 c2 01             	add    $0x1,%edx
80102a40:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a44:	89 14 24             	mov    %edx,(%esp)
80102a47:	e8 eb fe ff ff       	call   80102937 <ioapicwrite>
80102a4c:	eb 01                	jmp    80102a4f <ioapicenable+0x49>

void
ioapicenable(int irq, int cpunum)
{
  if(!ismp)
    return;
80102a4e:	90                   	nop
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
}
80102a4f:	c9                   	leave  
80102a50:	c3                   	ret    
80102a51:	00 00                	add    %al,(%eax)
	...

80102a54 <v2p>:
#define KERNBASE 0x80000000         // First kernel virtual address
#define KERNLINK (KERNBASE+EXTMEM)  // Address where kernel is linked

#ifndef __ASSEMBLER__

static inline uint v2p(void *a) { return ((uint) (a))  - KERNBASE; }
80102a54:	55                   	push   %ebp
80102a55:	89 e5                	mov    %esp,%ebp
80102a57:	8b 45 08             	mov    0x8(%ebp),%eax
80102a5a:	05 00 00 00 80       	add    $0x80000000,%eax
80102a5f:	5d                   	pop    %ebp
80102a60:	c3                   	ret    

80102a61 <kinit1>:
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void
kinit1(void *vstart, void *vend)
{
80102a61:	55                   	push   %ebp
80102a62:	89 e5                	mov    %esp,%ebp
80102a64:	83 ec 18             	sub    $0x18,%esp
  initlock(&kmem.lock, "kmem");
80102a67:	c7 44 24 04 fe 83 10 	movl   $0x801083fe,0x4(%esp)
80102a6e:	80 
80102a6f:	c7 04 24 40 fa 10 80 	movl   $0x8010fa40,(%esp)
80102a76:	e8 97 21 00 00       	call   80104c12 <initlock>
  kmem.use_lock = 0;
80102a7b:	c7 05 74 fa 10 80 00 	movl   $0x0,0x8010fa74
80102a82:	00 00 00 
  freerange(vstart, vend);
80102a85:	8b 45 0c             	mov    0xc(%ebp),%eax
80102a88:	89 44 24 04          	mov    %eax,0x4(%esp)
80102a8c:	8b 45 08             	mov    0x8(%ebp),%eax
80102a8f:	89 04 24             	mov    %eax,(%esp)
80102a92:	e8 26 00 00 00       	call   80102abd <freerange>
}
80102a97:	c9                   	leave  
80102a98:	c3                   	ret    

80102a99 <kinit2>:

void
kinit2(void *vstart, void *vend)
{
80102a99:	55                   	push   %ebp
80102a9a:	89 e5                	mov    %esp,%ebp
80102a9c:	83 ec 18             	sub    $0x18,%esp
  freerange(vstart, vend);
80102a9f:	8b 45 0c             	mov    0xc(%ebp),%eax
80102aa2:	89 44 24 04          	mov    %eax,0x4(%esp)
80102aa6:	8b 45 08             	mov    0x8(%ebp),%eax
80102aa9:	89 04 24             	mov    %eax,(%esp)
80102aac:	e8 0c 00 00 00       	call   80102abd <freerange>
  kmem.use_lock = 1;
80102ab1:	c7 05 74 fa 10 80 01 	movl   $0x1,0x8010fa74
80102ab8:	00 00 00 
}
80102abb:	c9                   	leave  
80102abc:	c3                   	ret    

80102abd <freerange>:

void
freerange(void *vstart, void *vend)
{
80102abd:	55                   	push   %ebp
80102abe:	89 e5                	mov    %esp,%ebp
80102ac0:	83 ec 28             	sub    $0x28,%esp
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
80102ac3:	8b 45 08             	mov    0x8(%ebp),%eax
80102ac6:	05 ff 0f 00 00       	add    $0xfff,%eax
80102acb:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80102ad0:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102ad3:	eb 12                	jmp    80102ae7 <freerange+0x2a>
    kfree(p);
80102ad5:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102ad8:	89 04 24             	mov    %eax,(%esp)
80102adb:	e8 16 00 00 00       	call   80102af6 <kfree>
void
freerange(void *vstart, void *vend)
{
  char *p;
  p = (char*)PGROUNDUP((uint)vstart);
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102ae0:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80102ae7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102aea:	05 00 10 00 00       	add    $0x1000,%eax
80102aef:	3b 45 0c             	cmp    0xc(%ebp),%eax
80102af2:	76 e1                	jbe    80102ad5 <freerange+0x18>
    kfree(p);
}
80102af4:	c9                   	leave  
80102af5:	c3                   	ret    

80102af6 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80102af6:	55                   	push   %ebp
80102af7:	89 e5                	mov    %esp,%ebp
80102af9:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if((uint)v % PGSIZE || v < end || v2p(v) >= PHYSTOP)
80102afc:	8b 45 08             	mov    0x8(%ebp),%eax
80102aff:	25 ff 0f 00 00       	and    $0xfff,%eax
80102b04:	85 c0                	test   %eax,%eax
80102b06:	75 1b                	jne    80102b23 <kfree+0x2d>
80102b08:	81 7d 08 fc 28 11 80 	cmpl   $0x801128fc,0x8(%ebp)
80102b0f:	72 12                	jb     80102b23 <kfree+0x2d>
80102b11:	8b 45 08             	mov    0x8(%ebp),%eax
80102b14:	89 04 24             	mov    %eax,(%esp)
80102b17:	e8 38 ff ff ff       	call   80102a54 <v2p>
80102b1c:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80102b21:	76 0c                	jbe    80102b2f <kfree+0x39>
    panic("kfree");
80102b23:	c7 04 24 03 84 10 80 	movl   $0x80108403,(%esp)
80102b2a:	e8 0e da ff ff       	call   8010053d <panic>

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
80102b2f:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80102b36:	00 
80102b37:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80102b3e:	00 
80102b3f:	8b 45 08             	mov    0x8(%ebp),%eax
80102b42:	89 04 24             	mov    %eax,(%esp)
80102b45:	e8 38 23 00 00       	call   80104e82 <memset>

  if(kmem.use_lock)
80102b4a:	a1 74 fa 10 80       	mov    0x8010fa74,%eax
80102b4f:	85 c0                	test   %eax,%eax
80102b51:	74 0c                	je     80102b5f <kfree+0x69>
    acquire(&kmem.lock);
80102b53:	c7 04 24 40 fa 10 80 	movl   $0x8010fa40,(%esp)
80102b5a:	e8 d4 20 00 00       	call   80104c33 <acquire>
  r = (struct run*)v;
80102b5f:	8b 45 08             	mov    0x8(%ebp),%eax
80102b62:	89 45 f4             	mov    %eax,-0xc(%ebp)
  r->next = kmem.freelist;
80102b65:	8b 15 78 fa 10 80    	mov    0x8010fa78,%edx
80102b6b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b6e:	89 10                	mov    %edx,(%eax)
  kmem.freelist = r;
80102b70:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102b73:	a3 78 fa 10 80       	mov    %eax,0x8010fa78
  if(kmem.use_lock)
80102b78:	a1 74 fa 10 80       	mov    0x8010fa74,%eax
80102b7d:	85 c0                	test   %eax,%eax
80102b7f:	74 0c                	je     80102b8d <kfree+0x97>
    release(&kmem.lock);
80102b81:	c7 04 24 40 fa 10 80 	movl   $0x8010fa40,(%esp)
80102b88:	e8 08 21 00 00       	call   80104c95 <release>
}
80102b8d:	c9                   	leave  
80102b8e:	c3                   	ret    

80102b8f <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
80102b8f:	55                   	push   %ebp
80102b90:	89 e5                	mov    %esp,%ebp
80102b92:	83 ec 28             	sub    $0x28,%esp
  struct run *r;

  if(kmem.use_lock)
80102b95:	a1 74 fa 10 80       	mov    0x8010fa74,%eax
80102b9a:	85 c0                	test   %eax,%eax
80102b9c:	74 0c                	je     80102baa <kalloc+0x1b>
    acquire(&kmem.lock);
80102b9e:	c7 04 24 40 fa 10 80 	movl   $0x8010fa40,(%esp)
80102ba5:	e8 89 20 00 00       	call   80104c33 <acquire>
  r = kmem.freelist;
80102baa:	a1 78 fa 10 80       	mov    0x8010fa78,%eax
80102baf:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(r)
80102bb2:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80102bb6:	74 0a                	je     80102bc2 <kalloc+0x33>
    kmem.freelist = r->next;
80102bb8:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102bbb:	8b 00                	mov    (%eax),%eax
80102bbd:	a3 78 fa 10 80       	mov    %eax,0x8010fa78
  if(kmem.use_lock)
80102bc2:	a1 74 fa 10 80       	mov    0x8010fa74,%eax
80102bc7:	85 c0                	test   %eax,%eax
80102bc9:	74 0c                	je     80102bd7 <kalloc+0x48>
    release(&kmem.lock);
80102bcb:	c7 04 24 40 fa 10 80 	movl   $0x8010fa40,(%esp)
80102bd2:	e8 be 20 00 00       	call   80104c95 <release>
  return (char*)r;
80102bd7:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80102bda:	c9                   	leave  
80102bdb:	c3                   	ret    

80102bdc <inb>:
// Routines to let C code use special x86 instructions.

static inline uchar
inb(ushort port)
{
80102bdc:	55                   	push   %ebp
80102bdd:	89 e5                	mov    %esp,%ebp
80102bdf:	53                   	push   %ebx
80102be0:	83 ec 14             	sub    $0x14,%esp
80102be3:	8b 45 08             	mov    0x8(%ebp),%eax
80102be6:	66 89 45 e8          	mov    %ax,-0x18(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102bea:	0f b7 55 e8          	movzwl -0x18(%ebp),%edx
80102bee:	66 89 55 ea          	mov    %dx,-0x16(%ebp)
80102bf2:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
80102bf6:	ec                   	in     (%dx),%al
80102bf7:	89 c3                	mov    %eax,%ebx
80102bf9:	88 5d fb             	mov    %bl,-0x5(%ebp)
  return data;
80102bfc:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
}
80102c00:	83 c4 14             	add    $0x14,%esp
80102c03:	5b                   	pop    %ebx
80102c04:	5d                   	pop    %ebp
80102c05:	c3                   	ret    

80102c06 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80102c06:	55                   	push   %ebp
80102c07:	89 e5                	mov    %esp,%ebp
80102c09:	83 ec 14             	sub    $0x14,%esp
  static uchar *charcode[4] = {
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
80102c0c:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80102c13:	e8 c4 ff ff ff       	call   80102bdc <inb>
80102c18:	0f b6 c0             	movzbl %al,%eax
80102c1b:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((st & KBS_DIB) == 0)
80102c1e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80102c21:	83 e0 01             	and    $0x1,%eax
80102c24:	85 c0                	test   %eax,%eax
80102c26:	75 0a                	jne    80102c32 <kbdgetc+0x2c>
    return -1;
80102c28:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102c2d:	e9 23 01 00 00       	jmp    80102d55 <kbdgetc+0x14f>
  data = inb(KBDATAP);
80102c32:	c7 04 24 60 00 00 00 	movl   $0x60,(%esp)
80102c39:	e8 9e ff ff ff       	call   80102bdc <inb>
80102c3e:	0f b6 c0             	movzbl %al,%eax
80102c41:	89 45 fc             	mov    %eax,-0x4(%ebp)

  if(data == 0xE0){
80102c44:	81 7d fc e0 00 00 00 	cmpl   $0xe0,-0x4(%ebp)
80102c4b:	75 17                	jne    80102c64 <kbdgetc+0x5e>
    shift |= E0ESC;
80102c4d:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102c52:	83 c8 40             	or     $0x40,%eax
80102c55:	a3 3c b6 10 80       	mov    %eax,0x8010b63c
    return 0;
80102c5a:	b8 00 00 00 00       	mov    $0x0,%eax
80102c5f:	e9 f1 00 00 00       	jmp    80102d55 <kbdgetc+0x14f>
  } else if(data & 0x80){
80102c64:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102c67:	25 80 00 00 00       	and    $0x80,%eax
80102c6c:	85 c0                	test   %eax,%eax
80102c6e:	74 45                	je     80102cb5 <kbdgetc+0xaf>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
80102c70:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102c75:	83 e0 40             	and    $0x40,%eax
80102c78:	85 c0                	test   %eax,%eax
80102c7a:	75 08                	jne    80102c84 <kbdgetc+0x7e>
80102c7c:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102c7f:	83 e0 7f             	and    $0x7f,%eax
80102c82:	eb 03                	jmp    80102c87 <kbdgetc+0x81>
80102c84:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102c87:	89 45 fc             	mov    %eax,-0x4(%ebp)
    shift &= ~(shiftcode[data] | E0ESC);
80102c8a:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102c8d:	05 20 90 10 80       	add    $0x80109020,%eax
80102c92:	0f b6 00             	movzbl (%eax),%eax
80102c95:	83 c8 40             	or     $0x40,%eax
80102c98:	0f b6 c0             	movzbl %al,%eax
80102c9b:	f7 d0                	not    %eax
80102c9d:	89 c2                	mov    %eax,%edx
80102c9f:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102ca4:	21 d0                	and    %edx,%eax
80102ca6:	a3 3c b6 10 80       	mov    %eax,0x8010b63c
    return 0;
80102cab:	b8 00 00 00 00       	mov    $0x0,%eax
80102cb0:	e9 a0 00 00 00       	jmp    80102d55 <kbdgetc+0x14f>
  } else if(shift & E0ESC){
80102cb5:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102cba:	83 e0 40             	and    $0x40,%eax
80102cbd:	85 c0                	test   %eax,%eax
80102cbf:	74 14                	je     80102cd5 <kbdgetc+0xcf>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
80102cc1:	81 4d fc 80 00 00 00 	orl    $0x80,-0x4(%ebp)
    shift &= ~E0ESC;
80102cc8:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102ccd:	83 e0 bf             	and    $0xffffffbf,%eax
80102cd0:	a3 3c b6 10 80       	mov    %eax,0x8010b63c
  }

  shift |= shiftcode[data];
80102cd5:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102cd8:	05 20 90 10 80       	add    $0x80109020,%eax
80102cdd:	0f b6 00             	movzbl (%eax),%eax
80102ce0:	0f b6 d0             	movzbl %al,%edx
80102ce3:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102ce8:	09 d0                	or     %edx,%eax
80102cea:	a3 3c b6 10 80       	mov    %eax,0x8010b63c
  shift ^= togglecode[data];
80102cef:	8b 45 fc             	mov    -0x4(%ebp),%eax
80102cf2:	05 20 91 10 80       	add    $0x80109120,%eax
80102cf7:	0f b6 00             	movzbl (%eax),%eax
80102cfa:	0f b6 d0             	movzbl %al,%edx
80102cfd:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102d02:	31 d0                	xor    %edx,%eax
80102d04:	a3 3c b6 10 80       	mov    %eax,0x8010b63c
  c = charcode[shift & (CTL | SHIFT)][data];
80102d09:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102d0e:	83 e0 03             	and    $0x3,%eax
80102d11:	8b 04 85 20 95 10 80 	mov    -0x7fef6ae0(,%eax,4),%eax
80102d18:	03 45 fc             	add    -0x4(%ebp),%eax
80102d1b:	0f b6 00             	movzbl (%eax),%eax
80102d1e:	0f b6 c0             	movzbl %al,%eax
80102d21:	89 45 f8             	mov    %eax,-0x8(%ebp)
  if(shift & CAPSLOCK){
80102d24:	a1 3c b6 10 80       	mov    0x8010b63c,%eax
80102d29:	83 e0 08             	and    $0x8,%eax
80102d2c:	85 c0                	test   %eax,%eax
80102d2e:	74 22                	je     80102d52 <kbdgetc+0x14c>
    if('a' <= c && c <= 'z')
80102d30:	83 7d f8 60          	cmpl   $0x60,-0x8(%ebp)
80102d34:	76 0c                	jbe    80102d42 <kbdgetc+0x13c>
80102d36:	83 7d f8 7a          	cmpl   $0x7a,-0x8(%ebp)
80102d3a:	77 06                	ja     80102d42 <kbdgetc+0x13c>
      c += 'A' - 'a';
80102d3c:	83 6d f8 20          	subl   $0x20,-0x8(%ebp)
80102d40:	eb 10                	jmp    80102d52 <kbdgetc+0x14c>
    else if('A' <= c && c <= 'Z')
80102d42:	83 7d f8 40          	cmpl   $0x40,-0x8(%ebp)
80102d46:	76 0a                	jbe    80102d52 <kbdgetc+0x14c>
80102d48:	83 7d f8 5a          	cmpl   $0x5a,-0x8(%ebp)
80102d4c:	77 04                	ja     80102d52 <kbdgetc+0x14c>
      c += 'a' - 'A';
80102d4e:	83 45 f8 20          	addl   $0x20,-0x8(%ebp)
  }
  return c;
80102d52:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80102d55:	c9                   	leave  
80102d56:	c3                   	ret    

80102d57 <kbdintr>:

void
kbdintr(void)
{
80102d57:	55                   	push   %ebp
80102d58:	89 e5                	mov    %esp,%ebp
80102d5a:	83 ec 18             	sub    $0x18,%esp
  consoleintr(kbdgetc);
80102d5d:	c7 04 24 06 2c 10 80 	movl   $0x80102c06,(%esp)
80102d64:	e8 d3 da ff ff       	call   8010083c <consoleintr>
}
80102d69:	c9                   	leave  
80102d6a:	c3                   	ret    
	...

80102d6c <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80102d6c:	55                   	push   %ebp
80102d6d:	89 e5                	mov    %esp,%ebp
80102d6f:	83 ec 08             	sub    $0x8,%esp
80102d72:	8b 55 08             	mov    0x8(%ebp),%edx
80102d75:	8b 45 0c             	mov    0xc(%ebp),%eax
80102d78:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80102d7c:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102d7f:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80102d83:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80102d87:	ee                   	out    %al,(%dx)
}
80102d88:	c9                   	leave  
80102d89:	c3                   	ret    

80102d8a <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80102d8a:	55                   	push   %ebp
80102d8b:	89 e5                	mov    %esp,%ebp
80102d8d:	53                   	push   %ebx
80102d8e:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80102d91:	9c                   	pushf  
80102d92:	5b                   	pop    %ebx
80102d93:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  return eflags;
80102d96:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80102d99:	83 c4 10             	add    $0x10,%esp
80102d9c:	5b                   	pop    %ebx
80102d9d:	5d                   	pop    %ebp
80102d9e:	c3                   	ret    

80102d9f <lapicw>:

volatile uint *lapic;  // Initialized in mp.c

static void
lapicw(int index, int value)
{
80102d9f:	55                   	push   %ebp
80102da0:	89 e5                	mov    %esp,%ebp
  lapic[index] = value;
80102da2:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102da7:	8b 55 08             	mov    0x8(%ebp),%edx
80102daa:	c1 e2 02             	shl    $0x2,%edx
80102dad:	01 c2                	add    %eax,%edx
80102daf:	8b 45 0c             	mov    0xc(%ebp),%eax
80102db2:	89 02                	mov    %eax,(%edx)
  lapic[ID];  // wait for write to finish, by reading
80102db4:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102db9:	83 c0 20             	add    $0x20,%eax
80102dbc:	8b 00                	mov    (%eax),%eax
}
80102dbe:	5d                   	pop    %ebp
80102dbf:	c3                   	ret    

80102dc0 <lapicinit>:
//PAGEBREAK!

void
lapicinit(int c)
{
80102dc0:	55                   	push   %ebp
80102dc1:	89 e5                	mov    %esp,%ebp
80102dc3:	83 ec 08             	sub    $0x8,%esp
  if(!lapic) 
80102dc6:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102dcb:	85 c0                	test   %eax,%eax
80102dcd:	0f 84 47 01 00 00    	je     80102f1a <lapicinit+0x15a>
    return;

  // Enable local APIC; set spurious interrupt vector.
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));
80102dd3:	c7 44 24 04 3f 01 00 	movl   $0x13f,0x4(%esp)
80102dda:	00 
80102ddb:	c7 04 24 3c 00 00 00 	movl   $0x3c,(%esp)
80102de2:	e8 b8 ff ff ff       	call   80102d9f <lapicw>

  // The timer repeatedly counts down at bus frequency
  // from lapic[TICR] and then issues an interrupt.  
  // If xv6 cared more about precise timekeeping,
  // TICR would be calibrated using an external time source.
  lapicw(TDCR, X1);
80102de7:	c7 44 24 04 0b 00 00 	movl   $0xb,0x4(%esp)
80102dee:	00 
80102def:	c7 04 24 f8 00 00 00 	movl   $0xf8,(%esp)
80102df6:	e8 a4 ff ff ff       	call   80102d9f <lapicw>
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
80102dfb:	c7 44 24 04 20 00 02 	movl   $0x20020,0x4(%esp)
80102e02:	00 
80102e03:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80102e0a:	e8 90 ff ff ff       	call   80102d9f <lapicw>
  lapicw(TICR, 10000000); 
80102e0f:	c7 44 24 04 80 96 98 	movl   $0x989680,0x4(%esp)
80102e16:	00 
80102e17:	c7 04 24 e0 00 00 00 	movl   $0xe0,(%esp)
80102e1e:	e8 7c ff ff ff       	call   80102d9f <lapicw>

  // Disable logical interrupt lines.
  lapicw(LINT0, MASKED);
80102e23:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80102e2a:	00 
80102e2b:	c7 04 24 d4 00 00 00 	movl   $0xd4,(%esp)
80102e32:	e8 68 ff ff ff       	call   80102d9f <lapicw>
  lapicw(LINT1, MASKED);
80102e37:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80102e3e:	00 
80102e3f:	c7 04 24 d8 00 00 00 	movl   $0xd8,(%esp)
80102e46:	e8 54 ff ff ff       	call   80102d9f <lapicw>

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80102e4b:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102e50:	83 c0 30             	add    $0x30,%eax
80102e53:	8b 00                	mov    (%eax),%eax
80102e55:	c1 e8 10             	shr    $0x10,%eax
80102e58:	25 ff 00 00 00       	and    $0xff,%eax
80102e5d:	83 f8 03             	cmp    $0x3,%eax
80102e60:	76 14                	jbe    80102e76 <lapicinit+0xb6>
    lapicw(PCINT, MASKED);
80102e62:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80102e69:	00 
80102e6a:	c7 04 24 d0 00 00 00 	movl   $0xd0,(%esp)
80102e71:	e8 29 ff ff ff       	call   80102d9f <lapicw>

  // Map error interrupt to IRQ_ERROR.
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
80102e76:	c7 44 24 04 33 00 00 	movl   $0x33,0x4(%esp)
80102e7d:	00 
80102e7e:	c7 04 24 dc 00 00 00 	movl   $0xdc,(%esp)
80102e85:	e8 15 ff ff ff       	call   80102d9f <lapicw>

  // Clear error status register (requires back-to-back writes).
  lapicw(ESR, 0);
80102e8a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102e91:	00 
80102e92:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80102e99:	e8 01 ff ff ff       	call   80102d9f <lapicw>
  lapicw(ESR, 0);
80102e9e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102ea5:	00 
80102ea6:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80102ead:	e8 ed fe ff ff       	call   80102d9f <lapicw>

  // Ack any outstanding interrupts.
  lapicw(EOI, 0);
80102eb2:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102eb9:	00 
80102eba:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
80102ec1:	e8 d9 fe ff ff       	call   80102d9f <lapicw>

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
80102ec6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102ecd:	00 
80102ece:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
80102ed5:	e8 c5 fe ff ff       	call   80102d9f <lapicw>
  lapicw(ICRLO, BCAST | INIT | LEVEL);
80102eda:	c7 44 24 04 00 85 08 	movl   $0x88500,0x4(%esp)
80102ee1:	00 
80102ee2:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80102ee9:	e8 b1 fe ff ff       	call   80102d9f <lapicw>
  while(lapic[ICRLO] & DELIVS)
80102eee:	90                   	nop
80102eef:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102ef4:	05 00 03 00 00       	add    $0x300,%eax
80102ef9:	8b 00                	mov    (%eax),%eax
80102efb:	25 00 10 00 00       	and    $0x1000,%eax
80102f00:	85 c0                	test   %eax,%eax
80102f02:	75 eb                	jne    80102eef <lapicinit+0x12f>
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
80102f04:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102f0b:	00 
80102f0c:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80102f13:	e8 87 fe ff ff       	call   80102d9f <lapicw>
80102f18:	eb 01                	jmp    80102f1b <lapicinit+0x15b>

void
lapicinit(int c)
{
  if(!lapic) 
    return;
80102f1a:	90                   	nop
  while(lapic[ICRLO] & DELIVS)
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80102f1b:	c9                   	leave  
80102f1c:	c3                   	ret    

80102f1d <cpunum>:

int
cpunum(void)
{
80102f1d:	55                   	push   %ebp
80102f1e:	89 e5                	mov    %esp,%ebp
80102f20:	83 ec 18             	sub    $0x18,%esp
  // Cannot call cpu when interrupts are enabled:
  // result not guaranteed to last long enough to be used!
  // Would prefer to panic but even printing is chancy here:
  // almost everything, including cprintf and panic, calls cpu,
  // often indirectly through acquire and release.
  if(readeflags()&FL_IF){
80102f23:	e8 62 fe ff ff       	call   80102d8a <readeflags>
80102f28:	25 00 02 00 00       	and    $0x200,%eax
80102f2d:	85 c0                	test   %eax,%eax
80102f2f:	74 29                	je     80102f5a <cpunum+0x3d>
    static int n;
    if(n++ == 0)
80102f31:	a1 40 b6 10 80       	mov    0x8010b640,%eax
80102f36:	85 c0                	test   %eax,%eax
80102f38:	0f 94 c2             	sete   %dl
80102f3b:	83 c0 01             	add    $0x1,%eax
80102f3e:	a3 40 b6 10 80       	mov    %eax,0x8010b640
80102f43:	84 d2                	test   %dl,%dl
80102f45:	74 13                	je     80102f5a <cpunum+0x3d>
      cprintf("cpu called from %x with interrupts enabled\n",
80102f47:	8b 45 04             	mov    0x4(%ebp),%eax
80102f4a:	89 44 24 04          	mov    %eax,0x4(%esp)
80102f4e:	c7 04 24 0c 84 10 80 	movl   $0x8010840c,(%esp)
80102f55:	e8 47 d4 ff ff       	call   801003a1 <cprintf>
        __builtin_return_address(0));
  }

  if(lapic)
80102f5a:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102f5f:	85 c0                	test   %eax,%eax
80102f61:	74 0f                	je     80102f72 <cpunum+0x55>
    return lapic[ID]>>24;
80102f63:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102f68:	83 c0 20             	add    $0x20,%eax
80102f6b:	8b 00                	mov    (%eax),%eax
80102f6d:	c1 e8 18             	shr    $0x18,%eax
80102f70:	eb 05                	jmp    80102f77 <cpunum+0x5a>
  return 0;
80102f72:	b8 00 00 00 00       	mov    $0x0,%eax
}
80102f77:	c9                   	leave  
80102f78:	c3                   	ret    

80102f79 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
80102f79:	55                   	push   %ebp
80102f7a:	89 e5                	mov    %esp,%ebp
80102f7c:	83 ec 08             	sub    $0x8,%esp
  if(lapic)
80102f7f:	a1 7c fa 10 80       	mov    0x8010fa7c,%eax
80102f84:	85 c0                	test   %eax,%eax
80102f86:	74 14                	je     80102f9c <lapiceoi+0x23>
    lapicw(EOI, 0);
80102f88:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80102f8f:	00 
80102f90:	c7 04 24 2c 00 00 00 	movl   $0x2c,(%esp)
80102f97:	e8 03 fe ff ff       	call   80102d9f <lapicw>
}
80102f9c:	c9                   	leave  
80102f9d:	c3                   	ret    

80102f9e <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
80102f9e:	55                   	push   %ebp
80102f9f:	89 e5                	mov    %esp,%ebp
}
80102fa1:	5d                   	pop    %ebp
80102fa2:	c3                   	ret    

80102fa3 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80102fa3:	55                   	push   %ebp
80102fa4:	89 e5                	mov    %esp,%ebp
80102fa6:	83 ec 1c             	sub    $0x1c,%esp
80102fa9:	8b 45 08             	mov    0x8(%ebp),%eax
80102fac:	88 45 ec             	mov    %al,-0x14(%ebp)
  ushort *wrv;
  
  // "The BSP must initialize CMOS shutdown code to 0AH
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(IO_RTC, 0xF);  // offset 0xF is shutdown code
80102faf:	c7 44 24 04 0f 00 00 	movl   $0xf,0x4(%esp)
80102fb6:	00 
80102fb7:	c7 04 24 70 00 00 00 	movl   $0x70,(%esp)
80102fbe:	e8 a9 fd ff ff       	call   80102d6c <outb>
  outb(IO_RTC+1, 0x0A);
80102fc3:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80102fca:	00 
80102fcb:	c7 04 24 71 00 00 00 	movl   $0x71,(%esp)
80102fd2:	e8 95 fd ff ff       	call   80102d6c <outb>
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
80102fd7:	c7 45 f8 67 04 00 80 	movl   $0x80000467,-0x8(%ebp)
  wrv[0] = 0;
80102fde:	8b 45 f8             	mov    -0x8(%ebp),%eax
80102fe1:	66 c7 00 00 00       	movw   $0x0,(%eax)
  wrv[1] = addr >> 4;
80102fe6:	8b 45 f8             	mov    -0x8(%ebp),%eax
80102fe9:	8d 50 02             	lea    0x2(%eax),%edx
80102fec:	8b 45 0c             	mov    0xc(%ebp),%eax
80102fef:	c1 e8 04             	shr    $0x4,%eax
80102ff2:	66 89 02             	mov    %ax,(%edx)

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80102ff5:	0f b6 45 ec          	movzbl -0x14(%ebp),%eax
80102ff9:	c1 e0 18             	shl    $0x18,%eax
80102ffc:	89 44 24 04          	mov    %eax,0x4(%esp)
80103000:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
80103007:	e8 93 fd ff ff       	call   80102d9f <lapicw>
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
8010300c:	c7 44 24 04 00 c5 00 	movl   $0xc500,0x4(%esp)
80103013:	00 
80103014:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
8010301b:	e8 7f fd ff ff       	call   80102d9f <lapicw>
  microdelay(200);
80103020:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
80103027:	e8 72 ff ff ff       	call   80102f9e <microdelay>
  lapicw(ICRLO, INIT | LEVEL);
8010302c:	c7 44 24 04 00 85 00 	movl   $0x8500,0x4(%esp)
80103033:	00 
80103034:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
8010303b:	e8 5f fd ff ff       	call   80102d9f <lapicw>
  microdelay(100);    // should be 10ms, but too slow in Bochs!
80103040:	c7 04 24 64 00 00 00 	movl   $0x64,(%esp)
80103047:	e8 52 ff ff ff       	call   80102f9e <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
8010304c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80103053:	eb 40                	jmp    80103095 <lapicstartap+0xf2>
    lapicw(ICRHI, apicid<<24);
80103055:	0f b6 45 ec          	movzbl -0x14(%ebp),%eax
80103059:	c1 e0 18             	shl    $0x18,%eax
8010305c:	89 44 24 04          	mov    %eax,0x4(%esp)
80103060:	c7 04 24 c4 00 00 00 	movl   $0xc4,(%esp)
80103067:	e8 33 fd ff ff       	call   80102d9f <lapicw>
    lapicw(ICRLO, STARTUP | (addr>>12));
8010306c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010306f:	c1 e8 0c             	shr    $0xc,%eax
80103072:	80 cc 06             	or     $0x6,%ah
80103075:	89 44 24 04          	mov    %eax,0x4(%esp)
80103079:	c7 04 24 c0 00 00 00 	movl   $0xc0,(%esp)
80103080:	e8 1a fd ff ff       	call   80102d9f <lapicw>
    microdelay(200);
80103085:	c7 04 24 c8 00 00 00 	movl   $0xc8,(%esp)
8010308c:	e8 0d ff ff ff       	call   80102f9e <microdelay>
  // Send startup IPI (twice!) to enter code.
  // Regular hardware is supposed to only accept a STARTUP
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
80103091:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80103095:	83 7d fc 01          	cmpl   $0x1,-0x4(%ebp)
80103099:	7e ba                	jle    80103055 <lapicstartap+0xb2>
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
    microdelay(200);
  }
}
8010309b:	c9                   	leave  
8010309c:	c3                   	ret    
8010309d:	00 00                	add    %al,(%eax)
	...

801030a0 <initlog>:

static void recover_from_log(void);

void
initlog(void)
{
801030a0:	55                   	push   %ebp
801030a1:	89 e5                	mov    %esp,%ebp
801030a3:	83 ec 28             	sub    $0x28,%esp
  if (sizeof(struct logheader) >= BSIZE)
    panic("initlog: too big logheader");

  struct superblock sb;
  initlock(&log.lock, "log");
801030a6:	c7 44 24 04 38 84 10 	movl   $0x80108438,0x4(%esp)
801030ad:	80 
801030ae:	c7 04 24 80 fa 10 80 	movl   $0x8010fa80,(%esp)
801030b5:	e8 58 1b 00 00       	call   80104c12 <initlock>
  readsb(ROOTDEV, &sb);
801030ba:	8d 45 e8             	lea    -0x18(%ebp),%eax
801030bd:	89 44 24 04          	mov    %eax,0x4(%esp)
801030c1:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801030c8:	e8 af e2 ff ff       	call   8010137c <readsb>
  log.start = sb.size - sb.nlog;
801030cd:	8b 55 e8             	mov    -0x18(%ebp),%edx
801030d0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801030d3:	89 d1                	mov    %edx,%ecx
801030d5:	29 c1                	sub    %eax,%ecx
801030d7:	89 c8                	mov    %ecx,%eax
801030d9:	a3 b4 fa 10 80       	mov    %eax,0x8010fab4
  log.size = sb.nlog;
801030de:	8b 45 f4             	mov    -0xc(%ebp),%eax
801030e1:	a3 b8 fa 10 80       	mov    %eax,0x8010fab8
  log.dev = ROOTDEV;
801030e6:	c7 05 c0 fa 10 80 01 	movl   $0x1,0x8010fac0
801030ed:	00 00 00 
  recover_from_log();
801030f0:	e8 97 01 00 00       	call   8010328c <recover_from_log>
}
801030f5:	c9                   	leave  
801030f6:	c3                   	ret    

801030f7 <install_trans>:

// Copy committed blocks from log to their home location
static void 
install_trans(void)
{
801030f7:	55                   	push   %ebp
801030f8:	89 e5                	mov    %esp,%ebp
801030fa:	83 ec 28             	sub    $0x28,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801030fd:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103104:	e9 89 00 00 00       	jmp    80103192 <install_trans+0x9b>
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
80103109:	a1 b4 fa 10 80       	mov    0x8010fab4,%eax
8010310e:	03 45 f4             	add    -0xc(%ebp),%eax
80103111:	83 c0 01             	add    $0x1,%eax
80103114:	89 c2                	mov    %eax,%edx
80103116:	a1 c0 fa 10 80       	mov    0x8010fac0,%eax
8010311b:	89 54 24 04          	mov    %edx,0x4(%esp)
8010311f:	89 04 24             	mov    %eax,(%esp)
80103122:	e8 7f d0 ff ff       	call   801001a6 <bread>
80103127:	89 45 f0             	mov    %eax,-0x10(%ebp)
    struct buf *dbuf = bread(log.dev, log.lh.sector[tail]); // read dst
8010312a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010312d:	83 c0 10             	add    $0x10,%eax
80103130:	8b 04 85 88 fa 10 80 	mov    -0x7fef0578(,%eax,4),%eax
80103137:	89 c2                	mov    %eax,%edx
80103139:	a1 c0 fa 10 80       	mov    0x8010fac0,%eax
8010313e:	89 54 24 04          	mov    %edx,0x4(%esp)
80103142:	89 04 24             	mov    %eax,(%esp)
80103145:	e8 5c d0 ff ff       	call   801001a6 <bread>
8010314a:	89 45 ec             	mov    %eax,-0x14(%ebp)
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
8010314d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103150:	8d 50 18             	lea    0x18(%eax),%edx
80103153:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103156:	83 c0 18             	add    $0x18,%eax
80103159:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
80103160:	00 
80103161:	89 54 24 04          	mov    %edx,0x4(%esp)
80103165:	89 04 24             	mov    %eax,(%esp)
80103168:	e8 e8 1d 00 00       	call   80104f55 <memmove>
    bwrite(dbuf);  // write dst to disk
8010316d:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103170:	89 04 24             	mov    %eax,(%esp)
80103173:	e8 65 d0 ff ff       	call   801001dd <bwrite>
    brelse(lbuf); 
80103178:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010317b:	89 04 24             	mov    %eax,(%esp)
8010317e:	e8 94 d0 ff ff       	call   80100217 <brelse>
    brelse(dbuf);
80103183:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103186:	89 04 24             	mov    %eax,(%esp)
80103189:	e8 89 d0 ff ff       	call   80100217 <brelse>
static void 
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
8010318e:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103192:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
80103197:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010319a:	0f 8f 69 ff ff ff    	jg     80103109 <install_trans+0x12>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
    bwrite(dbuf);  // write dst to disk
    brelse(lbuf); 
    brelse(dbuf);
  }
}
801031a0:	c9                   	leave  
801031a1:	c3                   	ret    

801031a2 <read_head>:

// Read the log header from disk into the in-memory log header
static void
read_head(void)
{
801031a2:	55                   	push   %ebp
801031a3:	89 e5                	mov    %esp,%ebp
801031a5:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
801031a8:	a1 b4 fa 10 80       	mov    0x8010fab4,%eax
801031ad:	89 c2                	mov    %eax,%edx
801031af:	a1 c0 fa 10 80       	mov    0x8010fac0,%eax
801031b4:	89 54 24 04          	mov    %edx,0x4(%esp)
801031b8:	89 04 24             	mov    %eax,(%esp)
801031bb:	e8 e6 cf ff ff       	call   801001a6 <bread>
801031c0:	89 45 f0             	mov    %eax,-0x10(%ebp)
  struct logheader *lh = (struct logheader *) (buf->data);
801031c3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801031c6:	83 c0 18             	add    $0x18,%eax
801031c9:	89 45 ec             	mov    %eax,-0x14(%ebp)
  int i;
  log.lh.n = lh->n;
801031cc:	8b 45 ec             	mov    -0x14(%ebp),%eax
801031cf:	8b 00                	mov    (%eax),%eax
801031d1:	a3 c4 fa 10 80       	mov    %eax,0x8010fac4
  for (i = 0; i < log.lh.n; i++) {
801031d6:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801031dd:	eb 1b                	jmp    801031fa <read_head+0x58>
    log.lh.sector[i] = lh->sector[i];
801031df:	8b 45 ec             	mov    -0x14(%ebp),%eax
801031e2:	8b 55 f4             	mov    -0xc(%ebp),%edx
801031e5:	8b 44 90 04          	mov    0x4(%eax,%edx,4),%eax
801031e9:	8b 55 f4             	mov    -0xc(%ebp),%edx
801031ec:	83 c2 10             	add    $0x10,%edx
801031ef:	89 04 95 88 fa 10 80 	mov    %eax,-0x7fef0578(,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  log.lh.n = lh->n;
  for (i = 0; i < log.lh.n; i++) {
801031f6:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801031fa:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
801031ff:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103202:	7f db                	jg     801031df <read_head+0x3d>
    log.lh.sector[i] = lh->sector[i];
  }
  brelse(buf);
80103204:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103207:	89 04 24             	mov    %eax,(%esp)
8010320a:	e8 08 d0 ff ff       	call   80100217 <brelse>
}
8010320f:	c9                   	leave  
80103210:	c3                   	ret    

80103211 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80103211:	55                   	push   %ebp
80103212:	89 e5                	mov    %esp,%ebp
80103214:	83 ec 28             	sub    $0x28,%esp
  struct buf *buf = bread(log.dev, log.start);
80103217:	a1 b4 fa 10 80       	mov    0x8010fab4,%eax
8010321c:	89 c2                	mov    %eax,%edx
8010321e:	a1 c0 fa 10 80       	mov    0x8010fac0,%eax
80103223:	89 54 24 04          	mov    %edx,0x4(%esp)
80103227:	89 04 24             	mov    %eax,(%esp)
8010322a:	e8 77 cf ff ff       	call   801001a6 <bread>
8010322f:	89 45 f0             	mov    %eax,-0x10(%ebp)
  struct logheader *hb = (struct logheader *) (buf->data);
80103232:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103235:	83 c0 18             	add    $0x18,%eax
80103238:	89 45 ec             	mov    %eax,-0x14(%ebp)
  int i;
  hb->n = log.lh.n;
8010323b:	8b 15 c4 fa 10 80    	mov    0x8010fac4,%edx
80103241:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103244:	89 10                	mov    %edx,(%eax)
  for (i = 0; i < log.lh.n; i++) {
80103246:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010324d:	eb 1b                	jmp    8010326a <write_head+0x59>
    hb->sector[i] = log.lh.sector[i];
8010324f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103252:	83 c0 10             	add    $0x10,%eax
80103255:	8b 0c 85 88 fa 10 80 	mov    -0x7fef0578(,%eax,4),%ecx
8010325c:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010325f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103262:	89 4c 90 04          	mov    %ecx,0x4(%eax,%edx,4)
{
  struct buf *buf = bread(log.dev, log.start);
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
80103266:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
8010326a:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
8010326f:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103272:	7f db                	jg     8010324f <write_head+0x3e>
    hb->sector[i] = log.lh.sector[i];
  }
  bwrite(buf);
80103274:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103277:	89 04 24             	mov    %eax,(%esp)
8010327a:	e8 5e cf ff ff       	call   801001dd <bwrite>
  brelse(buf);
8010327f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103282:	89 04 24             	mov    %eax,(%esp)
80103285:	e8 8d cf ff ff       	call   80100217 <brelse>
}
8010328a:	c9                   	leave  
8010328b:	c3                   	ret    

8010328c <recover_from_log>:

static void
recover_from_log(void)
{
8010328c:	55                   	push   %ebp
8010328d:	89 e5                	mov    %esp,%ebp
8010328f:	83 ec 08             	sub    $0x8,%esp
  read_head();      
80103292:	e8 0b ff ff ff       	call   801031a2 <read_head>
  install_trans(); // if committed, copy from log to disk
80103297:	e8 5b fe ff ff       	call   801030f7 <install_trans>
  log.lh.n = 0;
8010329c:	c7 05 c4 fa 10 80 00 	movl   $0x0,0x8010fac4
801032a3:	00 00 00 
  write_head(); // clear the log
801032a6:	e8 66 ff ff ff       	call   80103211 <write_head>
}
801032ab:	c9                   	leave  
801032ac:	c3                   	ret    

801032ad <begin_trans>:

void
begin_trans(void)
{
801032ad:	55                   	push   %ebp
801032ae:	89 e5                	mov    %esp,%ebp
801032b0:	83 ec 18             	sub    $0x18,%esp
  acquire(&log.lock);
801032b3:	c7 04 24 80 fa 10 80 	movl   $0x8010fa80,(%esp)
801032ba:	e8 74 19 00 00       	call   80104c33 <acquire>
  while (log.busy) {
801032bf:	eb 14                	jmp    801032d5 <begin_trans+0x28>
    sleep(&log, &log.lock);
801032c1:	c7 44 24 04 80 fa 10 	movl   $0x8010fa80,0x4(%esp)
801032c8:	80 
801032c9:	c7 04 24 80 fa 10 80 	movl   $0x8010fa80,(%esp)
801032d0:	e8 80 16 00 00       	call   80104955 <sleep>

void
begin_trans(void)
{
  acquire(&log.lock);
  while (log.busy) {
801032d5:	a1 bc fa 10 80       	mov    0x8010fabc,%eax
801032da:	85 c0                	test   %eax,%eax
801032dc:	75 e3                	jne    801032c1 <begin_trans+0x14>
    sleep(&log, &log.lock);
  }
  log.busy = 1;
801032de:	c7 05 bc fa 10 80 01 	movl   $0x1,0x8010fabc
801032e5:	00 00 00 
  release(&log.lock);
801032e8:	c7 04 24 80 fa 10 80 	movl   $0x8010fa80,(%esp)
801032ef:	e8 a1 19 00 00       	call   80104c95 <release>
}
801032f4:	c9                   	leave  
801032f5:	c3                   	ret    

801032f6 <commit_trans>:

void
commit_trans(void)
{
801032f6:	55                   	push   %ebp
801032f7:	89 e5                	mov    %esp,%ebp
801032f9:	83 ec 18             	sub    $0x18,%esp
  if (log.lh.n > 0) {
801032fc:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
80103301:	85 c0                	test   %eax,%eax
80103303:	7e 19                	jle    8010331e <commit_trans+0x28>
    write_head();    // Write header to disk -- the real commit
80103305:	e8 07 ff ff ff       	call   80103211 <write_head>
    install_trans(); // Now install writes to home locations
8010330a:	e8 e8 fd ff ff       	call   801030f7 <install_trans>
    log.lh.n = 0; 
8010330f:	c7 05 c4 fa 10 80 00 	movl   $0x0,0x8010fac4
80103316:	00 00 00 
    write_head();    // Erase the transaction from the log
80103319:	e8 f3 fe ff ff       	call   80103211 <write_head>
  }
  
  acquire(&log.lock);
8010331e:	c7 04 24 80 fa 10 80 	movl   $0x8010fa80,(%esp)
80103325:	e8 09 19 00 00       	call   80104c33 <acquire>
  log.busy = 0;
8010332a:	c7 05 bc fa 10 80 00 	movl   $0x0,0x8010fabc
80103331:	00 00 00 
  wakeup(&log);
80103334:	c7 04 24 80 fa 10 80 	movl   $0x8010fa80,(%esp)
8010333b:	e8 ee 16 00 00       	call   80104a2e <wakeup>
  release(&log.lock);
80103340:	c7 04 24 80 fa 10 80 	movl   $0x8010fa80,(%esp)
80103347:	e8 49 19 00 00       	call   80104c95 <release>
}
8010334c:	c9                   	leave  
8010334d:	c3                   	ret    

8010334e <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
8010334e:	55                   	push   %ebp
8010334f:	89 e5                	mov    %esp,%ebp
80103351:	83 ec 28             	sub    $0x28,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103354:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
80103359:	83 f8 09             	cmp    $0x9,%eax
8010335c:	7f 12                	jg     80103370 <log_write+0x22>
8010335e:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
80103363:	8b 15 b8 fa 10 80    	mov    0x8010fab8,%edx
80103369:	83 ea 01             	sub    $0x1,%edx
8010336c:	39 d0                	cmp    %edx,%eax
8010336e:	7c 0c                	jl     8010337c <log_write+0x2e>
    panic("too big a transaction");
80103370:	c7 04 24 3c 84 10 80 	movl   $0x8010843c,(%esp)
80103377:	e8 c1 d1 ff ff       	call   8010053d <panic>
  if (!log.busy)
8010337c:	a1 bc fa 10 80       	mov    0x8010fabc,%eax
80103381:	85 c0                	test   %eax,%eax
80103383:	75 0c                	jne    80103391 <log_write+0x43>
    panic("write outside of trans");
80103385:	c7 04 24 52 84 10 80 	movl   $0x80108452,(%esp)
8010338c:	e8 ac d1 ff ff       	call   8010053d <panic>

  for (i = 0; i < log.lh.n; i++) {
80103391:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103398:	eb 1d                	jmp    801033b7 <log_write+0x69>
    if (log.lh.sector[i] == b->sector)   // log absorbtion?
8010339a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010339d:	83 c0 10             	add    $0x10,%eax
801033a0:	8b 04 85 88 fa 10 80 	mov    -0x7fef0578(,%eax,4),%eax
801033a7:	89 c2                	mov    %eax,%edx
801033a9:	8b 45 08             	mov    0x8(%ebp),%eax
801033ac:	8b 40 08             	mov    0x8(%eax),%eax
801033af:	39 c2                	cmp    %eax,%edx
801033b1:	74 10                	je     801033c3 <log_write+0x75>
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
    panic("too big a transaction");
  if (!log.busy)
    panic("write outside of trans");

  for (i = 0; i < log.lh.n; i++) {
801033b3:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801033b7:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
801033bc:	3b 45 f4             	cmp    -0xc(%ebp),%eax
801033bf:	7f d9                	jg     8010339a <log_write+0x4c>
801033c1:	eb 01                	jmp    801033c4 <log_write+0x76>
    if (log.lh.sector[i] == b->sector)   // log absorbtion?
      break;
801033c3:	90                   	nop
  }
  log.lh.sector[i] = b->sector;
801033c4:	8b 45 08             	mov    0x8(%ebp),%eax
801033c7:	8b 40 08             	mov    0x8(%eax),%eax
801033ca:	8b 55 f4             	mov    -0xc(%ebp),%edx
801033cd:	83 c2 10             	add    $0x10,%edx
801033d0:	89 04 95 88 fa 10 80 	mov    %eax,-0x7fef0578(,%edx,4)
  struct buf *lbuf = bread(b->dev, log.start+i+1);
801033d7:	a1 b4 fa 10 80       	mov    0x8010fab4,%eax
801033dc:	03 45 f4             	add    -0xc(%ebp),%eax
801033df:	83 c0 01             	add    $0x1,%eax
801033e2:	89 c2                	mov    %eax,%edx
801033e4:	8b 45 08             	mov    0x8(%ebp),%eax
801033e7:	8b 40 04             	mov    0x4(%eax),%eax
801033ea:	89 54 24 04          	mov    %edx,0x4(%esp)
801033ee:	89 04 24             	mov    %eax,(%esp)
801033f1:	e8 b0 cd ff ff       	call   801001a6 <bread>
801033f6:	89 45 f0             	mov    %eax,-0x10(%ebp)
  memmove(lbuf->data, b->data, BSIZE);
801033f9:	8b 45 08             	mov    0x8(%ebp),%eax
801033fc:	8d 50 18             	lea    0x18(%eax),%edx
801033ff:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103402:	83 c0 18             	add    $0x18,%eax
80103405:	c7 44 24 08 00 02 00 	movl   $0x200,0x8(%esp)
8010340c:	00 
8010340d:	89 54 24 04          	mov    %edx,0x4(%esp)
80103411:	89 04 24             	mov    %eax,(%esp)
80103414:	e8 3c 1b 00 00       	call   80104f55 <memmove>
  bwrite(lbuf);
80103419:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010341c:	89 04 24             	mov    %eax,(%esp)
8010341f:	e8 b9 cd ff ff       	call   801001dd <bwrite>
  brelse(lbuf);
80103424:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103427:	89 04 24             	mov    %eax,(%esp)
8010342a:	e8 e8 cd ff ff       	call   80100217 <brelse>
  if (i == log.lh.n)
8010342f:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
80103434:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103437:	75 0d                	jne    80103446 <log_write+0xf8>
    log.lh.n++;
80103439:	a1 c4 fa 10 80       	mov    0x8010fac4,%eax
8010343e:	83 c0 01             	add    $0x1,%eax
80103441:	a3 c4 fa 10 80       	mov    %eax,0x8010fac4
  b->flags |= B_DIRTY; // XXX prevent eviction
80103446:	8b 45 08             	mov    0x8(%ebp),%eax
80103449:	8b 00                	mov    (%eax),%eax
8010344b:	89 c2                	mov    %eax,%edx
8010344d:	83 ca 04             	or     $0x4,%edx
80103450:	8b 45 08             	mov    0x8(%ebp),%eax
80103453:	89 10                	mov    %edx,(%eax)
}
80103455:	c9                   	leave  
80103456:	c3                   	ret    
	...

80103458 <v2p>:
80103458:	55                   	push   %ebp
80103459:	89 e5                	mov    %esp,%ebp
8010345b:	8b 45 08             	mov    0x8(%ebp),%eax
8010345e:	05 00 00 00 80       	add    $0x80000000,%eax
80103463:	5d                   	pop    %ebp
80103464:	c3                   	ret    

80103465 <p2v>:
static inline void *p2v(uint a) { return (void *) ((a) + KERNBASE); }
80103465:	55                   	push   %ebp
80103466:	89 e5                	mov    %esp,%ebp
80103468:	8b 45 08             	mov    0x8(%ebp),%eax
8010346b:	05 00 00 00 80       	add    $0x80000000,%eax
80103470:	5d                   	pop    %ebp
80103471:	c3                   	ret    

80103472 <xchg>:
  asm volatile("sti");
}

static inline uint
xchg(volatile uint *addr, uint newval)
{
80103472:	55                   	push   %ebp
80103473:	89 e5                	mov    %esp,%ebp
80103475:	53                   	push   %ebx
80103476:	83 ec 10             	sub    $0x10,%esp
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
               "+m" (*addr), "=a" (result) :
80103479:	8b 55 08             	mov    0x8(%ebp),%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
8010347c:	8b 45 0c             	mov    0xc(%ebp),%eax
               "+m" (*addr), "=a" (result) :
8010347f:	8b 4d 08             	mov    0x8(%ebp),%ecx
xchg(volatile uint *addr, uint newval)
{
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80103482:	89 c3                	mov    %eax,%ebx
80103484:	89 d8                	mov    %ebx,%eax
80103486:	f0 87 02             	lock xchg %eax,(%edx)
80103489:	89 c3                	mov    %eax,%ebx
8010348b:	89 5d f8             	mov    %ebx,-0x8(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
8010348e:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80103491:	83 c4 10             	add    $0x10,%esp
80103494:	5b                   	pop    %ebx
80103495:	5d                   	pop    %ebp
80103496:	c3                   	ret    

80103497 <main>:
// Bootstrap processor starts running C code here.
// Allocate a real stack and switch to it, first
// doing some setup required for memory allocator to work.
int
main(void)
{
80103497:	55                   	push   %ebp
80103498:	89 e5                	mov    %esp,%ebp
8010349a:	83 e4 f0             	and    $0xfffffff0,%esp
8010349d:	83 ec 10             	sub    $0x10,%esp
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
801034a0:	c7 44 24 04 00 00 40 	movl   $0x80400000,0x4(%esp)
801034a7:	80 
801034a8:	c7 04 24 fc 28 11 80 	movl   $0x801128fc,(%esp)
801034af:	e8 ad f5 ff ff       	call   80102a61 <kinit1>
  kvmalloc();      // kernel page table
801034b4:	e8 dd 45 00 00       	call   80107a96 <kvmalloc>
  mpinit();        // collect info about this machine
801034b9:	e8 63 04 00 00       	call   80103921 <mpinit>
  lapicinit(mpbcpu());
801034be:	e8 2e 02 00 00       	call   801036f1 <mpbcpu>
801034c3:	89 04 24             	mov    %eax,(%esp)
801034c6:	e8 f5 f8 ff ff       	call   80102dc0 <lapicinit>
  seginit();       // set up segments
801034cb:	e8 69 3f 00 00       	call   80107439 <seginit>
  cprintf("\ncpu%d: starting xv6\n\n", cpu->id);
801034d0:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801034d6:	0f b6 00             	movzbl (%eax),%eax
801034d9:	0f b6 c0             	movzbl %al,%eax
801034dc:	89 44 24 04          	mov    %eax,0x4(%esp)
801034e0:	c7 04 24 69 84 10 80 	movl   $0x80108469,(%esp)
801034e7:	e8 b5 ce ff ff       	call   801003a1 <cprintf>
  picinit();       // interrupt controller
801034ec:	e8 95 06 00 00       	call   80103b86 <picinit>
  ioapicinit();    // another interrupt controller
801034f1:	e8 5b f4 ff ff       	call   80102951 <ioapicinit>
  consoleinit();   // I/O devices & their interrupts
801034f6:	e8 21 d6 ff ff       	call   80100b1c <consoleinit>
  uartinit();      // serial port
801034fb:	e8 84 32 00 00       	call   80106784 <uartinit>
  pinit();         // process table
80103500:	e8 96 0b 00 00       	call   8010409b <pinit>
  tvinit();        // trap vectors
80103505:	e8 1d 2e 00 00       	call   80106327 <tvinit>
  binit();         // buffer cache
8010350a:	e8 25 cb ff ff       	call   80100034 <binit>
  fileinit();      // file table
8010350f:	e8 7c da ff ff       	call   80100f90 <fileinit>
  iinit();         // inode cache
80103514:	e8 2a e1 ff ff       	call   80101643 <iinit>
  ideinit();       // disk
80103519:	e8 98 f0 ff ff       	call   801025b6 <ideinit>
  if(!ismp)
8010351e:	a1 04 fb 10 80       	mov    0x8010fb04,%eax
80103523:	85 c0                	test   %eax,%eax
80103525:	75 05                	jne    8010352c <main+0x95>
    timerinit();   // uniprocessor timer
80103527:	e8 3e 2d 00 00       	call   8010626a <timerinit>
  startothers();   // start other processors
8010352c:	e8 87 00 00 00       	call   801035b8 <startothers>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80103531:	c7 44 24 04 00 00 00 	movl   $0x8e000000,0x4(%esp)
80103538:	8e 
80103539:	c7 04 24 00 00 40 80 	movl   $0x80400000,(%esp)
80103540:	e8 54 f5 ff ff       	call   80102a99 <kinit2>
  userinit();      // first user process
80103545:	e8 6c 0c 00 00       	call   801041b6 <userinit>
  // Finish setting up this processor in mpmain.
  mpmain();
8010354a:	e8 22 00 00 00       	call   80103571 <mpmain>

8010354f <mpenter>:
}

// Other CPUs jump here from entryother.S.
static void
mpenter(void)
{
8010354f:	55                   	push   %ebp
80103550:	89 e5                	mov    %esp,%ebp
80103552:	83 ec 18             	sub    $0x18,%esp
  switchkvm(); 
80103555:	e8 53 45 00 00       	call   80107aad <switchkvm>
  seginit();
8010355a:	e8 da 3e 00 00       	call   80107439 <seginit>
  lapicinit(cpunum());
8010355f:	e8 b9 f9 ff ff       	call   80102f1d <cpunum>
80103564:	89 04 24             	mov    %eax,(%esp)
80103567:	e8 54 f8 ff ff       	call   80102dc0 <lapicinit>
  mpmain();
8010356c:	e8 00 00 00 00       	call   80103571 <mpmain>

80103571 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80103571:	55                   	push   %ebp
80103572:	89 e5                	mov    %esp,%ebp
80103574:	83 ec 18             	sub    $0x18,%esp
  cprintf("cpu%d: starting\n", cpu->id);
80103577:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010357d:	0f b6 00             	movzbl (%eax),%eax
80103580:	0f b6 c0             	movzbl %al,%eax
80103583:	89 44 24 04          	mov    %eax,0x4(%esp)
80103587:	c7 04 24 80 84 10 80 	movl   $0x80108480,(%esp)
8010358e:	e8 0e ce ff ff       	call   801003a1 <cprintf>
  idtinit();       // load idt register
80103593:	e8 03 2f 00 00       	call   8010649b <idtinit>
  xchg(&cpu->started, 1); // tell startothers() we're up
80103598:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010359e:	05 a8 00 00 00       	add    $0xa8,%eax
801035a3:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
801035aa:	00 
801035ab:	89 04 24             	mov    %eax,(%esp)
801035ae:	e8 bf fe ff ff       	call   80103472 <xchg>
  scheduler();     // start running processes
801035b3:	e8 f4 11 00 00       	call   801047ac <scheduler>

801035b8 <startothers>:
pde_t entrypgdir[];  // For entry.S

// Start the non-boot (AP) processors.
static void
startothers(void)
{
801035b8:	55                   	push   %ebp
801035b9:	89 e5                	mov    %esp,%ebp
801035bb:	53                   	push   %ebx
801035bc:	83 ec 24             	sub    $0x24,%esp
  char *stack;

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = p2v(0x7000);
801035bf:	c7 04 24 00 70 00 00 	movl   $0x7000,(%esp)
801035c6:	e8 9a fe ff ff       	call   80103465 <p2v>
801035cb:	89 45 f0             	mov    %eax,-0x10(%ebp)
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
801035ce:	b8 8a 00 00 00       	mov    $0x8a,%eax
801035d3:	89 44 24 08          	mov    %eax,0x8(%esp)
801035d7:	c7 44 24 04 0c b5 10 	movl   $0x8010b50c,0x4(%esp)
801035de:	80 
801035df:	8b 45 f0             	mov    -0x10(%ebp),%eax
801035e2:	89 04 24             	mov    %eax,(%esp)
801035e5:	e8 6b 19 00 00       	call   80104f55 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
801035ea:	c7 45 f4 20 fb 10 80 	movl   $0x8010fb20,-0xc(%ebp)
801035f1:	e9 86 00 00 00       	jmp    8010367c <startothers+0xc4>
    if(c == cpus+cpunum())  // We've started already.
801035f6:	e8 22 f9 ff ff       	call   80102f1d <cpunum>
801035fb:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103601:	05 20 fb 10 80       	add    $0x8010fb20,%eax
80103606:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80103609:	74 69                	je     80103674 <startothers+0xbc>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what 
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
8010360b:	e8 7f f5 ff ff       	call   80102b8f <kalloc>
80103610:	89 45 ec             	mov    %eax,-0x14(%ebp)
    *(void**)(code-4) = stack + KSTACKSIZE;
80103613:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103616:	83 e8 04             	sub    $0x4,%eax
80103619:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010361c:	81 c2 00 10 00 00    	add    $0x1000,%edx
80103622:	89 10                	mov    %edx,(%eax)
    *(void**)(code-8) = mpenter;
80103624:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103627:	83 e8 08             	sub    $0x8,%eax
8010362a:	c7 00 4f 35 10 80    	movl   $0x8010354f,(%eax)
    *(int**)(code-12) = (void *) v2p(entrypgdir);
80103630:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103633:	8d 58 f4             	lea    -0xc(%eax),%ebx
80103636:	c7 04 24 00 a0 10 80 	movl   $0x8010a000,(%esp)
8010363d:	e8 16 fe ff ff       	call   80103458 <v2p>
80103642:	89 03                	mov    %eax,(%ebx)

    lapicstartap(c->id, v2p(code));
80103644:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103647:	89 04 24             	mov    %eax,(%esp)
8010364a:	e8 09 fe ff ff       	call   80103458 <v2p>
8010364f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103652:	0f b6 12             	movzbl (%edx),%edx
80103655:	0f b6 d2             	movzbl %dl,%edx
80103658:	89 44 24 04          	mov    %eax,0x4(%esp)
8010365c:	89 14 24             	mov    %edx,(%esp)
8010365f:	e8 3f f9 ff ff       	call   80102fa3 <lapicstartap>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103664:	90                   	nop
80103665:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103668:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
8010366e:	85 c0                	test   %eax,%eax
80103670:	74 f3                	je     80103665 <startothers+0xad>
80103672:	eb 01                	jmp    80103675 <startothers+0xbd>
  code = p2v(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
    if(c == cpus+cpunum())  // We've started already.
      continue;
80103674:	90                   	nop
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = p2v(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);

  for(c = cpus; c < cpus+ncpu; c++){
80103675:	81 45 f4 bc 00 00 00 	addl   $0xbc,-0xc(%ebp)
8010367c:	a1 00 01 11 80       	mov    0x80110100,%eax
80103681:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
80103687:	05 20 fb 10 80       	add    $0x8010fb20,%eax
8010368c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010368f:	0f 87 61 ff ff ff    	ja     801035f6 <startothers+0x3e>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
      ;
  }
}
80103695:	83 c4 24             	add    $0x24,%esp
80103698:	5b                   	pop    %ebx
80103699:	5d                   	pop    %ebp
8010369a:	c3                   	ret    
	...

8010369c <p2v>:
8010369c:	55                   	push   %ebp
8010369d:	89 e5                	mov    %esp,%ebp
8010369f:	8b 45 08             	mov    0x8(%ebp),%eax
801036a2:	05 00 00 00 80       	add    $0x80000000,%eax
801036a7:	5d                   	pop    %ebp
801036a8:	c3                   	ret    

801036a9 <inb>:
// Routines to let C code use special x86 instructions.

static inline uchar
inb(ushort port)
{
801036a9:	55                   	push   %ebp
801036aa:	89 e5                	mov    %esp,%ebp
801036ac:	53                   	push   %ebx
801036ad:	83 ec 14             	sub    $0x14,%esp
801036b0:	8b 45 08             	mov    0x8(%ebp),%eax
801036b3:	66 89 45 e8          	mov    %ax,-0x18(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801036b7:	0f b7 55 e8          	movzwl -0x18(%ebp),%edx
801036bb:	66 89 55 ea          	mov    %dx,-0x16(%ebp)
801036bf:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
801036c3:	ec                   	in     (%dx),%al
801036c4:	89 c3                	mov    %eax,%ebx
801036c6:	88 5d fb             	mov    %bl,-0x5(%ebp)
  return data;
801036c9:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
}
801036cd:	83 c4 14             	add    $0x14,%esp
801036d0:	5b                   	pop    %ebx
801036d1:	5d                   	pop    %ebp
801036d2:	c3                   	ret    

801036d3 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
801036d3:	55                   	push   %ebp
801036d4:	89 e5                	mov    %esp,%ebp
801036d6:	83 ec 08             	sub    $0x8,%esp
801036d9:	8b 55 08             	mov    0x8(%ebp),%edx
801036dc:	8b 45 0c             	mov    0xc(%ebp),%eax
801036df:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
801036e3:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801036e6:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
801036ea:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
801036ee:	ee                   	out    %al,(%dx)
}
801036ef:	c9                   	leave  
801036f0:	c3                   	ret    

801036f1 <mpbcpu>:
int ncpu;
uchar ioapicid;

int
mpbcpu(void)
{
801036f1:	55                   	push   %ebp
801036f2:	89 e5                	mov    %esp,%ebp
  return bcpu-cpus;
801036f4:	a1 44 b6 10 80       	mov    0x8010b644,%eax
801036f9:	89 c2                	mov    %eax,%edx
801036fb:	b8 20 fb 10 80       	mov    $0x8010fb20,%eax
80103700:	89 d1                	mov    %edx,%ecx
80103702:	29 c1                	sub    %eax,%ecx
80103704:	89 c8                	mov    %ecx,%eax
80103706:	c1 f8 02             	sar    $0x2,%eax
80103709:	69 c0 cf 46 7d 67    	imul   $0x677d46cf,%eax,%eax
}
8010370f:	5d                   	pop    %ebp
80103710:	c3                   	ret    

80103711 <sum>:

static uchar
sum(uchar *addr, int len)
{
80103711:	55                   	push   %ebp
80103712:	89 e5                	mov    %esp,%ebp
80103714:	83 ec 10             	sub    $0x10,%esp
  int i, sum;
  
  sum = 0;
80103717:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
  for(i=0; i<len; i++)
8010371e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
80103725:	eb 13                	jmp    8010373a <sum+0x29>
    sum += addr[i];
80103727:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010372a:	03 45 08             	add    0x8(%ebp),%eax
8010372d:	0f b6 00             	movzbl (%eax),%eax
80103730:	0f b6 c0             	movzbl %al,%eax
80103733:	01 45 f8             	add    %eax,-0x8(%ebp)
sum(uchar *addr, int len)
{
  int i, sum;
  
  sum = 0;
  for(i=0; i<len; i++)
80103736:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
8010373a:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010373d:	3b 45 0c             	cmp    0xc(%ebp),%eax
80103740:	7c e5                	jl     80103727 <sum+0x16>
    sum += addr[i];
  return sum;
80103742:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80103745:	c9                   	leave  
80103746:	c3                   	ret    

80103747 <mpsearch1>:

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103747:	55                   	push   %ebp
80103748:	89 e5                	mov    %esp,%ebp
8010374a:	83 ec 28             	sub    $0x28,%esp
  uchar *e, *p, *addr;

  addr = p2v(a);
8010374d:	8b 45 08             	mov    0x8(%ebp),%eax
80103750:	89 04 24             	mov    %eax,(%esp)
80103753:	e8 44 ff ff ff       	call   8010369c <p2v>
80103758:	89 45 f0             	mov    %eax,-0x10(%ebp)
  e = addr+len;
8010375b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010375e:	03 45 f0             	add    -0x10(%ebp),%eax
80103761:	89 45 ec             	mov    %eax,-0x14(%ebp)
  for(p = addr; p < e; p += sizeof(struct mp))
80103764:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103767:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010376a:	eb 3f                	jmp    801037ab <mpsearch1+0x64>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
8010376c:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
80103773:	00 
80103774:	c7 44 24 04 94 84 10 	movl   $0x80108494,0x4(%esp)
8010377b:	80 
8010377c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010377f:	89 04 24             	mov    %eax,(%esp)
80103782:	e8 72 17 00 00       	call   80104ef9 <memcmp>
80103787:	85 c0                	test   %eax,%eax
80103789:	75 1c                	jne    801037a7 <mpsearch1+0x60>
8010378b:	c7 44 24 04 10 00 00 	movl   $0x10,0x4(%esp)
80103792:	00 
80103793:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103796:	89 04 24             	mov    %eax,(%esp)
80103799:	e8 73 ff ff ff       	call   80103711 <sum>
8010379e:	84 c0                	test   %al,%al
801037a0:	75 05                	jne    801037a7 <mpsearch1+0x60>
      return (struct mp*)p;
801037a2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037a5:	eb 11                	jmp    801037b8 <mpsearch1+0x71>
{
  uchar *e, *p, *addr;

  addr = p2v(a);
  e = addr+len;
  for(p = addr; p < e; p += sizeof(struct mp))
801037a7:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
801037ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037ae:	3b 45 ec             	cmp    -0x14(%ebp),%eax
801037b1:	72 b9                	jb     8010376c <mpsearch1+0x25>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
      return (struct mp*)p;
  return 0;
801037b3:	b8 00 00 00 00       	mov    $0x0,%eax
}
801037b8:	c9                   	leave  
801037b9:	c3                   	ret    

801037ba <mpsearch>:
// 1) in the first KB of the EBDA;
// 2) in the last KB of system base memory;
// 3) in the BIOS ROM between 0xE0000 and 0xFFFFF.
static struct mp*
mpsearch(void)
{
801037ba:	55                   	push   %ebp
801037bb:	89 e5                	mov    %esp,%ebp
801037bd:	83 ec 28             	sub    $0x28,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar *) P2V(0x400);
801037c0:	c7 45 f4 00 04 00 80 	movl   $0x80000400,-0xc(%ebp)
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
801037c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037ca:	83 c0 0f             	add    $0xf,%eax
801037cd:	0f b6 00             	movzbl (%eax),%eax
801037d0:	0f b6 c0             	movzbl %al,%eax
801037d3:	89 c2                	mov    %eax,%edx
801037d5:	c1 e2 08             	shl    $0x8,%edx
801037d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801037db:	83 c0 0e             	add    $0xe,%eax
801037de:	0f b6 00             	movzbl (%eax),%eax
801037e1:	0f b6 c0             	movzbl %al,%eax
801037e4:	09 d0                	or     %edx,%eax
801037e6:	c1 e0 04             	shl    $0x4,%eax
801037e9:	89 45 f0             	mov    %eax,-0x10(%ebp)
801037ec:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801037f0:	74 21                	je     80103813 <mpsearch+0x59>
    if((mp = mpsearch1(p, 1024)))
801037f2:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
801037f9:	00 
801037fa:	8b 45 f0             	mov    -0x10(%ebp),%eax
801037fd:	89 04 24             	mov    %eax,(%esp)
80103800:	e8 42 ff ff ff       	call   80103747 <mpsearch1>
80103805:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103808:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
8010380c:	74 50                	je     8010385e <mpsearch+0xa4>
      return mp;
8010380e:	8b 45 ec             	mov    -0x14(%ebp),%eax
80103811:	eb 5f                	jmp    80103872 <mpsearch+0xb8>
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80103813:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103816:	83 c0 14             	add    $0x14,%eax
80103819:	0f b6 00             	movzbl (%eax),%eax
8010381c:	0f b6 c0             	movzbl %al,%eax
8010381f:	89 c2                	mov    %eax,%edx
80103821:	c1 e2 08             	shl    $0x8,%edx
80103824:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103827:	83 c0 13             	add    $0x13,%eax
8010382a:	0f b6 00             	movzbl (%eax),%eax
8010382d:	0f b6 c0             	movzbl %al,%eax
80103830:	09 d0                	or     %edx,%eax
80103832:	c1 e0 0a             	shl    $0xa,%eax
80103835:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if((mp = mpsearch1(p-1024, 1024)))
80103838:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010383b:	2d 00 04 00 00       	sub    $0x400,%eax
80103840:	c7 44 24 04 00 04 00 	movl   $0x400,0x4(%esp)
80103847:	00 
80103848:	89 04 24             	mov    %eax,(%esp)
8010384b:	e8 f7 fe ff ff       	call   80103747 <mpsearch1>
80103850:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103853:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80103857:	74 05                	je     8010385e <mpsearch+0xa4>
      return mp;
80103859:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010385c:	eb 14                	jmp    80103872 <mpsearch+0xb8>
  }
  return mpsearch1(0xF0000, 0x10000);
8010385e:	c7 44 24 04 00 00 01 	movl   $0x10000,0x4(%esp)
80103865:	00 
80103866:	c7 04 24 00 00 0f 00 	movl   $0xf0000,(%esp)
8010386d:	e8 d5 fe ff ff       	call   80103747 <mpsearch1>
}
80103872:	c9                   	leave  
80103873:	c3                   	ret    

80103874 <mpconfig>:
// Check for correct signature, calculate the checksum and,
// if correct, check the version.
// To do: check extended table checksum.
static struct mpconf*
mpconfig(struct mp **pmp)
{
80103874:	55                   	push   %ebp
80103875:	89 e5                	mov    %esp,%ebp
80103877:	83 ec 28             	sub    $0x28,%esp
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
8010387a:	e8 3b ff ff ff       	call   801037ba <mpsearch>
8010387f:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103882:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103886:	74 0a                	je     80103892 <mpconfig+0x1e>
80103888:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010388b:	8b 40 04             	mov    0x4(%eax),%eax
8010388e:	85 c0                	test   %eax,%eax
80103890:	75 0a                	jne    8010389c <mpconfig+0x28>
    return 0;
80103892:	b8 00 00 00 00       	mov    $0x0,%eax
80103897:	e9 83 00 00 00       	jmp    8010391f <mpconfig+0xab>
  conf = (struct mpconf*) p2v((uint) mp->physaddr);
8010389c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010389f:	8b 40 04             	mov    0x4(%eax),%eax
801038a2:	89 04 24             	mov    %eax,(%esp)
801038a5:	e8 f2 fd ff ff       	call   8010369c <p2v>
801038aa:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
801038ad:	c7 44 24 08 04 00 00 	movl   $0x4,0x8(%esp)
801038b4:	00 
801038b5:	c7 44 24 04 99 84 10 	movl   $0x80108499,0x4(%esp)
801038bc:	80 
801038bd:	8b 45 f0             	mov    -0x10(%ebp),%eax
801038c0:	89 04 24             	mov    %eax,(%esp)
801038c3:	e8 31 16 00 00       	call   80104ef9 <memcmp>
801038c8:	85 c0                	test   %eax,%eax
801038ca:	74 07                	je     801038d3 <mpconfig+0x5f>
    return 0;
801038cc:	b8 00 00 00 00       	mov    $0x0,%eax
801038d1:	eb 4c                	jmp    8010391f <mpconfig+0xab>
  if(conf->version != 1 && conf->version != 4)
801038d3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801038d6:	0f b6 40 06          	movzbl 0x6(%eax),%eax
801038da:	3c 01                	cmp    $0x1,%al
801038dc:	74 12                	je     801038f0 <mpconfig+0x7c>
801038de:	8b 45 f0             	mov    -0x10(%ebp),%eax
801038e1:	0f b6 40 06          	movzbl 0x6(%eax),%eax
801038e5:	3c 04                	cmp    $0x4,%al
801038e7:	74 07                	je     801038f0 <mpconfig+0x7c>
    return 0;
801038e9:	b8 00 00 00 00       	mov    $0x0,%eax
801038ee:	eb 2f                	jmp    8010391f <mpconfig+0xab>
  if(sum((uchar*)conf, conf->length) != 0)
801038f0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801038f3:	0f b7 40 04          	movzwl 0x4(%eax),%eax
801038f7:	0f b7 c0             	movzwl %ax,%eax
801038fa:	89 44 24 04          	mov    %eax,0x4(%esp)
801038fe:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103901:	89 04 24             	mov    %eax,(%esp)
80103904:	e8 08 fe ff ff       	call   80103711 <sum>
80103909:	84 c0                	test   %al,%al
8010390b:	74 07                	je     80103914 <mpconfig+0xa0>
    return 0;
8010390d:	b8 00 00 00 00       	mov    $0x0,%eax
80103912:	eb 0b                	jmp    8010391f <mpconfig+0xab>
  *pmp = mp;
80103914:	8b 45 08             	mov    0x8(%ebp),%eax
80103917:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010391a:	89 10                	mov    %edx,(%eax)
  return conf;
8010391c:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
8010391f:	c9                   	leave  
80103920:	c3                   	ret    

80103921 <mpinit>:

void
mpinit(void)
{
80103921:	55                   	push   %ebp
80103922:	89 e5                	mov    %esp,%ebp
80103924:	83 ec 38             	sub    $0x38,%esp
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
80103927:	c7 05 44 b6 10 80 20 	movl   $0x8010fb20,0x8010b644
8010392e:	fb 10 80 
  if((conf = mpconfig(&mp)) == 0)
80103931:	8d 45 e0             	lea    -0x20(%ebp),%eax
80103934:	89 04 24             	mov    %eax,(%esp)
80103937:	e8 38 ff ff ff       	call   80103874 <mpconfig>
8010393c:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010393f:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80103943:	0f 84 9c 01 00 00    	je     80103ae5 <mpinit+0x1c4>
    return;
  ismp = 1;
80103949:	c7 05 04 fb 10 80 01 	movl   $0x1,0x8010fb04
80103950:	00 00 00 
  lapic = (uint*)conf->lapicaddr;
80103953:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103956:	8b 40 24             	mov    0x24(%eax),%eax
80103959:	a3 7c fa 10 80       	mov    %eax,0x8010fa7c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
8010395e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80103961:	83 c0 2c             	add    $0x2c,%eax
80103964:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103967:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010396a:	0f b7 40 04          	movzwl 0x4(%eax),%eax
8010396e:	0f b7 c0             	movzwl %ax,%eax
80103971:	03 45 f0             	add    -0x10(%ebp),%eax
80103974:	89 45 ec             	mov    %eax,-0x14(%ebp)
80103977:	e9 f4 00 00 00       	jmp    80103a70 <mpinit+0x14f>
    switch(*p){
8010397c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010397f:	0f b6 00             	movzbl (%eax),%eax
80103982:	0f b6 c0             	movzbl %al,%eax
80103985:	83 f8 04             	cmp    $0x4,%eax
80103988:	0f 87 bf 00 00 00    	ja     80103a4d <mpinit+0x12c>
8010398e:	8b 04 85 dc 84 10 80 	mov    -0x7fef7b24(,%eax,4),%eax
80103995:	ff e0                	jmp    *%eax
    case MPPROC:
      proc = (struct mpproc*)p;
80103997:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010399a:	89 45 e8             	mov    %eax,-0x18(%ebp)
      if(ncpu != proc->apicid){
8010399d:	8b 45 e8             	mov    -0x18(%ebp),%eax
801039a0:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801039a4:	0f b6 d0             	movzbl %al,%edx
801039a7:	a1 00 01 11 80       	mov    0x80110100,%eax
801039ac:	39 c2                	cmp    %eax,%edx
801039ae:	74 2d                	je     801039dd <mpinit+0xbc>
        cprintf("mpinit: ncpu=%d apicid=%d\n", ncpu, proc->apicid);
801039b0:	8b 45 e8             	mov    -0x18(%ebp),%eax
801039b3:	0f b6 40 01          	movzbl 0x1(%eax),%eax
801039b7:	0f b6 d0             	movzbl %al,%edx
801039ba:	a1 00 01 11 80       	mov    0x80110100,%eax
801039bf:	89 54 24 08          	mov    %edx,0x8(%esp)
801039c3:	89 44 24 04          	mov    %eax,0x4(%esp)
801039c7:	c7 04 24 9e 84 10 80 	movl   $0x8010849e,(%esp)
801039ce:	e8 ce c9 ff ff       	call   801003a1 <cprintf>
        ismp = 0;
801039d3:	c7 05 04 fb 10 80 00 	movl   $0x0,0x8010fb04
801039da:	00 00 00 
      }
      if(proc->flags & MPBOOT)
801039dd:	8b 45 e8             	mov    -0x18(%ebp),%eax
801039e0:	0f b6 40 03          	movzbl 0x3(%eax),%eax
801039e4:	0f b6 c0             	movzbl %al,%eax
801039e7:	83 e0 02             	and    $0x2,%eax
801039ea:	85 c0                	test   %eax,%eax
801039ec:	74 15                	je     80103a03 <mpinit+0xe2>
        bcpu = &cpus[ncpu];
801039ee:	a1 00 01 11 80       	mov    0x80110100,%eax
801039f3:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
801039f9:	05 20 fb 10 80       	add    $0x8010fb20,%eax
801039fe:	a3 44 b6 10 80       	mov    %eax,0x8010b644
      cpus[ncpu].id = ncpu;
80103a03:	8b 15 00 01 11 80    	mov    0x80110100,%edx
80103a09:	a1 00 01 11 80       	mov    0x80110100,%eax
80103a0e:	69 d2 bc 00 00 00    	imul   $0xbc,%edx,%edx
80103a14:	81 c2 20 fb 10 80    	add    $0x8010fb20,%edx
80103a1a:	88 02                	mov    %al,(%edx)
      ncpu++;
80103a1c:	a1 00 01 11 80       	mov    0x80110100,%eax
80103a21:	83 c0 01             	add    $0x1,%eax
80103a24:	a3 00 01 11 80       	mov    %eax,0x80110100
      p += sizeof(struct mpproc);
80103a29:	83 45 f4 14          	addl   $0x14,-0xc(%ebp)
      continue;
80103a2d:	eb 41                	jmp    80103a70 <mpinit+0x14f>
    case MPIOAPIC:
      ioapic = (struct mpioapic*)p;
80103a2f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103a32:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      ioapicid = ioapic->apicno;
80103a35:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103a38:	0f b6 40 01          	movzbl 0x1(%eax),%eax
80103a3c:	a2 00 fb 10 80       	mov    %al,0x8010fb00
      p += sizeof(struct mpioapic);
80103a41:	83 45 f4 08          	addl   $0x8,-0xc(%ebp)
      continue;
80103a45:	eb 29                	jmp    80103a70 <mpinit+0x14f>
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80103a47:	83 45 f4 08          	addl   $0x8,-0xc(%ebp)
      continue;
80103a4b:	eb 23                	jmp    80103a70 <mpinit+0x14f>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
80103a4d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103a50:	0f b6 00             	movzbl (%eax),%eax
80103a53:	0f b6 c0             	movzbl %al,%eax
80103a56:	89 44 24 04          	mov    %eax,0x4(%esp)
80103a5a:	c7 04 24 bc 84 10 80 	movl   $0x801084bc,(%esp)
80103a61:	e8 3b c9 ff ff       	call   801003a1 <cprintf>
      ismp = 0;
80103a66:	c7 05 04 fb 10 80 00 	movl   $0x0,0x8010fb04
80103a6d:	00 00 00 
  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80103a70:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103a73:	3b 45 ec             	cmp    -0x14(%ebp),%eax
80103a76:	0f 82 00 ff ff ff    	jb     8010397c <mpinit+0x5b>
    default:
      cprintf("mpinit: unknown config type %x\n", *p);
      ismp = 0;
    }
  }
  if(!ismp){
80103a7c:	a1 04 fb 10 80       	mov    0x8010fb04,%eax
80103a81:	85 c0                	test   %eax,%eax
80103a83:	75 1d                	jne    80103aa2 <mpinit+0x181>
    // Didn't like what we found; fall back to no MP.
    ncpu = 1;
80103a85:	c7 05 00 01 11 80 01 	movl   $0x1,0x80110100
80103a8c:	00 00 00 
    lapic = 0;
80103a8f:	c7 05 7c fa 10 80 00 	movl   $0x0,0x8010fa7c
80103a96:	00 00 00 
    ioapicid = 0;
80103a99:	c6 05 00 fb 10 80 00 	movb   $0x0,0x8010fb00
    return;
80103aa0:	eb 44                	jmp    80103ae6 <mpinit+0x1c5>
  }

  if(mp->imcrp){
80103aa2:	8b 45 e0             	mov    -0x20(%ebp),%eax
80103aa5:	0f b6 40 0c          	movzbl 0xc(%eax),%eax
80103aa9:	84 c0                	test   %al,%al
80103aab:	74 39                	je     80103ae6 <mpinit+0x1c5>
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
80103aad:	c7 44 24 04 70 00 00 	movl   $0x70,0x4(%esp)
80103ab4:	00 
80103ab5:	c7 04 24 22 00 00 00 	movl   $0x22,(%esp)
80103abc:	e8 12 fc ff ff       	call   801036d3 <outb>
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80103ac1:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
80103ac8:	e8 dc fb ff ff       	call   801036a9 <inb>
80103acd:	83 c8 01             	or     $0x1,%eax
80103ad0:	0f b6 c0             	movzbl %al,%eax
80103ad3:	89 44 24 04          	mov    %eax,0x4(%esp)
80103ad7:	c7 04 24 23 00 00 00 	movl   $0x23,(%esp)
80103ade:	e8 f0 fb ff ff       	call   801036d3 <outb>
80103ae3:	eb 01                	jmp    80103ae6 <mpinit+0x1c5>
  struct mpproc *proc;
  struct mpioapic *ioapic;

  bcpu = &cpus[0];
  if((conf = mpconfig(&mp)) == 0)
    return;
80103ae5:	90                   	nop
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
  }
}
80103ae6:	c9                   	leave  
80103ae7:	c3                   	ret    

80103ae8 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80103ae8:	55                   	push   %ebp
80103ae9:	89 e5                	mov    %esp,%ebp
80103aeb:	83 ec 08             	sub    $0x8,%esp
80103aee:	8b 55 08             	mov    0x8(%ebp),%edx
80103af1:	8b 45 0c             	mov    0xc(%ebp),%eax
80103af4:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80103af8:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80103afb:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80103aff:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80103b03:	ee                   	out    %al,(%dx)
}
80103b04:	c9                   	leave  
80103b05:	c3                   	ret    

80103b06 <picsetmask>:
// Initial IRQ mask has interrupt 2 enabled (for slave 8259A).
static ushort irqmask = 0xFFFF & ~(1<<IRQ_SLAVE);

static void
picsetmask(ushort mask)
{
80103b06:	55                   	push   %ebp
80103b07:	89 e5                	mov    %esp,%ebp
80103b09:	83 ec 0c             	sub    $0xc,%esp
80103b0c:	8b 45 08             	mov    0x8(%ebp),%eax
80103b0f:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  irqmask = mask;
80103b13:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103b17:	66 a3 00 b0 10 80    	mov    %ax,0x8010b000
  outb(IO_PIC1+1, mask);
80103b1d:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103b21:	0f b6 c0             	movzbl %al,%eax
80103b24:	89 44 24 04          	mov    %eax,0x4(%esp)
80103b28:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80103b2f:	e8 b4 ff ff ff       	call   80103ae8 <outb>
  outb(IO_PIC2+1, mask >> 8);
80103b34:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80103b38:	66 c1 e8 08          	shr    $0x8,%ax
80103b3c:	0f b6 c0             	movzbl %al,%eax
80103b3f:	89 44 24 04          	mov    %eax,0x4(%esp)
80103b43:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80103b4a:	e8 99 ff ff ff       	call   80103ae8 <outb>
}
80103b4f:	c9                   	leave  
80103b50:	c3                   	ret    

80103b51 <picenable>:

void
picenable(int irq)
{
80103b51:	55                   	push   %ebp
80103b52:	89 e5                	mov    %esp,%ebp
80103b54:	53                   	push   %ebx
80103b55:	83 ec 04             	sub    $0x4,%esp
  picsetmask(irqmask & ~(1<<irq));
80103b58:	8b 45 08             	mov    0x8(%ebp),%eax
80103b5b:	ba 01 00 00 00       	mov    $0x1,%edx
80103b60:	89 d3                	mov    %edx,%ebx
80103b62:	89 c1                	mov    %eax,%ecx
80103b64:	d3 e3                	shl    %cl,%ebx
80103b66:	89 d8                	mov    %ebx,%eax
80103b68:	89 c2                	mov    %eax,%edx
80103b6a:	f7 d2                	not    %edx
80103b6c:	0f b7 05 00 b0 10 80 	movzwl 0x8010b000,%eax
80103b73:	21 d0                	and    %edx,%eax
80103b75:	0f b7 c0             	movzwl %ax,%eax
80103b78:	89 04 24             	mov    %eax,(%esp)
80103b7b:	e8 86 ff ff ff       	call   80103b06 <picsetmask>
}
80103b80:	83 c4 04             	add    $0x4,%esp
80103b83:	5b                   	pop    %ebx
80103b84:	5d                   	pop    %ebp
80103b85:	c3                   	ret    

80103b86 <picinit>:

// Initialize the 8259A interrupt controllers.
void
picinit(void)
{
80103b86:	55                   	push   %ebp
80103b87:	89 e5                	mov    %esp,%ebp
80103b89:	83 ec 08             	sub    $0x8,%esp
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
80103b8c:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
80103b93:	00 
80103b94:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80103b9b:	e8 48 ff ff ff       	call   80103ae8 <outb>
  outb(IO_PIC2+1, 0xFF);
80103ba0:	c7 44 24 04 ff 00 00 	movl   $0xff,0x4(%esp)
80103ba7:	00 
80103ba8:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80103baf:	e8 34 ff ff ff       	call   80103ae8 <outb>

  // ICW1:  0001g0hi
  //    g:  0 = edge triggering, 1 = level triggering
  //    h:  0 = cascaded PICs, 1 = master only
  //    i:  0 = no ICW4, 1 = ICW4 required
  outb(IO_PIC1, 0x11);
80103bb4:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
80103bbb:	00 
80103bbc:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80103bc3:	e8 20 ff ff ff       	call   80103ae8 <outb>

  // ICW2:  Vector offset
  outb(IO_PIC1+1, T_IRQ0);
80103bc8:	c7 44 24 04 20 00 00 	movl   $0x20,0x4(%esp)
80103bcf:	00 
80103bd0:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80103bd7:	e8 0c ff ff ff       	call   80103ae8 <outb>

  // ICW3:  (master PIC) bit mask of IR lines connected to slaves
  //        (slave PIC) 3-bit # of slave's connection to master
  outb(IO_PIC1+1, 1<<IRQ_SLAVE);
80103bdc:	c7 44 24 04 04 00 00 	movl   $0x4,0x4(%esp)
80103be3:	00 
80103be4:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80103beb:	e8 f8 fe ff ff       	call   80103ae8 <outb>
  //    m:  0 = slave PIC, 1 = master PIC
  //      (ignored when b is 0, as the master/slave role
  //      can be hardwired).
  //    a:  1 = Automatic EOI mode
  //    p:  0 = MCS-80/85 mode, 1 = intel x86 mode
  outb(IO_PIC1+1, 0x3);
80103bf0:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80103bf7:	00 
80103bf8:	c7 04 24 21 00 00 00 	movl   $0x21,(%esp)
80103bff:	e8 e4 fe ff ff       	call   80103ae8 <outb>

  // Set up slave (8259A-2)
  outb(IO_PIC2, 0x11);                  // ICW1
80103c04:	c7 44 24 04 11 00 00 	movl   $0x11,0x4(%esp)
80103c0b:	00 
80103c0c:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80103c13:	e8 d0 fe ff ff       	call   80103ae8 <outb>
  outb(IO_PIC2+1, T_IRQ0 + 8);      // ICW2
80103c18:	c7 44 24 04 28 00 00 	movl   $0x28,0x4(%esp)
80103c1f:	00 
80103c20:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80103c27:	e8 bc fe ff ff       	call   80103ae8 <outb>
  outb(IO_PIC2+1, IRQ_SLAVE);           // ICW3
80103c2c:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
80103c33:	00 
80103c34:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80103c3b:	e8 a8 fe ff ff       	call   80103ae8 <outb>
  // NB Automatic EOI mode doesn't tend to work on the slave.
  // Linux source code says it's "to be investigated".
  outb(IO_PIC2+1, 0x3);                 // ICW4
80103c40:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80103c47:	00 
80103c48:	c7 04 24 a1 00 00 00 	movl   $0xa1,(%esp)
80103c4f:	e8 94 fe ff ff       	call   80103ae8 <outb>

  // OCW3:  0ef01prs
  //   ef:  0x = NOP, 10 = clear specific mask, 11 = set specific mask
  //    p:  0 = no polling, 1 = polling mode
  //   rs:  0x = NOP, 10 = read IRR, 11 = read ISR
  outb(IO_PIC1, 0x68);             // clear specific mask
80103c54:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
80103c5b:	00 
80103c5c:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80103c63:	e8 80 fe ff ff       	call   80103ae8 <outb>
  outb(IO_PIC1, 0x0a);             // read IRR by default
80103c68:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80103c6f:	00 
80103c70:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80103c77:	e8 6c fe ff ff       	call   80103ae8 <outb>

  outb(IO_PIC2, 0x68);             // OCW3
80103c7c:	c7 44 24 04 68 00 00 	movl   $0x68,0x4(%esp)
80103c83:	00 
80103c84:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80103c8b:	e8 58 fe ff ff       	call   80103ae8 <outb>
  outb(IO_PIC2, 0x0a);             // OCW3
80103c90:	c7 44 24 04 0a 00 00 	movl   $0xa,0x4(%esp)
80103c97:	00 
80103c98:	c7 04 24 a0 00 00 00 	movl   $0xa0,(%esp)
80103c9f:	e8 44 fe ff ff       	call   80103ae8 <outb>

  if(irqmask != 0xFFFF)
80103ca4:	0f b7 05 00 b0 10 80 	movzwl 0x8010b000,%eax
80103cab:	66 83 f8 ff          	cmp    $0xffff,%ax
80103caf:	74 12                	je     80103cc3 <picinit+0x13d>
    picsetmask(irqmask);
80103cb1:	0f b7 05 00 b0 10 80 	movzwl 0x8010b000,%eax
80103cb8:	0f b7 c0             	movzwl %ax,%eax
80103cbb:	89 04 24             	mov    %eax,(%esp)
80103cbe:	e8 43 fe ff ff       	call   80103b06 <picsetmask>
}
80103cc3:	c9                   	leave  
80103cc4:	c3                   	ret    
80103cc5:	00 00                	add    %al,(%eax)
	...

80103cc8 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80103cc8:	55                   	push   %ebp
80103cc9:	89 e5                	mov    %esp,%ebp
80103ccb:	83 ec 28             	sub    $0x28,%esp
  struct pipe *p;

  p = 0;
80103cce:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
  *f0 = *f1 = 0;
80103cd5:	8b 45 0c             	mov    0xc(%ebp),%eax
80103cd8:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80103cde:	8b 45 0c             	mov    0xc(%ebp),%eax
80103ce1:	8b 10                	mov    (%eax),%edx
80103ce3:	8b 45 08             	mov    0x8(%ebp),%eax
80103ce6:	89 10                	mov    %edx,(%eax)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
80103ce8:	e8 bf d2 ff ff       	call   80100fac <filealloc>
80103ced:	8b 55 08             	mov    0x8(%ebp),%edx
80103cf0:	89 02                	mov    %eax,(%edx)
80103cf2:	8b 45 08             	mov    0x8(%ebp),%eax
80103cf5:	8b 00                	mov    (%eax),%eax
80103cf7:	85 c0                	test   %eax,%eax
80103cf9:	0f 84 c8 00 00 00    	je     80103dc7 <pipealloc+0xff>
80103cff:	e8 a8 d2 ff ff       	call   80100fac <filealloc>
80103d04:	8b 55 0c             	mov    0xc(%ebp),%edx
80103d07:	89 02                	mov    %eax,(%edx)
80103d09:	8b 45 0c             	mov    0xc(%ebp),%eax
80103d0c:	8b 00                	mov    (%eax),%eax
80103d0e:	85 c0                	test   %eax,%eax
80103d10:	0f 84 b1 00 00 00    	je     80103dc7 <pipealloc+0xff>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80103d16:	e8 74 ee ff ff       	call   80102b8f <kalloc>
80103d1b:	89 45 f4             	mov    %eax,-0xc(%ebp)
80103d1e:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103d22:	0f 84 9e 00 00 00    	je     80103dc6 <pipealloc+0xfe>
    goto bad;
  p->readopen = 1;
80103d28:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d2b:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
80103d32:	00 00 00 
  p->writeopen = 1;
80103d35:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d38:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
80103d3f:	00 00 00 
  p->nwrite = 0;
80103d42:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d45:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
80103d4c:	00 00 00 
  p->nread = 0;
80103d4f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d52:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
80103d59:	00 00 00 
  initlock(&p->lock, "pipe");
80103d5c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103d5f:	c7 44 24 04 f0 84 10 	movl   $0x801084f0,0x4(%esp)
80103d66:	80 
80103d67:	89 04 24             	mov    %eax,(%esp)
80103d6a:	e8 a3 0e 00 00       	call   80104c12 <initlock>
  (*f0)->type = FD_PIPE;
80103d6f:	8b 45 08             	mov    0x8(%ebp),%eax
80103d72:	8b 00                	mov    (%eax),%eax
80103d74:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
80103d7a:	8b 45 08             	mov    0x8(%ebp),%eax
80103d7d:	8b 00                	mov    (%eax),%eax
80103d7f:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
80103d83:	8b 45 08             	mov    0x8(%ebp),%eax
80103d86:	8b 00                	mov    (%eax),%eax
80103d88:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
80103d8c:	8b 45 08             	mov    0x8(%ebp),%eax
80103d8f:	8b 00                	mov    (%eax),%eax
80103d91:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103d94:	89 50 0c             	mov    %edx,0xc(%eax)
  (*f1)->type = FD_PIPE;
80103d97:	8b 45 0c             	mov    0xc(%ebp),%eax
80103d9a:	8b 00                	mov    (%eax),%eax
80103d9c:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
80103da2:	8b 45 0c             	mov    0xc(%ebp),%eax
80103da5:	8b 00                	mov    (%eax),%eax
80103da7:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
80103dab:	8b 45 0c             	mov    0xc(%ebp),%eax
80103dae:	8b 00                	mov    (%eax),%eax
80103db0:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80103db4:	8b 45 0c             	mov    0xc(%ebp),%eax
80103db7:	8b 00                	mov    (%eax),%eax
80103db9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103dbc:	89 50 0c             	mov    %edx,0xc(%eax)
  return 0;
80103dbf:	b8 00 00 00 00       	mov    $0x0,%eax
80103dc4:	eb 43                	jmp    80103e09 <pipealloc+0x141>
  p = 0;
  *f0 = *f1 = 0;
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
    goto bad;
80103dc6:	90                   	nop
  (*f1)->pipe = p;
  return 0;

//PAGEBREAK: 20
 bad:
  if(p)
80103dc7:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80103dcb:	74 0b                	je     80103dd8 <pipealloc+0x110>
    kfree((char*)p);
80103dcd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103dd0:	89 04 24             	mov    %eax,(%esp)
80103dd3:	e8 1e ed ff ff       	call   80102af6 <kfree>
  if(*f0)
80103dd8:	8b 45 08             	mov    0x8(%ebp),%eax
80103ddb:	8b 00                	mov    (%eax),%eax
80103ddd:	85 c0                	test   %eax,%eax
80103ddf:	74 0d                	je     80103dee <pipealloc+0x126>
    fileclose(*f0);
80103de1:	8b 45 08             	mov    0x8(%ebp),%eax
80103de4:	8b 00                	mov    (%eax),%eax
80103de6:	89 04 24             	mov    %eax,(%esp)
80103de9:	e8 66 d2 ff ff       	call   80101054 <fileclose>
  if(*f1)
80103dee:	8b 45 0c             	mov    0xc(%ebp),%eax
80103df1:	8b 00                	mov    (%eax),%eax
80103df3:	85 c0                	test   %eax,%eax
80103df5:	74 0d                	je     80103e04 <pipealloc+0x13c>
    fileclose(*f1);
80103df7:	8b 45 0c             	mov    0xc(%ebp),%eax
80103dfa:	8b 00                	mov    (%eax),%eax
80103dfc:	89 04 24             	mov    %eax,(%esp)
80103dff:	e8 50 d2 ff ff       	call   80101054 <fileclose>
  return -1;
80103e04:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80103e09:	c9                   	leave  
80103e0a:	c3                   	ret    

80103e0b <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80103e0b:	55                   	push   %ebp
80103e0c:	89 e5                	mov    %esp,%ebp
80103e0e:	83 ec 18             	sub    $0x18,%esp
  acquire(&p->lock);
80103e11:	8b 45 08             	mov    0x8(%ebp),%eax
80103e14:	89 04 24             	mov    %eax,(%esp)
80103e17:	e8 17 0e 00 00       	call   80104c33 <acquire>
  if(writable){
80103e1c:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80103e20:	74 1f                	je     80103e41 <pipeclose+0x36>
    p->writeopen = 0;
80103e22:	8b 45 08             	mov    0x8(%ebp),%eax
80103e25:	c7 80 40 02 00 00 00 	movl   $0x0,0x240(%eax)
80103e2c:	00 00 00 
    wakeup(&p->nread);
80103e2f:	8b 45 08             	mov    0x8(%ebp),%eax
80103e32:	05 34 02 00 00       	add    $0x234,%eax
80103e37:	89 04 24             	mov    %eax,(%esp)
80103e3a:	e8 ef 0b 00 00       	call   80104a2e <wakeup>
80103e3f:	eb 1d                	jmp    80103e5e <pipeclose+0x53>
  } else {
    p->readopen = 0;
80103e41:	8b 45 08             	mov    0x8(%ebp),%eax
80103e44:	c7 80 3c 02 00 00 00 	movl   $0x0,0x23c(%eax)
80103e4b:	00 00 00 
    wakeup(&p->nwrite);
80103e4e:	8b 45 08             	mov    0x8(%ebp),%eax
80103e51:	05 38 02 00 00       	add    $0x238,%eax
80103e56:	89 04 24             	mov    %eax,(%esp)
80103e59:	e8 d0 0b 00 00       	call   80104a2e <wakeup>
  }
  if(p->readopen == 0 && p->writeopen == 0){
80103e5e:	8b 45 08             	mov    0x8(%ebp),%eax
80103e61:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
80103e67:	85 c0                	test   %eax,%eax
80103e69:	75 25                	jne    80103e90 <pipeclose+0x85>
80103e6b:	8b 45 08             	mov    0x8(%ebp),%eax
80103e6e:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
80103e74:	85 c0                	test   %eax,%eax
80103e76:	75 18                	jne    80103e90 <pipeclose+0x85>
    release(&p->lock);
80103e78:	8b 45 08             	mov    0x8(%ebp),%eax
80103e7b:	89 04 24             	mov    %eax,(%esp)
80103e7e:	e8 12 0e 00 00       	call   80104c95 <release>
    kfree((char*)p);
80103e83:	8b 45 08             	mov    0x8(%ebp),%eax
80103e86:	89 04 24             	mov    %eax,(%esp)
80103e89:	e8 68 ec ff ff       	call   80102af6 <kfree>
80103e8e:	eb 0b                	jmp    80103e9b <pipeclose+0x90>
  } else
    release(&p->lock);
80103e90:	8b 45 08             	mov    0x8(%ebp),%eax
80103e93:	89 04 24             	mov    %eax,(%esp)
80103e96:	e8 fa 0d 00 00       	call   80104c95 <release>
}
80103e9b:	c9                   	leave  
80103e9c:	c3                   	ret    

80103e9d <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
80103e9d:	55                   	push   %ebp
80103e9e:	89 e5                	mov    %esp,%ebp
80103ea0:	53                   	push   %ebx
80103ea1:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
80103ea4:	8b 45 08             	mov    0x8(%ebp),%eax
80103ea7:	89 04 24             	mov    %eax,(%esp)
80103eaa:	e8 84 0d 00 00       	call   80104c33 <acquire>
  for(i = 0; i < n; i++){
80103eaf:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80103eb6:	e9 a6 00 00 00       	jmp    80103f61 <pipewrite+0xc4>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || proc->killed){
80103ebb:	8b 45 08             	mov    0x8(%ebp),%eax
80103ebe:	8b 80 3c 02 00 00    	mov    0x23c(%eax),%eax
80103ec4:	85 c0                	test   %eax,%eax
80103ec6:	74 0d                	je     80103ed5 <pipewrite+0x38>
80103ec8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80103ece:	8b 40 24             	mov    0x24(%eax),%eax
80103ed1:	85 c0                	test   %eax,%eax
80103ed3:	74 15                	je     80103eea <pipewrite+0x4d>
        release(&p->lock);
80103ed5:	8b 45 08             	mov    0x8(%ebp),%eax
80103ed8:	89 04 24             	mov    %eax,(%esp)
80103edb:	e8 b5 0d 00 00       	call   80104c95 <release>
        return -1;
80103ee0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103ee5:	e9 9d 00 00 00       	jmp    80103f87 <pipewrite+0xea>
      }
      wakeup(&p->nread);
80103eea:	8b 45 08             	mov    0x8(%ebp),%eax
80103eed:	05 34 02 00 00       	add    $0x234,%eax
80103ef2:	89 04 24             	mov    %eax,(%esp)
80103ef5:	e8 34 0b 00 00       	call   80104a2e <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103efa:	8b 45 08             	mov    0x8(%ebp),%eax
80103efd:	8b 55 08             	mov    0x8(%ebp),%edx
80103f00:	81 c2 38 02 00 00    	add    $0x238,%edx
80103f06:	89 44 24 04          	mov    %eax,0x4(%esp)
80103f0a:	89 14 24             	mov    %edx,(%esp)
80103f0d:	e8 43 0a 00 00       	call   80104955 <sleep>
80103f12:	eb 01                	jmp    80103f15 <pipewrite+0x78>
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103f14:	90                   	nop
80103f15:	8b 45 08             	mov    0x8(%ebp),%eax
80103f18:	8b 90 38 02 00 00    	mov    0x238(%eax),%edx
80103f1e:	8b 45 08             	mov    0x8(%ebp),%eax
80103f21:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
80103f27:	05 00 02 00 00       	add    $0x200,%eax
80103f2c:	39 c2                	cmp    %eax,%edx
80103f2e:	74 8b                	je     80103ebb <pipewrite+0x1e>
        return -1;
      }
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80103f30:	8b 45 08             	mov    0x8(%ebp),%eax
80103f33:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80103f39:	89 c3                	mov    %eax,%ebx
80103f3b:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
80103f41:	8b 55 f4             	mov    -0xc(%ebp),%edx
80103f44:	03 55 0c             	add    0xc(%ebp),%edx
80103f47:	0f b6 0a             	movzbl (%edx),%ecx
80103f4a:	8b 55 08             	mov    0x8(%ebp),%edx
80103f4d:	88 4c 1a 34          	mov    %cl,0x34(%edx,%ebx,1)
80103f51:	8d 50 01             	lea    0x1(%eax),%edx
80103f54:	8b 45 08             	mov    0x8(%ebp),%eax
80103f57:	89 90 38 02 00 00    	mov    %edx,0x238(%eax)
pipewrite(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  for(i = 0; i < n; i++){
80103f5d:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80103f61:	8b 45 f4             	mov    -0xc(%ebp),%eax
80103f64:	3b 45 10             	cmp    0x10(%ebp),%eax
80103f67:	7c ab                	jl     80103f14 <pipewrite+0x77>
      wakeup(&p->nread);
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
    }
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
80103f69:	8b 45 08             	mov    0x8(%ebp),%eax
80103f6c:	05 34 02 00 00       	add    $0x234,%eax
80103f71:	89 04 24             	mov    %eax,(%esp)
80103f74:	e8 b5 0a 00 00       	call   80104a2e <wakeup>
  release(&p->lock);
80103f79:	8b 45 08             	mov    0x8(%ebp),%eax
80103f7c:	89 04 24             	mov    %eax,(%esp)
80103f7f:	e8 11 0d 00 00       	call   80104c95 <release>
  return n;
80103f84:	8b 45 10             	mov    0x10(%ebp),%eax
}
80103f87:	83 c4 24             	add    $0x24,%esp
80103f8a:	5b                   	pop    %ebx
80103f8b:	5d                   	pop    %ebp
80103f8c:	c3                   	ret    

80103f8d <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
80103f8d:	55                   	push   %ebp
80103f8e:	89 e5                	mov    %esp,%ebp
80103f90:	53                   	push   %ebx
80103f91:	83 ec 24             	sub    $0x24,%esp
  int i;

  acquire(&p->lock);
80103f94:	8b 45 08             	mov    0x8(%ebp),%eax
80103f97:	89 04 24             	mov    %eax,(%esp)
80103f9a:	e8 94 0c 00 00       	call   80104c33 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80103f9f:	eb 3a                	jmp    80103fdb <piperead+0x4e>
    if(proc->killed){
80103fa1:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80103fa7:	8b 40 24             	mov    0x24(%eax),%eax
80103faa:	85 c0                	test   %eax,%eax
80103fac:	74 15                	je     80103fc3 <piperead+0x36>
      release(&p->lock);
80103fae:	8b 45 08             	mov    0x8(%ebp),%eax
80103fb1:	89 04 24             	mov    %eax,(%esp)
80103fb4:	e8 dc 0c 00 00       	call   80104c95 <release>
      return -1;
80103fb9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103fbe:	e9 b6 00 00 00       	jmp    80104079 <piperead+0xec>
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80103fc3:	8b 45 08             	mov    0x8(%ebp),%eax
80103fc6:	8b 55 08             	mov    0x8(%ebp),%edx
80103fc9:	81 c2 34 02 00 00    	add    $0x234,%edx
80103fcf:	89 44 24 04          	mov    %eax,0x4(%esp)
80103fd3:	89 14 24             	mov    %edx,(%esp)
80103fd6:	e8 7a 09 00 00       	call   80104955 <sleep>
piperead(struct pipe *p, char *addr, int n)
{
  int i;

  acquire(&p->lock);
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80103fdb:	8b 45 08             	mov    0x8(%ebp),%eax
80103fde:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80103fe4:	8b 45 08             	mov    0x8(%ebp),%eax
80103fe7:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80103fed:	39 c2                	cmp    %eax,%edx
80103fef:	75 0d                	jne    80103ffe <piperead+0x71>
80103ff1:	8b 45 08             	mov    0x8(%ebp),%eax
80103ff4:	8b 80 40 02 00 00    	mov    0x240(%eax),%eax
80103ffa:	85 c0                	test   %eax,%eax
80103ffc:	75 a3                	jne    80103fa1 <piperead+0x14>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103ffe:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104005:	eb 49                	jmp    80104050 <piperead+0xc3>
    if(p->nread == p->nwrite)
80104007:	8b 45 08             	mov    0x8(%ebp),%eax
8010400a:	8b 90 34 02 00 00    	mov    0x234(%eax),%edx
80104010:	8b 45 08             	mov    0x8(%ebp),%eax
80104013:	8b 80 38 02 00 00    	mov    0x238(%eax),%eax
80104019:	39 c2                	cmp    %eax,%edx
8010401b:	74 3d                	je     8010405a <piperead+0xcd>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
8010401d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104020:	89 c2                	mov    %eax,%edx
80104022:	03 55 0c             	add    0xc(%ebp),%edx
80104025:	8b 45 08             	mov    0x8(%ebp),%eax
80104028:	8b 80 34 02 00 00    	mov    0x234(%eax),%eax
8010402e:	89 c3                	mov    %eax,%ebx
80104030:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
80104036:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104039:	0f b6 4c 19 34       	movzbl 0x34(%ecx,%ebx,1),%ecx
8010403e:	88 0a                	mov    %cl,(%edx)
80104040:	8d 50 01             	lea    0x1(%eax),%edx
80104043:	8b 45 08             	mov    0x8(%ebp),%eax
80104046:	89 90 34 02 00 00    	mov    %edx,0x234(%eax)
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
8010404c:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80104050:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104053:	3b 45 10             	cmp    0x10(%ebp),%eax
80104056:	7c af                	jl     80104007 <piperead+0x7a>
80104058:	eb 01                	jmp    8010405b <piperead+0xce>
    if(p->nread == p->nwrite)
      break;
8010405a:	90                   	nop
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
8010405b:	8b 45 08             	mov    0x8(%ebp),%eax
8010405e:	05 38 02 00 00       	add    $0x238,%eax
80104063:	89 04 24             	mov    %eax,(%esp)
80104066:	e8 c3 09 00 00       	call   80104a2e <wakeup>
  release(&p->lock);
8010406b:	8b 45 08             	mov    0x8(%ebp),%eax
8010406e:	89 04 24             	mov    %eax,(%esp)
80104071:	e8 1f 0c 00 00       	call   80104c95 <release>
  return i;
80104076:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80104079:	83 c4 24             	add    $0x24,%esp
8010407c:	5b                   	pop    %ebx
8010407d:	5d                   	pop    %ebp
8010407e:	c3                   	ret    
	...

80104080 <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80104080:	55                   	push   %ebp
80104081:	89 e5                	mov    %esp,%ebp
80104083:	53                   	push   %ebx
80104084:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80104087:	9c                   	pushf  
80104088:	5b                   	pop    %ebx
80104089:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  return eflags;
8010408c:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
8010408f:	83 c4 10             	add    $0x10,%esp
80104092:	5b                   	pop    %ebx
80104093:	5d                   	pop    %ebp
80104094:	c3                   	ret    

80104095 <sti>:
  asm volatile("cli");
}

static inline void
sti(void)
{
80104095:	55                   	push   %ebp
80104096:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80104098:	fb                   	sti    
}
80104099:	5d                   	pop    %ebp
8010409a:	c3                   	ret    

8010409b <pinit>:

static void wakeup1(void *chan);

void
pinit(void)
{
8010409b:	55                   	push   %ebp
8010409c:	89 e5                	mov    %esp,%ebp
8010409e:	83 ec 18             	sub    $0x18,%esp
  initlock(&ptable.lock, "ptable");
801040a1:	c7 44 24 04 f5 84 10 	movl   $0x801084f5,0x4(%esp)
801040a8:	80 
801040a9:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
801040b0:	e8 5d 0b 00 00       	call   80104c12 <initlock>
}
801040b5:	c9                   	leave  
801040b6:	c3                   	ret    

801040b7 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
801040b7:	55                   	push   %ebp
801040b8:	89 e5                	mov    %esp,%ebp
801040ba:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
801040bd:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
801040c4:	e8 6a 0b 00 00       	call   80104c33 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801040c9:	c7 45 f4 54 01 11 80 	movl   $0x80110154,-0xc(%ebp)
801040d0:	eb 0e                	jmp    801040e0 <allocproc+0x29>
    if(p->state == UNUSED)
801040d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801040d5:	8b 40 0c             	mov    0xc(%eax),%eax
801040d8:	85 c0                	test   %eax,%eax
801040da:	74 23                	je     801040ff <allocproc+0x48>
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801040dc:	83 45 f4 7c          	addl   $0x7c,-0xc(%ebp)
801040e0:	81 7d f4 54 20 11 80 	cmpl   $0x80112054,-0xc(%ebp)
801040e7:	72 e9                	jb     801040d2 <allocproc+0x1b>
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
801040e9:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
801040f0:	e8 a0 0b 00 00       	call   80104c95 <release>
  return 0;
801040f5:	b8 00 00 00 00       	mov    $0x0,%eax
801040fa:	e9 b5 00 00 00       	jmp    801041b4 <allocproc+0xfd>
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
801040ff:	90                   	nop
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
80104100:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104103:	c7 40 0c 01 00 00 00 	movl   $0x1,0xc(%eax)
  p->pid = nextpid++;
8010410a:	a1 04 b0 10 80       	mov    0x8010b004,%eax
8010410f:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104112:	89 42 10             	mov    %eax,0x10(%edx)
80104115:	83 c0 01             	add    $0x1,%eax
80104118:	a3 04 b0 10 80       	mov    %eax,0x8010b004
  release(&ptable.lock);
8010411d:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104124:	e8 6c 0b 00 00       	call   80104c95 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
80104129:	e8 61 ea ff ff       	call   80102b8f <kalloc>
8010412e:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104131:	89 42 08             	mov    %eax,0x8(%edx)
80104134:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104137:	8b 40 08             	mov    0x8(%eax),%eax
8010413a:	85 c0                	test   %eax,%eax
8010413c:	75 11                	jne    8010414f <allocproc+0x98>
    p->state = UNUSED;
8010413e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104141:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return 0;
80104148:	b8 00 00 00 00       	mov    $0x0,%eax
8010414d:	eb 65                	jmp    801041b4 <allocproc+0xfd>
  }
  sp = p->kstack + KSTACKSIZE;
8010414f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104152:	8b 40 08             	mov    0x8(%eax),%eax
80104155:	05 00 10 00 00       	add    $0x1000,%eax
8010415a:	89 45 f0             	mov    %eax,-0x10(%ebp)
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
8010415d:	83 6d f0 4c          	subl   $0x4c,-0x10(%ebp)
  p->tf = (struct trapframe*)sp;
80104161:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104164:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104167:	89 50 18             	mov    %edx,0x18(%eax)
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
8010416a:	83 6d f0 04          	subl   $0x4,-0x10(%ebp)
  *(uint*)sp = (uint)trapret;
8010416e:	ba dc 62 10 80       	mov    $0x801062dc,%edx
80104173:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104176:	89 10                	mov    %edx,(%eax)

  sp -= sizeof *p->context;
80104178:	83 6d f0 14          	subl   $0x14,-0x10(%ebp)
  p->context = (struct context*)sp;
8010417c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010417f:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104182:	89 50 1c             	mov    %edx,0x1c(%eax)
  memset(p->context, 0, sizeof *p->context);
80104185:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104188:	8b 40 1c             	mov    0x1c(%eax),%eax
8010418b:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
80104192:	00 
80104193:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010419a:	00 
8010419b:	89 04 24             	mov    %eax,(%esp)
8010419e:	e8 df 0c 00 00       	call   80104e82 <memset>
  p->context->eip = (uint)forkret;
801041a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801041a6:	8b 40 1c             	mov    0x1c(%eax),%eax
801041a9:	ba 29 49 10 80       	mov    $0x80104929,%edx
801041ae:	89 50 10             	mov    %edx,0x10(%eax)

  return p;
801041b1:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
801041b4:	c9                   	leave  
801041b5:	c3                   	ret    

801041b6 <userinit>:

//PAGEBREAK: 32
// Set up first user process.
void
userinit(void)
{
801041b6:	55                   	push   %ebp
801041b7:	89 e5                	mov    %esp,%ebp
801041b9:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
801041bc:	e8 f6 fe ff ff       	call   801040b7 <allocproc>
801041c1:	89 45 f4             	mov    %eax,-0xc(%ebp)
  initproc = p;
801041c4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801041c7:	a3 48 b6 10 80       	mov    %eax,0x8010b648
  if((p->pgdir = setupkvm(kalloc)) == 0)
801041cc:	c7 04 24 8f 2b 10 80 	movl   $0x80102b8f,(%esp)
801041d3:	e8 01 38 00 00       	call   801079d9 <setupkvm>
801041d8:	8b 55 f4             	mov    -0xc(%ebp),%edx
801041db:	89 42 04             	mov    %eax,0x4(%edx)
801041de:	8b 45 f4             	mov    -0xc(%ebp),%eax
801041e1:	8b 40 04             	mov    0x4(%eax),%eax
801041e4:	85 c0                	test   %eax,%eax
801041e6:	75 0c                	jne    801041f4 <userinit+0x3e>
    panic("userinit: out of memory?");
801041e8:	c7 04 24 fc 84 10 80 	movl   $0x801084fc,(%esp)
801041ef:	e8 49 c3 ff ff       	call   8010053d <panic>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
801041f4:	ba 2c 00 00 00       	mov    $0x2c,%edx
801041f9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801041fc:	8b 40 04             	mov    0x4(%eax),%eax
801041ff:	89 54 24 08          	mov    %edx,0x8(%esp)
80104203:	c7 44 24 04 e0 b4 10 	movl   $0x8010b4e0,0x4(%esp)
8010420a:	80 
8010420b:	89 04 24             	mov    %eax,(%esp)
8010420e:	e8 1e 3a 00 00       	call   80107c31 <inituvm>
  p->sz = PGSIZE;
80104213:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104216:	c7 00 00 10 00 00    	movl   $0x1000,(%eax)
  memset(p->tf, 0, sizeof(*p->tf));
8010421c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010421f:	8b 40 18             	mov    0x18(%eax),%eax
80104222:	c7 44 24 08 4c 00 00 	movl   $0x4c,0x8(%esp)
80104229:	00 
8010422a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104231:	00 
80104232:	89 04 24             	mov    %eax,(%esp)
80104235:	e8 48 0c 00 00       	call   80104e82 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
8010423a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010423d:	8b 40 18             	mov    0x18(%eax),%eax
80104240:	66 c7 40 3c 23 00    	movw   $0x23,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80104246:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104249:	8b 40 18             	mov    0x18(%eax),%eax
8010424c:	66 c7 40 2c 2b 00    	movw   $0x2b,0x2c(%eax)
  p->tf->es = p->tf->ds;
80104252:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104255:	8b 40 18             	mov    0x18(%eax),%eax
80104258:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010425b:	8b 52 18             	mov    0x18(%edx),%edx
8010425e:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104262:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80104266:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104269:	8b 40 18             	mov    0x18(%eax),%eax
8010426c:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010426f:	8b 52 18             	mov    0x18(%edx),%edx
80104272:	0f b7 52 2c          	movzwl 0x2c(%edx),%edx
80104276:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
8010427a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010427d:	8b 40 18             	mov    0x18(%eax),%eax
80104280:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80104287:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010428a:	8b 40 18             	mov    0x18(%eax),%eax
8010428d:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80104294:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104297:	8b 40 18             	mov    0x18(%eax),%eax
8010429a:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)

  safestrcpy(p->name, "initcode", sizeof(p->name));
801042a1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801042a4:	83 c0 6c             	add    $0x6c,%eax
801042a7:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801042ae:	00 
801042af:	c7 44 24 04 15 85 10 	movl   $0x80108515,0x4(%esp)
801042b6:	80 
801042b7:	89 04 24             	mov    %eax,(%esp)
801042ba:	e8 f3 0d 00 00       	call   801050b2 <safestrcpy>
  p->cwd = namei("/");
801042bf:	c7 04 24 1e 85 10 80 	movl   $0x8010851e,(%esp)
801042c6:	e8 cf e1 ff ff       	call   8010249a <namei>
801042cb:	8b 55 f4             	mov    -0xc(%ebp),%edx
801042ce:	89 42 68             	mov    %eax,0x68(%edx)

  p->state = RUNNABLE;
801042d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801042d4:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
}
801042db:	c9                   	leave  
801042dc:	c3                   	ret    

801042dd <growproc>:

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
801042dd:	55                   	push   %ebp
801042de:	89 e5                	mov    %esp,%ebp
801042e0:	83 ec 28             	sub    $0x28,%esp
  uint sz;
  
  sz = proc->sz;
801042e3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801042e9:	8b 00                	mov    (%eax),%eax
801042eb:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(n > 0){
801042ee:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
801042f2:	7e 34                	jle    80104328 <growproc+0x4b>
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
801042f4:	8b 45 08             	mov    0x8(%ebp),%eax
801042f7:	89 c2                	mov    %eax,%edx
801042f9:	03 55 f4             	add    -0xc(%ebp),%edx
801042fc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104302:	8b 40 04             	mov    0x4(%eax),%eax
80104305:	89 54 24 08          	mov    %edx,0x8(%esp)
80104309:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010430c:	89 54 24 04          	mov    %edx,0x4(%esp)
80104310:	89 04 24             	mov    %eax,(%esp)
80104313:	e8 93 3a 00 00       	call   80107dab <allocuvm>
80104318:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010431b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010431f:	75 41                	jne    80104362 <growproc+0x85>
      return -1;
80104321:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104326:	eb 58                	jmp    80104380 <growproc+0xa3>
  } else if(n < 0){
80104328:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
8010432c:	79 34                	jns    80104362 <growproc+0x85>
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
8010432e:	8b 45 08             	mov    0x8(%ebp),%eax
80104331:	89 c2                	mov    %eax,%edx
80104333:	03 55 f4             	add    -0xc(%ebp),%edx
80104336:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010433c:	8b 40 04             	mov    0x4(%eax),%eax
8010433f:	89 54 24 08          	mov    %edx,0x8(%esp)
80104343:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104346:	89 54 24 04          	mov    %edx,0x4(%esp)
8010434a:	89 04 24             	mov    %eax,(%esp)
8010434d:	e8 33 3b 00 00       	call   80107e85 <deallocuvm>
80104352:	89 45 f4             	mov    %eax,-0xc(%ebp)
80104355:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80104359:	75 07                	jne    80104362 <growproc+0x85>
      return -1;
8010435b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104360:	eb 1e                	jmp    80104380 <growproc+0xa3>
  }
  proc->sz = sz;
80104362:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104368:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010436b:	89 10                	mov    %edx,(%eax)
  switchuvm(proc);
8010436d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104373:	89 04 24             	mov    %eax,(%esp)
80104376:	e8 4f 37 00 00       	call   80107aca <switchuvm>
  return 0;
8010437b:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104380:	c9                   	leave  
80104381:	c3                   	ret    

80104382 <fork>:
// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
80104382:	55                   	push   %ebp
80104383:	89 e5                	mov    %esp,%ebp
80104385:	57                   	push   %edi
80104386:	56                   	push   %esi
80104387:	53                   	push   %ebx
80104388:	83 ec 2c             	sub    $0x2c,%esp
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
8010438b:	e8 27 fd ff ff       	call   801040b7 <allocproc>
80104390:	89 45 e0             	mov    %eax,-0x20(%ebp)
80104393:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80104397:	75 0a                	jne    801043a3 <fork+0x21>
    return -1;
80104399:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010439e:	e9 3a 01 00 00       	jmp    801044dd <fork+0x15b>

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
801043a3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801043a9:	8b 10                	mov    (%eax),%edx
801043ab:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801043b1:	8b 40 04             	mov    0x4(%eax),%eax
801043b4:	89 54 24 04          	mov    %edx,0x4(%esp)
801043b8:	89 04 24             	mov    %eax,(%esp)
801043bb:	e8 55 3c 00 00       	call   80108015 <copyuvm>
801043c0:	8b 55 e0             	mov    -0x20(%ebp),%edx
801043c3:	89 42 04             	mov    %eax,0x4(%edx)
801043c6:	8b 45 e0             	mov    -0x20(%ebp),%eax
801043c9:	8b 40 04             	mov    0x4(%eax),%eax
801043cc:	85 c0                	test   %eax,%eax
801043ce:	75 2c                	jne    801043fc <fork+0x7a>
    kfree(np->kstack);
801043d0:	8b 45 e0             	mov    -0x20(%ebp),%eax
801043d3:	8b 40 08             	mov    0x8(%eax),%eax
801043d6:	89 04 24             	mov    %eax,(%esp)
801043d9:	e8 18 e7 ff ff       	call   80102af6 <kfree>
    np->kstack = 0;
801043de:	8b 45 e0             	mov    -0x20(%ebp),%eax
801043e1:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
    np->state = UNUSED;
801043e8:	8b 45 e0             	mov    -0x20(%ebp),%eax
801043eb:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
    return -1;
801043f2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801043f7:	e9 e1 00 00 00       	jmp    801044dd <fork+0x15b>
  }
  np->sz = proc->sz;
801043fc:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104402:	8b 10                	mov    (%eax),%edx
80104404:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104407:	89 10                	mov    %edx,(%eax)
  np->parent = proc;
80104409:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104410:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104413:	89 50 14             	mov    %edx,0x14(%eax)
  *np->tf = *proc->tf;
80104416:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104419:	8b 50 18             	mov    0x18(%eax),%edx
8010441c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104422:	8b 40 18             	mov    0x18(%eax),%eax
80104425:	89 c3                	mov    %eax,%ebx
80104427:	b8 13 00 00 00       	mov    $0x13,%eax
8010442c:	89 d7                	mov    %edx,%edi
8010442e:	89 de                	mov    %ebx,%esi
80104430:	89 c1                	mov    %eax,%ecx
80104432:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;
80104434:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104437:	8b 40 18             	mov    0x18(%eax),%eax
8010443a:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)

  for(i = 0; i < NOFILE; i++)
80104441:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80104448:	eb 3d                	jmp    80104487 <fork+0x105>
    if(proc->ofile[i])
8010444a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104450:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104453:	83 c2 08             	add    $0x8,%edx
80104456:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
8010445a:	85 c0                	test   %eax,%eax
8010445c:	74 25                	je     80104483 <fork+0x101>
      np->ofile[i] = filedup(proc->ofile[i]);
8010445e:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104464:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80104467:	83 c2 08             	add    $0x8,%edx
8010446a:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
8010446e:	89 04 24             	mov    %eax,(%esp)
80104471:	e8 96 cb ff ff       	call   8010100c <filedup>
80104476:	8b 55 e0             	mov    -0x20(%ebp),%edx
80104479:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
8010447c:	83 c1 08             	add    $0x8,%ecx
8010447f:	89 44 8a 08          	mov    %eax,0x8(%edx,%ecx,4)
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
80104483:	83 45 e4 01          	addl   $0x1,-0x1c(%ebp)
80104487:	83 7d e4 0f          	cmpl   $0xf,-0x1c(%ebp)
8010448b:	7e bd                	jle    8010444a <fork+0xc8>
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);
8010448d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104493:	8b 40 68             	mov    0x68(%eax),%eax
80104496:	89 04 24             	mov    %eax,(%esp)
80104499:	e8 28 d4 ff ff       	call   801018c6 <idup>
8010449e:	8b 55 e0             	mov    -0x20(%ebp),%edx
801044a1:	89 42 68             	mov    %eax,0x68(%edx)
 
  pid = np->pid;
801044a4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801044a7:	8b 40 10             	mov    0x10(%eax),%eax
801044aa:	89 45 dc             	mov    %eax,-0x24(%ebp)
  np->state = RUNNABLE;
801044ad:	8b 45 e0             	mov    -0x20(%ebp),%eax
801044b0:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  safestrcpy(np->name, proc->name, sizeof(proc->name));
801044b7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801044bd:	8d 50 6c             	lea    0x6c(%eax),%edx
801044c0:	8b 45 e0             	mov    -0x20(%ebp),%eax
801044c3:	83 c0 6c             	add    $0x6c,%eax
801044c6:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
801044cd:	00 
801044ce:	89 54 24 04          	mov    %edx,0x4(%esp)
801044d2:	89 04 24             	mov    %eax,(%esp)
801044d5:	e8 d8 0b 00 00       	call   801050b2 <safestrcpy>
  return pid;
801044da:	8b 45 dc             	mov    -0x24(%ebp),%eax
}
801044dd:	83 c4 2c             	add    $0x2c,%esp
801044e0:	5b                   	pop    %ebx
801044e1:	5e                   	pop    %esi
801044e2:	5f                   	pop    %edi
801044e3:	5d                   	pop    %ebp
801044e4:	c3                   	ret    

801044e5 <exit>:
// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void
exit(void)
{
801044e5:	55                   	push   %ebp
801044e6:	89 e5                	mov    %esp,%ebp
801044e8:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int fd;

  if(proc == initproc)
801044eb:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
801044f2:	a1 48 b6 10 80       	mov    0x8010b648,%eax
801044f7:	39 c2                	cmp    %eax,%edx
801044f9:	75 0c                	jne    80104507 <exit+0x22>
    panic("init exiting");
801044fb:	c7 04 24 20 85 10 80 	movl   $0x80108520,(%esp)
80104502:	e8 36 c0 ff ff       	call   8010053d <panic>

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104507:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
8010450e:	eb 44                	jmp    80104554 <exit+0x6f>
    if(proc->ofile[fd]){
80104510:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104516:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104519:	83 c2 08             	add    $0x8,%edx
8010451c:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104520:	85 c0                	test   %eax,%eax
80104522:	74 2c                	je     80104550 <exit+0x6b>
      fileclose(proc->ofile[fd]);
80104524:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010452a:	8b 55 f0             	mov    -0x10(%ebp),%edx
8010452d:	83 c2 08             	add    $0x8,%edx
80104530:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80104534:	89 04 24             	mov    %eax,(%esp)
80104537:	e8 18 cb ff ff       	call   80101054 <fileclose>
      proc->ofile[fd] = 0;
8010453c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104542:	8b 55 f0             	mov    -0x10(%ebp),%edx
80104545:	83 c2 08             	add    $0x8,%edx
80104548:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
8010454f:	00 

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
80104550:	83 45 f0 01          	addl   $0x1,-0x10(%ebp)
80104554:	83 7d f0 0f          	cmpl   $0xf,-0x10(%ebp)
80104558:	7e b6                	jle    80104510 <exit+0x2b>
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  iput(proc->cwd);
8010455a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104560:	8b 40 68             	mov    0x68(%eax),%eax
80104563:	89 04 24             	mov    %eax,(%esp)
80104566:	e8 40 d5 ff ff       	call   80101aab <iput>
  proc->cwd = 0;
8010456b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104571:	c7 40 68 00 00 00 00 	movl   $0x0,0x68(%eax)

  acquire(&ptable.lock);
80104578:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
8010457f:	e8 af 06 00 00       	call   80104c33 <acquire>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);
80104584:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010458a:	8b 40 14             	mov    0x14(%eax),%eax
8010458d:	89 04 24             	mov    %eax,(%esp)
80104590:	e8 5b 04 00 00       	call   801049f0 <wakeup1>

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104595:	c7 45 f4 54 01 11 80 	movl   $0x80110154,-0xc(%ebp)
8010459c:	eb 38                	jmp    801045d6 <exit+0xf1>
    if(p->parent == proc){
8010459e:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045a1:	8b 50 14             	mov    0x14(%eax),%edx
801045a4:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801045aa:	39 c2                	cmp    %eax,%edx
801045ac:	75 24                	jne    801045d2 <exit+0xed>
      p->parent = initproc;
801045ae:	8b 15 48 b6 10 80    	mov    0x8010b648,%edx
801045b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045b7:	89 50 14             	mov    %edx,0x14(%eax)
      if(p->state == ZOMBIE)
801045ba:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045bd:	8b 40 0c             	mov    0xc(%eax),%eax
801045c0:	83 f8 05             	cmp    $0x5,%eax
801045c3:	75 0d                	jne    801045d2 <exit+0xed>
        wakeup1(initproc);
801045c5:	a1 48 b6 10 80       	mov    0x8010b648,%eax
801045ca:	89 04 24             	mov    %eax,(%esp)
801045cd:	e8 1e 04 00 00       	call   801049f0 <wakeup1>

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801045d2:	83 45 f4 7c          	addl   $0x7c,-0xc(%ebp)
801045d6:	81 7d f4 54 20 11 80 	cmpl   $0x80112054,-0xc(%ebp)
801045dd:	72 bf                	jb     8010459e <exit+0xb9>
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
801045df:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801045e5:	c7 40 0c 05 00 00 00 	movl   $0x5,0xc(%eax)
  sched();
801045ec:	e8 54 02 00 00       	call   80104845 <sched>
  panic("zombie exit");
801045f1:	c7 04 24 2d 85 10 80 	movl   $0x8010852d,(%esp)
801045f8:	e8 40 bf ff ff       	call   8010053d <panic>

801045fd <wait>:

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int
wait(void)
{
801045fd:	55                   	push   %ebp
801045fe:	89 e5                	mov    %esp,%ebp
80104600:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
80104603:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
8010460a:	e8 24 06 00 00       	call   80104c33 <acquire>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
8010460f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%ebp)
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104616:	c7 45 f4 54 01 11 80 	movl   $0x80110154,-0xc(%ebp)
8010461d:	e9 9a 00 00 00       	jmp    801046bc <wait+0xbf>
      if(p->parent != proc)
80104622:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104625:	8b 50 14             	mov    0x14(%eax),%edx
80104628:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010462e:	39 c2                	cmp    %eax,%edx
80104630:	0f 85 81 00 00 00    	jne    801046b7 <wait+0xba>
        continue;
      havekids = 1;
80104636:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%ebp)
      if(p->state == ZOMBIE){
8010463d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104640:	8b 40 0c             	mov    0xc(%eax),%eax
80104643:	83 f8 05             	cmp    $0x5,%eax
80104646:	75 70                	jne    801046b8 <wait+0xbb>
        // Found one.
        pid = p->pid;
80104648:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010464b:	8b 40 10             	mov    0x10(%eax),%eax
8010464e:	89 45 ec             	mov    %eax,-0x14(%ebp)
        kfree(p->kstack);
80104651:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104654:	8b 40 08             	mov    0x8(%eax),%eax
80104657:	89 04 24             	mov    %eax,(%esp)
8010465a:	e8 97 e4 ff ff       	call   80102af6 <kfree>
        p->kstack = 0;
8010465f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104662:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
        freevm(p->pgdir);
80104669:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010466c:	8b 40 04             	mov    0x4(%eax),%eax
8010466f:	89 04 24             	mov    %eax,(%esp)
80104672:	e8 ca 38 00 00       	call   80107f41 <freevm>
        p->state = UNUSED;
80104677:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010467a:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
        p->pid = 0;
80104681:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104684:	c7 40 10 00 00 00 00 	movl   $0x0,0x10(%eax)
        p->parent = 0;
8010468b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010468e:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
        p->name[0] = 0;
80104695:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104698:	c6 40 6c 00          	movb   $0x0,0x6c(%eax)
        p->killed = 0;
8010469c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010469f:	c7 40 24 00 00 00 00 	movl   $0x0,0x24(%eax)
        release(&ptable.lock);
801046a6:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
801046ad:	e8 e3 05 00 00       	call   80104c95 <release>
        return pid;
801046b2:	8b 45 ec             	mov    -0x14(%ebp),%eax
801046b5:	eb 53                	jmp    8010470a <wait+0x10d>
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
801046b7:	90                   	nop

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801046b8:	83 45 f4 7c          	addl   $0x7c,-0xc(%ebp)
801046bc:	81 7d f4 54 20 11 80 	cmpl   $0x80112054,-0xc(%ebp)
801046c3:	0f 82 59 ff ff ff    	jb     80104622 <wait+0x25>
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
801046c9:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801046cd:	74 0d                	je     801046dc <wait+0xdf>
801046cf:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801046d5:	8b 40 24             	mov    0x24(%eax),%eax
801046d8:	85 c0                	test   %eax,%eax
801046da:	74 13                	je     801046ef <wait+0xf2>
      release(&ptable.lock);
801046dc:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
801046e3:	e8 ad 05 00 00       	call   80104c95 <release>
      return -1;
801046e8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801046ed:	eb 1b                	jmp    8010470a <wait+0x10d>
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
801046ef:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801046f5:	c7 44 24 04 20 01 11 	movl   $0x80110120,0x4(%esp)
801046fc:	80 
801046fd:	89 04 24             	mov    %eax,(%esp)
80104700:	e8 50 02 00 00       	call   80104955 <sleep>
  }
80104705:	e9 05 ff ff ff       	jmp    8010460f <wait+0x12>
}
8010470a:	c9                   	leave  
8010470b:	c3                   	ret    

8010470c <register_handler>:

void
register_handler(sighandler_t sighandler)
{
8010470c:	55                   	push   %ebp
8010470d:	89 e5                	mov    %esp,%ebp
8010470f:	83 ec 28             	sub    $0x28,%esp
  char* addr = uva2ka(proc->pgdir, (char*)proc->tf->esp);
80104712:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104718:	8b 40 18             	mov    0x18(%eax),%eax
8010471b:	8b 40 44             	mov    0x44(%eax),%eax
8010471e:	89 c2                	mov    %eax,%edx
80104720:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104726:	8b 40 04             	mov    0x4(%eax),%eax
80104729:	89 54 24 04          	mov    %edx,0x4(%esp)
8010472d:	89 04 24             	mov    %eax,(%esp)
80104730:	e8 f1 39 00 00       	call   80108126 <uva2ka>
80104735:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if ((proc->tf->esp & 0xFFF) == 0)
80104738:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010473e:	8b 40 18             	mov    0x18(%eax),%eax
80104741:	8b 40 44             	mov    0x44(%eax),%eax
80104744:	25 ff 0f 00 00       	and    $0xfff,%eax
80104749:	85 c0                	test   %eax,%eax
8010474b:	75 0c                	jne    80104759 <register_handler+0x4d>
    panic("esp_offset == 0");
8010474d:	c7 04 24 39 85 10 80 	movl   $0x80108539,(%esp)
80104754:	e8 e4 bd ff ff       	call   8010053d <panic>

    /* open a new frame */
  *(int*)(addr + ((proc->tf->esp - 4) & 0xFFF))
80104759:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010475f:	8b 40 18             	mov    0x18(%eax),%eax
80104762:	8b 40 44             	mov    0x44(%eax),%eax
80104765:	83 e8 04             	sub    $0x4,%eax
80104768:	25 ff 0f 00 00       	and    $0xfff,%eax
8010476d:	03 45 f4             	add    -0xc(%ebp),%eax
          = proc->tf->eip;
80104770:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80104777:	8b 52 18             	mov    0x18(%edx),%edx
8010477a:	8b 52 38             	mov    0x38(%edx),%edx
8010477d:	89 10                	mov    %edx,(%eax)
  proc->tf->esp -= 4;
8010477f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104785:	8b 40 18             	mov    0x18(%eax),%eax
80104788:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
8010478f:	8b 52 18             	mov    0x18(%edx),%edx
80104792:	8b 52 44             	mov    0x44(%edx),%edx
80104795:	83 ea 04             	sub    $0x4,%edx
80104798:	89 50 44             	mov    %edx,0x44(%eax)

    /* update eip */
  proc->tf->eip = (uint)sighandler;
8010479b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801047a1:	8b 40 18             	mov    0x18(%eax),%eax
801047a4:	8b 55 08             	mov    0x8(%ebp),%edx
801047a7:	89 50 38             	mov    %edx,0x38(%eax)
}
801047aa:	c9                   	leave  
801047ab:	c3                   	ret    

801047ac <scheduler>:
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void
scheduler(void)
{
801047ac:	55                   	push   %ebp
801047ad:	89 e5                	mov    %esp,%ebp
801047af:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;

  for(;;){
    // Enable interrupts on this processor.
    sti();
801047b2:	e8 de f8 ff ff       	call   80104095 <sti>

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
801047b7:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
801047be:	e8 70 04 00 00       	call   80104c33 <acquire>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801047c3:	c7 45 f4 54 01 11 80 	movl   $0x80110154,-0xc(%ebp)
801047ca:	eb 5f                	jmp    8010482b <scheduler+0x7f>
      if(p->state != RUNNABLE)
801047cc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801047cf:	8b 40 0c             	mov    0xc(%eax),%eax
801047d2:	83 f8 03             	cmp    $0x3,%eax
801047d5:	75 4f                	jne    80104826 <scheduler+0x7a>
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      proc = p;
801047d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801047da:	65 a3 04 00 00 00    	mov    %eax,%gs:0x4
      switchuvm(p);
801047e0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801047e3:	89 04 24             	mov    %eax,(%esp)
801047e6:	e8 df 32 00 00       	call   80107aca <switchuvm>
      p->state = RUNNING;
801047eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801047ee:	c7 40 0c 04 00 00 00 	movl   $0x4,0xc(%eax)
      swtch(&cpu->scheduler, proc->context);
801047f5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801047fb:	8b 40 1c             	mov    0x1c(%eax),%eax
801047fe:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80104805:	83 c2 04             	add    $0x4,%edx
80104808:	89 44 24 04          	mov    %eax,0x4(%esp)
8010480c:	89 14 24             	mov    %edx,(%esp)
8010480f:	e8 14 09 00 00       	call   80105128 <swtch>
      switchkvm();
80104814:	e8 94 32 00 00       	call   80107aad <switchkvm>

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
80104819:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
80104820:	00 00 00 00 
80104824:	eb 01                	jmp    80104827 <scheduler+0x7b>

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;
80104826:	90                   	nop
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104827:	83 45 f4 7c          	addl   $0x7c,-0xc(%ebp)
8010482b:	81 7d f4 54 20 11 80 	cmpl   $0x80112054,-0xc(%ebp)
80104832:	72 98                	jb     801047cc <scheduler+0x20>

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
80104834:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
8010483b:	e8 55 04 00 00       	call   80104c95 <release>

  }
80104840:	e9 6d ff ff ff       	jmp    801047b2 <scheduler+0x6>

80104845 <sched>:

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
void
sched(void)
{
80104845:	55                   	push   %ebp
80104846:	89 e5                	mov    %esp,%ebp
80104848:	83 ec 28             	sub    $0x28,%esp
  int intena;

  if(!holding(&ptable.lock))
8010484b:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104852:	e8 fa 04 00 00       	call   80104d51 <holding>
80104857:	85 c0                	test   %eax,%eax
80104859:	75 0c                	jne    80104867 <sched+0x22>
    panic("sched ptable.lock");
8010485b:	c7 04 24 49 85 10 80 	movl   $0x80108549,(%esp)
80104862:	e8 d6 bc ff ff       	call   8010053d <panic>
  if(cpu->ncli != 1)
80104867:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010486d:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80104873:	83 f8 01             	cmp    $0x1,%eax
80104876:	74 0c                	je     80104884 <sched+0x3f>
    panic("sched locks");
80104878:	c7 04 24 5b 85 10 80 	movl   $0x8010855b,(%esp)
8010487f:	e8 b9 bc ff ff       	call   8010053d <panic>
  if(proc->state == RUNNING)
80104884:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010488a:	8b 40 0c             	mov    0xc(%eax),%eax
8010488d:	83 f8 04             	cmp    $0x4,%eax
80104890:	75 0c                	jne    8010489e <sched+0x59>
    panic("sched running");
80104892:	c7 04 24 67 85 10 80 	movl   $0x80108567,(%esp)
80104899:	e8 9f bc ff ff       	call   8010053d <panic>
  if(readeflags()&FL_IF)
8010489e:	e8 dd f7 ff ff       	call   80104080 <readeflags>
801048a3:	25 00 02 00 00       	and    $0x200,%eax
801048a8:	85 c0                	test   %eax,%eax
801048aa:	74 0c                	je     801048b8 <sched+0x73>
    panic("sched interruptible");
801048ac:	c7 04 24 75 85 10 80 	movl   $0x80108575,(%esp)
801048b3:	e8 85 bc ff ff       	call   8010053d <panic>
  intena = cpu->intena;
801048b8:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801048be:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
801048c4:	89 45 f4             	mov    %eax,-0xc(%ebp)
  swtch(&proc->context, cpu->scheduler);
801048c7:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801048cd:	8b 40 04             	mov    0x4(%eax),%eax
801048d0:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
801048d7:	83 c2 1c             	add    $0x1c,%edx
801048da:	89 44 24 04          	mov    %eax,0x4(%esp)
801048de:	89 14 24             	mov    %edx,(%esp)
801048e1:	e8 42 08 00 00       	call   80105128 <swtch>
  cpu->intena = intena;
801048e6:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801048ec:	8b 55 f4             	mov    -0xc(%ebp),%edx
801048ef:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
801048f5:	c9                   	leave  
801048f6:	c3                   	ret    

801048f7 <yield>:

// Give up the CPU for one scheduling round.
void
yield(void)
{
801048f7:	55                   	push   %ebp
801048f8:	89 e5                	mov    %esp,%ebp
801048fa:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
801048fd:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104904:	e8 2a 03 00 00       	call   80104c33 <acquire>
  proc->state = RUNNABLE;
80104909:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010490f:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  sched();
80104916:	e8 2a ff ff ff       	call   80104845 <sched>
  release(&ptable.lock);
8010491b:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104922:	e8 6e 03 00 00       	call   80104c95 <release>
}
80104927:	c9                   	leave  
80104928:	c3                   	ret    

80104929 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
80104929:	55                   	push   %ebp
8010492a:	89 e5                	mov    %esp,%ebp
8010492c:	83 ec 18             	sub    $0x18,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
8010492f:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104936:	e8 5a 03 00 00       	call   80104c95 <release>

  if (first) {
8010493b:	a1 20 b0 10 80       	mov    0x8010b020,%eax
80104940:	85 c0                	test   %eax,%eax
80104942:	74 0f                	je     80104953 <forkret+0x2a>
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
80104944:	c7 05 20 b0 10 80 00 	movl   $0x0,0x8010b020
8010494b:	00 00 00 
    initlog();
8010494e:	e8 4d e7 ff ff       	call   801030a0 <initlog>
  }
  
  // Return to "caller", actually trapret (see allocproc).
}
80104953:	c9                   	leave  
80104954:	c3                   	ret    

80104955 <sleep>:

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void
sleep(void *chan, struct spinlock *lk)
{
80104955:	55                   	push   %ebp
80104956:	89 e5                	mov    %esp,%ebp
80104958:	83 ec 18             	sub    $0x18,%esp
  if(proc == 0)
8010495b:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80104961:	85 c0                	test   %eax,%eax
80104963:	75 0c                	jne    80104971 <sleep+0x1c>
    panic("sleep");
80104965:	c7 04 24 89 85 10 80 	movl   $0x80108589,(%esp)
8010496c:	e8 cc bb ff ff       	call   8010053d <panic>

  if(lk == 0)
80104971:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80104975:	75 0c                	jne    80104983 <sleep+0x2e>
    panic("sleep without lk");
80104977:	c7 04 24 8f 85 10 80 	movl   $0x8010858f,(%esp)
8010497e:	e8 ba bb ff ff       	call   8010053d <panic>
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){  //DOC: sleeplock0
80104983:	81 7d 0c 20 01 11 80 	cmpl   $0x80110120,0xc(%ebp)
8010498a:	74 17                	je     801049a3 <sleep+0x4e>
    acquire(&ptable.lock);  //DOC: sleeplock1
8010498c:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104993:	e8 9b 02 00 00       	call   80104c33 <acquire>
    release(lk);
80104998:	8b 45 0c             	mov    0xc(%ebp),%eax
8010499b:	89 04 24             	mov    %eax,(%esp)
8010499e:	e8 f2 02 00 00       	call   80104c95 <release>
  }

  // Go to sleep.
  proc->chan = chan;
801049a3:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801049a9:	8b 55 08             	mov    0x8(%ebp),%edx
801049ac:	89 50 20             	mov    %edx,0x20(%eax)
  proc->state = SLEEPING;
801049af:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801049b5:	c7 40 0c 02 00 00 00 	movl   $0x2,0xc(%eax)
  sched();
801049bc:	e8 84 fe ff ff       	call   80104845 <sched>

  // Tidy up.
  proc->chan = 0;
801049c1:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801049c7:	c7 40 20 00 00 00 00 	movl   $0x0,0x20(%eax)

  // Reacquire original lock.
  if(lk != &ptable.lock){  //DOC: sleeplock2
801049ce:	81 7d 0c 20 01 11 80 	cmpl   $0x80110120,0xc(%ebp)
801049d5:	74 17                	je     801049ee <sleep+0x99>
    release(&ptable.lock);
801049d7:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
801049de:	e8 b2 02 00 00       	call   80104c95 <release>
    acquire(lk);
801049e3:	8b 45 0c             	mov    0xc(%ebp),%eax
801049e6:	89 04 24             	mov    %eax,(%esp)
801049e9:	e8 45 02 00 00       	call   80104c33 <acquire>
  }
}
801049ee:	c9                   	leave  
801049ef:	c3                   	ret    

801049f0 <wakeup1>:
//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
801049f0:	55                   	push   %ebp
801049f1:	89 e5                	mov    %esp,%ebp
801049f3:	83 ec 10             	sub    $0x10,%esp
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801049f6:	c7 45 fc 54 01 11 80 	movl   $0x80110154,-0x4(%ebp)
801049fd:	eb 24                	jmp    80104a23 <wakeup1+0x33>
    if(p->state == SLEEPING && p->chan == chan)
801049ff:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104a02:	8b 40 0c             	mov    0xc(%eax),%eax
80104a05:	83 f8 02             	cmp    $0x2,%eax
80104a08:	75 15                	jne    80104a1f <wakeup1+0x2f>
80104a0a:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104a0d:	8b 40 20             	mov    0x20(%eax),%eax
80104a10:	3b 45 08             	cmp    0x8(%ebp),%eax
80104a13:	75 0a                	jne    80104a1f <wakeup1+0x2f>
      p->state = RUNNABLE;
80104a15:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104a18:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104a1f:	83 45 fc 7c          	addl   $0x7c,-0x4(%ebp)
80104a23:	81 7d fc 54 20 11 80 	cmpl   $0x80112054,-0x4(%ebp)
80104a2a:	72 d3                	jb     801049ff <wakeup1+0xf>
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}
80104a2c:	c9                   	leave  
80104a2d:	c3                   	ret    

80104a2e <wakeup>:

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
80104a2e:	55                   	push   %ebp
80104a2f:	89 e5                	mov    %esp,%ebp
80104a31:	83 ec 18             	sub    $0x18,%esp
  acquire(&ptable.lock);
80104a34:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104a3b:	e8 f3 01 00 00       	call   80104c33 <acquire>
  wakeup1(chan);
80104a40:	8b 45 08             	mov    0x8(%ebp),%eax
80104a43:	89 04 24             	mov    %eax,(%esp)
80104a46:	e8 a5 ff ff ff       	call   801049f0 <wakeup1>
  release(&ptable.lock);
80104a4b:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104a52:	e8 3e 02 00 00       	call   80104c95 <release>
}
80104a57:	c9                   	leave  
80104a58:	c3                   	ret    

80104a59 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80104a59:	55                   	push   %ebp
80104a5a:	89 e5                	mov    %esp,%ebp
80104a5c:	83 ec 28             	sub    $0x28,%esp
  struct proc *p;

  acquire(&ptable.lock);
80104a5f:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104a66:	e8 c8 01 00 00       	call   80104c33 <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104a6b:	c7 45 f4 54 01 11 80 	movl   $0x80110154,-0xc(%ebp)
80104a72:	eb 41                	jmp    80104ab5 <kill+0x5c>
    if(p->pid == pid){
80104a74:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104a77:	8b 40 10             	mov    0x10(%eax),%eax
80104a7a:	3b 45 08             	cmp    0x8(%ebp),%eax
80104a7d:	75 32                	jne    80104ab1 <kill+0x58>
      p->killed = 1;
80104a7f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104a82:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
80104a89:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104a8c:	8b 40 0c             	mov    0xc(%eax),%eax
80104a8f:	83 f8 02             	cmp    $0x2,%eax
80104a92:	75 0a                	jne    80104a9e <kill+0x45>
        p->state = RUNNABLE;
80104a94:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104a97:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
80104a9e:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104aa5:	e8 eb 01 00 00       	call   80104c95 <release>
      return 0;
80104aaa:	b8 00 00 00 00       	mov    $0x0,%eax
80104aaf:	eb 1e                	jmp    80104acf <kill+0x76>
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104ab1:	83 45 f4 7c          	addl   $0x7c,-0xc(%ebp)
80104ab5:	81 7d f4 54 20 11 80 	cmpl   $0x80112054,-0xc(%ebp)
80104abc:	72 b6                	jb     80104a74 <kill+0x1b>
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
80104abe:	c7 04 24 20 01 11 80 	movl   $0x80110120,(%esp)
80104ac5:	e8 cb 01 00 00       	call   80104c95 <release>
  return -1;
80104aca:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104acf:	c9                   	leave  
80104ad0:	c3                   	ret    

80104ad1 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
80104ad1:	55                   	push   %ebp
80104ad2:	89 e5                	mov    %esp,%ebp
80104ad4:	83 ec 58             	sub    $0x58,%esp
  int i;
  struct proc *p;
  char *state;
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104ad7:	c7 45 f0 54 01 11 80 	movl   $0x80110154,-0x10(%ebp)
80104ade:	e9 d8 00 00 00       	jmp    80104bbb <procdump+0xea>
    if(p->state == UNUSED)
80104ae3:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ae6:	8b 40 0c             	mov    0xc(%eax),%eax
80104ae9:	85 c0                	test   %eax,%eax
80104aeb:	0f 84 c5 00 00 00    	je     80104bb6 <procdump+0xe5>
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
80104af1:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104af4:	8b 40 0c             	mov    0xc(%eax),%eax
80104af7:	83 f8 05             	cmp    $0x5,%eax
80104afa:	77 23                	ja     80104b1f <procdump+0x4e>
80104afc:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104aff:	8b 40 0c             	mov    0xc(%eax),%eax
80104b02:	8b 04 85 08 b0 10 80 	mov    -0x7fef4ff8(,%eax,4),%eax
80104b09:	85 c0                	test   %eax,%eax
80104b0b:	74 12                	je     80104b1f <procdump+0x4e>
      state = states[p->state];
80104b0d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b10:	8b 40 0c             	mov    0xc(%eax),%eax
80104b13:	8b 04 85 08 b0 10 80 	mov    -0x7fef4ff8(,%eax,4),%eax
80104b1a:	89 45 ec             	mov    %eax,-0x14(%ebp)
80104b1d:	eb 07                	jmp    80104b26 <procdump+0x55>
    else
      state = "???";
80104b1f:	c7 45 ec a0 85 10 80 	movl   $0x801085a0,-0x14(%ebp)
    cprintf("%d %s %s", p->pid, state, p->name);
80104b26:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b29:	8d 50 6c             	lea    0x6c(%eax),%edx
80104b2c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b2f:	8b 40 10             	mov    0x10(%eax),%eax
80104b32:	89 54 24 0c          	mov    %edx,0xc(%esp)
80104b36:	8b 55 ec             	mov    -0x14(%ebp),%edx
80104b39:	89 54 24 08          	mov    %edx,0x8(%esp)
80104b3d:	89 44 24 04          	mov    %eax,0x4(%esp)
80104b41:	c7 04 24 a4 85 10 80 	movl   $0x801085a4,(%esp)
80104b48:	e8 54 b8 ff ff       	call   801003a1 <cprintf>
    if(p->state == SLEEPING){
80104b4d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b50:	8b 40 0c             	mov    0xc(%eax),%eax
80104b53:	83 f8 02             	cmp    $0x2,%eax
80104b56:	75 50                	jne    80104ba8 <procdump+0xd7>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80104b58:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104b5b:	8b 40 1c             	mov    0x1c(%eax),%eax
80104b5e:	8b 40 0c             	mov    0xc(%eax),%eax
80104b61:	83 c0 08             	add    $0x8,%eax
80104b64:	8d 55 c4             	lea    -0x3c(%ebp),%edx
80104b67:	89 54 24 04          	mov    %edx,0x4(%esp)
80104b6b:	89 04 24             	mov    %eax,(%esp)
80104b6e:	e8 71 01 00 00       	call   80104ce4 <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
80104b73:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80104b7a:	eb 1b                	jmp    80104b97 <procdump+0xc6>
        cprintf(" %p", pc[i]);
80104b7c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104b7f:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
80104b83:	89 44 24 04          	mov    %eax,0x4(%esp)
80104b87:	c7 04 24 ad 85 10 80 	movl   $0x801085ad,(%esp)
80104b8e:	e8 0e b8 ff ff       	call   801003a1 <cprintf>
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
80104b93:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80104b97:	83 7d f4 09          	cmpl   $0x9,-0xc(%ebp)
80104b9b:	7f 0b                	jg     80104ba8 <procdump+0xd7>
80104b9d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104ba0:	8b 44 85 c4          	mov    -0x3c(%ebp,%eax,4),%eax
80104ba4:	85 c0                	test   %eax,%eax
80104ba6:	75 d4                	jne    80104b7c <procdump+0xab>
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
80104ba8:	c7 04 24 b1 85 10 80 	movl   $0x801085b1,(%esp)
80104baf:	e8 ed b7 ff ff       	call   801003a1 <cprintf>
80104bb4:	eb 01                	jmp    80104bb7 <procdump+0xe6>
  char *state;
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
80104bb6:	90                   	nop
  int i;
  struct proc *p;
  char *state;
  uint pc[10];
  
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104bb7:	83 45 f0 7c          	addl   $0x7c,-0x10(%ebp)
80104bbb:	81 7d f0 54 20 11 80 	cmpl   $0x80112054,-0x10(%ebp)
80104bc2:	0f 82 1b ff ff ff    	jb     80104ae3 <procdump+0x12>
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}
80104bc8:	c9                   	leave  
80104bc9:	c3                   	ret    
	...

80104bcc <readeflags>:
  asm volatile("ltr %0" : : "r" (sel));
}

static inline uint
readeflags(void)
{
80104bcc:	55                   	push   %ebp
80104bcd:	89 e5                	mov    %esp,%ebp
80104bcf:	53                   	push   %ebx
80104bd0:	83 ec 10             	sub    $0x10,%esp
  uint eflags;
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80104bd3:	9c                   	pushf  
80104bd4:	5b                   	pop    %ebx
80104bd5:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  return eflags;
80104bd8:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80104bdb:	83 c4 10             	add    $0x10,%esp
80104bde:	5b                   	pop    %ebx
80104bdf:	5d                   	pop    %ebp
80104be0:	c3                   	ret    

80104be1 <cli>:
  asm volatile("movw %0, %%gs" : : "r" (v));
}

static inline void
cli(void)
{
80104be1:	55                   	push   %ebp
80104be2:	89 e5                	mov    %esp,%ebp
  asm volatile("cli");
80104be4:	fa                   	cli    
}
80104be5:	5d                   	pop    %ebp
80104be6:	c3                   	ret    

80104be7 <sti>:

static inline void
sti(void)
{
80104be7:	55                   	push   %ebp
80104be8:	89 e5                	mov    %esp,%ebp
  asm volatile("sti");
80104bea:	fb                   	sti    
}
80104beb:	5d                   	pop    %ebp
80104bec:	c3                   	ret    

80104bed <xchg>:

static inline uint
xchg(volatile uint *addr, uint newval)
{
80104bed:	55                   	push   %ebp
80104bee:	89 e5                	mov    %esp,%ebp
80104bf0:	53                   	push   %ebx
80104bf1:	83 ec 10             	sub    $0x10,%esp
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
               "+m" (*addr), "=a" (result) :
80104bf4:	8b 55 08             	mov    0x8(%ebp),%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80104bf7:	8b 45 0c             	mov    0xc(%ebp),%eax
               "+m" (*addr), "=a" (result) :
80104bfa:	8b 4d 08             	mov    0x8(%ebp),%ecx
xchg(volatile uint *addr, uint newval)
{
  uint result;
  
  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80104bfd:	89 c3                	mov    %eax,%ebx
80104bff:	89 d8                	mov    %ebx,%eax
80104c01:	f0 87 02             	lock xchg %eax,(%edx)
80104c04:	89 c3                	mov    %eax,%ebx
80104c06:	89 5d f8             	mov    %ebx,-0x8(%ebp)
               "+m" (*addr), "=a" (result) :
               "1" (newval) :
               "cc");
  return result;
80104c09:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80104c0c:	83 c4 10             	add    $0x10,%esp
80104c0f:	5b                   	pop    %ebx
80104c10:	5d                   	pop    %ebp
80104c11:	c3                   	ret    

80104c12 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80104c12:	55                   	push   %ebp
80104c13:	89 e5                	mov    %esp,%ebp
  lk->name = name;
80104c15:	8b 45 08             	mov    0x8(%ebp),%eax
80104c18:	8b 55 0c             	mov    0xc(%ebp),%edx
80104c1b:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
80104c1e:	8b 45 08             	mov    0x8(%ebp),%eax
80104c21:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->cpu = 0;
80104c27:	8b 45 08             	mov    0x8(%ebp),%eax
80104c2a:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
80104c31:	5d                   	pop    %ebp
80104c32:	c3                   	ret    

80104c33 <acquire>:
// Loops (spins) until the lock is acquired.
// Holding a lock for a long time may cause
// other CPUs to waste time spinning to acquire it.
void
acquire(struct spinlock *lk)
{
80104c33:	55                   	push   %ebp
80104c34:	89 e5                	mov    %esp,%ebp
80104c36:	83 ec 18             	sub    $0x18,%esp
  pushcli(); // disable interrupts to avoid deadlock.
80104c39:	e8 3d 01 00 00       	call   80104d7b <pushcli>
  if(holding(lk))
80104c3e:	8b 45 08             	mov    0x8(%ebp),%eax
80104c41:	89 04 24             	mov    %eax,(%esp)
80104c44:	e8 08 01 00 00       	call   80104d51 <holding>
80104c49:	85 c0                	test   %eax,%eax
80104c4b:	74 0c                	je     80104c59 <acquire+0x26>
    panic("acquire");
80104c4d:	c7 04 24 dd 85 10 80 	movl   $0x801085dd,(%esp)
80104c54:	e8 e4 b8 ff ff       	call   8010053d <panic>

  // The xchg is atomic.
  // It also serializes, so that reads after acquire are not
  // reordered before it. 
  while(xchg(&lk->locked, 1) != 0)
80104c59:	90                   	nop
80104c5a:	8b 45 08             	mov    0x8(%ebp),%eax
80104c5d:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80104c64:	00 
80104c65:	89 04 24             	mov    %eax,(%esp)
80104c68:	e8 80 ff ff ff       	call   80104bed <xchg>
80104c6d:	85 c0                	test   %eax,%eax
80104c6f:	75 e9                	jne    80104c5a <acquire+0x27>
    ;

  // Record info about lock acquisition for debugging.
  lk->cpu = cpu;
80104c71:	8b 45 08             	mov    0x8(%ebp),%eax
80104c74:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80104c7b:	89 50 08             	mov    %edx,0x8(%eax)
  getcallerpcs(&lk, lk->pcs);
80104c7e:	8b 45 08             	mov    0x8(%ebp),%eax
80104c81:	83 c0 0c             	add    $0xc,%eax
80104c84:	89 44 24 04          	mov    %eax,0x4(%esp)
80104c88:	8d 45 08             	lea    0x8(%ebp),%eax
80104c8b:	89 04 24             	mov    %eax,(%esp)
80104c8e:	e8 51 00 00 00       	call   80104ce4 <getcallerpcs>
}
80104c93:	c9                   	leave  
80104c94:	c3                   	ret    

80104c95 <release>:

// Release the lock.
void
release(struct spinlock *lk)
{
80104c95:	55                   	push   %ebp
80104c96:	89 e5                	mov    %esp,%ebp
80104c98:	83 ec 18             	sub    $0x18,%esp
  if(!holding(lk))
80104c9b:	8b 45 08             	mov    0x8(%ebp),%eax
80104c9e:	89 04 24             	mov    %eax,(%esp)
80104ca1:	e8 ab 00 00 00       	call   80104d51 <holding>
80104ca6:	85 c0                	test   %eax,%eax
80104ca8:	75 0c                	jne    80104cb6 <release+0x21>
    panic("release");
80104caa:	c7 04 24 e5 85 10 80 	movl   $0x801085e5,(%esp)
80104cb1:	e8 87 b8 ff ff       	call   8010053d <panic>

  lk->pcs[0] = 0;
80104cb6:	8b 45 08             	mov    0x8(%ebp),%eax
80104cb9:	c7 40 0c 00 00 00 00 	movl   $0x0,0xc(%eax)
  lk->cpu = 0;
80104cc0:	8b 45 08             	mov    0x8(%ebp),%eax
80104cc3:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
  // But the 2007 Intel 64 Architecture Memory Ordering White
  // Paper says that Intel 64 and IA-32 will not move a load
  // after a store. So lock->locked = 0 would work here.
  // The xchg being asm volatile ensures gcc emits it after
  // the above assignments (and after the critical section).
  xchg(&lk->locked, 0);
80104cca:	8b 45 08             	mov    0x8(%ebp),%eax
80104ccd:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80104cd4:	00 
80104cd5:	89 04 24             	mov    %eax,(%esp)
80104cd8:	e8 10 ff ff ff       	call   80104bed <xchg>

  popcli();
80104cdd:	e8 e1 00 00 00       	call   80104dc3 <popcli>
}
80104ce2:	c9                   	leave  
80104ce3:	c3                   	ret    

80104ce4 <getcallerpcs>:

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80104ce4:	55                   	push   %ebp
80104ce5:	89 e5                	mov    %esp,%ebp
80104ce7:	83 ec 10             	sub    $0x10,%esp
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
80104cea:	8b 45 08             	mov    0x8(%ebp),%eax
80104ced:	83 e8 08             	sub    $0x8,%eax
80104cf0:	89 45 fc             	mov    %eax,-0x4(%ebp)
  for(i = 0; i < 10; i++){
80104cf3:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%ebp)
80104cfa:	eb 32                	jmp    80104d2e <getcallerpcs+0x4a>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104cfc:	83 7d fc 00          	cmpl   $0x0,-0x4(%ebp)
80104d00:	74 47                	je     80104d49 <getcallerpcs+0x65>
80104d02:	81 7d fc ff ff ff 7f 	cmpl   $0x7fffffff,-0x4(%ebp)
80104d09:	76 3e                	jbe    80104d49 <getcallerpcs+0x65>
80104d0b:	83 7d fc ff          	cmpl   $0xffffffff,-0x4(%ebp)
80104d0f:	74 38                	je     80104d49 <getcallerpcs+0x65>
      break;
    pcs[i] = ebp[1];     // saved %eip
80104d11:	8b 45 f8             	mov    -0x8(%ebp),%eax
80104d14:	c1 e0 02             	shl    $0x2,%eax
80104d17:	03 45 0c             	add    0xc(%ebp),%eax
80104d1a:	8b 55 fc             	mov    -0x4(%ebp),%edx
80104d1d:	8b 52 04             	mov    0x4(%edx),%edx
80104d20:	89 10                	mov    %edx,(%eax)
    ebp = (uint*)ebp[0]; // saved %ebp
80104d22:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104d25:	8b 00                	mov    (%eax),%eax
80104d27:	89 45 fc             	mov    %eax,-0x4(%ebp)
{
  uint *ebp;
  int i;
  
  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
80104d2a:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80104d2e:	83 7d f8 09          	cmpl   $0x9,-0x8(%ebp)
80104d32:	7e c8                	jle    80104cfc <getcallerpcs+0x18>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
80104d34:	eb 13                	jmp    80104d49 <getcallerpcs+0x65>
    pcs[i] = 0;
80104d36:	8b 45 f8             	mov    -0x8(%ebp),%eax
80104d39:	c1 e0 02             	shl    $0x2,%eax
80104d3c:	03 45 0c             	add    0xc(%ebp),%eax
80104d3f:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
      break;
    pcs[i] = ebp[1];     // saved %eip
    ebp = (uint*)ebp[0]; // saved %ebp
  }
  for(; i < 10; i++)
80104d45:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80104d49:	83 7d f8 09          	cmpl   $0x9,-0x8(%ebp)
80104d4d:	7e e7                	jle    80104d36 <getcallerpcs+0x52>
    pcs[i] = 0;
}
80104d4f:	c9                   	leave  
80104d50:	c3                   	ret    

80104d51 <holding>:

// Check whether this cpu is holding the lock.
int
holding(struct spinlock *lock)
{
80104d51:	55                   	push   %ebp
80104d52:	89 e5                	mov    %esp,%ebp
  return lock->locked && lock->cpu == cpu;
80104d54:	8b 45 08             	mov    0x8(%ebp),%eax
80104d57:	8b 00                	mov    (%eax),%eax
80104d59:	85 c0                	test   %eax,%eax
80104d5b:	74 17                	je     80104d74 <holding+0x23>
80104d5d:	8b 45 08             	mov    0x8(%ebp),%eax
80104d60:	8b 50 08             	mov    0x8(%eax),%edx
80104d63:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80104d69:	39 c2                	cmp    %eax,%edx
80104d6b:	75 07                	jne    80104d74 <holding+0x23>
80104d6d:	b8 01 00 00 00       	mov    $0x1,%eax
80104d72:	eb 05                	jmp    80104d79 <holding+0x28>
80104d74:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104d79:	5d                   	pop    %ebp
80104d7a:	c3                   	ret    

80104d7b <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80104d7b:	55                   	push   %ebp
80104d7c:	89 e5                	mov    %esp,%ebp
80104d7e:	83 ec 10             	sub    $0x10,%esp
  int eflags;
  
  eflags = readeflags();
80104d81:	e8 46 fe ff ff       	call   80104bcc <readeflags>
80104d86:	89 45 fc             	mov    %eax,-0x4(%ebp)
  cli();
80104d89:	e8 53 fe ff ff       	call   80104be1 <cli>
  if(cpu->ncli++ == 0)
80104d8e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80104d94:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80104d9a:	85 d2                	test   %edx,%edx
80104d9c:	0f 94 c1             	sete   %cl
80104d9f:	83 c2 01             	add    $0x1,%edx
80104da2:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80104da8:	84 c9                	test   %cl,%cl
80104daa:	74 15                	je     80104dc1 <pushcli+0x46>
    cpu->intena = eflags & FL_IF;
80104dac:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80104db2:	8b 55 fc             	mov    -0x4(%ebp),%edx
80104db5:	81 e2 00 02 00 00    	and    $0x200,%edx
80104dbb:	89 90 b0 00 00 00    	mov    %edx,0xb0(%eax)
}
80104dc1:	c9                   	leave  
80104dc2:	c3                   	ret    

80104dc3 <popcli>:

void
popcli(void)
{
80104dc3:	55                   	push   %ebp
80104dc4:	89 e5                	mov    %esp,%ebp
80104dc6:	83 ec 18             	sub    $0x18,%esp
  if(readeflags()&FL_IF)
80104dc9:	e8 fe fd ff ff       	call   80104bcc <readeflags>
80104dce:	25 00 02 00 00       	and    $0x200,%eax
80104dd3:	85 c0                	test   %eax,%eax
80104dd5:	74 0c                	je     80104de3 <popcli+0x20>
    panic("popcli - interruptible");
80104dd7:	c7 04 24 ed 85 10 80 	movl   $0x801085ed,(%esp)
80104dde:	e8 5a b7 ff ff       	call   8010053d <panic>
  if(--cpu->ncli < 0)
80104de3:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80104de9:	8b 90 ac 00 00 00    	mov    0xac(%eax),%edx
80104def:	83 ea 01             	sub    $0x1,%edx
80104df2:	89 90 ac 00 00 00    	mov    %edx,0xac(%eax)
80104df8:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80104dfe:	85 c0                	test   %eax,%eax
80104e00:	79 0c                	jns    80104e0e <popcli+0x4b>
    panic("popcli");
80104e02:	c7 04 24 04 86 10 80 	movl   $0x80108604,(%esp)
80104e09:	e8 2f b7 ff ff       	call   8010053d <panic>
  if(cpu->ncli == 0 && cpu->intena)
80104e0e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80104e14:	8b 80 ac 00 00 00    	mov    0xac(%eax),%eax
80104e1a:	85 c0                	test   %eax,%eax
80104e1c:	75 15                	jne    80104e33 <popcli+0x70>
80104e1e:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80104e24:	8b 80 b0 00 00 00    	mov    0xb0(%eax),%eax
80104e2a:	85 c0                	test   %eax,%eax
80104e2c:	74 05                	je     80104e33 <popcli+0x70>
    sti();
80104e2e:	e8 b4 fd ff ff       	call   80104be7 <sti>
}
80104e33:	c9                   	leave  
80104e34:	c3                   	ret    
80104e35:	00 00                	add    %al,(%eax)
	...

80104e38 <stosb>:
               "cc");
}

static inline void
stosb(void *addr, int data, int cnt)
{
80104e38:	55                   	push   %ebp
80104e39:	89 e5                	mov    %esp,%ebp
80104e3b:	57                   	push   %edi
80104e3c:	53                   	push   %ebx
  asm volatile("cld; rep stosb" :
80104e3d:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104e40:	8b 55 10             	mov    0x10(%ebp),%edx
80104e43:	8b 45 0c             	mov    0xc(%ebp),%eax
80104e46:	89 cb                	mov    %ecx,%ebx
80104e48:	89 df                	mov    %ebx,%edi
80104e4a:	89 d1                	mov    %edx,%ecx
80104e4c:	fc                   	cld    
80104e4d:	f3 aa                	rep stos %al,%es:(%edi)
80104e4f:	89 ca                	mov    %ecx,%edx
80104e51:	89 fb                	mov    %edi,%ebx
80104e53:	89 5d 08             	mov    %ebx,0x8(%ebp)
80104e56:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80104e59:	5b                   	pop    %ebx
80104e5a:	5f                   	pop    %edi
80104e5b:	5d                   	pop    %ebp
80104e5c:	c3                   	ret    

80104e5d <stosl>:

static inline void
stosl(void *addr, int data, int cnt)
{
80104e5d:	55                   	push   %ebp
80104e5e:	89 e5                	mov    %esp,%ebp
80104e60:	57                   	push   %edi
80104e61:	53                   	push   %ebx
  asm volatile("cld; rep stosl" :
80104e62:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104e65:	8b 55 10             	mov    0x10(%ebp),%edx
80104e68:	8b 45 0c             	mov    0xc(%ebp),%eax
80104e6b:	89 cb                	mov    %ecx,%ebx
80104e6d:	89 df                	mov    %ebx,%edi
80104e6f:	89 d1                	mov    %edx,%ecx
80104e71:	fc                   	cld    
80104e72:	f3 ab                	rep stos %eax,%es:(%edi)
80104e74:	89 ca                	mov    %ecx,%edx
80104e76:	89 fb                	mov    %edi,%ebx
80104e78:	89 5d 08             	mov    %ebx,0x8(%ebp)
80104e7b:	89 55 10             	mov    %edx,0x10(%ebp)
               "=D" (addr), "=c" (cnt) :
               "0" (addr), "1" (cnt), "a" (data) :
               "memory", "cc");
}
80104e7e:	5b                   	pop    %ebx
80104e7f:	5f                   	pop    %edi
80104e80:	5d                   	pop    %ebp
80104e81:	c3                   	ret    

80104e82 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
80104e82:	55                   	push   %ebp
80104e83:	89 e5                	mov    %esp,%ebp
80104e85:	83 ec 0c             	sub    $0xc,%esp
  if ((int)dst%4 == 0 && n%4 == 0){
80104e88:	8b 45 08             	mov    0x8(%ebp),%eax
80104e8b:	83 e0 03             	and    $0x3,%eax
80104e8e:	85 c0                	test   %eax,%eax
80104e90:	75 49                	jne    80104edb <memset+0x59>
80104e92:	8b 45 10             	mov    0x10(%ebp),%eax
80104e95:	83 e0 03             	and    $0x3,%eax
80104e98:	85 c0                	test   %eax,%eax
80104e9a:	75 3f                	jne    80104edb <memset+0x59>
    c &= 0xFF;
80104e9c:	81 65 0c ff 00 00 00 	andl   $0xff,0xc(%ebp)
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
80104ea3:	8b 45 10             	mov    0x10(%ebp),%eax
80104ea6:	c1 e8 02             	shr    $0x2,%eax
80104ea9:	89 c2                	mov    %eax,%edx
80104eab:	8b 45 0c             	mov    0xc(%ebp),%eax
80104eae:	89 c1                	mov    %eax,%ecx
80104eb0:	c1 e1 18             	shl    $0x18,%ecx
80104eb3:	8b 45 0c             	mov    0xc(%ebp),%eax
80104eb6:	c1 e0 10             	shl    $0x10,%eax
80104eb9:	09 c1                	or     %eax,%ecx
80104ebb:	8b 45 0c             	mov    0xc(%ebp),%eax
80104ebe:	c1 e0 08             	shl    $0x8,%eax
80104ec1:	09 c8                	or     %ecx,%eax
80104ec3:	0b 45 0c             	or     0xc(%ebp),%eax
80104ec6:	89 54 24 08          	mov    %edx,0x8(%esp)
80104eca:	89 44 24 04          	mov    %eax,0x4(%esp)
80104ece:	8b 45 08             	mov    0x8(%ebp),%eax
80104ed1:	89 04 24             	mov    %eax,(%esp)
80104ed4:	e8 84 ff ff ff       	call   80104e5d <stosl>
80104ed9:	eb 19                	jmp    80104ef4 <memset+0x72>
  } else
    stosb(dst, c, n);
80104edb:	8b 45 10             	mov    0x10(%ebp),%eax
80104ede:	89 44 24 08          	mov    %eax,0x8(%esp)
80104ee2:	8b 45 0c             	mov    0xc(%ebp),%eax
80104ee5:	89 44 24 04          	mov    %eax,0x4(%esp)
80104ee9:	8b 45 08             	mov    0x8(%ebp),%eax
80104eec:	89 04 24             	mov    %eax,(%esp)
80104eef:	e8 44 ff ff ff       	call   80104e38 <stosb>
  return dst;
80104ef4:	8b 45 08             	mov    0x8(%ebp),%eax
}
80104ef7:	c9                   	leave  
80104ef8:	c3                   	ret    

80104ef9 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
80104ef9:	55                   	push   %ebp
80104efa:	89 e5                	mov    %esp,%ebp
80104efc:	83 ec 10             	sub    $0x10,%esp
  const uchar *s1, *s2;
  
  s1 = v1;
80104eff:	8b 45 08             	mov    0x8(%ebp),%eax
80104f02:	89 45 fc             	mov    %eax,-0x4(%ebp)
  s2 = v2;
80104f05:	8b 45 0c             	mov    0xc(%ebp),%eax
80104f08:	89 45 f8             	mov    %eax,-0x8(%ebp)
  while(n-- > 0){
80104f0b:	eb 32                	jmp    80104f3f <memcmp+0x46>
    if(*s1 != *s2)
80104f0d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104f10:	0f b6 10             	movzbl (%eax),%edx
80104f13:	8b 45 f8             	mov    -0x8(%ebp),%eax
80104f16:	0f b6 00             	movzbl (%eax),%eax
80104f19:	38 c2                	cmp    %al,%dl
80104f1b:	74 1a                	je     80104f37 <memcmp+0x3e>
      return *s1 - *s2;
80104f1d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104f20:	0f b6 00             	movzbl (%eax),%eax
80104f23:	0f b6 d0             	movzbl %al,%edx
80104f26:	8b 45 f8             	mov    -0x8(%ebp),%eax
80104f29:	0f b6 00             	movzbl (%eax),%eax
80104f2c:	0f b6 c0             	movzbl %al,%eax
80104f2f:	89 d1                	mov    %edx,%ecx
80104f31:	29 c1                	sub    %eax,%ecx
80104f33:	89 c8                	mov    %ecx,%eax
80104f35:	eb 1c                	jmp    80104f53 <memcmp+0x5a>
    s1++, s2++;
80104f37:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80104f3b:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
{
  const uchar *s1, *s2;
  
  s1 = v1;
  s2 = v2;
  while(n-- > 0){
80104f3f:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80104f43:	0f 95 c0             	setne  %al
80104f46:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80104f4a:	84 c0                	test   %al,%al
80104f4c:	75 bf                	jne    80104f0d <memcmp+0x14>
    if(*s1 != *s2)
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
80104f4e:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104f53:	c9                   	leave  
80104f54:	c3                   	ret    

80104f55 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80104f55:	55                   	push   %ebp
80104f56:	89 e5                	mov    %esp,%ebp
80104f58:	83 ec 10             	sub    $0x10,%esp
  const char *s;
  char *d;

  s = src;
80104f5b:	8b 45 0c             	mov    0xc(%ebp),%eax
80104f5e:	89 45 fc             	mov    %eax,-0x4(%ebp)
  d = dst;
80104f61:	8b 45 08             	mov    0x8(%ebp),%eax
80104f64:	89 45 f8             	mov    %eax,-0x8(%ebp)
  if(s < d && s + n > d){
80104f67:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104f6a:	3b 45 f8             	cmp    -0x8(%ebp),%eax
80104f6d:	73 54                	jae    80104fc3 <memmove+0x6e>
80104f6f:	8b 45 10             	mov    0x10(%ebp),%eax
80104f72:	8b 55 fc             	mov    -0x4(%ebp),%edx
80104f75:	01 d0                	add    %edx,%eax
80104f77:	3b 45 f8             	cmp    -0x8(%ebp),%eax
80104f7a:	76 47                	jbe    80104fc3 <memmove+0x6e>
    s += n;
80104f7c:	8b 45 10             	mov    0x10(%ebp),%eax
80104f7f:	01 45 fc             	add    %eax,-0x4(%ebp)
    d += n;
80104f82:	8b 45 10             	mov    0x10(%ebp),%eax
80104f85:	01 45 f8             	add    %eax,-0x8(%ebp)
    while(n-- > 0)
80104f88:	eb 13                	jmp    80104f9d <memmove+0x48>
      *--d = *--s;
80104f8a:	83 6d f8 01          	subl   $0x1,-0x8(%ebp)
80104f8e:	83 6d fc 01          	subl   $0x1,-0x4(%ebp)
80104f92:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104f95:	0f b6 10             	movzbl (%eax),%edx
80104f98:	8b 45 f8             	mov    -0x8(%ebp),%eax
80104f9b:	88 10                	mov    %dl,(%eax)
  s = src;
  d = dst;
  if(s < d && s + n > d){
    s += n;
    d += n;
    while(n-- > 0)
80104f9d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80104fa1:	0f 95 c0             	setne  %al
80104fa4:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80104fa8:	84 c0                	test   %al,%al
80104faa:	75 de                	jne    80104f8a <memmove+0x35>
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80104fac:	eb 25                	jmp    80104fd3 <memmove+0x7e>
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
      *d++ = *s++;
80104fae:	8b 45 fc             	mov    -0x4(%ebp),%eax
80104fb1:	0f b6 10             	movzbl (%eax),%edx
80104fb4:	8b 45 f8             	mov    -0x8(%ebp),%eax
80104fb7:	88 10                	mov    %dl,(%eax)
80104fb9:	83 45 f8 01          	addl   $0x1,-0x8(%ebp)
80104fbd:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80104fc1:	eb 01                	jmp    80104fc4 <memmove+0x6f>
    s += n;
    d += n;
    while(n-- > 0)
      *--d = *--s;
  } else
    while(n-- > 0)
80104fc3:	90                   	nop
80104fc4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80104fc8:	0f 95 c0             	setne  %al
80104fcb:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80104fcf:	84 c0                	test   %al,%al
80104fd1:	75 db                	jne    80104fae <memmove+0x59>
      *d++ = *s++;

  return dst;
80104fd3:	8b 45 08             	mov    0x8(%ebp),%eax
}
80104fd6:	c9                   	leave  
80104fd7:	c3                   	ret    

80104fd8 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
80104fd8:	55                   	push   %ebp
80104fd9:	89 e5                	mov    %esp,%ebp
80104fdb:	83 ec 0c             	sub    $0xc,%esp
  return memmove(dst, src, n);
80104fde:	8b 45 10             	mov    0x10(%ebp),%eax
80104fe1:	89 44 24 08          	mov    %eax,0x8(%esp)
80104fe5:	8b 45 0c             	mov    0xc(%ebp),%eax
80104fe8:	89 44 24 04          	mov    %eax,0x4(%esp)
80104fec:	8b 45 08             	mov    0x8(%ebp),%eax
80104fef:	89 04 24             	mov    %eax,(%esp)
80104ff2:	e8 5e ff ff ff       	call   80104f55 <memmove>
}
80104ff7:	c9                   	leave  
80104ff8:	c3                   	ret    

80104ff9 <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
80104ff9:	55                   	push   %ebp
80104ffa:	89 e5                	mov    %esp,%ebp
  while(n > 0 && *p && *p == *q)
80104ffc:	eb 0c                	jmp    8010500a <strncmp+0x11>
    n--, p++, q++;
80104ffe:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105002:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105006:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
}

int
strncmp(const char *p, const char *q, uint n)
{
  while(n > 0 && *p && *p == *q)
8010500a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010500e:	74 1a                	je     8010502a <strncmp+0x31>
80105010:	8b 45 08             	mov    0x8(%ebp),%eax
80105013:	0f b6 00             	movzbl (%eax),%eax
80105016:	84 c0                	test   %al,%al
80105018:	74 10                	je     8010502a <strncmp+0x31>
8010501a:	8b 45 08             	mov    0x8(%ebp),%eax
8010501d:	0f b6 10             	movzbl (%eax),%edx
80105020:	8b 45 0c             	mov    0xc(%ebp),%eax
80105023:	0f b6 00             	movzbl (%eax),%eax
80105026:	38 c2                	cmp    %al,%dl
80105028:	74 d4                	je     80104ffe <strncmp+0x5>
    n--, p++, q++;
  if(n == 0)
8010502a:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
8010502e:	75 07                	jne    80105037 <strncmp+0x3e>
    return 0;
80105030:	b8 00 00 00 00       	mov    $0x0,%eax
80105035:	eb 18                	jmp    8010504f <strncmp+0x56>
  return (uchar)*p - (uchar)*q;
80105037:	8b 45 08             	mov    0x8(%ebp),%eax
8010503a:	0f b6 00             	movzbl (%eax),%eax
8010503d:	0f b6 d0             	movzbl %al,%edx
80105040:	8b 45 0c             	mov    0xc(%ebp),%eax
80105043:	0f b6 00             	movzbl (%eax),%eax
80105046:	0f b6 c0             	movzbl %al,%eax
80105049:	89 d1                	mov    %edx,%ecx
8010504b:	29 c1                	sub    %eax,%ecx
8010504d:	89 c8                	mov    %ecx,%eax
}
8010504f:	5d                   	pop    %ebp
80105050:	c3                   	ret    

80105051 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
80105051:	55                   	push   %ebp
80105052:	89 e5                	mov    %esp,%ebp
80105054:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
80105057:	8b 45 08             	mov    0x8(%ebp),%eax
8010505a:	89 45 fc             	mov    %eax,-0x4(%ebp)
  while(n-- > 0 && (*s++ = *t++) != 0)
8010505d:	90                   	nop
8010505e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80105062:	0f 9f c0             	setg   %al
80105065:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
80105069:	84 c0                	test   %al,%al
8010506b:	74 30                	je     8010509d <strncpy+0x4c>
8010506d:	8b 45 0c             	mov    0xc(%ebp),%eax
80105070:	0f b6 10             	movzbl (%eax),%edx
80105073:	8b 45 08             	mov    0x8(%ebp),%eax
80105076:	88 10                	mov    %dl,(%eax)
80105078:	8b 45 08             	mov    0x8(%ebp),%eax
8010507b:	0f b6 00             	movzbl (%eax),%eax
8010507e:	84 c0                	test   %al,%al
80105080:	0f 95 c0             	setne  %al
80105083:	83 45 08 01          	addl   $0x1,0x8(%ebp)
80105087:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
8010508b:	84 c0                	test   %al,%al
8010508d:	75 cf                	jne    8010505e <strncpy+0xd>
    ;
  while(n-- > 0)
8010508f:	eb 0c                	jmp    8010509d <strncpy+0x4c>
    *s++ = 0;
80105091:	8b 45 08             	mov    0x8(%ebp),%eax
80105094:	c6 00 00             	movb   $0x0,(%eax)
80105097:	83 45 08 01          	addl   $0x1,0x8(%ebp)
8010509b:	eb 01                	jmp    8010509e <strncpy+0x4d>
  char *os;
  
  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
    ;
  while(n-- > 0)
8010509d:	90                   	nop
8010509e:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801050a2:	0f 9f c0             	setg   %al
801050a5:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
801050a9:	84 c0                	test   %al,%al
801050ab:	75 e4                	jne    80105091 <strncpy+0x40>
    *s++ = 0;
  return os;
801050ad:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801050b0:	c9                   	leave  
801050b1:	c3                   	ret    

801050b2 <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
801050b2:	55                   	push   %ebp
801050b3:	89 e5                	mov    %esp,%ebp
801050b5:	83 ec 10             	sub    $0x10,%esp
  char *os;
  
  os = s;
801050b8:	8b 45 08             	mov    0x8(%ebp),%eax
801050bb:	89 45 fc             	mov    %eax,-0x4(%ebp)
  if(n <= 0)
801050be:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801050c2:	7f 05                	jg     801050c9 <safestrcpy+0x17>
    return os;
801050c4:	8b 45 fc             	mov    -0x4(%ebp),%eax
801050c7:	eb 35                	jmp    801050fe <safestrcpy+0x4c>
  while(--n > 0 && (*s++ = *t++) != 0)
801050c9:	83 6d 10 01          	subl   $0x1,0x10(%ebp)
801050cd:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801050d1:	7e 22                	jle    801050f5 <safestrcpy+0x43>
801050d3:	8b 45 0c             	mov    0xc(%ebp),%eax
801050d6:	0f b6 10             	movzbl (%eax),%edx
801050d9:	8b 45 08             	mov    0x8(%ebp),%eax
801050dc:	88 10                	mov    %dl,(%eax)
801050de:	8b 45 08             	mov    0x8(%ebp),%eax
801050e1:	0f b6 00             	movzbl (%eax),%eax
801050e4:	84 c0                	test   %al,%al
801050e6:	0f 95 c0             	setne  %al
801050e9:	83 45 08 01          	addl   $0x1,0x8(%ebp)
801050ed:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
801050f1:	84 c0                	test   %al,%al
801050f3:	75 d4                	jne    801050c9 <safestrcpy+0x17>
    ;
  *s = 0;
801050f5:	8b 45 08             	mov    0x8(%ebp),%eax
801050f8:	c6 00 00             	movb   $0x0,(%eax)
  return os;
801050fb:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
801050fe:	c9                   	leave  
801050ff:	c3                   	ret    

80105100 <strlen>:

int
strlen(const char *s)
{
80105100:	55                   	push   %ebp
80105101:	89 e5                	mov    %esp,%ebp
80105103:	83 ec 10             	sub    $0x10,%esp
  int n;

  for(n = 0; s[n]; n++)
80105106:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
8010510d:	eb 04                	jmp    80105113 <strlen+0x13>
8010510f:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105113:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105116:	03 45 08             	add    0x8(%ebp),%eax
80105119:	0f b6 00             	movzbl (%eax),%eax
8010511c:	84 c0                	test   %al,%al
8010511e:	75 ef                	jne    8010510f <strlen+0xf>
    ;
  return n;
80105120:	8b 45 fc             	mov    -0x4(%ebp),%eax
}
80105123:	c9                   	leave  
80105124:	c3                   	ret    
80105125:	00 00                	add    %al,(%eax)
	...

80105128 <swtch>:
# Save current register context in old
# and then load register context from new.

.globl swtch
swtch:
  movl 4(%esp), %eax
80105128:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
8010512c:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-save registers
  pushl %ebp
80105130:	55                   	push   %ebp
  pushl %ebx
80105131:	53                   	push   %ebx
  pushl %esi
80105132:	56                   	push   %esi
  pushl %edi
80105133:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
80105134:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
80105136:	89 d4                	mov    %edx,%esp

  # Load new callee-save registers
  popl %edi
80105138:	5f                   	pop    %edi
  popl %esi
80105139:	5e                   	pop    %esi
  popl %ebx
8010513a:	5b                   	pop    %ebx
  popl %ebp
8010513b:	5d                   	pop    %ebp
  ret
8010513c:	c3                   	ret    
8010513d:	00 00                	add    %al,(%eax)
	...

80105140 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from process p.
int
fetchint(struct proc *p, uint addr, int *ip)
{
80105140:	55                   	push   %ebp
80105141:	89 e5                	mov    %esp,%ebp
  if(addr >= p->sz || addr+4 > p->sz)
80105143:	8b 45 08             	mov    0x8(%ebp),%eax
80105146:	8b 00                	mov    (%eax),%eax
80105148:	3b 45 0c             	cmp    0xc(%ebp),%eax
8010514b:	76 0f                	jbe    8010515c <fetchint+0x1c>
8010514d:	8b 45 0c             	mov    0xc(%ebp),%eax
80105150:	8d 50 04             	lea    0x4(%eax),%edx
80105153:	8b 45 08             	mov    0x8(%ebp),%eax
80105156:	8b 00                	mov    (%eax),%eax
80105158:	39 c2                	cmp    %eax,%edx
8010515a:	76 07                	jbe    80105163 <fetchint+0x23>
    return -1;
8010515c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105161:	eb 0f                	jmp    80105172 <fetchint+0x32>
  *ip = *(int*)(addr);
80105163:	8b 45 0c             	mov    0xc(%ebp),%eax
80105166:	8b 10                	mov    (%eax),%edx
80105168:	8b 45 10             	mov    0x10(%ebp),%eax
8010516b:	89 10                	mov    %edx,(%eax)
  return 0;
8010516d:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105172:	5d                   	pop    %ebp
80105173:	c3                   	ret    

80105174 <fetchstr>:
// Fetch the nul-terminated string at addr from process p.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(struct proc *p, uint addr, char **pp)
{
80105174:	55                   	push   %ebp
80105175:	89 e5                	mov    %esp,%ebp
80105177:	83 ec 10             	sub    $0x10,%esp
  char *s, *ep;

  if(addr >= p->sz)
8010517a:	8b 45 08             	mov    0x8(%ebp),%eax
8010517d:	8b 00                	mov    (%eax),%eax
8010517f:	3b 45 0c             	cmp    0xc(%ebp),%eax
80105182:	77 07                	ja     8010518b <fetchstr+0x17>
    return -1;
80105184:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105189:	eb 45                	jmp    801051d0 <fetchstr+0x5c>
  *pp = (char*)addr;
8010518b:	8b 55 0c             	mov    0xc(%ebp),%edx
8010518e:	8b 45 10             	mov    0x10(%ebp),%eax
80105191:	89 10                	mov    %edx,(%eax)
  ep = (char*)p->sz;
80105193:	8b 45 08             	mov    0x8(%ebp),%eax
80105196:	8b 00                	mov    (%eax),%eax
80105198:	89 45 f8             	mov    %eax,-0x8(%ebp)
  for(s = *pp; s < ep; s++)
8010519b:	8b 45 10             	mov    0x10(%ebp),%eax
8010519e:	8b 00                	mov    (%eax),%eax
801051a0:	89 45 fc             	mov    %eax,-0x4(%ebp)
801051a3:	eb 1e                	jmp    801051c3 <fetchstr+0x4f>
    if(*s == 0)
801051a5:	8b 45 fc             	mov    -0x4(%ebp),%eax
801051a8:	0f b6 00             	movzbl (%eax),%eax
801051ab:	84 c0                	test   %al,%al
801051ad:	75 10                	jne    801051bf <fetchstr+0x4b>
      return s - *pp;
801051af:	8b 55 fc             	mov    -0x4(%ebp),%edx
801051b2:	8b 45 10             	mov    0x10(%ebp),%eax
801051b5:	8b 00                	mov    (%eax),%eax
801051b7:	89 d1                	mov    %edx,%ecx
801051b9:	29 c1                	sub    %eax,%ecx
801051bb:	89 c8                	mov    %ecx,%eax
801051bd:	eb 11                	jmp    801051d0 <fetchstr+0x5c>

  if(addr >= p->sz)
    return -1;
  *pp = (char*)addr;
  ep = (char*)p->sz;
  for(s = *pp; s < ep; s++)
801051bf:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
801051c3:	8b 45 fc             	mov    -0x4(%ebp),%eax
801051c6:	3b 45 f8             	cmp    -0x8(%ebp),%eax
801051c9:	72 da                	jb     801051a5 <fetchstr+0x31>
    if(*s == 0)
      return s - *pp;
  return -1;
801051cb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801051d0:	c9                   	leave  
801051d1:	c3                   	ret    

801051d2 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
801051d2:	55                   	push   %ebp
801051d3:	89 e5                	mov    %esp,%ebp
801051d5:	83 ec 0c             	sub    $0xc,%esp
  return fetchint(proc, proc->tf->esp + 4 + 4*n, ip);
801051d8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801051de:	8b 40 18             	mov    0x18(%eax),%eax
801051e1:	8b 50 44             	mov    0x44(%eax),%edx
801051e4:	8b 45 08             	mov    0x8(%ebp),%eax
801051e7:	c1 e0 02             	shl    $0x2,%eax
801051ea:	01 d0                	add    %edx,%eax
801051ec:	8d 48 04             	lea    0x4(%eax),%ecx
801051ef:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801051f5:	8b 55 0c             	mov    0xc(%ebp),%edx
801051f8:	89 54 24 08          	mov    %edx,0x8(%esp)
801051fc:	89 4c 24 04          	mov    %ecx,0x4(%esp)
80105200:	89 04 24             	mov    %eax,(%esp)
80105203:	e8 38 ff ff ff       	call   80105140 <fetchint>
}
80105208:	c9                   	leave  
80105209:	c3                   	ret    

8010520a <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size n bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
8010520a:	55                   	push   %ebp
8010520b:	89 e5                	mov    %esp,%ebp
8010520d:	83 ec 18             	sub    $0x18,%esp
  int i;
  
  if(argint(n, &i) < 0)
80105210:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105213:	89 44 24 04          	mov    %eax,0x4(%esp)
80105217:	8b 45 08             	mov    0x8(%ebp),%eax
8010521a:	89 04 24             	mov    %eax,(%esp)
8010521d:	e8 b0 ff ff ff       	call   801051d2 <argint>
80105222:	85 c0                	test   %eax,%eax
80105224:	79 07                	jns    8010522d <argptr+0x23>
    return -1;
80105226:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010522b:	eb 3d                	jmp    8010526a <argptr+0x60>
  if((uint)i >= proc->sz || (uint)i+size > proc->sz)
8010522d:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105230:	89 c2                	mov    %eax,%edx
80105232:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105238:	8b 00                	mov    (%eax),%eax
8010523a:	39 c2                	cmp    %eax,%edx
8010523c:	73 16                	jae    80105254 <argptr+0x4a>
8010523e:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105241:	89 c2                	mov    %eax,%edx
80105243:	8b 45 10             	mov    0x10(%ebp),%eax
80105246:	01 c2                	add    %eax,%edx
80105248:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010524e:	8b 00                	mov    (%eax),%eax
80105250:	39 c2                	cmp    %eax,%edx
80105252:	76 07                	jbe    8010525b <argptr+0x51>
    return -1;
80105254:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105259:	eb 0f                	jmp    8010526a <argptr+0x60>
  *pp = (char*)i;
8010525b:	8b 45 fc             	mov    -0x4(%ebp),%eax
8010525e:	89 c2                	mov    %eax,%edx
80105260:	8b 45 0c             	mov    0xc(%ebp),%eax
80105263:	89 10                	mov    %edx,(%eax)
  return 0;
80105265:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010526a:	c9                   	leave  
8010526b:	c3                   	ret    

8010526c <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
8010526c:	55                   	push   %ebp
8010526d:	89 e5                	mov    %esp,%ebp
8010526f:	83 ec 1c             	sub    $0x1c,%esp
  int addr;
  if(argint(n, &addr) < 0)
80105272:	8d 45 fc             	lea    -0x4(%ebp),%eax
80105275:	89 44 24 04          	mov    %eax,0x4(%esp)
80105279:	8b 45 08             	mov    0x8(%ebp),%eax
8010527c:	89 04 24             	mov    %eax,(%esp)
8010527f:	e8 4e ff ff ff       	call   801051d2 <argint>
80105284:	85 c0                	test   %eax,%eax
80105286:	79 07                	jns    8010528f <argstr+0x23>
    return -1;
80105288:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010528d:	eb 1e                	jmp    801052ad <argstr+0x41>
  return fetchstr(proc, addr, pp);
8010528f:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105292:	89 c2                	mov    %eax,%edx
80105294:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010529a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010529d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
801052a1:	89 54 24 04          	mov    %edx,0x4(%esp)
801052a5:	89 04 24             	mov    %eax,(%esp)
801052a8:	e8 c7 fe ff ff       	call   80105174 <fetchstr>
}
801052ad:	c9                   	leave  
801052ae:	c3                   	ret    

801052af <syscall>:
[SYS_close]   sys_close,
};

void
syscall(void)
{
801052af:	55                   	push   %ebp
801052b0:	89 e5                	mov    %esp,%ebp
801052b2:	53                   	push   %ebx
801052b3:	83 ec 24             	sub    $0x24,%esp
  int num;

  num = proc->tf->eax;
801052b6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801052bc:	8b 40 18             	mov    0x18(%eax),%eax
801052bf:	8b 40 1c             	mov    0x1c(%eax),%eax
801052c2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(num >= 0 && num < SYS_open && syscalls[num]) {
801052c5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801052c9:	78 2e                	js     801052f9 <syscall+0x4a>
801052cb:	83 7d f4 0e          	cmpl   $0xe,-0xc(%ebp)
801052cf:	7f 28                	jg     801052f9 <syscall+0x4a>
801052d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801052d4:	8b 04 85 40 b0 10 80 	mov    -0x7fef4fc0(,%eax,4),%eax
801052db:	85 c0                	test   %eax,%eax
801052dd:	74 1a                	je     801052f9 <syscall+0x4a>
    proc->tf->eax = syscalls[num]();
801052df:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801052e5:	8b 58 18             	mov    0x18(%eax),%ebx
801052e8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801052eb:	8b 04 85 40 b0 10 80 	mov    -0x7fef4fc0(,%eax,4),%eax
801052f2:	ff d0                	call   *%eax
801052f4:	89 43 1c             	mov    %eax,0x1c(%ebx)
801052f7:	eb 73                	jmp    8010536c <syscall+0xbd>
  } else if (num >= SYS_open && num < NELEM(syscalls) && syscalls[num]) {
801052f9:	83 7d f4 0e          	cmpl   $0xe,-0xc(%ebp)
801052fd:	7e 30                	jle    8010532f <syscall+0x80>
801052ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105302:	83 f8 15             	cmp    $0x15,%eax
80105305:	77 28                	ja     8010532f <syscall+0x80>
80105307:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010530a:	8b 04 85 40 b0 10 80 	mov    -0x7fef4fc0(,%eax,4),%eax
80105311:	85 c0                	test   %eax,%eax
80105313:	74 1a                	je     8010532f <syscall+0x80>
    proc->tf->eax = syscalls[num]();
80105315:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010531b:	8b 58 18             	mov    0x18(%eax),%ebx
8010531e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105321:	8b 04 85 40 b0 10 80 	mov    -0x7fef4fc0(,%eax,4),%eax
80105328:	ff d0                	call   *%eax
8010532a:	89 43 1c             	mov    %eax,0x1c(%ebx)
8010532d:	eb 3d                	jmp    8010536c <syscall+0xbd>
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            proc->pid, proc->name, num);
8010532f:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105335:	8d 48 6c             	lea    0x6c(%eax),%ecx
80105338:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
  if(num >= 0 && num < SYS_open && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
  } else if (num >= SYS_open && num < NELEM(syscalls) && syscalls[num]) {
    proc->tf->eax = syscalls[num]();
  } else {
    cprintf("%d %s: unknown sys call %d\n",
8010533e:	8b 40 10             	mov    0x10(%eax),%eax
80105341:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105344:	89 54 24 0c          	mov    %edx,0xc(%esp)
80105348:	89 4c 24 08          	mov    %ecx,0x8(%esp)
8010534c:	89 44 24 04          	mov    %eax,0x4(%esp)
80105350:	c7 04 24 0b 86 10 80 	movl   $0x8010860b,(%esp)
80105357:	e8 45 b0 ff ff       	call   801003a1 <cprintf>
            proc->pid, proc->name, num);
    proc->tf->eax = -1;
8010535c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105362:	8b 40 18             	mov    0x18(%eax),%eax
80105365:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
  }
}
8010536c:	83 c4 24             	add    $0x24,%esp
8010536f:	5b                   	pop    %ebx
80105370:	5d                   	pop    %ebp
80105371:	c3                   	ret    
	...

80105374 <argfd>:

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
static int
argfd(int n, int *pfd, struct file **pf)
{
80105374:	55                   	push   %ebp
80105375:	89 e5                	mov    %esp,%ebp
80105377:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
8010537a:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010537d:	89 44 24 04          	mov    %eax,0x4(%esp)
80105381:	8b 45 08             	mov    0x8(%ebp),%eax
80105384:	89 04 24             	mov    %eax,(%esp)
80105387:	e8 46 fe ff ff       	call   801051d2 <argint>
8010538c:	85 c0                	test   %eax,%eax
8010538e:	79 07                	jns    80105397 <argfd+0x23>
    return -1;
80105390:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105395:	eb 50                	jmp    801053e7 <argfd+0x73>
  if(fd < 0 || fd >= NOFILE || (f=proc->ofile[fd]) == 0)
80105397:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010539a:	85 c0                	test   %eax,%eax
8010539c:	78 21                	js     801053bf <argfd+0x4b>
8010539e:	8b 45 f0             	mov    -0x10(%ebp),%eax
801053a1:	83 f8 0f             	cmp    $0xf,%eax
801053a4:	7f 19                	jg     801053bf <argfd+0x4b>
801053a6:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801053ac:	8b 55 f0             	mov    -0x10(%ebp),%edx
801053af:	83 c2 08             	add    $0x8,%edx
801053b2:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
801053b6:	89 45 f4             	mov    %eax,-0xc(%ebp)
801053b9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801053bd:	75 07                	jne    801053c6 <argfd+0x52>
    return -1;
801053bf:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801053c4:	eb 21                	jmp    801053e7 <argfd+0x73>
  if(pfd)
801053c6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
801053ca:	74 08                	je     801053d4 <argfd+0x60>
    *pfd = fd;
801053cc:	8b 55 f0             	mov    -0x10(%ebp),%edx
801053cf:	8b 45 0c             	mov    0xc(%ebp),%eax
801053d2:	89 10                	mov    %edx,(%eax)
  if(pf)
801053d4:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801053d8:	74 08                	je     801053e2 <argfd+0x6e>
    *pf = f;
801053da:	8b 45 10             	mov    0x10(%ebp),%eax
801053dd:	8b 55 f4             	mov    -0xc(%ebp),%edx
801053e0:	89 10                	mov    %edx,(%eax)
  return 0;
801053e2:	b8 00 00 00 00       	mov    $0x0,%eax
}
801053e7:	c9                   	leave  
801053e8:	c3                   	ret    

801053e9 <fdalloc>:

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
801053e9:	55                   	push   %ebp
801053ea:	89 e5                	mov    %esp,%ebp
801053ec:	83 ec 10             	sub    $0x10,%esp
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
801053ef:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%ebp)
801053f6:	eb 30                	jmp    80105428 <fdalloc+0x3f>
    if(proc->ofile[fd] == 0){
801053f8:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801053fe:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105401:	83 c2 08             	add    $0x8,%edx
80105404:	8b 44 90 08          	mov    0x8(%eax,%edx,4),%eax
80105408:	85 c0                	test   %eax,%eax
8010540a:	75 18                	jne    80105424 <fdalloc+0x3b>
      proc->ofile[fd] = f;
8010540c:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105412:	8b 55 fc             	mov    -0x4(%ebp),%edx
80105415:	8d 4a 08             	lea    0x8(%edx),%ecx
80105418:	8b 55 08             	mov    0x8(%ebp),%edx
8010541b:	89 54 88 08          	mov    %edx,0x8(%eax,%ecx,4)
      return fd;
8010541f:	8b 45 fc             	mov    -0x4(%ebp),%eax
80105422:	eb 0f                	jmp    80105433 <fdalloc+0x4a>
static int
fdalloc(struct file *f)
{
  int fd;

  for(fd = 0; fd < NOFILE; fd++){
80105424:	83 45 fc 01          	addl   $0x1,-0x4(%ebp)
80105428:	83 7d fc 0f          	cmpl   $0xf,-0x4(%ebp)
8010542c:	7e ca                	jle    801053f8 <fdalloc+0xf>
    if(proc->ofile[fd] == 0){
      proc->ofile[fd] = f;
      return fd;
    }
  }
  return -1;
8010542e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105433:	c9                   	leave  
80105434:	c3                   	ret    

80105435 <sys_dup>:

int
sys_dup(void)
{
80105435:	55                   	push   %ebp
80105436:	89 e5                	mov    %esp,%ebp
80105438:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int fd;
  
  if(argfd(0, 0, &f) < 0)
8010543b:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010543e:	89 44 24 08          	mov    %eax,0x8(%esp)
80105442:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105449:	00 
8010544a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105451:	e8 1e ff ff ff       	call   80105374 <argfd>
80105456:	85 c0                	test   %eax,%eax
80105458:	79 07                	jns    80105461 <sys_dup+0x2c>
    return -1;
8010545a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010545f:	eb 29                	jmp    8010548a <sys_dup+0x55>
  if((fd=fdalloc(f)) < 0)
80105461:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105464:	89 04 24             	mov    %eax,(%esp)
80105467:	e8 7d ff ff ff       	call   801053e9 <fdalloc>
8010546c:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010546f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105473:	79 07                	jns    8010547c <sys_dup+0x47>
    return -1;
80105475:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010547a:	eb 0e                	jmp    8010548a <sys_dup+0x55>
  filedup(f);
8010547c:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010547f:	89 04 24             	mov    %eax,(%esp)
80105482:	e8 85 bb ff ff       	call   8010100c <filedup>
  return fd;
80105487:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
8010548a:	c9                   	leave  
8010548b:	c3                   	ret    

8010548c <sys_read>:

int
sys_read(void)
{
8010548c:	55                   	push   %ebp
8010548d:	89 e5                	mov    %esp,%ebp
8010548f:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80105492:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105495:	89 44 24 08          	mov    %eax,0x8(%esp)
80105499:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801054a0:	00 
801054a1:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801054a8:	e8 c7 fe ff ff       	call   80105374 <argfd>
801054ad:	85 c0                	test   %eax,%eax
801054af:	78 35                	js     801054e6 <sys_read+0x5a>
801054b1:	8d 45 f0             	lea    -0x10(%ebp),%eax
801054b4:	89 44 24 04          	mov    %eax,0x4(%esp)
801054b8:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
801054bf:	e8 0e fd ff ff       	call   801051d2 <argint>
801054c4:	85 c0                	test   %eax,%eax
801054c6:	78 1e                	js     801054e6 <sys_read+0x5a>
801054c8:	8b 45 f0             	mov    -0x10(%ebp),%eax
801054cb:	89 44 24 08          	mov    %eax,0x8(%esp)
801054cf:	8d 45 ec             	lea    -0x14(%ebp),%eax
801054d2:	89 44 24 04          	mov    %eax,0x4(%esp)
801054d6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801054dd:	e8 28 fd ff ff       	call   8010520a <argptr>
801054e2:	85 c0                	test   %eax,%eax
801054e4:	79 07                	jns    801054ed <sys_read+0x61>
    return -1;
801054e6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801054eb:	eb 19                	jmp    80105506 <sys_read+0x7a>
  return fileread(f, p, n);
801054ed:	8b 4d f0             	mov    -0x10(%ebp),%ecx
801054f0:	8b 55 ec             	mov    -0x14(%ebp),%edx
801054f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801054f6:	89 4c 24 08          	mov    %ecx,0x8(%esp)
801054fa:	89 54 24 04          	mov    %edx,0x4(%esp)
801054fe:	89 04 24             	mov    %eax,(%esp)
80105501:	e8 73 bc ff ff       	call   80101179 <fileread>
}
80105506:	c9                   	leave  
80105507:	c3                   	ret    

80105508 <sys_write>:

int
sys_write(void)
{
80105508:	55                   	push   %ebp
80105509:	89 e5                	mov    %esp,%ebp
8010550b:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  int n;
  char *p;

  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
8010550e:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105511:	89 44 24 08          	mov    %eax,0x8(%esp)
80105515:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010551c:	00 
8010551d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105524:	e8 4b fe ff ff       	call   80105374 <argfd>
80105529:	85 c0                	test   %eax,%eax
8010552b:	78 35                	js     80105562 <sys_write+0x5a>
8010552d:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105530:	89 44 24 04          	mov    %eax,0x4(%esp)
80105534:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
8010553b:	e8 92 fc ff ff       	call   801051d2 <argint>
80105540:	85 c0                	test   %eax,%eax
80105542:	78 1e                	js     80105562 <sys_write+0x5a>
80105544:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105547:	89 44 24 08          	mov    %eax,0x8(%esp)
8010554b:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010554e:	89 44 24 04          	mov    %eax,0x4(%esp)
80105552:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105559:	e8 ac fc ff ff       	call   8010520a <argptr>
8010555e:	85 c0                	test   %eax,%eax
80105560:	79 07                	jns    80105569 <sys_write+0x61>
    return -1;
80105562:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105567:	eb 19                	jmp    80105582 <sys_write+0x7a>
  return filewrite(f, p, n);
80105569:	8b 4d f0             	mov    -0x10(%ebp),%ecx
8010556c:	8b 55 ec             	mov    -0x14(%ebp),%edx
8010556f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105572:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105576:	89 54 24 04          	mov    %edx,0x4(%esp)
8010557a:	89 04 24             	mov    %eax,(%esp)
8010557d:	e8 b3 bc ff ff       	call   80101235 <filewrite>
}
80105582:	c9                   	leave  
80105583:	c3                   	ret    

80105584 <sys_close>:

int
sys_close(void)
{
80105584:	55                   	push   %ebp
80105585:	89 e5                	mov    %esp,%ebp
80105587:	83 ec 28             	sub    $0x28,%esp
  int fd;
  struct file *f;
  
  if(argfd(0, &fd, &f) < 0)
8010558a:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010558d:	89 44 24 08          	mov    %eax,0x8(%esp)
80105591:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105594:	89 44 24 04          	mov    %eax,0x4(%esp)
80105598:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
8010559f:	e8 d0 fd ff ff       	call   80105374 <argfd>
801055a4:	85 c0                	test   %eax,%eax
801055a6:	79 07                	jns    801055af <sys_close+0x2b>
    return -1;
801055a8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801055ad:	eb 24                	jmp    801055d3 <sys_close+0x4f>
  proc->ofile[fd] = 0;
801055af:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801055b5:	8b 55 f4             	mov    -0xc(%ebp),%edx
801055b8:	83 c2 08             	add    $0x8,%edx
801055bb:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
801055c2:	00 
  fileclose(f);
801055c3:	8b 45 f0             	mov    -0x10(%ebp),%eax
801055c6:	89 04 24             	mov    %eax,(%esp)
801055c9:	e8 86 ba ff ff       	call   80101054 <fileclose>
  return 0;
801055ce:	b8 00 00 00 00       	mov    $0x0,%eax
}
801055d3:	c9                   	leave  
801055d4:	c3                   	ret    

801055d5 <sys_fstat>:

int
sys_fstat(void)
{
801055d5:	55                   	push   %ebp
801055d6:	89 e5                	mov    %esp,%ebp
801055d8:	83 ec 28             	sub    $0x28,%esp
  struct file *f;
  struct stat *st;
  
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
801055db:	8d 45 f4             	lea    -0xc(%ebp),%eax
801055de:	89 44 24 08          	mov    %eax,0x8(%esp)
801055e2:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801055e9:	00 
801055ea:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801055f1:	e8 7e fd ff ff       	call   80105374 <argfd>
801055f6:	85 c0                	test   %eax,%eax
801055f8:	78 1f                	js     80105619 <sys_fstat+0x44>
801055fa:	c7 44 24 08 14 00 00 	movl   $0x14,0x8(%esp)
80105601:	00 
80105602:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105605:	89 44 24 04          	mov    %eax,0x4(%esp)
80105609:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105610:	e8 f5 fb ff ff       	call   8010520a <argptr>
80105615:	85 c0                	test   %eax,%eax
80105617:	79 07                	jns    80105620 <sys_fstat+0x4b>
    return -1;
80105619:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010561e:	eb 12                	jmp    80105632 <sys_fstat+0x5d>
  return filestat(f, st);
80105620:	8b 55 f0             	mov    -0x10(%ebp),%edx
80105623:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105626:	89 54 24 04          	mov    %edx,0x4(%esp)
8010562a:	89 04 24             	mov    %eax,(%esp)
8010562d:	e8 f8 ba ff ff       	call   8010112a <filestat>
}
80105632:	c9                   	leave  
80105633:	c3                   	ret    

80105634 <sys_link>:

// Create the path new as a link to the same inode as old.
int
sys_link(void)
{
80105634:	55                   	push   %ebp
80105635:	89 e5                	mov    %esp,%ebp
80105637:	83 ec 38             	sub    $0x38,%esp
  char name[DIRSIZ], *new, *old;
  struct inode *dp, *ip;

  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
8010563a:	8d 45 d8             	lea    -0x28(%ebp),%eax
8010563d:	89 44 24 04          	mov    %eax,0x4(%esp)
80105641:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105648:	e8 1f fc ff ff       	call   8010526c <argstr>
8010564d:	85 c0                	test   %eax,%eax
8010564f:	78 17                	js     80105668 <sys_link+0x34>
80105651:	8d 45 dc             	lea    -0x24(%ebp),%eax
80105654:	89 44 24 04          	mov    %eax,0x4(%esp)
80105658:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010565f:	e8 08 fc ff ff       	call   8010526c <argstr>
80105664:	85 c0                	test   %eax,%eax
80105666:	79 0a                	jns    80105672 <sys_link+0x3e>
    return -1;
80105668:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010566d:	e9 3c 01 00 00       	jmp    801057ae <sys_link+0x17a>
  if((ip = namei(old)) == 0)
80105672:	8b 45 d8             	mov    -0x28(%ebp),%eax
80105675:	89 04 24             	mov    %eax,(%esp)
80105678:	e8 1d ce ff ff       	call   8010249a <namei>
8010567d:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105680:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105684:	75 0a                	jne    80105690 <sys_link+0x5c>
    return -1;
80105686:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010568b:	e9 1e 01 00 00       	jmp    801057ae <sys_link+0x17a>

  begin_trans();
80105690:	e8 18 dc ff ff       	call   801032ad <begin_trans>

  ilock(ip);
80105695:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105698:	89 04 24             	mov    %eax,(%esp)
8010569b:	e8 58 c2 ff ff       	call   801018f8 <ilock>
  if(ip->type == T_DIR){
801056a0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056a3:	0f b7 40 10          	movzwl 0x10(%eax),%eax
801056a7:	66 83 f8 01          	cmp    $0x1,%ax
801056ab:	75 1a                	jne    801056c7 <sys_link+0x93>
    iunlockput(ip);
801056ad:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056b0:	89 04 24             	mov    %eax,(%esp)
801056b3:	e8 c4 c4 ff ff       	call   80101b7c <iunlockput>
    commit_trans();
801056b8:	e8 39 dc ff ff       	call   801032f6 <commit_trans>
    return -1;
801056bd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801056c2:	e9 e7 00 00 00       	jmp    801057ae <sys_link+0x17a>
  }

  ip->nlink++;
801056c7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056ca:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801056ce:	8d 50 01             	lea    0x1(%eax),%edx
801056d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056d4:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
801056d8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056db:	89 04 24             	mov    %eax,(%esp)
801056de:	e8 59 c0 ff ff       	call   8010173c <iupdate>
  iunlock(ip);
801056e3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801056e6:	89 04 24             	mov    %eax,(%esp)
801056e9:	e8 58 c3 ff ff       	call   80101a46 <iunlock>

  if((dp = nameiparent(new, name)) == 0)
801056ee:	8b 45 dc             	mov    -0x24(%ebp),%eax
801056f1:	8d 55 e2             	lea    -0x1e(%ebp),%edx
801056f4:	89 54 24 04          	mov    %edx,0x4(%esp)
801056f8:	89 04 24             	mov    %eax,(%esp)
801056fb:	e8 bc cd ff ff       	call   801024bc <nameiparent>
80105700:	89 45 f0             	mov    %eax,-0x10(%ebp)
80105703:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105707:	74 68                	je     80105771 <sys_link+0x13d>
    goto bad;
  ilock(dp);
80105709:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010570c:	89 04 24             	mov    %eax,(%esp)
8010570f:	e8 e4 c1 ff ff       	call   801018f8 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80105714:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105717:	8b 10                	mov    (%eax),%edx
80105719:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010571c:	8b 00                	mov    (%eax),%eax
8010571e:	39 c2                	cmp    %eax,%edx
80105720:	75 20                	jne    80105742 <sys_link+0x10e>
80105722:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105725:	8b 40 04             	mov    0x4(%eax),%eax
80105728:	89 44 24 08          	mov    %eax,0x8(%esp)
8010572c:	8d 45 e2             	lea    -0x1e(%ebp),%eax
8010572f:	89 44 24 04          	mov    %eax,0x4(%esp)
80105733:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105736:	89 04 24             	mov    %eax,(%esp)
80105739:	e8 9b ca ff ff       	call   801021d9 <dirlink>
8010573e:	85 c0                	test   %eax,%eax
80105740:	79 0d                	jns    8010574f <sys_link+0x11b>
    iunlockput(dp);
80105742:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105745:	89 04 24             	mov    %eax,(%esp)
80105748:	e8 2f c4 ff ff       	call   80101b7c <iunlockput>
    goto bad;
8010574d:	eb 23                	jmp    80105772 <sys_link+0x13e>
  }
  iunlockput(dp);
8010574f:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105752:	89 04 24             	mov    %eax,(%esp)
80105755:	e8 22 c4 ff ff       	call   80101b7c <iunlockput>
  iput(ip);
8010575a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010575d:	89 04 24             	mov    %eax,(%esp)
80105760:	e8 46 c3 ff ff       	call   80101aab <iput>

  commit_trans();
80105765:	e8 8c db ff ff       	call   801032f6 <commit_trans>

  return 0;
8010576a:	b8 00 00 00 00       	mov    $0x0,%eax
8010576f:	eb 3d                	jmp    801057ae <sys_link+0x17a>
  ip->nlink++;
  iupdate(ip);
  iunlock(ip);

  if((dp = nameiparent(new, name)) == 0)
    goto bad;
80105771:	90                   	nop
  commit_trans();

  return 0;

bad:
  ilock(ip);
80105772:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105775:	89 04 24             	mov    %eax,(%esp)
80105778:	e8 7b c1 ff ff       	call   801018f8 <ilock>
  ip->nlink--;
8010577d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105780:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80105784:	8d 50 ff             	lea    -0x1(%eax),%edx
80105787:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010578a:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
8010578e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105791:	89 04 24             	mov    %eax,(%esp)
80105794:	e8 a3 bf ff ff       	call   8010173c <iupdate>
  iunlockput(ip);
80105799:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010579c:	89 04 24             	mov    %eax,(%esp)
8010579f:	e8 d8 c3 ff ff       	call   80101b7c <iunlockput>
  commit_trans();
801057a4:	e8 4d db ff ff       	call   801032f6 <commit_trans>
  return -1;
801057a9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801057ae:	c9                   	leave  
801057af:	c3                   	ret    

801057b0 <isdirempty>:

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
801057b0:	55                   	push   %ebp
801057b1:	89 e5                	mov    %esp,%ebp
801057b3:	83 ec 38             	sub    $0x38,%esp
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801057b6:	c7 45 f4 20 00 00 00 	movl   $0x20,-0xc(%ebp)
801057bd:	eb 4b                	jmp    8010580a <isdirempty+0x5a>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801057bf:	8b 45 f4             	mov    -0xc(%ebp),%eax
801057c2:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
801057c9:	00 
801057ca:	89 44 24 08          	mov    %eax,0x8(%esp)
801057ce:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801057d1:	89 44 24 04          	mov    %eax,0x4(%esp)
801057d5:	8b 45 08             	mov    0x8(%ebp),%eax
801057d8:	89 04 24             	mov    %eax,(%esp)
801057db:	e8 0e c6 ff ff       	call   80101dee <readi>
801057e0:	83 f8 10             	cmp    $0x10,%eax
801057e3:	74 0c                	je     801057f1 <isdirempty+0x41>
      panic("isdirempty: readi");
801057e5:	c7 04 24 27 86 10 80 	movl   $0x80108627,(%esp)
801057ec:	e8 4c ad ff ff       	call   8010053d <panic>
    if(de.inum != 0)
801057f1:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
801057f5:	66 85 c0             	test   %ax,%ax
801057f8:	74 07                	je     80105801 <isdirempty+0x51>
      return 0;
801057fa:	b8 00 00 00 00       	mov    $0x0,%eax
801057ff:	eb 1b                	jmp    8010581c <isdirempty+0x6c>
isdirempty(struct inode *dp)
{
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80105801:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105804:	83 c0 10             	add    $0x10,%eax
80105807:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010580a:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010580d:	8b 45 08             	mov    0x8(%ebp),%eax
80105810:	8b 40 18             	mov    0x18(%eax),%eax
80105813:	39 c2                	cmp    %eax,%edx
80105815:	72 a8                	jb     801057bf <isdirempty+0xf>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
      panic("isdirempty: readi");
    if(de.inum != 0)
      return 0;
  }
  return 1;
80105817:	b8 01 00 00 00       	mov    $0x1,%eax
}
8010581c:	c9                   	leave  
8010581d:	c3                   	ret    

8010581e <sys_unlink>:

//PAGEBREAK!
int
sys_unlink(void)
{
8010581e:	55                   	push   %ebp
8010581f:	89 e5                	mov    %esp,%ebp
80105821:	83 ec 48             	sub    $0x48,%esp
  struct inode *ip, *dp;
  struct dirent de;
  char name[DIRSIZ], *path;
  uint off;

  if(argstr(0, &path) < 0)
80105824:	8d 45 cc             	lea    -0x34(%ebp),%eax
80105827:	89 44 24 04          	mov    %eax,0x4(%esp)
8010582b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105832:	e8 35 fa ff ff       	call   8010526c <argstr>
80105837:	85 c0                	test   %eax,%eax
80105839:	79 0a                	jns    80105845 <sys_unlink+0x27>
    return -1;
8010583b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105840:	e9 aa 01 00 00       	jmp    801059ef <sys_unlink+0x1d1>
  if((dp = nameiparent(path, name)) == 0)
80105845:	8b 45 cc             	mov    -0x34(%ebp),%eax
80105848:	8d 55 d2             	lea    -0x2e(%ebp),%edx
8010584b:	89 54 24 04          	mov    %edx,0x4(%esp)
8010584f:	89 04 24             	mov    %eax,(%esp)
80105852:	e8 65 cc ff ff       	call   801024bc <nameiparent>
80105857:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010585a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010585e:	75 0a                	jne    8010586a <sys_unlink+0x4c>
    return -1;
80105860:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105865:	e9 85 01 00 00       	jmp    801059ef <sys_unlink+0x1d1>

  begin_trans();
8010586a:	e8 3e da ff ff       	call   801032ad <begin_trans>

  ilock(dp);
8010586f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105872:	89 04 24             	mov    %eax,(%esp)
80105875:	e8 7e c0 ff ff       	call   801018f8 <ilock>

  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
8010587a:	c7 44 24 04 39 86 10 	movl   $0x80108639,0x4(%esp)
80105881:	80 
80105882:	8d 45 d2             	lea    -0x2e(%ebp),%eax
80105885:	89 04 24             	mov    %eax,(%esp)
80105888:	e8 62 c8 ff ff       	call   801020ef <namecmp>
8010588d:	85 c0                	test   %eax,%eax
8010588f:	0f 84 45 01 00 00    	je     801059da <sys_unlink+0x1bc>
80105895:	c7 44 24 04 3b 86 10 	movl   $0x8010863b,0x4(%esp)
8010589c:	80 
8010589d:	8d 45 d2             	lea    -0x2e(%ebp),%eax
801058a0:	89 04 24             	mov    %eax,(%esp)
801058a3:	e8 47 c8 ff ff       	call   801020ef <namecmp>
801058a8:	85 c0                	test   %eax,%eax
801058aa:	0f 84 2a 01 00 00    	je     801059da <sys_unlink+0x1bc>
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
801058b0:	8d 45 c8             	lea    -0x38(%ebp),%eax
801058b3:	89 44 24 08          	mov    %eax,0x8(%esp)
801058b7:	8d 45 d2             	lea    -0x2e(%ebp),%eax
801058ba:	89 44 24 04          	mov    %eax,0x4(%esp)
801058be:	8b 45 f4             	mov    -0xc(%ebp),%eax
801058c1:	89 04 24             	mov    %eax,(%esp)
801058c4:	e8 48 c8 ff ff       	call   80102111 <dirlookup>
801058c9:	89 45 f0             	mov    %eax,-0x10(%ebp)
801058cc:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801058d0:	0f 84 03 01 00 00    	je     801059d9 <sys_unlink+0x1bb>
    goto bad;
  ilock(ip);
801058d6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058d9:	89 04 24             	mov    %eax,(%esp)
801058dc:	e8 17 c0 ff ff       	call   801018f8 <ilock>

  if(ip->nlink < 1)
801058e1:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058e4:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801058e8:	66 85 c0             	test   %ax,%ax
801058eb:	7f 0c                	jg     801058f9 <sys_unlink+0xdb>
    panic("unlink: nlink < 1");
801058ed:	c7 04 24 3e 86 10 80 	movl   $0x8010863e,(%esp)
801058f4:	e8 44 ac ff ff       	call   8010053d <panic>
  if(ip->type == T_DIR && !isdirempty(ip)){
801058f9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801058fc:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80105900:	66 83 f8 01          	cmp    $0x1,%ax
80105904:	75 1f                	jne    80105925 <sys_unlink+0x107>
80105906:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105909:	89 04 24             	mov    %eax,(%esp)
8010590c:	e8 9f fe ff ff       	call   801057b0 <isdirempty>
80105911:	85 c0                	test   %eax,%eax
80105913:	75 10                	jne    80105925 <sys_unlink+0x107>
    iunlockput(ip);
80105915:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105918:	89 04 24             	mov    %eax,(%esp)
8010591b:	e8 5c c2 ff ff       	call   80101b7c <iunlockput>
    goto bad;
80105920:	e9 b5 00 00 00       	jmp    801059da <sys_unlink+0x1bc>
  }

  memset(&de, 0, sizeof(de));
80105925:	c7 44 24 08 10 00 00 	movl   $0x10,0x8(%esp)
8010592c:	00 
8010592d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105934:	00 
80105935:	8d 45 e0             	lea    -0x20(%ebp),%eax
80105938:	89 04 24             	mov    %eax,(%esp)
8010593b:	e8 42 f5 ff ff       	call   80104e82 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80105940:	8b 45 c8             	mov    -0x38(%ebp),%eax
80105943:	c7 44 24 0c 10 00 00 	movl   $0x10,0xc(%esp)
8010594a:	00 
8010594b:	89 44 24 08          	mov    %eax,0x8(%esp)
8010594f:	8d 45 e0             	lea    -0x20(%ebp),%eax
80105952:	89 44 24 04          	mov    %eax,0x4(%esp)
80105956:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105959:	89 04 24             	mov    %eax,(%esp)
8010595c:	e8 f8 c5 ff ff       	call   80101f59 <writei>
80105961:	83 f8 10             	cmp    $0x10,%eax
80105964:	74 0c                	je     80105972 <sys_unlink+0x154>
    panic("unlink: writei");
80105966:	c7 04 24 50 86 10 80 	movl   $0x80108650,(%esp)
8010596d:	e8 cb ab ff ff       	call   8010053d <panic>
  if(ip->type == T_DIR){
80105972:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105975:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80105979:	66 83 f8 01          	cmp    $0x1,%ax
8010597d:	75 1c                	jne    8010599b <sys_unlink+0x17d>
    dp->nlink--;
8010597f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105982:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80105986:	8d 50 ff             	lea    -0x1(%eax),%edx
80105989:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010598c:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
80105990:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105993:	89 04 24             	mov    %eax,(%esp)
80105996:	e8 a1 bd ff ff       	call   8010173c <iupdate>
  }
  iunlockput(dp);
8010599b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010599e:	89 04 24             	mov    %eax,(%esp)
801059a1:	e8 d6 c1 ff ff       	call   80101b7c <iunlockput>

  ip->nlink--;
801059a6:	8b 45 f0             	mov    -0x10(%ebp),%eax
801059a9:	0f b7 40 16          	movzwl 0x16(%eax),%eax
801059ad:	8d 50 ff             	lea    -0x1(%eax),%edx
801059b0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801059b3:	66 89 50 16          	mov    %dx,0x16(%eax)
  iupdate(ip);
801059b7:	8b 45 f0             	mov    -0x10(%ebp),%eax
801059ba:	89 04 24             	mov    %eax,(%esp)
801059bd:	e8 7a bd ff ff       	call   8010173c <iupdate>
  iunlockput(ip);
801059c2:	8b 45 f0             	mov    -0x10(%ebp),%eax
801059c5:	89 04 24             	mov    %eax,(%esp)
801059c8:	e8 af c1 ff ff       	call   80101b7c <iunlockput>

  commit_trans();
801059cd:	e8 24 d9 ff ff       	call   801032f6 <commit_trans>

  return 0;
801059d2:	b8 00 00 00 00       	mov    $0x0,%eax
801059d7:	eb 16                	jmp    801059ef <sys_unlink+0x1d1>
  // Cannot unlink "." or "..".
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
    goto bad;

  if((ip = dirlookup(dp, name, &off)) == 0)
    goto bad;
801059d9:	90                   	nop
  commit_trans();

  return 0;

bad:
  iunlockput(dp);
801059da:	8b 45 f4             	mov    -0xc(%ebp),%eax
801059dd:	89 04 24             	mov    %eax,(%esp)
801059e0:	e8 97 c1 ff ff       	call   80101b7c <iunlockput>
  commit_trans();
801059e5:	e8 0c d9 ff ff       	call   801032f6 <commit_trans>
  return -1;
801059ea:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801059ef:	c9                   	leave  
801059f0:	c3                   	ret    

801059f1 <create>:

static struct inode*
create(char *path, short type, short major, short minor)
{
801059f1:	55                   	push   %ebp
801059f2:	89 e5                	mov    %esp,%ebp
801059f4:	83 ec 48             	sub    $0x48,%esp
801059f7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801059fa:	8b 55 10             	mov    0x10(%ebp),%edx
801059fd:	8b 45 14             	mov    0x14(%ebp),%eax
80105a00:	66 89 4d d4          	mov    %cx,-0x2c(%ebp)
80105a04:	66 89 55 d0          	mov    %dx,-0x30(%ebp)
80105a08:	66 89 45 cc          	mov    %ax,-0x34(%ebp)
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
80105a0c:	8d 45 de             	lea    -0x22(%ebp),%eax
80105a0f:	89 44 24 04          	mov    %eax,0x4(%esp)
80105a13:	8b 45 08             	mov    0x8(%ebp),%eax
80105a16:	89 04 24             	mov    %eax,(%esp)
80105a19:	e8 9e ca ff ff       	call   801024bc <nameiparent>
80105a1e:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105a21:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105a25:	75 0a                	jne    80105a31 <create+0x40>
    return 0;
80105a27:	b8 00 00 00 00       	mov    $0x0,%eax
80105a2c:	e9 7e 01 00 00       	jmp    80105baf <create+0x1be>
  ilock(dp);
80105a31:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105a34:	89 04 24             	mov    %eax,(%esp)
80105a37:	e8 bc be ff ff       	call   801018f8 <ilock>

  if((ip = dirlookup(dp, name, &off)) != 0){
80105a3c:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105a3f:	89 44 24 08          	mov    %eax,0x8(%esp)
80105a43:	8d 45 de             	lea    -0x22(%ebp),%eax
80105a46:	89 44 24 04          	mov    %eax,0x4(%esp)
80105a4a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105a4d:	89 04 24             	mov    %eax,(%esp)
80105a50:	e8 bc c6 ff ff       	call   80102111 <dirlookup>
80105a55:	89 45 f0             	mov    %eax,-0x10(%ebp)
80105a58:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105a5c:	74 47                	je     80105aa5 <create+0xb4>
    iunlockput(dp);
80105a5e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105a61:	89 04 24             	mov    %eax,(%esp)
80105a64:	e8 13 c1 ff ff       	call   80101b7c <iunlockput>
    ilock(ip);
80105a69:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105a6c:	89 04 24             	mov    %eax,(%esp)
80105a6f:	e8 84 be ff ff       	call   801018f8 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
80105a74:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80105a79:	75 15                	jne    80105a90 <create+0x9f>
80105a7b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105a7e:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80105a82:	66 83 f8 02          	cmp    $0x2,%ax
80105a86:	75 08                	jne    80105a90 <create+0x9f>
      return ip;
80105a88:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105a8b:	e9 1f 01 00 00       	jmp    80105baf <create+0x1be>
    iunlockput(ip);
80105a90:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105a93:	89 04 24             	mov    %eax,(%esp)
80105a96:	e8 e1 c0 ff ff       	call   80101b7c <iunlockput>
    return 0;
80105a9b:	b8 00 00 00 00       	mov    $0x0,%eax
80105aa0:	e9 0a 01 00 00       	jmp    80105baf <create+0x1be>
  }

  if((ip = ialloc(dp->dev, type)) == 0)
80105aa5:	0f bf 55 d4          	movswl -0x2c(%ebp),%edx
80105aa9:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105aac:	8b 00                	mov    (%eax),%eax
80105aae:	89 54 24 04          	mov    %edx,0x4(%esp)
80105ab2:	89 04 24             	mov    %eax,(%esp)
80105ab5:	e8 a5 bb ff ff       	call   8010165f <ialloc>
80105aba:	89 45 f0             	mov    %eax,-0x10(%ebp)
80105abd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105ac1:	75 0c                	jne    80105acf <create+0xde>
    panic("create: ialloc");
80105ac3:	c7 04 24 5f 86 10 80 	movl   $0x8010865f,(%esp)
80105aca:	e8 6e aa ff ff       	call   8010053d <panic>

  ilock(ip);
80105acf:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ad2:	89 04 24             	mov    %eax,(%esp)
80105ad5:	e8 1e be ff ff       	call   801018f8 <ilock>
  ip->major = major;
80105ada:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105add:	0f b7 55 d0          	movzwl -0x30(%ebp),%edx
80105ae1:	66 89 50 12          	mov    %dx,0x12(%eax)
  ip->minor = minor;
80105ae5:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ae8:	0f b7 55 cc          	movzwl -0x34(%ebp),%edx
80105aec:	66 89 50 14          	mov    %dx,0x14(%eax)
  ip->nlink = 1;
80105af0:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105af3:	66 c7 40 16 01 00    	movw   $0x1,0x16(%eax)
  iupdate(ip);
80105af9:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105afc:	89 04 24             	mov    %eax,(%esp)
80105aff:	e8 38 bc ff ff       	call   8010173c <iupdate>

  if(type == T_DIR){  // Create . and .. entries.
80105b04:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
80105b09:	75 6a                	jne    80105b75 <create+0x184>
    dp->nlink++;  // for ".."
80105b0b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105b0e:	0f b7 40 16          	movzwl 0x16(%eax),%eax
80105b12:	8d 50 01             	lea    0x1(%eax),%edx
80105b15:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105b18:	66 89 50 16          	mov    %dx,0x16(%eax)
    iupdate(dp);
80105b1c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105b1f:	89 04 24             	mov    %eax,(%esp)
80105b22:	e8 15 bc ff ff       	call   8010173c <iupdate>
    // No ip->nlink++ for ".": avoid cyclic ref count.
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
80105b27:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b2a:	8b 40 04             	mov    0x4(%eax),%eax
80105b2d:	89 44 24 08          	mov    %eax,0x8(%esp)
80105b31:	c7 44 24 04 39 86 10 	movl   $0x80108639,0x4(%esp)
80105b38:	80 
80105b39:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b3c:	89 04 24             	mov    %eax,(%esp)
80105b3f:	e8 95 c6 ff ff       	call   801021d9 <dirlink>
80105b44:	85 c0                	test   %eax,%eax
80105b46:	78 21                	js     80105b69 <create+0x178>
80105b48:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105b4b:	8b 40 04             	mov    0x4(%eax),%eax
80105b4e:	89 44 24 08          	mov    %eax,0x8(%esp)
80105b52:	c7 44 24 04 3b 86 10 	movl   $0x8010863b,0x4(%esp)
80105b59:	80 
80105b5a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b5d:	89 04 24             	mov    %eax,(%esp)
80105b60:	e8 74 c6 ff ff       	call   801021d9 <dirlink>
80105b65:	85 c0                	test   %eax,%eax
80105b67:	79 0c                	jns    80105b75 <create+0x184>
      panic("create dots");
80105b69:	c7 04 24 6e 86 10 80 	movl   $0x8010866e,(%esp)
80105b70:	e8 c8 a9 ff ff       	call   8010053d <panic>
  }

  if(dirlink(dp, name, ip->inum) < 0)
80105b75:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105b78:	8b 40 04             	mov    0x4(%eax),%eax
80105b7b:	89 44 24 08          	mov    %eax,0x8(%esp)
80105b7f:	8d 45 de             	lea    -0x22(%ebp),%eax
80105b82:	89 44 24 04          	mov    %eax,0x4(%esp)
80105b86:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105b89:	89 04 24             	mov    %eax,(%esp)
80105b8c:	e8 48 c6 ff ff       	call   801021d9 <dirlink>
80105b91:	85 c0                	test   %eax,%eax
80105b93:	79 0c                	jns    80105ba1 <create+0x1b0>
    panic("create: dirlink");
80105b95:	c7 04 24 7a 86 10 80 	movl   $0x8010867a,(%esp)
80105b9c:	e8 9c a9 ff ff       	call   8010053d <panic>

  iunlockput(dp);
80105ba1:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105ba4:	89 04 24             	mov    %eax,(%esp)
80105ba7:	e8 d0 bf ff ff       	call   80101b7c <iunlockput>

  return ip;
80105bac:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80105baf:	c9                   	leave  
80105bb0:	c3                   	ret    

80105bb1 <sys_open>:

int
sys_open(void)
{
80105bb1:	55                   	push   %ebp
80105bb2:	89 e5                	mov    %esp,%ebp
80105bb4:	83 ec 38             	sub    $0x38,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80105bb7:	8d 45 e8             	lea    -0x18(%ebp),%eax
80105bba:	89 44 24 04          	mov    %eax,0x4(%esp)
80105bbe:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105bc5:	e8 a2 f6 ff ff       	call   8010526c <argstr>
80105bca:	85 c0                	test   %eax,%eax
80105bcc:	78 17                	js     80105be5 <sys_open+0x34>
80105bce:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80105bd1:	89 44 24 04          	mov    %eax,0x4(%esp)
80105bd5:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105bdc:	e8 f1 f5 ff ff       	call   801051d2 <argint>
80105be1:	85 c0                	test   %eax,%eax
80105be3:	79 0a                	jns    80105bef <sys_open+0x3e>
    return -1;
80105be5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105bea:	e9 46 01 00 00       	jmp    80105d35 <sys_open+0x184>
  if(omode & O_CREATE){
80105bef:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105bf2:	25 00 02 00 00       	and    $0x200,%eax
80105bf7:	85 c0                	test   %eax,%eax
80105bf9:	74 40                	je     80105c3b <sys_open+0x8a>
    begin_trans();
80105bfb:	e8 ad d6 ff ff       	call   801032ad <begin_trans>
    ip = create(path, T_FILE, 0, 0);
80105c00:	8b 45 e8             	mov    -0x18(%ebp),%eax
80105c03:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
80105c0a:	00 
80105c0b:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80105c12:	00 
80105c13:	c7 44 24 04 02 00 00 	movl   $0x2,0x4(%esp)
80105c1a:	00 
80105c1b:	89 04 24             	mov    %eax,(%esp)
80105c1e:	e8 ce fd ff ff       	call   801059f1 <create>
80105c23:	89 45 f4             	mov    %eax,-0xc(%ebp)
    commit_trans();
80105c26:	e8 cb d6 ff ff       	call   801032f6 <commit_trans>
    if(ip == 0)
80105c2b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105c2f:	75 5c                	jne    80105c8d <sys_open+0xdc>
      return -1;
80105c31:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c36:	e9 fa 00 00 00       	jmp    80105d35 <sys_open+0x184>
  } else {
    if((ip = namei(path)) == 0)
80105c3b:	8b 45 e8             	mov    -0x18(%ebp),%eax
80105c3e:	89 04 24             	mov    %eax,(%esp)
80105c41:	e8 54 c8 ff ff       	call   8010249a <namei>
80105c46:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105c49:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105c4d:	75 0a                	jne    80105c59 <sys_open+0xa8>
      return -1;
80105c4f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c54:	e9 dc 00 00 00       	jmp    80105d35 <sys_open+0x184>
    ilock(ip);
80105c59:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105c5c:	89 04 24             	mov    %eax,(%esp)
80105c5f:	e8 94 bc ff ff       	call   801018f8 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
80105c64:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105c67:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80105c6b:	66 83 f8 01          	cmp    $0x1,%ax
80105c6f:	75 1c                	jne    80105c8d <sys_open+0xdc>
80105c71:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105c74:	85 c0                	test   %eax,%eax
80105c76:	74 15                	je     80105c8d <sys_open+0xdc>
      iunlockput(ip);
80105c78:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105c7b:	89 04 24             	mov    %eax,(%esp)
80105c7e:	e8 f9 be ff ff       	call   80101b7c <iunlockput>
      return -1;
80105c83:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105c88:	e9 a8 00 00 00       	jmp    80105d35 <sys_open+0x184>
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
80105c8d:	e8 1a b3 ff ff       	call   80100fac <filealloc>
80105c92:	89 45 f0             	mov    %eax,-0x10(%ebp)
80105c95:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105c99:	74 14                	je     80105caf <sys_open+0xfe>
80105c9b:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105c9e:	89 04 24             	mov    %eax,(%esp)
80105ca1:	e8 43 f7 ff ff       	call   801053e9 <fdalloc>
80105ca6:	89 45 ec             	mov    %eax,-0x14(%ebp)
80105ca9:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80105cad:	79 23                	jns    80105cd2 <sys_open+0x121>
    if(f)
80105caf:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105cb3:	74 0b                	je     80105cc0 <sys_open+0x10f>
      fileclose(f);
80105cb5:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105cb8:	89 04 24             	mov    %eax,(%esp)
80105cbb:	e8 94 b3 ff ff       	call   80101054 <fileclose>
    iunlockput(ip);
80105cc0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105cc3:	89 04 24             	mov    %eax,(%esp)
80105cc6:	e8 b1 be ff ff       	call   80101b7c <iunlockput>
    return -1;
80105ccb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105cd0:	eb 63                	jmp    80105d35 <sys_open+0x184>
  }
  iunlock(ip);
80105cd2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105cd5:	89 04 24             	mov    %eax,(%esp)
80105cd8:	e8 69 bd ff ff       	call   80101a46 <iunlock>

  f->type = FD_INODE;
80105cdd:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ce0:	c7 00 02 00 00 00    	movl   $0x2,(%eax)
  f->ip = ip;
80105ce6:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105ce9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105cec:	89 50 10             	mov    %edx,0x10(%eax)
  f->off = 0;
80105cef:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105cf2:	c7 40 14 00 00 00 00 	movl   $0x0,0x14(%eax)
  f->readable = !(omode & O_WRONLY);
80105cf9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105cfc:	83 e0 01             	and    $0x1,%eax
80105cff:	85 c0                	test   %eax,%eax
80105d01:	0f 94 c2             	sete   %dl
80105d04:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105d07:	88 50 08             	mov    %dl,0x8(%eax)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105d0a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105d0d:	83 e0 01             	and    $0x1,%eax
80105d10:	84 c0                	test   %al,%al
80105d12:	75 0a                	jne    80105d1e <sys_open+0x16d>
80105d14:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105d17:	83 e0 02             	and    $0x2,%eax
80105d1a:	85 c0                	test   %eax,%eax
80105d1c:	74 07                	je     80105d25 <sys_open+0x174>
80105d1e:	b8 01 00 00 00       	mov    $0x1,%eax
80105d23:	eb 05                	jmp    80105d2a <sys_open+0x179>
80105d25:	b8 00 00 00 00       	mov    $0x0,%eax
80105d2a:	89 c2                	mov    %eax,%edx
80105d2c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105d2f:	88 50 09             	mov    %dl,0x9(%eax)
  return fd;
80105d32:	8b 45 ec             	mov    -0x14(%ebp),%eax
}
80105d35:	c9                   	leave  
80105d36:	c3                   	ret    

80105d37 <sys_mkdir>:

int
sys_mkdir(void)
{
80105d37:	55                   	push   %ebp
80105d38:	89 e5                	mov    %esp,%ebp
80105d3a:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  begin_trans();
80105d3d:	e8 6b d5 ff ff       	call   801032ad <begin_trans>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
80105d42:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105d45:	89 44 24 04          	mov    %eax,0x4(%esp)
80105d49:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105d50:	e8 17 f5 ff ff       	call   8010526c <argstr>
80105d55:	85 c0                	test   %eax,%eax
80105d57:	78 2c                	js     80105d85 <sys_mkdir+0x4e>
80105d59:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105d5c:	c7 44 24 0c 00 00 00 	movl   $0x0,0xc(%esp)
80105d63:	00 
80105d64:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80105d6b:	00 
80105d6c:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80105d73:	00 
80105d74:	89 04 24             	mov    %eax,(%esp)
80105d77:	e8 75 fc ff ff       	call   801059f1 <create>
80105d7c:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105d7f:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105d83:	75 0c                	jne    80105d91 <sys_mkdir+0x5a>
    commit_trans();
80105d85:	e8 6c d5 ff ff       	call   801032f6 <commit_trans>
    return -1;
80105d8a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105d8f:	eb 15                	jmp    80105da6 <sys_mkdir+0x6f>
  }
  iunlockput(ip);
80105d91:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105d94:	89 04 24             	mov    %eax,(%esp)
80105d97:	e8 e0 bd ff ff       	call   80101b7c <iunlockput>
  commit_trans();
80105d9c:	e8 55 d5 ff ff       	call   801032f6 <commit_trans>
  return 0;
80105da1:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105da6:	c9                   	leave  
80105da7:	c3                   	ret    

80105da8 <sys_mknod>:

int
sys_mknod(void)
{
80105da8:	55                   	push   %ebp
80105da9:	89 e5                	mov    %esp,%ebp
80105dab:	83 ec 38             	sub    $0x38,%esp
  struct inode *ip;
  char *path;
  int len;
  int major, minor;
  
  begin_trans();
80105dae:	e8 fa d4 ff ff       	call   801032ad <begin_trans>
  if((len=argstr(0, &path)) < 0 ||
80105db3:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105db6:	89 44 24 04          	mov    %eax,0x4(%esp)
80105dba:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105dc1:	e8 a6 f4 ff ff       	call   8010526c <argstr>
80105dc6:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105dc9:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105dcd:	78 5e                	js     80105e2d <sys_mknod+0x85>
     argint(1, &major) < 0 ||
80105dcf:	8d 45 e8             	lea    -0x18(%ebp),%eax
80105dd2:	89 44 24 04          	mov    %eax,0x4(%esp)
80105dd6:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105ddd:	e8 f0 f3 ff ff       	call   801051d2 <argint>
  char *path;
  int len;
  int major, minor;
  
  begin_trans();
  if((len=argstr(0, &path)) < 0 ||
80105de2:	85 c0                	test   %eax,%eax
80105de4:	78 47                	js     80105e2d <sys_mknod+0x85>
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
80105de6:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80105de9:	89 44 24 04          	mov    %eax,0x4(%esp)
80105ded:	c7 04 24 02 00 00 00 	movl   $0x2,(%esp)
80105df4:	e8 d9 f3 ff ff       	call   801051d2 <argint>
  int len;
  int major, minor;
  
  begin_trans();
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
80105df9:	85 c0                	test   %eax,%eax
80105dfb:	78 30                	js     80105e2d <sys_mknod+0x85>
     argint(2, &minor) < 0 ||
     (ip = create(path, T_DEV, major, minor)) == 0){
80105dfd:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80105e00:	0f bf c8             	movswl %ax,%ecx
80105e03:	8b 45 e8             	mov    -0x18(%ebp),%eax
80105e06:	0f bf d0             	movswl %ax,%edx
80105e09:	8b 45 ec             	mov    -0x14(%ebp),%eax
  int major, minor;
  
  begin_trans();
  if((len=argstr(0, &path)) < 0 ||
     argint(1, &major) < 0 ||
     argint(2, &minor) < 0 ||
80105e0c:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80105e10:	89 54 24 08          	mov    %edx,0x8(%esp)
80105e14:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
80105e1b:	00 
80105e1c:	89 04 24             	mov    %eax,(%esp)
80105e1f:	e8 cd fb ff ff       	call   801059f1 <create>
80105e24:	89 45 f0             	mov    %eax,-0x10(%ebp)
80105e27:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80105e2b:	75 0c                	jne    80105e39 <sys_mknod+0x91>
     (ip = create(path, T_DEV, major, minor)) == 0){
    commit_trans();
80105e2d:	e8 c4 d4 ff ff       	call   801032f6 <commit_trans>
    return -1;
80105e32:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105e37:	eb 15                	jmp    80105e4e <sys_mknod+0xa6>
  }
  iunlockput(ip);
80105e39:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105e3c:	89 04 24             	mov    %eax,(%esp)
80105e3f:	e8 38 bd ff ff       	call   80101b7c <iunlockput>
  commit_trans();
80105e44:	e8 ad d4 ff ff       	call   801032f6 <commit_trans>
  return 0;
80105e49:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105e4e:	c9                   	leave  
80105e4f:	c3                   	ret    

80105e50 <sys_chdir>:

int
sys_chdir(void)
{
80105e50:	55                   	push   %ebp
80105e51:	89 e5                	mov    %esp,%ebp
80105e53:	83 ec 28             	sub    $0x28,%esp
  char *path;
  struct inode *ip;

  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0)
80105e56:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105e59:	89 44 24 04          	mov    %eax,0x4(%esp)
80105e5d:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105e64:	e8 03 f4 ff ff       	call   8010526c <argstr>
80105e69:	85 c0                	test   %eax,%eax
80105e6b:	78 14                	js     80105e81 <sys_chdir+0x31>
80105e6d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105e70:	89 04 24             	mov    %eax,(%esp)
80105e73:	e8 22 c6 ff ff       	call   8010249a <namei>
80105e78:	89 45 f4             	mov    %eax,-0xc(%ebp)
80105e7b:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80105e7f:	75 07                	jne    80105e88 <sys_chdir+0x38>
    return -1;
80105e81:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105e86:	eb 57                	jmp    80105edf <sys_chdir+0x8f>
  ilock(ip);
80105e88:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105e8b:	89 04 24             	mov    %eax,(%esp)
80105e8e:	e8 65 ba ff ff       	call   801018f8 <ilock>
  if(ip->type != T_DIR){
80105e93:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105e96:	0f b7 40 10          	movzwl 0x10(%eax),%eax
80105e9a:	66 83 f8 01          	cmp    $0x1,%ax
80105e9e:	74 12                	je     80105eb2 <sys_chdir+0x62>
    iunlockput(ip);
80105ea0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105ea3:	89 04 24             	mov    %eax,(%esp)
80105ea6:	e8 d1 bc ff ff       	call   80101b7c <iunlockput>
    return -1;
80105eab:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105eb0:	eb 2d                	jmp    80105edf <sys_chdir+0x8f>
  }
  iunlock(ip);
80105eb2:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105eb5:	89 04 24             	mov    %eax,(%esp)
80105eb8:	e8 89 bb ff ff       	call   80101a46 <iunlock>
  iput(proc->cwd);
80105ebd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105ec3:	8b 40 68             	mov    0x68(%eax),%eax
80105ec6:	89 04 24             	mov    %eax,(%esp)
80105ec9:	e8 dd bb ff ff       	call   80101aab <iput>
  proc->cwd = ip;
80105ece:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105ed4:	8b 55 f4             	mov    -0xc(%ebp),%edx
80105ed7:	89 50 68             	mov    %edx,0x68(%eax)
  return 0;
80105eda:	b8 00 00 00 00       	mov    $0x0,%eax
}
80105edf:	c9                   	leave  
80105ee0:	c3                   	ret    

80105ee1 <sys_exec>:

int
sys_exec(void)
{
80105ee1:	55                   	push   %ebp
80105ee2:	89 e5                	mov    %esp,%ebp
80105ee4:	81 ec a8 00 00 00    	sub    $0xa8,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80105eea:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105eed:	89 44 24 04          	mov    %eax,0x4(%esp)
80105ef1:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80105ef8:	e8 6f f3 ff ff       	call   8010526c <argstr>
80105efd:	85 c0                	test   %eax,%eax
80105eff:	78 1a                	js     80105f1b <sys_exec+0x3a>
80105f01:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
80105f07:	89 44 24 04          	mov    %eax,0x4(%esp)
80105f0b:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
80105f12:	e8 bb f2 ff ff       	call   801051d2 <argint>
80105f17:	85 c0                	test   %eax,%eax
80105f19:	79 0a                	jns    80105f25 <sys_exec+0x44>
    return -1;
80105f1b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f20:	e9 e2 00 00 00       	jmp    80106007 <sys_exec+0x126>
  }
  memset(argv, 0, sizeof(argv));
80105f25:	c7 44 24 08 80 00 00 	movl   $0x80,0x8(%esp)
80105f2c:	00 
80105f2d:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80105f34:	00 
80105f35:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80105f3b:	89 04 24             	mov    %eax,(%esp)
80105f3e:	e8 3f ef ff ff       	call   80104e82 <memset>
  for(i=0;; i++){
80105f43:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
    if(i >= NELEM(argv))
80105f4a:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f4d:	83 f8 1f             	cmp    $0x1f,%eax
80105f50:	76 0a                	jbe    80105f5c <sys_exec+0x7b>
      return -1;
80105f52:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f57:	e9 ab 00 00 00       	jmp    80106007 <sys_exec+0x126>
    if(fetchint(proc, uargv+4*i, (int*)&uarg) < 0)
80105f5c:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105f5f:	c1 e0 02             	shl    $0x2,%eax
80105f62:	89 c2                	mov    %eax,%edx
80105f64:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
80105f6a:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
80105f6d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105f73:	8d 95 68 ff ff ff    	lea    -0x98(%ebp),%edx
80105f79:	89 54 24 08          	mov    %edx,0x8(%esp)
80105f7d:	89 4c 24 04          	mov    %ecx,0x4(%esp)
80105f81:	89 04 24             	mov    %eax,(%esp)
80105f84:	e8 b7 f1 ff ff       	call   80105140 <fetchint>
80105f89:	85 c0                	test   %eax,%eax
80105f8b:	79 07                	jns    80105f94 <sys_exec+0xb3>
      return -1;
80105f8d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105f92:	eb 73                	jmp    80106007 <sys_exec+0x126>
    if(uarg == 0){
80105f94:	8b 85 68 ff ff ff    	mov    -0x98(%ebp),%eax
80105f9a:	85 c0                	test   %eax,%eax
80105f9c:	75 26                	jne    80105fc4 <sys_exec+0xe3>
      argv[i] = 0;
80105f9e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105fa1:	c7 84 85 70 ff ff ff 	movl   $0x0,-0x90(%ebp,%eax,4)
80105fa8:	00 00 00 00 
      break;
80105fac:	90                   	nop
    }
    if(fetchstr(proc, uarg, &argv[i]) < 0)
      return -1;
  }
  return exec(path, argv);
80105fad:	8b 45 f0             	mov    -0x10(%ebp),%eax
80105fb0:	8d 95 70 ff ff ff    	lea    -0x90(%ebp),%edx
80105fb6:	89 54 24 04          	mov    %edx,0x4(%esp)
80105fba:	89 04 24             	mov    %eax,(%esp)
80105fbd:	e8 ca ab ff ff       	call   80100b8c <exec>
80105fc2:	eb 43                	jmp    80106007 <sys_exec+0x126>
      return -1;
    if(uarg == 0){
      argv[i] = 0;
      break;
    }
    if(fetchstr(proc, uarg, &argv[i]) < 0)
80105fc4:	8b 45 f4             	mov    -0xc(%ebp),%eax
80105fc7:	8d 14 85 00 00 00 00 	lea    0x0(,%eax,4),%edx
80105fce:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80105fd4:	8d 0c 10             	lea    (%eax,%edx,1),%ecx
80105fd7:	8b 95 68 ff ff ff    	mov    -0x98(%ebp),%edx
80105fdd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80105fe3:	89 4c 24 08          	mov    %ecx,0x8(%esp)
80105fe7:	89 54 24 04          	mov    %edx,0x4(%esp)
80105feb:	89 04 24             	mov    %eax,(%esp)
80105fee:	e8 81 f1 ff ff       	call   80105174 <fetchstr>
80105ff3:	85 c0                	test   %eax,%eax
80105ff5:	79 07                	jns    80105ffe <sys_exec+0x11d>
      return -1;
80105ff7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105ffc:	eb 09                	jmp    80106007 <sys_exec+0x126>

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
    return -1;
  }
  memset(argv, 0, sizeof(argv));
  for(i=0;; i++){
80105ffe:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
      argv[i] = 0;
      break;
    }
    if(fetchstr(proc, uarg, &argv[i]) < 0)
      return -1;
  }
80106002:	e9 43 ff ff ff       	jmp    80105f4a <sys_exec+0x69>
  return exec(path, argv);
}
80106007:	c9                   	leave  
80106008:	c3                   	ret    

80106009 <sys_pipe>:

int
sys_pipe(void)
{
80106009:	55                   	push   %ebp
8010600a:	89 e5                	mov    %esp,%ebp
8010600c:	83 ec 38             	sub    $0x38,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
8010600f:	c7 44 24 08 08 00 00 	movl   $0x8,0x8(%esp)
80106016:	00 
80106017:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010601a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010601e:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106025:	e8 e0 f1 ff ff       	call   8010520a <argptr>
8010602a:	85 c0                	test   %eax,%eax
8010602c:	79 0a                	jns    80106038 <sys_pipe+0x2f>
    return -1;
8010602e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106033:	e9 9b 00 00 00       	jmp    801060d3 <sys_pipe+0xca>
  if(pipealloc(&rf, &wf) < 0)
80106038:	8d 45 e4             	lea    -0x1c(%ebp),%eax
8010603b:	89 44 24 04          	mov    %eax,0x4(%esp)
8010603f:	8d 45 e8             	lea    -0x18(%ebp),%eax
80106042:	89 04 24             	mov    %eax,(%esp)
80106045:	e8 7e dc ff ff       	call   80103cc8 <pipealloc>
8010604a:	85 c0                	test   %eax,%eax
8010604c:	79 07                	jns    80106055 <sys_pipe+0x4c>
    return -1;
8010604e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106053:	eb 7e                	jmp    801060d3 <sys_pipe+0xca>
  fd0 = -1;
80106055:	c7 45 f4 ff ff ff ff 	movl   $0xffffffff,-0xc(%ebp)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
8010605c:	8b 45 e8             	mov    -0x18(%ebp),%eax
8010605f:	89 04 24             	mov    %eax,(%esp)
80106062:	e8 82 f3 ff ff       	call   801053e9 <fdalloc>
80106067:	89 45 f4             	mov    %eax,-0xc(%ebp)
8010606a:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
8010606e:	78 14                	js     80106084 <sys_pipe+0x7b>
80106070:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106073:	89 04 24             	mov    %eax,(%esp)
80106076:	e8 6e f3 ff ff       	call   801053e9 <fdalloc>
8010607b:	89 45 f0             	mov    %eax,-0x10(%ebp)
8010607e:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80106082:	79 37                	jns    801060bb <sys_pipe+0xb2>
    if(fd0 >= 0)
80106084:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80106088:	78 14                	js     8010609e <sys_pipe+0x95>
      proc->ofile[fd0] = 0;
8010608a:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106090:	8b 55 f4             	mov    -0xc(%ebp),%edx
80106093:	83 c2 08             	add    $0x8,%edx
80106096:	c7 44 90 08 00 00 00 	movl   $0x0,0x8(%eax,%edx,4)
8010609d:	00 
    fileclose(rf);
8010609e:	8b 45 e8             	mov    -0x18(%ebp),%eax
801060a1:	89 04 24             	mov    %eax,(%esp)
801060a4:	e8 ab af ff ff       	call   80101054 <fileclose>
    fileclose(wf);
801060a9:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801060ac:	89 04 24             	mov    %eax,(%esp)
801060af:	e8 a0 af ff ff       	call   80101054 <fileclose>
    return -1;
801060b4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801060b9:	eb 18                	jmp    801060d3 <sys_pipe+0xca>
  }
  fd[0] = fd0;
801060bb:	8b 45 ec             	mov    -0x14(%ebp),%eax
801060be:	8b 55 f4             	mov    -0xc(%ebp),%edx
801060c1:	89 10                	mov    %edx,(%eax)
  fd[1] = fd1;
801060c3:	8b 45 ec             	mov    -0x14(%ebp),%eax
801060c6:	8d 50 04             	lea    0x4(%eax),%edx
801060c9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801060cc:	89 02                	mov    %eax,(%edx)
  return 0;
801060ce:	b8 00 00 00 00       	mov    $0x0,%eax
}
801060d3:	c9                   	leave  
801060d4:	c3                   	ret    
801060d5:	00 00                	add    %al,(%eax)
	...

801060d8 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
801060d8:	55                   	push   %ebp
801060d9:	89 e5                	mov    %esp,%ebp
801060db:	83 ec 08             	sub    $0x8,%esp
  return fork();
801060de:	e8 9f e2 ff ff       	call   80104382 <fork>
}
801060e3:	c9                   	leave  
801060e4:	c3                   	ret    

801060e5 <sys_exit>:

int
sys_exit(void)
{
801060e5:	55                   	push   %ebp
801060e6:	89 e5                	mov    %esp,%ebp
801060e8:	83 ec 08             	sub    $0x8,%esp
  exit();
801060eb:	e8 f5 e3 ff ff       	call   801044e5 <exit>
  return 0;  // not reached
801060f0:	b8 00 00 00 00       	mov    $0x0,%eax
}
801060f5:	c9                   	leave  
801060f6:	c3                   	ret    

801060f7 <sys_wait>:

int
sys_wait(void)
{
801060f7:	55                   	push   %ebp
801060f8:	89 e5                	mov    %esp,%ebp
801060fa:	83 ec 08             	sub    $0x8,%esp
  return wait();
801060fd:	e8 fb e4 ff ff       	call   801045fd <wait>
}
80106102:	c9                   	leave  
80106103:	c3                   	ret    

80106104 <sys_kill>:

int
sys_kill(void)
{
80106104:	55                   	push   %ebp
80106105:	89 e5                	mov    %esp,%ebp
80106107:	83 ec 28             	sub    $0x28,%esp
  int pid;

  if(argint(0, &pid) < 0)
8010610a:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010610d:	89 44 24 04          	mov    %eax,0x4(%esp)
80106111:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106118:	e8 b5 f0 ff ff       	call   801051d2 <argint>
8010611d:	85 c0                	test   %eax,%eax
8010611f:	79 07                	jns    80106128 <sys_kill+0x24>
    return -1;
80106121:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106126:	eb 0b                	jmp    80106133 <sys_kill+0x2f>
  return kill(pid);
80106128:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010612b:	89 04 24             	mov    %eax,(%esp)
8010612e:	e8 26 e9 ff ff       	call   80104a59 <kill>
}
80106133:	c9                   	leave  
80106134:	c3                   	ret    

80106135 <sys_getpid>:

int
sys_getpid(void)
{
80106135:	55                   	push   %ebp
80106136:	89 e5                	mov    %esp,%ebp
  return proc->pid;
80106138:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010613e:	8b 40 10             	mov    0x10(%eax),%eax
}
80106141:	5d                   	pop    %ebp
80106142:	c3                   	ret    

80106143 <sys_sbrk>:

int
sys_sbrk(void)
{
80106143:	55                   	push   %ebp
80106144:	89 e5                	mov    %esp,%ebp
80106146:	83 ec 28             	sub    $0x28,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
80106149:	8d 45 f0             	lea    -0x10(%ebp),%eax
8010614c:	89 44 24 04          	mov    %eax,0x4(%esp)
80106150:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
80106157:	e8 76 f0 ff ff       	call   801051d2 <argint>
8010615c:	85 c0                	test   %eax,%eax
8010615e:	79 07                	jns    80106167 <sys_sbrk+0x24>
    return -1;
80106160:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106165:	eb 24                	jmp    8010618b <sys_sbrk+0x48>
  addr = proc->sz;
80106167:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
8010616d:	8b 00                	mov    (%eax),%eax
8010616f:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(growproc(n) < 0)
80106172:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106175:	89 04 24             	mov    %eax,(%esp)
80106178:	e8 60 e1 ff ff       	call   801042dd <growproc>
8010617d:	85 c0                	test   %eax,%eax
8010617f:	79 07                	jns    80106188 <sys_sbrk+0x45>
    return -1;
80106181:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106186:	eb 03                	jmp    8010618b <sys_sbrk+0x48>
  return addr;
80106188:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
8010618b:	c9                   	leave  
8010618c:	c3                   	ret    

8010618d <sys_sleep>:

int
sys_sleep(void)
{
8010618d:	55                   	push   %ebp
8010618e:	89 e5                	mov    %esp,%ebp
80106190:	83 ec 28             	sub    $0x28,%esp
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
80106193:	8d 45 f0             	lea    -0x10(%ebp),%eax
80106196:	89 44 24 04          	mov    %eax,0x4(%esp)
8010619a:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801061a1:	e8 2c f0 ff ff       	call   801051d2 <argint>
801061a6:	85 c0                	test   %eax,%eax
801061a8:	79 07                	jns    801061b1 <sys_sleep+0x24>
    return -1;
801061aa:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801061af:	eb 6c                	jmp    8010621d <sys_sleep+0x90>
  acquire(&tickslock);
801061b1:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
801061b8:	e8 76 ea ff ff       	call   80104c33 <acquire>
  ticks0 = ticks;
801061bd:	a1 a0 28 11 80       	mov    0x801128a0,%eax
801061c2:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(ticks - ticks0 < n){
801061c5:	eb 34                	jmp    801061fb <sys_sleep+0x6e>
    if(proc->killed){
801061c7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801061cd:	8b 40 24             	mov    0x24(%eax),%eax
801061d0:	85 c0                	test   %eax,%eax
801061d2:	74 13                	je     801061e7 <sys_sleep+0x5a>
      release(&tickslock);
801061d4:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
801061db:	e8 b5 ea ff ff       	call   80104c95 <release>
      return -1;
801061e0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801061e5:	eb 36                	jmp    8010621d <sys_sleep+0x90>
    }
    sleep(&ticks, &tickslock);
801061e7:	c7 44 24 04 60 20 11 	movl   $0x80112060,0x4(%esp)
801061ee:	80 
801061ef:	c7 04 24 a0 28 11 80 	movl   $0x801128a0,(%esp)
801061f6:	e8 5a e7 ff ff       	call   80104955 <sleep>
  
  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
801061fb:	a1 a0 28 11 80       	mov    0x801128a0,%eax
80106200:	89 c2                	mov    %eax,%edx
80106202:	2b 55 f4             	sub    -0xc(%ebp),%edx
80106205:	8b 45 f0             	mov    -0x10(%ebp),%eax
80106208:	39 c2                	cmp    %eax,%edx
8010620a:	72 bb                	jb     801061c7 <sys_sleep+0x3a>
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
8010620c:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
80106213:	e8 7d ea ff ff       	call   80104c95 <release>
  return 0;
80106218:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010621d:	c9                   	leave  
8010621e:	c3                   	ret    

8010621f <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
8010621f:	55                   	push   %ebp
80106220:	89 e5                	mov    %esp,%ebp
80106222:	83 ec 28             	sub    $0x28,%esp
  uint xticks;
  
  acquire(&tickslock);
80106225:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
8010622c:	e8 02 ea ff ff       	call   80104c33 <acquire>
  xticks = ticks;
80106231:	a1 a0 28 11 80       	mov    0x801128a0,%eax
80106236:	89 45 f4             	mov    %eax,-0xc(%ebp)
  release(&tickslock);
80106239:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
80106240:	e8 50 ea ff ff       	call   80104c95 <release>
  return xticks;
80106245:	8b 45 f4             	mov    -0xc(%ebp),%eax
}
80106248:	c9                   	leave  
80106249:	c3                   	ret    
	...

8010624c <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
8010624c:	55                   	push   %ebp
8010624d:	89 e5                	mov    %esp,%ebp
8010624f:	83 ec 08             	sub    $0x8,%esp
80106252:	8b 55 08             	mov    0x8(%ebp),%edx
80106255:	8b 45 0c             	mov    0xc(%ebp),%eax
80106258:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
8010625c:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010625f:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
80106263:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80106267:	ee                   	out    %al,(%dx)
}
80106268:	c9                   	leave  
80106269:	c3                   	ret    

8010626a <timerinit>:
#define TIMER_RATEGEN   0x04    // mode 2, rate generator
#define TIMER_16BIT     0x30    // r/w counter 16 bits, LSB first

void
timerinit(void)
{
8010626a:	55                   	push   %ebp
8010626b:	89 e5                	mov    %esp,%ebp
8010626d:	83 ec 18             	sub    $0x18,%esp
  // Interrupt 100 times/sec.
  outb(TIMER_MODE, TIMER_SEL0 | TIMER_RATEGEN | TIMER_16BIT);
80106270:	c7 44 24 04 34 00 00 	movl   $0x34,0x4(%esp)
80106277:	00 
80106278:	c7 04 24 43 00 00 00 	movl   $0x43,(%esp)
8010627f:	e8 c8 ff ff ff       	call   8010624c <outb>
  outb(IO_TIMER1, TIMER_DIV(100) % 256);
80106284:	c7 44 24 04 9c 00 00 	movl   $0x9c,0x4(%esp)
8010628b:	00 
8010628c:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
80106293:	e8 b4 ff ff ff       	call   8010624c <outb>
  outb(IO_TIMER1, TIMER_DIV(100) / 256);
80106298:	c7 44 24 04 2e 00 00 	movl   $0x2e,0x4(%esp)
8010629f:	00 
801062a0:	c7 04 24 40 00 00 00 	movl   $0x40,(%esp)
801062a7:	e8 a0 ff ff ff       	call   8010624c <outb>
  picenable(IRQ_TIMER);
801062ac:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
801062b3:	e8 99 d8 ff ff       	call   80103b51 <picenable>
}
801062b8:	c9                   	leave  
801062b9:	c3                   	ret    
	...

801062bc <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
801062bc:	1e                   	push   %ds
  pushl %es
801062bd:	06                   	push   %es
  pushl %fs
801062be:	0f a0                	push   %fs
  pushl %gs
801062c0:	0f a8                	push   %gs
  pushal
801062c2:	60                   	pusha  
  
  # Set up data and per-cpu segments.
  movw $(SEG_KDATA<<3), %ax
801062c3:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
801062c7:	8e d8                	mov    %eax,%ds
  movw %ax, %es
801062c9:	8e c0                	mov    %eax,%es
  movw $(SEG_KCPU<<3), %ax
801062cb:	66 b8 18 00          	mov    $0x18,%ax
  movw %ax, %fs
801062cf:	8e e0                	mov    %eax,%fs
  movw %ax, %gs
801062d1:	8e e8                	mov    %eax,%gs

  # Call trap(tf), where tf=%esp
  pushl %esp
801062d3:	54                   	push   %esp
  call trap
801062d4:	e8 de 01 00 00       	call   801064b7 <trap>
  addl $4, %esp
801062d9:	83 c4 04             	add    $0x4,%esp

801062dc <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
801062dc:	61                   	popa   
  popl %gs
801062dd:	0f a9                	pop    %gs
  popl %fs
801062df:	0f a1                	pop    %fs
  popl %es
801062e1:	07                   	pop    %es
  popl %ds
801062e2:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
801062e3:	83 c4 08             	add    $0x8,%esp
  iret
801062e6:	cf                   	iret   
	...

801062e8 <lidt>:

struct gatedesc;

static inline void
lidt(struct gatedesc *p, int size)
{
801062e8:	55                   	push   %ebp
801062e9:	89 e5                	mov    %esp,%ebp
801062eb:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
801062ee:	8b 45 0c             	mov    0xc(%ebp),%eax
801062f1:	83 e8 01             	sub    $0x1,%eax
801062f4:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
801062f8:	8b 45 08             	mov    0x8(%ebp),%eax
801062fb:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
801062ff:	8b 45 08             	mov    0x8(%ebp),%eax
80106302:	c1 e8 10             	shr    $0x10,%eax
80106305:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lidt (%0)" : : "r" (pd));
80106309:	8d 45 fa             	lea    -0x6(%ebp),%eax
8010630c:	0f 01 18             	lidtl  (%eax)
}
8010630f:	c9                   	leave  
80106310:	c3                   	ret    

80106311 <rcr2>:
  return result;
}

static inline uint
rcr2(void)
{
80106311:	55                   	push   %ebp
80106312:	89 e5                	mov    %esp,%ebp
80106314:	53                   	push   %ebx
80106315:	83 ec 10             	sub    $0x10,%esp
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80106318:	0f 20 d3             	mov    %cr2,%ebx
8010631b:	89 5d f8             	mov    %ebx,-0x8(%ebp)
  return val;
8010631e:	8b 45 f8             	mov    -0x8(%ebp),%eax
}
80106321:	83 c4 10             	add    $0x10,%esp
80106324:	5b                   	pop    %ebx
80106325:	5d                   	pop    %ebp
80106326:	c3                   	ret    

80106327 <tvinit>:
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
80106327:	55                   	push   %ebp
80106328:	89 e5                	mov    %esp,%ebp
8010632a:	83 ec 28             	sub    $0x28,%esp
  int i;

  for(i = 0; i < 256; i++)
8010632d:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80106334:	e9 c3 00 00 00       	jmp    801063fc <tvinit+0xd5>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
80106339:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010633c:	8b 04 85 98 b0 10 80 	mov    -0x7fef4f68(,%eax,4),%eax
80106343:	89 c2                	mov    %eax,%edx
80106345:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106348:	66 89 14 c5 a0 20 11 	mov    %dx,-0x7feedf60(,%eax,8)
8010634f:	80 
80106350:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106353:	66 c7 04 c5 a2 20 11 	movw   $0x8,-0x7feedf5e(,%eax,8)
8010635a:	80 08 00 
8010635d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106360:	0f b6 14 c5 a4 20 11 	movzbl -0x7feedf5c(,%eax,8),%edx
80106367:	80 
80106368:	83 e2 e0             	and    $0xffffffe0,%edx
8010636b:	88 14 c5 a4 20 11 80 	mov    %dl,-0x7feedf5c(,%eax,8)
80106372:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106375:	0f b6 14 c5 a4 20 11 	movzbl -0x7feedf5c(,%eax,8),%edx
8010637c:	80 
8010637d:	83 e2 1f             	and    $0x1f,%edx
80106380:	88 14 c5 a4 20 11 80 	mov    %dl,-0x7feedf5c(,%eax,8)
80106387:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010638a:	0f b6 14 c5 a5 20 11 	movzbl -0x7feedf5b(,%eax,8),%edx
80106391:	80 
80106392:	83 e2 f0             	and    $0xfffffff0,%edx
80106395:	83 ca 0e             	or     $0xe,%edx
80106398:	88 14 c5 a5 20 11 80 	mov    %dl,-0x7feedf5b(,%eax,8)
8010639f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063a2:	0f b6 14 c5 a5 20 11 	movzbl -0x7feedf5b(,%eax,8),%edx
801063a9:	80 
801063aa:	83 e2 ef             	and    $0xffffffef,%edx
801063ad:	88 14 c5 a5 20 11 80 	mov    %dl,-0x7feedf5b(,%eax,8)
801063b4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063b7:	0f b6 14 c5 a5 20 11 	movzbl -0x7feedf5b(,%eax,8),%edx
801063be:	80 
801063bf:	83 e2 9f             	and    $0xffffff9f,%edx
801063c2:	88 14 c5 a5 20 11 80 	mov    %dl,-0x7feedf5b(,%eax,8)
801063c9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063cc:	0f b6 14 c5 a5 20 11 	movzbl -0x7feedf5b(,%eax,8),%edx
801063d3:	80 
801063d4:	83 ca 80             	or     $0xffffff80,%edx
801063d7:	88 14 c5 a5 20 11 80 	mov    %dl,-0x7feedf5b(,%eax,8)
801063de:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063e1:	8b 04 85 98 b0 10 80 	mov    -0x7fef4f68(,%eax,4),%eax
801063e8:	c1 e8 10             	shr    $0x10,%eax
801063eb:	89 c2                	mov    %eax,%edx
801063ed:	8b 45 f4             	mov    -0xc(%ebp),%eax
801063f0:	66 89 14 c5 a6 20 11 	mov    %dx,-0x7feedf5a(,%eax,8)
801063f7:	80 
void
tvinit(void)
{
  int i;

  for(i = 0; i < 256; i++)
801063f8:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801063fc:	81 7d f4 ff 00 00 00 	cmpl   $0xff,-0xc(%ebp)
80106403:	0f 8e 30 ff ff ff    	jle    80106339 <tvinit+0x12>
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80106409:	a1 98 b1 10 80       	mov    0x8010b198,%eax
8010640e:	66 a3 a0 22 11 80    	mov    %ax,0x801122a0
80106414:	66 c7 05 a2 22 11 80 	movw   $0x8,0x801122a2
8010641b:	08 00 
8010641d:	0f b6 05 a4 22 11 80 	movzbl 0x801122a4,%eax
80106424:	83 e0 e0             	and    $0xffffffe0,%eax
80106427:	a2 a4 22 11 80       	mov    %al,0x801122a4
8010642c:	0f b6 05 a4 22 11 80 	movzbl 0x801122a4,%eax
80106433:	83 e0 1f             	and    $0x1f,%eax
80106436:	a2 a4 22 11 80       	mov    %al,0x801122a4
8010643b:	0f b6 05 a5 22 11 80 	movzbl 0x801122a5,%eax
80106442:	83 c8 0f             	or     $0xf,%eax
80106445:	a2 a5 22 11 80       	mov    %al,0x801122a5
8010644a:	0f b6 05 a5 22 11 80 	movzbl 0x801122a5,%eax
80106451:	83 e0 ef             	and    $0xffffffef,%eax
80106454:	a2 a5 22 11 80       	mov    %al,0x801122a5
80106459:	0f b6 05 a5 22 11 80 	movzbl 0x801122a5,%eax
80106460:	83 c8 60             	or     $0x60,%eax
80106463:	a2 a5 22 11 80       	mov    %al,0x801122a5
80106468:	0f b6 05 a5 22 11 80 	movzbl 0x801122a5,%eax
8010646f:	83 c8 80             	or     $0xffffff80,%eax
80106472:	a2 a5 22 11 80       	mov    %al,0x801122a5
80106477:	a1 98 b1 10 80       	mov    0x8010b198,%eax
8010647c:	c1 e8 10             	shr    $0x10,%eax
8010647f:	66 a3 a6 22 11 80    	mov    %ax,0x801122a6
  
  initlock(&tickslock, "time");
80106485:	c7 44 24 04 8c 86 10 	movl   $0x8010868c,0x4(%esp)
8010648c:	80 
8010648d:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
80106494:	e8 79 e7 ff ff       	call   80104c12 <initlock>
}
80106499:	c9                   	leave  
8010649a:	c3                   	ret    

8010649b <idtinit>:

void
idtinit(void)
{
8010649b:	55                   	push   %ebp
8010649c:	89 e5                	mov    %esp,%ebp
8010649e:	83 ec 08             	sub    $0x8,%esp
  lidt(idt, sizeof(idt));
801064a1:	c7 44 24 04 00 08 00 	movl   $0x800,0x4(%esp)
801064a8:	00 
801064a9:	c7 04 24 a0 20 11 80 	movl   $0x801120a0,(%esp)
801064b0:	e8 33 fe ff ff       	call   801062e8 <lidt>
}
801064b5:	c9                   	leave  
801064b6:	c3                   	ret    

801064b7 <trap>:

//PAGEBREAK: 41
void
trap(struct trapframe *tf)
{
801064b7:	55                   	push   %ebp
801064b8:	89 e5                	mov    %esp,%ebp
801064ba:	57                   	push   %edi
801064bb:	56                   	push   %esi
801064bc:	53                   	push   %ebx
801064bd:	83 ec 3c             	sub    $0x3c,%esp
  if(tf->trapno == T_SYSCALL){
801064c0:	8b 45 08             	mov    0x8(%ebp),%eax
801064c3:	8b 40 30             	mov    0x30(%eax),%eax
801064c6:	83 f8 40             	cmp    $0x40,%eax
801064c9:	75 3e                	jne    80106509 <trap+0x52>
    if(proc->killed)
801064cb:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801064d1:	8b 40 24             	mov    0x24(%eax),%eax
801064d4:	85 c0                	test   %eax,%eax
801064d6:	74 05                	je     801064dd <trap+0x26>
      exit();
801064d8:	e8 08 e0 ff ff       	call   801044e5 <exit>
    proc->tf = tf;
801064dd:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801064e3:	8b 55 08             	mov    0x8(%ebp),%edx
801064e6:	89 50 18             	mov    %edx,0x18(%eax)
    syscall();
801064e9:	e8 c1 ed ff ff       	call   801052af <syscall>
    if(proc->killed)
801064ee:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801064f4:	8b 40 24             	mov    0x24(%eax),%eax
801064f7:	85 c0                	test   %eax,%eax
801064f9:	0f 84 34 02 00 00    	je     80106733 <trap+0x27c>
      exit();
801064ff:	e8 e1 df ff ff       	call   801044e5 <exit>
    return;
80106504:	e9 2a 02 00 00       	jmp    80106733 <trap+0x27c>
  }

  switch(tf->trapno){
80106509:	8b 45 08             	mov    0x8(%ebp),%eax
8010650c:	8b 40 30             	mov    0x30(%eax),%eax
8010650f:	83 e8 20             	sub    $0x20,%eax
80106512:	83 f8 1f             	cmp    $0x1f,%eax
80106515:	0f 87 bc 00 00 00    	ja     801065d7 <trap+0x120>
8010651b:	8b 04 85 34 87 10 80 	mov    -0x7fef78cc(,%eax,4),%eax
80106522:	ff e0                	jmp    *%eax
  case T_IRQ0 + IRQ_TIMER:
    if(cpu->id == 0){
80106524:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010652a:	0f b6 00             	movzbl (%eax),%eax
8010652d:	84 c0                	test   %al,%al
8010652f:	75 31                	jne    80106562 <trap+0xab>
      acquire(&tickslock);
80106531:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
80106538:	e8 f6 e6 ff ff       	call   80104c33 <acquire>
      ticks++;
8010653d:	a1 a0 28 11 80       	mov    0x801128a0,%eax
80106542:	83 c0 01             	add    $0x1,%eax
80106545:	a3 a0 28 11 80       	mov    %eax,0x801128a0
      wakeup(&ticks);
8010654a:	c7 04 24 a0 28 11 80 	movl   $0x801128a0,(%esp)
80106551:	e8 d8 e4 ff ff       	call   80104a2e <wakeup>
      release(&tickslock);
80106556:	c7 04 24 60 20 11 80 	movl   $0x80112060,(%esp)
8010655d:	e8 33 e7 ff ff       	call   80104c95 <release>
    }
    lapiceoi();
80106562:	e8 12 ca ff ff       	call   80102f79 <lapiceoi>
    break;
80106567:	e9 41 01 00 00       	jmp    801066ad <trap+0x1f6>
  case T_IRQ0 + IRQ_IDE:
    ideintr();
8010656c:	e8 10 c2 ff ff       	call   80102781 <ideintr>
    lapiceoi();
80106571:	e8 03 ca ff ff       	call   80102f79 <lapiceoi>
    break;
80106576:	e9 32 01 00 00       	jmp    801066ad <trap+0x1f6>
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
8010657b:	e8 d7 c7 ff ff       	call   80102d57 <kbdintr>
    lapiceoi();
80106580:	e8 f4 c9 ff ff       	call   80102f79 <lapiceoi>
    break;
80106585:	e9 23 01 00 00       	jmp    801066ad <trap+0x1f6>
  case T_IRQ0 + IRQ_COM1:
    uartintr();
8010658a:	e8 a9 03 00 00       	call   80106938 <uartintr>
    lapiceoi();
8010658f:	e8 e5 c9 ff ff       	call   80102f79 <lapiceoi>
    break;
80106594:	e9 14 01 00 00       	jmp    801066ad <trap+0x1f6>
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
            cpu->id, tf->cs, tf->eip);
80106599:	8b 45 08             	mov    0x8(%ebp),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
8010659c:	8b 48 38             	mov    0x38(%eax),%ecx
            cpu->id, tf->cs, tf->eip);
8010659f:	8b 45 08             	mov    0x8(%ebp),%eax
801065a2:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801065a6:	0f b7 d0             	movzwl %ax,%edx
            cpu->id, tf->cs, tf->eip);
801065a9:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
801065af:	0f b6 00             	movzbl (%eax),%eax
    uartintr();
    lapiceoi();
    break;
  case T_IRQ0 + 7:
  case T_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801065b2:	0f b6 c0             	movzbl %al,%eax
801065b5:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
801065b9:	89 54 24 08          	mov    %edx,0x8(%esp)
801065bd:	89 44 24 04          	mov    %eax,0x4(%esp)
801065c1:	c7 04 24 94 86 10 80 	movl   $0x80108694,(%esp)
801065c8:	e8 d4 9d ff ff       	call   801003a1 <cprintf>
            cpu->id, tf->cs, tf->eip);
    lapiceoi();
801065cd:	e8 a7 c9 ff ff       	call   80102f79 <lapiceoi>
    break;
801065d2:	e9 d6 00 00 00       	jmp    801066ad <trap+0x1f6>
   
  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
801065d7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801065dd:	85 c0                	test   %eax,%eax
801065df:	74 11                	je     801065f2 <trap+0x13b>
801065e1:	8b 45 08             	mov    0x8(%ebp),%eax
801065e4:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
801065e8:	0f b7 c0             	movzwl %ax,%eax
801065eb:	83 e0 03             	and    $0x3,%eax
801065ee:	85 c0                	test   %eax,%eax
801065f0:	75 46                	jne    80106638 <trap+0x181>
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
801065f2:	e8 1a fd ff ff       	call   80106311 <rcr2>
              tf->trapno, cpu->id, tf->eip, rcr2());
801065f7:	8b 55 08             	mov    0x8(%ebp),%edx
   
  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
801065fa:	8b 5a 38             	mov    0x38(%edx),%ebx
              tf->trapno, cpu->id, tf->eip, rcr2());
801065fd:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80106604:	0f b6 12             	movzbl (%edx),%edx
   
  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80106607:	0f b6 ca             	movzbl %dl,%ecx
              tf->trapno, cpu->id, tf->eip, rcr2());
8010660a:	8b 55 08             	mov    0x8(%ebp),%edx
   
  //PAGEBREAK: 13
  default:
    if(proc == 0 || (tf->cs&3) == 0){
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
8010660d:	8b 52 30             	mov    0x30(%edx),%edx
80106610:	89 44 24 10          	mov    %eax,0x10(%esp)
80106614:	89 5c 24 0c          	mov    %ebx,0xc(%esp)
80106618:	89 4c 24 08          	mov    %ecx,0x8(%esp)
8010661c:	89 54 24 04          	mov    %edx,0x4(%esp)
80106620:	c7 04 24 b8 86 10 80 	movl   $0x801086b8,(%esp)
80106627:	e8 75 9d ff ff       	call   801003a1 <cprintf>
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
8010662c:	c7 04 24 ea 86 10 80 	movl   $0x801086ea,(%esp)
80106633:	e8 05 9f ff ff       	call   8010053d <panic>
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106638:	e8 d4 fc ff ff       	call   80106311 <rcr2>
8010663d:	89 c2                	mov    %eax,%edx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
8010663f:	8b 45 08             	mov    0x8(%ebp),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106642:	8b 78 38             	mov    0x38(%eax),%edi
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
80106645:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
8010664b:	0f b6 00             	movzbl (%eax),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010664e:	0f b6 f0             	movzbl %al,%esi
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
80106651:	8b 45 08             	mov    0x8(%ebp),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80106654:	8b 58 34             	mov    0x34(%eax),%ebx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
80106657:	8b 45 08             	mov    0x8(%ebp),%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010665a:	8b 48 30             	mov    0x30(%eax),%ecx
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
8010665d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106663:	83 c0 6c             	add    $0x6c,%eax
80106666:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106669:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpu->id, tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010666f:	8b 40 10             	mov    0x10(%eax),%eax
80106672:	89 54 24 1c          	mov    %edx,0x1c(%esp)
80106676:	89 7c 24 18          	mov    %edi,0x18(%esp)
8010667a:	89 74 24 14          	mov    %esi,0x14(%esp)
8010667e:	89 5c 24 10          	mov    %ebx,0x10(%esp)
80106682:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
80106686:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80106689:	89 54 24 08          	mov    %edx,0x8(%esp)
8010668d:	89 44 24 04          	mov    %eax,0x4(%esp)
80106691:	c7 04 24 f0 86 10 80 	movl   $0x801086f0,(%esp)
80106698:	e8 04 9d ff ff       	call   801003a1 <cprintf>
            "eip 0x%x addr 0x%x--kill proc\n",
            proc->pid, proc->name, tf->trapno, tf->err, cpu->id, tf->eip, 
            rcr2());
    proc->killed = 1;
8010669d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801066a3:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
801066aa:	eb 01                	jmp    801066ad <trap+0x1f6>
    ideintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_IDE+1:
    // Bochs generates spurious IDE1 interrupts.
    break;
801066ac:	90                   	nop
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running 
  // until it gets to the regular system call return.)
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
801066ad:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801066b3:	85 c0                	test   %eax,%eax
801066b5:	74 24                	je     801066db <trap+0x224>
801066b7:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801066bd:	8b 40 24             	mov    0x24(%eax),%eax
801066c0:	85 c0                	test   %eax,%eax
801066c2:	74 17                	je     801066db <trap+0x224>
801066c4:	8b 45 08             	mov    0x8(%ebp),%eax
801066c7:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
801066cb:	0f b7 c0             	movzwl %ax,%eax
801066ce:	83 e0 03             	and    $0x3,%eax
801066d1:	83 f8 03             	cmp    $0x3,%eax
801066d4:	75 05                	jne    801066db <trap+0x224>
    exit();
801066d6:	e8 0a de ff ff       	call   801044e5 <exit>

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(proc && proc->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER)
801066db:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801066e1:	85 c0                	test   %eax,%eax
801066e3:	74 1e                	je     80106703 <trap+0x24c>
801066e5:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
801066eb:	8b 40 0c             	mov    0xc(%eax),%eax
801066ee:	83 f8 04             	cmp    $0x4,%eax
801066f1:	75 10                	jne    80106703 <trap+0x24c>
801066f3:	8b 45 08             	mov    0x8(%ebp),%eax
801066f6:	8b 40 30             	mov    0x30(%eax),%eax
801066f9:	83 f8 20             	cmp    $0x20,%eax
801066fc:	75 05                	jne    80106703 <trap+0x24c>
    yield();
801066fe:	e8 f4 e1 ff ff       	call   801048f7 <yield>

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
80106703:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106709:	85 c0                	test   %eax,%eax
8010670b:	74 27                	je     80106734 <trap+0x27d>
8010670d:	65 a1 04 00 00 00    	mov    %gs:0x4,%eax
80106713:	8b 40 24             	mov    0x24(%eax),%eax
80106716:	85 c0                	test   %eax,%eax
80106718:	74 1a                	je     80106734 <trap+0x27d>
8010671a:	8b 45 08             	mov    0x8(%ebp),%eax
8010671d:	0f b7 40 3c          	movzwl 0x3c(%eax),%eax
80106721:	0f b7 c0             	movzwl %ax,%eax
80106724:	83 e0 03             	and    $0x3,%eax
80106727:	83 f8 03             	cmp    $0x3,%eax
8010672a:	75 08                	jne    80106734 <trap+0x27d>
    exit();
8010672c:	e8 b4 dd ff ff       	call   801044e5 <exit>
80106731:	eb 01                	jmp    80106734 <trap+0x27d>
      exit();
    proc->tf = tf;
    syscall();
    if(proc->killed)
      exit();
    return;
80106733:	90                   	nop
    yield();

  // Check if the process has been killed since we yielded
  if(proc && proc->killed && (tf->cs&3) == DPL_USER)
    exit();
}
80106734:	83 c4 3c             	add    $0x3c,%esp
80106737:	5b                   	pop    %ebx
80106738:	5e                   	pop    %esi
80106739:	5f                   	pop    %edi
8010673a:	5d                   	pop    %ebp
8010673b:	c3                   	ret    

8010673c <inb>:
// Routines to let C code use special x86 instructions.

static inline uchar
inb(ushort port)
{
8010673c:	55                   	push   %ebp
8010673d:	89 e5                	mov    %esp,%ebp
8010673f:	53                   	push   %ebx
80106740:	83 ec 14             	sub    $0x14,%esp
80106743:	8b 45 08             	mov    0x8(%ebp),%eax
80106746:	66 89 45 e8          	mov    %ax,-0x18(%ebp)
  uchar data;

  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010674a:	0f b7 55 e8          	movzwl -0x18(%ebp),%edx
8010674e:	66 89 55 ea          	mov    %dx,-0x16(%ebp)
80106752:	0f b7 55 ea          	movzwl -0x16(%ebp),%edx
80106756:	ec                   	in     (%dx),%al
80106757:	89 c3                	mov    %eax,%ebx
80106759:	88 5d fb             	mov    %bl,-0x5(%ebp)
  return data;
8010675c:	0f b6 45 fb          	movzbl -0x5(%ebp),%eax
}
80106760:	83 c4 14             	add    $0x14,%esp
80106763:	5b                   	pop    %ebx
80106764:	5d                   	pop    %ebp
80106765:	c3                   	ret    

80106766 <outb>:
               "memory", "cc");
}

static inline void
outb(ushort port, uchar data)
{
80106766:	55                   	push   %ebp
80106767:	89 e5                	mov    %esp,%ebp
80106769:	83 ec 08             	sub    $0x8,%esp
8010676c:	8b 55 08             	mov    0x8(%ebp),%edx
8010676f:	8b 45 0c             	mov    0xc(%ebp),%eax
80106772:	66 89 55 fc          	mov    %dx,-0x4(%ebp)
80106776:	88 45 f8             	mov    %al,-0x8(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80106779:	0f b6 45 f8          	movzbl -0x8(%ebp),%eax
8010677d:	0f b7 55 fc          	movzwl -0x4(%ebp),%edx
80106781:	ee                   	out    %al,(%dx)
}
80106782:	c9                   	leave  
80106783:	c3                   	ret    

80106784 <uartinit>:

static int uart;    // is there a uart?

void
uartinit(void)
{
80106784:	55                   	push   %ebp
80106785:	89 e5                	mov    %esp,%ebp
80106787:	83 ec 28             	sub    $0x28,%esp
  char *p;

  // Turn off the FIFO
  outb(COM1+2, 0);
8010678a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80106791:	00 
80106792:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
80106799:	e8 c8 ff ff ff       	call   80106766 <outb>
  
  // 9600 baud, 8 data bits, 1 stop bit, parity off.
  outb(COM1+3, 0x80);    // Unlock divisor
8010679e:	c7 44 24 04 80 00 00 	movl   $0x80,0x4(%esp)
801067a5:	00 
801067a6:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
801067ad:	e8 b4 ff ff ff       	call   80106766 <outb>
  outb(COM1+0, 115200/9600);
801067b2:	c7 44 24 04 0c 00 00 	movl   $0xc,0x4(%esp)
801067b9:	00 
801067ba:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
801067c1:	e8 a0 ff ff ff       	call   80106766 <outb>
  outb(COM1+1, 0);
801067c6:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801067cd:	00 
801067ce:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
801067d5:	e8 8c ff ff ff       	call   80106766 <outb>
  outb(COM1+3, 0x03);    // Lock divisor, 8 data bits.
801067da:	c7 44 24 04 03 00 00 	movl   $0x3,0x4(%esp)
801067e1:	00 
801067e2:	c7 04 24 fb 03 00 00 	movl   $0x3fb,(%esp)
801067e9:	e8 78 ff ff ff       	call   80106766 <outb>
  outb(COM1+4, 0);
801067ee:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
801067f5:	00 
801067f6:	c7 04 24 fc 03 00 00 	movl   $0x3fc,(%esp)
801067fd:	e8 64 ff ff ff       	call   80106766 <outb>
  outb(COM1+1, 0x01);    // Enable receive interrupts.
80106802:	c7 44 24 04 01 00 00 	movl   $0x1,0x4(%esp)
80106809:	00 
8010680a:	c7 04 24 f9 03 00 00 	movl   $0x3f9,(%esp)
80106811:	e8 50 ff ff ff       	call   80106766 <outb>

  // If status is 0xFF, no serial port.
  if(inb(COM1+5) == 0xFF)
80106816:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
8010681d:	e8 1a ff ff ff       	call   8010673c <inb>
80106822:	3c ff                	cmp    $0xff,%al
80106824:	74 6c                	je     80106892 <uartinit+0x10e>
    return;
  uart = 1;
80106826:	c7 05 4c b6 10 80 01 	movl   $0x1,0x8010b64c
8010682d:	00 00 00 

  // Acknowledge pre-existing interrupt conditions;
  // enable interrupts.
  inb(COM1+2);
80106830:	c7 04 24 fa 03 00 00 	movl   $0x3fa,(%esp)
80106837:	e8 00 ff ff ff       	call   8010673c <inb>
  inb(COM1+0);
8010683c:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
80106843:	e8 f4 fe ff ff       	call   8010673c <inb>
  picenable(IRQ_COM1);
80106848:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
8010684f:	e8 fd d2 ff ff       	call   80103b51 <picenable>
  ioapicenable(IRQ_COM1, 0);
80106854:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
8010685b:	00 
8010685c:	c7 04 24 04 00 00 00 	movl   $0x4,(%esp)
80106863:	e8 9e c1 ff ff       	call   80102a06 <ioapicenable>
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
80106868:	c7 45 f4 b4 87 10 80 	movl   $0x801087b4,-0xc(%ebp)
8010686f:	eb 15                	jmp    80106886 <uartinit+0x102>
    uartputc(*p);
80106871:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106874:	0f b6 00             	movzbl (%eax),%eax
80106877:	0f be c0             	movsbl %al,%eax
8010687a:	89 04 24             	mov    %eax,(%esp)
8010687d:	e8 13 00 00 00       	call   80106895 <uartputc>
  inb(COM1+0);
  picenable(IRQ_COM1);
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
80106882:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80106886:	8b 45 f4             	mov    -0xc(%ebp),%eax
80106889:	0f b6 00             	movzbl (%eax),%eax
8010688c:	84 c0                	test   %al,%al
8010688e:	75 e1                	jne    80106871 <uartinit+0xed>
80106890:	eb 01                	jmp    80106893 <uartinit+0x10f>
  outb(COM1+4, 0);
  outb(COM1+1, 0x01);    // Enable receive interrupts.

  // If status is 0xFF, no serial port.
  if(inb(COM1+5) == 0xFF)
    return;
80106892:	90                   	nop
  ioapicenable(IRQ_COM1, 0);
  
  // Announce that we're here.
  for(p="xv6...\n"; *p; p++)
    uartputc(*p);
}
80106893:	c9                   	leave  
80106894:	c3                   	ret    

80106895 <uartputc>:

void
uartputc(int c)
{
80106895:	55                   	push   %ebp
80106896:	89 e5                	mov    %esp,%ebp
80106898:	83 ec 28             	sub    $0x28,%esp
  int i;

  if(!uart)
8010689b:	a1 4c b6 10 80       	mov    0x8010b64c,%eax
801068a0:	85 c0                	test   %eax,%eax
801068a2:	74 4d                	je     801068f1 <uartputc+0x5c>
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
801068a4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
801068ab:	eb 10                	jmp    801068bd <uartputc+0x28>
    microdelay(10);
801068ad:	c7 04 24 0a 00 00 00 	movl   $0xa,(%esp)
801068b4:	e8 e5 c6 ff ff       	call   80102f9e <microdelay>
{
  int i;

  if(!uart)
    return;
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
801068b9:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
801068bd:	83 7d f4 7f          	cmpl   $0x7f,-0xc(%ebp)
801068c1:	7f 16                	jg     801068d9 <uartputc+0x44>
801068c3:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
801068ca:	e8 6d fe ff ff       	call   8010673c <inb>
801068cf:	0f b6 c0             	movzbl %al,%eax
801068d2:	83 e0 20             	and    $0x20,%eax
801068d5:	85 c0                	test   %eax,%eax
801068d7:	74 d4                	je     801068ad <uartputc+0x18>
    microdelay(10);
  outb(COM1+0, c);
801068d9:	8b 45 08             	mov    0x8(%ebp),%eax
801068dc:	0f b6 c0             	movzbl %al,%eax
801068df:	89 44 24 04          	mov    %eax,0x4(%esp)
801068e3:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
801068ea:	e8 77 fe ff ff       	call   80106766 <outb>
801068ef:	eb 01                	jmp    801068f2 <uartputc+0x5d>
uartputc(int c)
{
  int i;

  if(!uart)
    return;
801068f1:	90                   	nop
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
    microdelay(10);
  outb(COM1+0, c);
}
801068f2:	c9                   	leave  
801068f3:	c3                   	ret    

801068f4 <uartgetc>:

static int
uartgetc(void)
{
801068f4:	55                   	push   %ebp
801068f5:	89 e5                	mov    %esp,%ebp
801068f7:	83 ec 04             	sub    $0x4,%esp
  if(!uart)
801068fa:	a1 4c b6 10 80       	mov    0x8010b64c,%eax
801068ff:	85 c0                	test   %eax,%eax
80106901:	75 07                	jne    8010690a <uartgetc+0x16>
    return -1;
80106903:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106908:	eb 2c                	jmp    80106936 <uartgetc+0x42>
  if(!(inb(COM1+5) & 0x01))
8010690a:	c7 04 24 fd 03 00 00 	movl   $0x3fd,(%esp)
80106911:	e8 26 fe ff ff       	call   8010673c <inb>
80106916:	0f b6 c0             	movzbl %al,%eax
80106919:	83 e0 01             	and    $0x1,%eax
8010691c:	85 c0                	test   %eax,%eax
8010691e:	75 07                	jne    80106927 <uartgetc+0x33>
    return -1;
80106920:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106925:	eb 0f                	jmp    80106936 <uartgetc+0x42>
  return inb(COM1+0);
80106927:	c7 04 24 f8 03 00 00 	movl   $0x3f8,(%esp)
8010692e:	e8 09 fe ff ff       	call   8010673c <inb>
80106933:	0f b6 c0             	movzbl %al,%eax
}
80106936:	c9                   	leave  
80106937:	c3                   	ret    

80106938 <uartintr>:

void
uartintr(void)
{
80106938:	55                   	push   %ebp
80106939:	89 e5                	mov    %esp,%ebp
8010693b:	83 ec 18             	sub    $0x18,%esp
  consoleintr(uartgetc);
8010693e:	c7 04 24 f4 68 10 80 	movl   $0x801068f4,(%esp)
80106945:	e8 f2 9e ff ff       	call   8010083c <consoleintr>
}
8010694a:	c9                   	leave  
8010694b:	c3                   	ret    

8010694c <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
8010694c:	6a 00                	push   $0x0
  pushl $0
8010694e:	6a 00                	push   $0x0
  jmp alltraps
80106950:	e9 67 f9 ff ff       	jmp    801062bc <alltraps>

80106955 <vector1>:
.globl vector1
vector1:
  pushl $0
80106955:	6a 00                	push   $0x0
  pushl $1
80106957:	6a 01                	push   $0x1
  jmp alltraps
80106959:	e9 5e f9 ff ff       	jmp    801062bc <alltraps>

8010695e <vector2>:
.globl vector2
vector2:
  pushl $0
8010695e:	6a 00                	push   $0x0
  pushl $2
80106960:	6a 02                	push   $0x2
  jmp alltraps
80106962:	e9 55 f9 ff ff       	jmp    801062bc <alltraps>

80106967 <vector3>:
.globl vector3
vector3:
  pushl $0
80106967:	6a 00                	push   $0x0
  pushl $3
80106969:	6a 03                	push   $0x3
  jmp alltraps
8010696b:	e9 4c f9 ff ff       	jmp    801062bc <alltraps>

80106970 <vector4>:
.globl vector4
vector4:
  pushl $0
80106970:	6a 00                	push   $0x0
  pushl $4
80106972:	6a 04                	push   $0x4
  jmp alltraps
80106974:	e9 43 f9 ff ff       	jmp    801062bc <alltraps>

80106979 <vector5>:
.globl vector5
vector5:
  pushl $0
80106979:	6a 00                	push   $0x0
  pushl $5
8010697b:	6a 05                	push   $0x5
  jmp alltraps
8010697d:	e9 3a f9 ff ff       	jmp    801062bc <alltraps>

80106982 <vector6>:
.globl vector6
vector6:
  pushl $0
80106982:	6a 00                	push   $0x0
  pushl $6
80106984:	6a 06                	push   $0x6
  jmp alltraps
80106986:	e9 31 f9 ff ff       	jmp    801062bc <alltraps>

8010698b <vector7>:
.globl vector7
vector7:
  pushl $0
8010698b:	6a 00                	push   $0x0
  pushl $7
8010698d:	6a 07                	push   $0x7
  jmp alltraps
8010698f:	e9 28 f9 ff ff       	jmp    801062bc <alltraps>

80106994 <vector8>:
.globl vector8
vector8:
  pushl $8
80106994:	6a 08                	push   $0x8
  jmp alltraps
80106996:	e9 21 f9 ff ff       	jmp    801062bc <alltraps>

8010699b <vector9>:
.globl vector9
vector9:
  pushl $0
8010699b:	6a 00                	push   $0x0
  pushl $9
8010699d:	6a 09                	push   $0x9
  jmp alltraps
8010699f:	e9 18 f9 ff ff       	jmp    801062bc <alltraps>

801069a4 <vector10>:
.globl vector10
vector10:
  pushl $10
801069a4:	6a 0a                	push   $0xa
  jmp alltraps
801069a6:	e9 11 f9 ff ff       	jmp    801062bc <alltraps>

801069ab <vector11>:
.globl vector11
vector11:
  pushl $11
801069ab:	6a 0b                	push   $0xb
  jmp alltraps
801069ad:	e9 0a f9 ff ff       	jmp    801062bc <alltraps>

801069b2 <vector12>:
.globl vector12
vector12:
  pushl $12
801069b2:	6a 0c                	push   $0xc
  jmp alltraps
801069b4:	e9 03 f9 ff ff       	jmp    801062bc <alltraps>

801069b9 <vector13>:
.globl vector13
vector13:
  pushl $13
801069b9:	6a 0d                	push   $0xd
  jmp alltraps
801069bb:	e9 fc f8 ff ff       	jmp    801062bc <alltraps>

801069c0 <vector14>:
.globl vector14
vector14:
  pushl $14
801069c0:	6a 0e                	push   $0xe
  jmp alltraps
801069c2:	e9 f5 f8 ff ff       	jmp    801062bc <alltraps>

801069c7 <vector15>:
.globl vector15
vector15:
  pushl $0
801069c7:	6a 00                	push   $0x0
  pushl $15
801069c9:	6a 0f                	push   $0xf
  jmp alltraps
801069cb:	e9 ec f8 ff ff       	jmp    801062bc <alltraps>

801069d0 <vector16>:
.globl vector16
vector16:
  pushl $0
801069d0:	6a 00                	push   $0x0
  pushl $16
801069d2:	6a 10                	push   $0x10
  jmp alltraps
801069d4:	e9 e3 f8 ff ff       	jmp    801062bc <alltraps>

801069d9 <vector17>:
.globl vector17
vector17:
  pushl $17
801069d9:	6a 11                	push   $0x11
  jmp alltraps
801069db:	e9 dc f8 ff ff       	jmp    801062bc <alltraps>

801069e0 <vector18>:
.globl vector18
vector18:
  pushl $0
801069e0:	6a 00                	push   $0x0
  pushl $18
801069e2:	6a 12                	push   $0x12
  jmp alltraps
801069e4:	e9 d3 f8 ff ff       	jmp    801062bc <alltraps>

801069e9 <vector19>:
.globl vector19
vector19:
  pushl $0
801069e9:	6a 00                	push   $0x0
  pushl $19
801069eb:	6a 13                	push   $0x13
  jmp alltraps
801069ed:	e9 ca f8 ff ff       	jmp    801062bc <alltraps>

801069f2 <vector20>:
.globl vector20
vector20:
  pushl $0
801069f2:	6a 00                	push   $0x0
  pushl $20
801069f4:	6a 14                	push   $0x14
  jmp alltraps
801069f6:	e9 c1 f8 ff ff       	jmp    801062bc <alltraps>

801069fb <vector21>:
.globl vector21
vector21:
  pushl $0
801069fb:	6a 00                	push   $0x0
  pushl $21
801069fd:	6a 15                	push   $0x15
  jmp alltraps
801069ff:	e9 b8 f8 ff ff       	jmp    801062bc <alltraps>

80106a04 <vector22>:
.globl vector22
vector22:
  pushl $0
80106a04:	6a 00                	push   $0x0
  pushl $22
80106a06:	6a 16                	push   $0x16
  jmp alltraps
80106a08:	e9 af f8 ff ff       	jmp    801062bc <alltraps>

80106a0d <vector23>:
.globl vector23
vector23:
  pushl $0
80106a0d:	6a 00                	push   $0x0
  pushl $23
80106a0f:	6a 17                	push   $0x17
  jmp alltraps
80106a11:	e9 a6 f8 ff ff       	jmp    801062bc <alltraps>

80106a16 <vector24>:
.globl vector24
vector24:
  pushl $0
80106a16:	6a 00                	push   $0x0
  pushl $24
80106a18:	6a 18                	push   $0x18
  jmp alltraps
80106a1a:	e9 9d f8 ff ff       	jmp    801062bc <alltraps>

80106a1f <vector25>:
.globl vector25
vector25:
  pushl $0
80106a1f:	6a 00                	push   $0x0
  pushl $25
80106a21:	6a 19                	push   $0x19
  jmp alltraps
80106a23:	e9 94 f8 ff ff       	jmp    801062bc <alltraps>

80106a28 <vector26>:
.globl vector26
vector26:
  pushl $0
80106a28:	6a 00                	push   $0x0
  pushl $26
80106a2a:	6a 1a                	push   $0x1a
  jmp alltraps
80106a2c:	e9 8b f8 ff ff       	jmp    801062bc <alltraps>

80106a31 <vector27>:
.globl vector27
vector27:
  pushl $0
80106a31:	6a 00                	push   $0x0
  pushl $27
80106a33:	6a 1b                	push   $0x1b
  jmp alltraps
80106a35:	e9 82 f8 ff ff       	jmp    801062bc <alltraps>

80106a3a <vector28>:
.globl vector28
vector28:
  pushl $0
80106a3a:	6a 00                	push   $0x0
  pushl $28
80106a3c:	6a 1c                	push   $0x1c
  jmp alltraps
80106a3e:	e9 79 f8 ff ff       	jmp    801062bc <alltraps>

80106a43 <vector29>:
.globl vector29
vector29:
  pushl $0
80106a43:	6a 00                	push   $0x0
  pushl $29
80106a45:	6a 1d                	push   $0x1d
  jmp alltraps
80106a47:	e9 70 f8 ff ff       	jmp    801062bc <alltraps>

80106a4c <vector30>:
.globl vector30
vector30:
  pushl $0
80106a4c:	6a 00                	push   $0x0
  pushl $30
80106a4e:	6a 1e                	push   $0x1e
  jmp alltraps
80106a50:	e9 67 f8 ff ff       	jmp    801062bc <alltraps>

80106a55 <vector31>:
.globl vector31
vector31:
  pushl $0
80106a55:	6a 00                	push   $0x0
  pushl $31
80106a57:	6a 1f                	push   $0x1f
  jmp alltraps
80106a59:	e9 5e f8 ff ff       	jmp    801062bc <alltraps>

80106a5e <vector32>:
.globl vector32
vector32:
  pushl $0
80106a5e:	6a 00                	push   $0x0
  pushl $32
80106a60:	6a 20                	push   $0x20
  jmp alltraps
80106a62:	e9 55 f8 ff ff       	jmp    801062bc <alltraps>

80106a67 <vector33>:
.globl vector33
vector33:
  pushl $0
80106a67:	6a 00                	push   $0x0
  pushl $33
80106a69:	6a 21                	push   $0x21
  jmp alltraps
80106a6b:	e9 4c f8 ff ff       	jmp    801062bc <alltraps>

80106a70 <vector34>:
.globl vector34
vector34:
  pushl $0
80106a70:	6a 00                	push   $0x0
  pushl $34
80106a72:	6a 22                	push   $0x22
  jmp alltraps
80106a74:	e9 43 f8 ff ff       	jmp    801062bc <alltraps>

80106a79 <vector35>:
.globl vector35
vector35:
  pushl $0
80106a79:	6a 00                	push   $0x0
  pushl $35
80106a7b:	6a 23                	push   $0x23
  jmp alltraps
80106a7d:	e9 3a f8 ff ff       	jmp    801062bc <alltraps>

80106a82 <vector36>:
.globl vector36
vector36:
  pushl $0
80106a82:	6a 00                	push   $0x0
  pushl $36
80106a84:	6a 24                	push   $0x24
  jmp alltraps
80106a86:	e9 31 f8 ff ff       	jmp    801062bc <alltraps>

80106a8b <vector37>:
.globl vector37
vector37:
  pushl $0
80106a8b:	6a 00                	push   $0x0
  pushl $37
80106a8d:	6a 25                	push   $0x25
  jmp alltraps
80106a8f:	e9 28 f8 ff ff       	jmp    801062bc <alltraps>

80106a94 <vector38>:
.globl vector38
vector38:
  pushl $0
80106a94:	6a 00                	push   $0x0
  pushl $38
80106a96:	6a 26                	push   $0x26
  jmp alltraps
80106a98:	e9 1f f8 ff ff       	jmp    801062bc <alltraps>

80106a9d <vector39>:
.globl vector39
vector39:
  pushl $0
80106a9d:	6a 00                	push   $0x0
  pushl $39
80106a9f:	6a 27                	push   $0x27
  jmp alltraps
80106aa1:	e9 16 f8 ff ff       	jmp    801062bc <alltraps>

80106aa6 <vector40>:
.globl vector40
vector40:
  pushl $0
80106aa6:	6a 00                	push   $0x0
  pushl $40
80106aa8:	6a 28                	push   $0x28
  jmp alltraps
80106aaa:	e9 0d f8 ff ff       	jmp    801062bc <alltraps>

80106aaf <vector41>:
.globl vector41
vector41:
  pushl $0
80106aaf:	6a 00                	push   $0x0
  pushl $41
80106ab1:	6a 29                	push   $0x29
  jmp alltraps
80106ab3:	e9 04 f8 ff ff       	jmp    801062bc <alltraps>

80106ab8 <vector42>:
.globl vector42
vector42:
  pushl $0
80106ab8:	6a 00                	push   $0x0
  pushl $42
80106aba:	6a 2a                	push   $0x2a
  jmp alltraps
80106abc:	e9 fb f7 ff ff       	jmp    801062bc <alltraps>

80106ac1 <vector43>:
.globl vector43
vector43:
  pushl $0
80106ac1:	6a 00                	push   $0x0
  pushl $43
80106ac3:	6a 2b                	push   $0x2b
  jmp alltraps
80106ac5:	e9 f2 f7 ff ff       	jmp    801062bc <alltraps>

80106aca <vector44>:
.globl vector44
vector44:
  pushl $0
80106aca:	6a 00                	push   $0x0
  pushl $44
80106acc:	6a 2c                	push   $0x2c
  jmp alltraps
80106ace:	e9 e9 f7 ff ff       	jmp    801062bc <alltraps>

80106ad3 <vector45>:
.globl vector45
vector45:
  pushl $0
80106ad3:	6a 00                	push   $0x0
  pushl $45
80106ad5:	6a 2d                	push   $0x2d
  jmp alltraps
80106ad7:	e9 e0 f7 ff ff       	jmp    801062bc <alltraps>

80106adc <vector46>:
.globl vector46
vector46:
  pushl $0
80106adc:	6a 00                	push   $0x0
  pushl $46
80106ade:	6a 2e                	push   $0x2e
  jmp alltraps
80106ae0:	e9 d7 f7 ff ff       	jmp    801062bc <alltraps>

80106ae5 <vector47>:
.globl vector47
vector47:
  pushl $0
80106ae5:	6a 00                	push   $0x0
  pushl $47
80106ae7:	6a 2f                	push   $0x2f
  jmp alltraps
80106ae9:	e9 ce f7 ff ff       	jmp    801062bc <alltraps>

80106aee <vector48>:
.globl vector48
vector48:
  pushl $0
80106aee:	6a 00                	push   $0x0
  pushl $48
80106af0:	6a 30                	push   $0x30
  jmp alltraps
80106af2:	e9 c5 f7 ff ff       	jmp    801062bc <alltraps>

80106af7 <vector49>:
.globl vector49
vector49:
  pushl $0
80106af7:	6a 00                	push   $0x0
  pushl $49
80106af9:	6a 31                	push   $0x31
  jmp alltraps
80106afb:	e9 bc f7 ff ff       	jmp    801062bc <alltraps>

80106b00 <vector50>:
.globl vector50
vector50:
  pushl $0
80106b00:	6a 00                	push   $0x0
  pushl $50
80106b02:	6a 32                	push   $0x32
  jmp alltraps
80106b04:	e9 b3 f7 ff ff       	jmp    801062bc <alltraps>

80106b09 <vector51>:
.globl vector51
vector51:
  pushl $0
80106b09:	6a 00                	push   $0x0
  pushl $51
80106b0b:	6a 33                	push   $0x33
  jmp alltraps
80106b0d:	e9 aa f7 ff ff       	jmp    801062bc <alltraps>

80106b12 <vector52>:
.globl vector52
vector52:
  pushl $0
80106b12:	6a 00                	push   $0x0
  pushl $52
80106b14:	6a 34                	push   $0x34
  jmp alltraps
80106b16:	e9 a1 f7 ff ff       	jmp    801062bc <alltraps>

80106b1b <vector53>:
.globl vector53
vector53:
  pushl $0
80106b1b:	6a 00                	push   $0x0
  pushl $53
80106b1d:	6a 35                	push   $0x35
  jmp alltraps
80106b1f:	e9 98 f7 ff ff       	jmp    801062bc <alltraps>

80106b24 <vector54>:
.globl vector54
vector54:
  pushl $0
80106b24:	6a 00                	push   $0x0
  pushl $54
80106b26:	6a 36                	push   $0x36
  jmp alltraps
80106b28:	e9 8f f7 ff ff       	jmp    801062bc <alltraps>

80106b2d <vector55>:
.globl vector55
vector55:
  pushl $0
80106b2d:	6a 00                	push   $0x0
  pushl $55
80106b2f:	6a 37                	push   $0x37
  jmp alltraps
80106b31:	e9 86 f7 ff ff       	jmp    801062bc <alltraps>

80106b36 <vector56>:
.globl vector56
vector56:
  pushl $0
80106b36:	6a 00                	push   $0x0
  pushl $56
80106b38:	6a 38                	push   $0x38
  jmp alltraps
80106b3a:	e9 7d f7 ff ff       	jmp    801062bc <alltraps>

80106b3f <vector57>:
.globl vector57
vector57:
  pushl $0
80106b3f:	6a 00                	push   $0x0
  pushl $57
80106b41:	6a 39                	push   $0x39
  jmp alltraps
80106b43:	e9 74 f7 ff ff       	jmp    801062bc <alltraps>

80106b48 <vector58>:
.globl vector58
vector58:
  pushl $0
80106b48:	6a 00                	push   $0x0
  pushl $58
80106b4a:	6a 3a                	push   $0x3a
  jmp alltraps
80106b4c:	e9 6b f7 ff ff       	jmp    801062bc <alltraps>

80106b51 <vector59>:
.globl vector59
vector59:
  pushl $0
80106b51:	6a 00                	push   $0x0
  pushl $59
80106b53:	6a 3b                	push   $0x3b
  jmp alltraps
80106b55:	e9 62 f7 ff ff       	jmp    801062bc <alltraps>

80106b5a <vector60>:
.globl vector60
vector60:
  pushl $0
80106b5a:	6a 00                	push   $0x0
  pushl $60
80106b5c:	6a 3c                	push   $0x3c
  jmp alltraps
80106b5e:	e9 59 f7 ff ff       	jmp    801062bc <alltraps>

80106b63 <vector61>:
.globl vector61
vector61:
  pushl $0
80106b63:	6a 00                	push   $0x0
  pushl $61
80106b65:	6a 3d                	push   $0x3d
  jmp alltraps
80106b67:	e9 50 f7 ff ff       	jmp    801062bc <alltraps>

80106b6c <vector62>:
.globl vector62
vector62:
  pushl $0
80106b6c:	6a 00                	push   $0x0
  pushl $62
80106b6e:	6a 3e                	push   $0x3e
  jmp alltraps
80106b70:	e9 47 f7 ff ff       	jmp    801062bc <alltraps>

80106b75 <vector63>:
.globl vector63
vector63:
  pushl $0
80106b75:	6a 00                	push   $0x0
  pushl $63
80106b77:	6a 3f                	push   $0x3f
  jmp alltraps
80106b79:	e9 3e f7 ff ff       	jmp    801062bc <alltraps>

80106b7e <vector64>:
.globl vector64
vector64:
  pushl $0
80106b7e:	6a 00                	push   $0x0
  pushl $64
80106b80:	6a 40                	push   $0x40
  jmp alltraps
80106b82:	e9 35 f7 ff ff       	jmp    801062bc <alltraps>

80106b87 <vector65>:
.globl vector65
vector65:
  pushl $0
80106b87:	6a 00                	push   $0x0
  pushl $65
80106b89:	6a 41                	push   $0x41
  jmp alltraps
80106b8b:	e9 2c f7 ff ff       	jmp    801062bc <alltraps>

80106b90 <vector66>:
.globl vector66
vector66:
  pushl $0
80106b90:	6a 00                	push   $0x0
  pushl $66
80106b92:	6a 42                	push   $0x42
  jmp alltraps
80106b94:	e9 23 f7 ff ff       	jmp    801062bc <alltraps>

80106b99 <vector67>:
.globl vector67
vector67:
  pushl $0
80106b99:	6a 00                	push   $0x0
  pushl $67
80106b9b:	6a 43                	push   $0x43
  jmp alltraps
80106b9d:	e9 1a f7 ff ff       	jmp    801062bc <alltraps>

80106ba2 <vector68>:
.globl vector68
vector68:
  pushl $0
80106ba2:	6a 00                	push   $0x0
  pushl $68
80106ba4:	6a 44                	push   $0x44
  jmp alltraps
80106ba6:	e9 11 f7 ff ff       	jmp    801062bc <alltraps>

80106bab <vector69>:
.globl vector69
vector69:
  pushl $0
80106bab:	6a 00                	push   $0x0
  pushl $69
80106bad:	6a 45                	push   $0x45
  jmp alltraps
80106baf:	e9 08 f7 ff ff       	jmp    801062bc <alltraps>

80106bb4 <vector70>:
.globl vector70
vector70:
  pushl $0
80106bb4:	6a 00                	push   $0x0
  pushl $70
80106bb6:	6a 46                	push   $0x46
  jmp alltraps
80106bb8:	e9 ff f6 ff ff       	jmp    801062bc <alltraps>

80106bbd <vector71>:
.globl vector71
vector71:
  pushl $0
80106bbd:	6a 00                	push   $0x0
  pushl $71
80106bbf:	6a 47                	push   $0x47
  jmp alltraps
80106bc1:	e9 f6 f6 ff ff       	jmp    801062bc <alltraps>

80106bc6 <vector72>:
.globl vector72
vector72:
  pushl $0
80106bc6:	6a 00                	push   $0x0
  pushl $72
80106bc8:	6a 48                	push   $0x48
  jmp alltraps
80106bca:	e9 ed f6 ff ff       	jmp    801062bc <alltraps>

80106bcf <vector73>:
.globl vector73
vector73:
  pushl $0
80106bcf:	6a 00                	push   $0x0
  pushl $73
80106bd1:	6a 49                	push   $0x49
  jmp alltraps
80106bd3:	e9 e4 f6 ff ff       	jmp    801062bc <alltraps>

80106bd8 <vector74>:
.globl vector74
vector74:
  pushl $0
80106bd8:	6a 00                	push   $0x0
  pushl $74
80106bda:	6a 4a                	push   $0x4a
  jmp alltraps
80106bdc:	e9 db f6 ff ff       	jmp    801062bc <alltraps>

80106be1 <vector75>:
.globl vector75
vector75:
  pushl $0
80106be1:	6a 00                	push   $0x0
  pushl $75
80106be3:	6a 4b                	push   $0x4b
  jmp alltraps
80106be5:	e9 d2 f6 ff ff       	jmp    801062bc <alltraps>

80106bea <vector76>:
.globl vector76
vector76:
  pushl $0
80106bea:	6a 00                	push   $0x0
  pushl $76
80106bec:	6a 4c                	push   $0x4c
  jmp alltraps
80106bee:	e9 c9 f6 ff ff       	jmp    801062bc <alltraps>

80106bf3 <vector77>:
.globl vector77
vector77:
  pushl $0
80106bf3:	6a 00                	push   $0x0
  pushl $77
80106bf5:	6a 4d                	push   $0x4d
  jmp alltraps
80106bf7:	e9 c0 f6 ff ff       	jmp    801062bc <alltraps>

80106bfc <vector78>:
.globl vector78
vector78:
  pushl $0
80106bfc:	6a 00                	push   $0x0
  pushl $78
80106bfe:	6a 4e                	push   $0x4e
  jmp alltraps
80106c00:	e9 b7 f6 ff ff       	jmp    801062bc <alltraps>

80106c05 <vector79>:
.globl vector79
vector79:
  pushl $0
80106c05:	6a 00                	push   $0x0
  pushl $79
80106c07:	6a 4f                	push   $0x4f
  jmp alltraps
80106c09:	e9 ae f6 ff ff       	jmp    801062bc <alltraps>

80106c0e <vector80>:
.globl vector80
vector80:
  pushl $0
80106c0e:	6a 00                	push   $0x0
  pushl $80
80106c10:	6a 50                	push   $0x50
  jmp alltraps
80106c12:	e9 a5 f6 ff ff       	jmp    801062bc <alltraps>

80106c17 <vector81>:
.globl vector81
vector81:
  pushl $0
80106c17:	6a 00                	push   $0x0
  pushl $81
80106c19:	6a 51                	push   $0x51
  jmp alltraps
80106c1b:	e9 9c f6 ff ff       	jmp    801062bc <alltraps>

80106c20 <vector82>:
.globl vector82
vector82:
  pushl $0
80106c20:	6a 00                	push   $0x0
  pushl $82
80106c22:	6a 52                	push   $0x52
  jmp alltraps
80106c24:	e9 93 f6 ff ff       	jmp    801062bc <alltraps>

80106c29 <vector83>:
.globl vector83
vector83:
  pushl $0
80106c29:	6a 00                	push   $0x0
  pushl $83
80106c2b:	6a 53                	push   $0x53
  jmp alltraps
80106c2d:	e9 8a f6 ff ff       	jmp    801062bc <alltraps>

80106c32 <vector84>:
.globl vector84
vector84:
  pushl $0
80106c32:	6a 00                	push   $0x0
  pushl $84
80106c34:	6a 54                	push   $0x54
  jmp alltraps
80106c36:	e9 81 f6 ff ff       	jmp    801062bc <alltraps>

80106c3b <vector85>:
.globl vector85
vector85:
  pushl $0
80106c3b:	6a 00                	push   $0x0
  pushl $85
80106c3d:	6a 55                	push   $0x55
  jmp alltraps
80106c3f:	e9 78 f6 ff ff       	jmp    801062bc <alltraps>

80106c44 <vector86>:
.globl vector86
vector86:
  pushl $0
80106c44:	6a 00                	push   $0x0
  pushl $86
80106c46:	6a 56                	push   $0x56
  jmp alltraps
80106c48:	e9 6f f6 ff ff       	jmp    801062bc <alltraps>

80106c4d <vector87>:
.globl vector87
vector87:
  pushl $0
80106c4d:	6a 00                	push   $0x0
  pushl $87
80106c4f:	6a 57                	push   $0x57
  jmp alltraps
80106c51:	e9 66 f6 ff ff       	jmp    801062bc <alltraps>

80106c56 <vector88>:
.globl vector88
vector88:
  pushl $0
80106c56:	6a 00                	push   $0x0
  pushl $88
80106c58:	6a 58                	push   $0x58
  jmp alltraps
80106c5a:	e9 5d f6 ff ff       	jmp    801062bc <alltraps>

80106c5f <vector89>:
.globl vector89
vector89:
  pushl $0
80106c5f:	6a 00                	push   $0x0
  pushl $89
80106c61:	6a 59                	push   $0x59
  jmp alltraps
80106c63:	e9 54 f6 ff ff       	jmp    801062bc <alltraps>

80106c68 <vector90>:
.globl vector90
vector90:
  pushl $0
80106c68:	6a 00                	push   $0x0
  pushl $90
80106c6a:	6a 5a                	push   $0x5a
  jmp alltraps
80106c6c:	e9 4b f6 ff ff       	jmp    801062bc <alltraps>

80106c71 <vector91>:
.globl vector91
vector91:
  pushl $0
80106c71:	6a 00                	push   $0x0
  pushl $91
80106c73:	6a 5b                	push   $0x5b
  jmp alltraps
80106c75:	e9 42 f6 ff ff       	jmp    801062bc <alltraps>

80106c7a <vector92>:
.globl vector92
vector92:
  pushl $0
80106c7a:	6a 00                	push   $0x0
  pushl $92
80106c7c:	6a 5c                	push   $0x5c
  jmp alltraps
80106c7e:	e9 39 f6 ff ff       	jmp    801062bc <alltraps>

80106c83 <vector93>:
.globl vector93
vector93:
  pushl $0
80106c83:	6a 00                	push   $0x0
  pushl $93
80106c85:	6a 5d                	push   $0x5d
  jmp alltraps
80106c87:	e9 30 f6 ff ff       	jmp    801062bc <alltraps>

80106c8c <vector94>:
.globl vector94
vector94:
  pushl $0
80106c8c:	6a 00                	push   $0x0
  pushl $94
80106c8e:	6a 5e                	push   $0x5e
  jmp alltraps
80106c90:	e9 27 f6 ff ff       	jmp    801062bc <alltraps>

80106c95 <vector95>:
.globl vector95
vector95:
  pushl $0
80106c95:	6a 00                	push   $0x0
  pushl $95
80106c97:	6a 5f                	push   $0x5f
  jmp alltraps
80106c99:	e9 1e f6 ff ff       	jmp    801062bc <alltraps>

80106c9e <vector96>:
.globl vector96
vector96:
  pushl $0
80106c9e:	6a 00                	push   $0x0
  pushl $96
80106ca0:	6a 60                	push   $0x60
  jmp alltraps
80106ca2:	e9 15 f6 ff ff       	jmp    801062bc <alltraps>

80106ca7 <vector97>:
.globl vector97
vector97:
  pushl $0
80106ca7:	6a 00                	push   $0x0
  pushl $97
80106ca9:	6a 61                	push   $0x61
  jmp alltraps
80106cab:	e9 0c f6 ff ff       	jmp    801062bc <alltraps>

80106cb0 <vector98>:
.globl vector98
vector98:
  pushl $0
80106cb0:	6a 00                	push   $0x0
  pushl $98
80106cb2:	6a 62                	push   $0x62
  jmp alltraps
80106cb4:	e9 03 f6 ff ff       	jmp    801062bc <alltraps>

80106cb9 <vector99>:
.globl vector99
vector99:
  pushl $0
80106cb9:	6a 00                	push   $0x0
  pushl $99
80106cbb:	6a 63                	push   $0x63
  jmp alltraps
80106cbd:	e9 fa f5 ff ff       	jmp    801062bc <alltraps>

80106cc2 <vector100>:
.globl vector100
vector100:
  pushl $0
80106cc2:	6a 00                	push   $0x0
  pushl $100
80106cc4:	6a 64                	push   $0x64
  jmp alltraps
80106cc6:	e9 f1 f5 ff ff       	jmp    801062bc <alltraps>

80106ccb <vector101>:
.globl vector101
vector101:
  pushl $0
80106ccb:	6a 00                	push   $0x0
  pushl $101
80106ccd:	6a 65                	push   $0x65
  jmp alltraps
80106ccf:	e9 e8 f5 ff ff       	jmp    801062bc <alltraps>

80106cd4 <vector102>:
.globl vector102
vector102:
  pushl $0
80106cd4:	6a 00                	push   $0x0
  pushl $102
80106cd6:	6a 66                	push   $0x66
  jmp alltraps
80106cd8:	e9 df f5 ff ff       	jmp    801062bc <alltraps>

80106cdd <vector103>:
.globl vector103
vector103:
  pushl $0
80106cdd:	6a 00                	push   $0x0
  pushl $103
80106cdf:	6a 67                	push   $0x67
  jmp alltraps
80106ce1:	e9 d6 f5 ff ff       	jmp    801062bc <alltraps>

80106ce6 <vector104>:
.globl vector104
vector104:
  pushl $0
80106ce6:	6a 00                	push   $0x0
  pushl $104
80106ce8:	6a 68                	push   $0x68
  jmp alltraps
80106cea:	e9 cd f5 ff ff       	jmp    801062bc <alltraps>

80106cef <vector105>:
.globl vector105
vector105:
  pushl $0
80106cef:	6a 00                	push   $0x0
  pushl $105
80106cf1:	6a 69                	push   $0x69
  jmp alltraps
80106cf3:	e9 c4 f5 ff ff       	jmp    801062bc <alltraps>

80106cf8 <vector106>:
.globl vector106
vector106:
  pushl $0
80106cf8:	6a 00                	push   $0x0
  pushl $106
80106cfa:	6a 6a                	push   $0x6a
  jmp alltraps
80106cfc:	e9 bb f5 ff ff       	jmp    801062bc <alltraps>

80106d01 <vector107>:
.globl vector107
vector107:
  pushl $0
80106d01:	6a 00                	push   $0x0
  pushl $107
80106d03:	6a 6b                	push   $0x6b
  jmp alltraps
80106d05:	e9 b2 f5 ff ff       	jmp    801062bc <alltraps>

80106d0a <vector108>:
.globl vector108
vector108:
  pushl $0
80106d0a:	6a 00                	push   $0x0
  pushl $108
80106d0c:	6a 6c                	push   $0x6c
  jmp alltraps
80106d0e:	e9 a9 f5 ff ff       	jmp    801062bc <alltraps>

80106d13 <vector109>:
.globl vector109
vector109:
  pushl $0
80106d13:	6a 00                	push   $0x0
  pushl $109
80106d15:	6a 6d                	push   $0x6d
  jmp alltraps
80106d17:	e9 a0 f5 ff ff       	jmp    801062bc <alltraps>

80106d1c <vector110>:
.globl vector110
vector110:
  pushl $0
80106d1c:	6a 00                	push   $0x0
  pushl $110
80106d1e:	6a 6e                	push   $0x6e
  jmp alltraps
80106d20:	e9 97 f5 ff ff       	jmp    801062bc <alltraps>

80106d25 <vector111>:
.globl vector111
vector111:
  pushl $0
80106d25:	6a 00                	push   $0x0
  pushl $111
80106d27:	6a 6f                	push   $0x6f
  jmp alltraps
80106d29:	e9 8e f5 ff ff       	jmp    801062bc <alltraps>

80106d2e <vector112>:
.globl vector112
vector112:
  pushl $0
80106d2e:	6a 00                	push   $0x0
  pushl $112
80106d30:	6a 70                	push   $0x70
  jmp alltraps
80106d32:	e9 85 f5 ff ff       	jmp    801062bc <alltraps>

80106d37 <vector113>:
.globl vector113
vector113:
  pushl $0
80106d37:	6a 00                	push   $0x0
  pushl $113
80106d39:	6a 71                	push   $0x71
  jmp alltraps
80106d3b:	e9 7c f5 ff ff       	jmp    801062bc <alltraps>

80106d40 <vector114>:
.globl vector114
vector114:
  pushl $0
80106d40:	6a 00                	push   $0x0
  pushl $114
80106d42:	6a 72                	push   $0x72
  jmp alltraps
80106d44:	e9 73 f5 ff ff       	jmp    801062bc <alltraps>

80106d49 <vector115>:
.globl vector115
vector115:
  pushl $0
80106d49:	6a 00                	push   $0x0
  pushl $115
80106d4b:	6a 73                	push   $0x73
  jmp alltraps
80106d4d:	e9 6a f5 ff ff       	jmp    801062bc <alltraps>

80106d52 <vector116>:
.globl vector116
vector116:
  pushl $0
80106d52:	6a 00                	push   $0x0
  pushl $116
80106d54:	6a 74                	push   $0x74
  jmp alltraps
80106d56:	e9 61 f5 ff ff       	jmp    801062bc <alltraps>

80106d5b <vector117>:
.globl vector117
vector117:
  pushl $0
80106d5b:	6a 00                	push   $0x0
  pushl $117
80106d5d:	6a 75                	push   $0x75
  jmp alltraps
80106d5f:	e9 58 f5 ff ff       	jmp    801062bc <alltraps>

80106d64 <vector118>:
.globl vector118
vector118:
  pushl $0
80106d64:	6a 00                	push   $0x0
  pushl $118
80106d66:	6a 76                	push   $0x76
  jmp alltraps
80106d68:	e9 4f f5 ff ff       	jmp    801062bc <alltraps>

80106d6d <vector119>:
.globl vector119
vector119:
  pushl $0
80106d6d:	6a 00                	push   $0x0
  pushl $119
80106d6f:	6a 77                	push   $0x77
  jmp alltraps
80106d71:	e9 46 f5 ff ff       	jmp    801062bc <alltraps>

80106d76 <vector120>:
.globl vector120
vector120:
  pushl $0
80106d76:	6a 00                	push   $0x0
  pushl $120
80106d78:	6a 78                	push   $0x78
  jmp alltraps
80106d7a:	e9 3d f5 ff ff       	jmp    801062bc <alltraps>

80106d7f <vector121>:
.globl vector121
vector121:
  pushl $0
80106d7f:	6a 00                	push   $0x0
  pushl $121
80106d81:	6a 79                	push   $0x79
  jmp alltraps
80106d83:	e9 34 f5 ff ff       	jmp    801062bc <alltraps>

80106d88 <vector122>:
.globl vector122
vector122:
  pushl $0
80106d88:	6a 00                	push   $0x0
  pushl $122
80106d8a:	6a 7a                	push   $0x7a
  jmp alltraps
80106d8c:	e9 2b f5 ff ff       	jmp    801062bc <alltraps>

80106d91 <vector123>:
.globl vector123
vector123:
  pushl $0
80106d91:	6a 00                	push   $0x0
  pushl $123
80106d93:	6a 7b                	push   $0x7b
  jmp alltraps
80106d95:	e9 22 f5 ff ff       	jmp    801062bc <alltraps>

80106d9a <vector124>:
.globl vector124
vector124:
  pushl $0
80106d9a:	6a 00                	push   $0x0
  pushl $124
80106d9c:	6a 7c                	push   $0x7c
  jmp alltraps
80106d9e:	e9 19 f5 ff ff       	jmp    801062bc <alltraps>

80106da3 <vector125>:
.globl vector125
vector125:
  pushl $0
80106da3:	6a 00                	push   $0x0
  pushl $125
80106da5:	6a 7d                	push   $0x7d
  jmp alltraps
80106da7:	e9 10 f5 ff ff       	jmp    801062bc <alltraps>

80106dac <vector126>:
.globl vector126
vector126:
  pushl $0
80106dac:	6a 00                	push   $0x0
  pushl $126
80106dae:	6a 7e                	push   $0x7e
  jmp alltraps
80106db0:	e9 07 f5 ff ff       	jmp    801062bc <alltraps>

80106db5 <vector127>:
.globl vector127
vector127:
  pushl $0
80106db5:	6a 00                	push   $0x0
  pushl $127
80106db7:	6a 7f                	push   $0x7f
  jmp alltraps
80106db9:	e9 fe f4 ff ff       	jmp    801062bc <alltraps>

80106dbe <vector128>:
.globl vector128
vector128:
  pushl $0
80106dbe:	6a 00                	push   $0x0
  pushl $128
80106dc0:	68 80 00 00 00       	push   $0x80
  jmp alltraps
80106dc5:	e9 f2 f4 ff ff       	jmp    801062bc <alltraps>

80106dca <vector129>:
.globl vector129
vector129:
  pushl $0
80106dca:	6a 00                	push   $0x0
  pushl $129
80106dcc:	68 81 00 00 00       	push   $0x81
  jmp alltraps
80106dd1:	e9 e6 f4 ff ff       	jmp    801062bc <alltraps>

80106dd6 <vector130>:
.globl vector130
vector130:
  pushl $0
80106dd6:	6a 00                	push   $0x0
  pushl $130
80106dd8:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80106ddd:	e9 da f4 ff ff       	jmp    801062bc <alltraps>

80106de2 <vector131>:
.globl vector131
vector131:
  pushl $0
80106de2:	6a 00                	push   $0x0
  pushl $131
80106de4:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80106de9:	e9 ce f4 ff ff       	jmp    801062bc <alltraps>

80106dee <vector132>:
.globl vector132
vector132:
  pushl $0
80106dee:	6a 00                	push   $0x0
  pushl $132
80106df0:	68 84 00 00 00       	push   $0x84
  jmp alltraps
80106df5:	e9 c2 f4 ff ff       	jmp    801062bc <alltraps>

80106dfa <vector133>:
.globl vector133
vector133:
  pushl $0
80106dfa:	6a 00                	push   $0x0
  pushl $133
80106dfc:	68 85 00 00 00       	push   $0x85
  jmp alltraps
80106e01:	e9 b6 f4 ff ff       	jmp    801062bc <alltraps>

80106e06 <vector134>:
.globl vector134
vector134:
  pushl $0
80106e06:	6a 00                	push   $0x0
  pushl $134
80106e08:	68 86 00 00 00       	push   $0x86
  jmp alltraps
80106e0d:	e9 aa f4 ff ff       	jmp    801062bc <alltraps>

80106e12 <vector135>:
.globl vector135
vector135:
  pushl $0
80106e12:	6a 00                	push   $0x0
  pushl $135
80106e14:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80106e19:	e9 9e f4 ff ff       	jmp    801062bc <alltraps>

80106e1e <vector136>:
.globl vector136
vector136:
  pushl $0
80106e1e:	6a 00                	push   $0x0
  pushl $136
80106e20:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80106e25:	e9 92 f4 ff ff       	jmp    801062bc <alltraps>

80106e2a <vector137>:
.globl vector137
vector137:
  pushl $0
80106e2a:	6a 00                	push   $0x0
  pushl $137
80106e2c:	68 89 00 00 00       	push   $0x89
  jmp alltraps
80106e31:	e9 86 f4 ff ff       	jmp    801062bc <alltraps>

80106e36 <vector138>:
.globl vector138
vector138:
  pushl $0
80106e36:	6a 00                	push   $0x0
  pushl $138
80106e38:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80106e3d:	e9 7a f4 ff ff       	jmp    801062bc <alltraps>

80106e42 <vector139>:
.globl vector139
vector139:
  pushl $0
80106e42:	6a 00                	push   $0x0
  pushl $139
80106e44:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80106e49:	e9 6e f4 ff ff       	jmp    801062bc <alltraps>

80106e4e <vector140>:
.globl vector140
vector140:
  pushl $0
80106e4e:	6a 00                	push   $0x0
  pushl $140
80106e50:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80106e55:	e9 62 f4 ff ff       	jmp    801062bc <alltraps>

80106e5a <vector141>:
.globl vector141
vector141:
  pushl $0
80106e5a:	6a 00                	push   $0x0
  pushl $141
80106e5c:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
80106e61:	e9 56 f4 ff ff       	jmp    801062bc <alltraps>

80106e66 <vector142>:
.globl vector142
vector142:
  pushl $0
80106e66:	6a 00                	push   $0x0
  pushl $142
80106e68:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80106e6d:	e9 4a f4 ff ff       	jmp    801062bc <alltraps>

80106e72 <vector143>:
.globl vector143
vector143:
  pushl $0
80106e72:	6a 00                	push   $0x0
  pushl $143
80106e74:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80106e79:	e9 3e f4 ff ff       	jmp    801062bc <alltraps>

80106e7e <vector144>:
.globl vector144
vector144:
  pushl $0
80106e7e:	6a 00                	push   $0x0
  pushl $144
80106e80:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80106e85:	e9 32 f4 ff ff       	jmp    801062bc <alltraps>

80106e8a <vector145>:
.globl vector145
vector145:
  pushl $0
80106e8a:	6a 00                	push   $0x0
  pushl $145
80106e8c:	68 91 00 00 00       	push   $0x91
  jmp alltraps
80106e91:	e9 26 f4 ff ff       	jmp    801062bc <alltraps>

80106e96 <vector146>:
.globl vector146
vector146:
  pushl $0
80106e96:	6a 00                	push   $0x0
  pushl $146
80106e98:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80106e9d:	e9 1a f4 ff ff       	jmp    801062bc <alltraps>

80106ea2 <vector147>:
.globl vector147
vector147:
  pushl $0
80106ea2:	6a 00                	push   $0x0
  pushl $147
80106ea4:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80106ea9:	e9 0e f4 ff ff       	jmp    801062bc <alltraps>

80106eae <vector148>:
.globl vector148
vector148:
  pushl $0
80106eae:	6a 00                	push   $0x0
  pushl $148
80106eb0:	68 94 00 00 00       	push   $0x94
  jmp alltraps
80106eb5:	e9 02 f4 ff ff       	jmp    801062bc <alltraps>

80106eba <vector149>:
.globl vector149
vector149:
  pushl $0
80106eba:	6a 00                	push   $0x0
  pushl $149
80106ebc:	68 95 00 00 00       	push   $0x95
  jmp alltraps
80106ec1:	e9 f6 f3 ff ff       	jmp    801062bc <alltraps>

80106ec6 <vector150>:
.globl vector150
vector150:
  pushl $0
80106ec6:	6a 00                	push   $0x0
  pushl $150
80106ec8:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80106ecd:	e9 ea f3 ff ff       	jmp    801062bc <alltraps>

80106ed2 <vector151>:
.globl vector151
vector151:
  pushl $0
80106ed2:	6a 00                	push   $0x0
  pushl $151
80106ed4:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80106ed9:	e9 de f3 ff ff       	jmp    801062bc <alltraps>

80106ede <vector152>:
.globl vector152
vector152:
  pushl $0
80106ede:	6a 00                	push   $0x0
  pushl $152
80106ee0:	68 98 00 00 00       	push   $0x98
  jmp alltraps
80106ee5:	e9 d2 f3 ff ff       	jmp    801062bc <alltraps>

80106eea <vector153>:
.globl vector153
vector153:
  pushl $0
80106eea:	6a 00                	push   $0x0
  pushl $153
80106eec:	68 99 00 00 00       	push   $0x99
  jmp alltraps
80106ef1:	e9 c6 f3 ff ff       	jmp    801062bc <alltraps>

80106ef6 <vector154>:
.globl vector154
vector154:
  pushl $0
80106ef6:	6a 00                	push   $0x0
  pushl $154
80106ef8:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80106efd:	e9 ba f3 ff ff       	jmp    801062bc <alltraps>

80106f02 <vector155>:
.globl vector155
vector155:
  pushl $0
80106f02:	6a 00                	push   $0x0
  pushl $155
80106f04:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
80106f09:	e9 ae f3 ff ff       	jmp    801062bc <alltraps>

80106f0e <vector156>:
.globl vector156
vector156:
  pushl $0
80106f0e:	6a 00                	push   $0x0
  pushl $156
80106f10:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
80106f15:	e9 a2 f3 ff ff       	jmp    801062bc <alltraps>

80106f1a <vector157>:
.globl vector157
vector157:
  pushl $0
80106f1a:	6a 00                	push   $0x0
  pushl $157
80106f1c:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
80106f21:	e9 96 f3 ff ff       	jmp    801062bc <alltraps>

80106f26 <vector158>:
.globl vector158
vector158:
  pushl $0
80106f26:	6a 00                	push   $0x0
  pushl $158
80106f28:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80106f2d:	e9 8a f3 ff ff       	jmp    801062bc <alltraps>

80106f32 <vector159>:
.globl vector159
vector159:
  pushl $0
80106f32:	6a 00                	push   $0x0
  pushl $159
80106f34:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80106f39:	e9 7e f3 ff ff       	jmp    801062bc <alltraps>

80106f3e <vector160>:
.globl vector160
vector160:
  pushl $0
80106f3e:	6a 00                	push   $0x0
  pushl $160
80106f40:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80106f45:	e9 72 f3 ff ff       	jmp    801062bc <alltraps>

80106f4a <vector161>:
.globl vector161
vector161:
  pushl $0
80106f4a:	6a 00                	push   $0x0
  pushl $161
80106f4c:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
80106f51:	e9 66 f3 ff ff       	jmp    801062bc <alltraps>

80106f56 <vector162>:
.globl vector162
vector162:
  pushl $0
80106f56:	6a 00                	push   $0x0
  pushl $162
80106f58:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80106f5d:	e9 5a f3 ff ff       	jmp    801062bc <alltraps>

80106f62 <vector163>:
.globl vector163
vector163:
  pushl $0
80106f62:	6a 00                	push   $0x0
  pushl $163
80106f64:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80106f69:	e9 4e f3 ff ff       	jmp    801062bc <alltraps>

80106f6e <vector164>:
.globl vector164
vector164:
  pushl $0
80106f6e:	6a 00                	push   $0x0
  pushl $164
80106f70:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80106f75:	e9 42 f3 ff ff       	jmp    801062bc <alltraps>

80106f7a <vector165>:
.globl vector165
vector165:
  pushl $0
80106f7a:	6a 00                	push   $0x0
  pushl $165
80106f7c:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
80106f81:	e9 36 f3 ff ff       	jmp    801062bc <alltraps>

80106f86 <vector166>:
.globl vector166
vector166:
  pushl $0
80106f86:	6a 00                	push   $0x0
  pushl $166
80106f88:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80106f8d:	e9 2a f3 ff ff       	jmp    801062bc <alltraps>

80106f92 <vector167>:
.globl vector167
vector167:
  pushl $0
80106f92:	6a 00                	push   $0x0
  pushl $167
80106f94:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80106f99:	e9 1e f3 ff ff       	jmp    801062bc <alltraps>

80106f9e <vector168>:
.globl vector168
vector168:
  pushl $0
80106f9e:	6a 00                	push   $0x0
  pushl $168
80106fa0:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80106fa5:	e9 12 f3 ff ff       	jmp    801062bc <alltraps>

80106faa <vector169>:
.globl vector169
vector169:
  pushl $0
80106faa:	6a 00                	push   $0x0
  pushl $169
80106fac:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
80106fb1:	e9 06 f3 ff ff       	jmp    801062bc <alltraps>

80106fb6 <vector170>:
.globl vector170
vector170:
  pushl $0
80106fb6:	6a 00                	push   $0x0
  pushl $170
80106fb8:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80106fbd:	e9 fa f2 ff ff       	jmp    801062bc <alltraps>

80106fc2 <vector171>:
.globl vector171
vector171:
  pushl $0
80106fc2:	6a 00                	push   $0x0
  pushl $171
80106fc4:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80106fc9:	e9 ee f2 ff ff       	jmp    801062bc <alltraps>

80106fce <vector172>:
.globl vector172
vector172:
  pushl $0
80106fce:	6a 00                	push   $0x0
  pushl $172
80106fd0:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
80106fd5:	e9 e2 f2 ff ff       	jmp    801062bc <alltraps>

80106fda <vector173>:
.globl vector173
vector173:
  pushl $0
80106fda:	6a 00                	push   $0x0
  pushl $173
80106fdc:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
80106fe1:	e9 d6 f2 ff ff       	jmp    801062bc <alltraps>

80106fe6 <vector174>:
.globl vector174
vector174:
  pushl $0
80106fe6:	6a 00                	push   $0x0
  pushl $174
80106fe8:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80106fed:	e9 ca f2 ff ff       	jmp    801062bc <alltraps>

80106ff2 <vector175>:
.globl vector175
vector175:
  pushl $0
80106ff2:	6a 00                	push   $0x0
  pushl $175
80106ff4:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80106ff9:	e9 be f2 ff ff       	jmp    801062bc <alltraps>

80106ffe <vector176>:
.globl vector176
vector176:
  pushl $0
80106ffe:	6a 00                	push   $0x0
  pushl $176
80107000:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
80107005:	e9 b2 f2 ff ff       	jmp    801062bc <alltraps>

8010700a <vector177>:
.globl vector177
vector177:
  pushl $0
8010700a:	6a 00                	push   $0x0
  pushl $177
8010700c:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
80107011:	e9 a6 f2 ff ff       	jmp    801062bc <alltraps>

80107016 <vector178>:
.globl vector178
vector178:
  pushl $0
80107016:	6a 00                	push   $0x0
  pushl $178
80107018:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
8010701d:	e9 9a f2 ff ff       	jmp    801062bc <alltraps>

80107022 <vector179>:
.globl vector179
vector179:
  pushl $0
80107022:	6a 00                	push   $0x0
  pushl $179
80107024:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
80107029:	e9 8e f2 ff ff       	jmp    801062bc <alltraps>

8010702e <vector180>:
.globl vector180
vector180:
  pushl $0
8010702e:	6a 00                	push   $0x0
  pushl $180
80107030:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80107035:	e9 82 f2 ff ff       	jmp    801062bc <alltraps>

8010703a <vector181>:
.globl vector181
vector181:
  pushl $0
8010703a:	6a 00                	push   $0x0
  pushl $181
8010703c:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
80107041:	e9 76 f2 ff ff       	jmp    801062bc <alltraps>

80107046 <vector182>:
.globl vector182
vector182:
  pushl $0
80107046:	6a 00                	push   $0x0
  pushl $182
80107048:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
8010704d:	e9 6a f2 ff ff       	jmp    801062bc <alltraps>

80107052 <vector183>:
.globl vector183
vector183:
  pushl $0
80107052:	6a 00                	push   $0x0
  pushl $183
80107054:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
80107059:	e9 5e f2 ff ff       	jmp    801062bc <alltraps>

8010705e <vector184>:
.globl vector184
vector184:
  pushl $0
8010705e:	6a 00                	push   $0x0
  pushl $184
80107060:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80107065:	e9 52 f2 ff ff       	jmp    801062bc <alltraps>

8010706a <vector185>:
.globl vector185
vector185:
  pushl $0
8010706a:	6a 00                	push   $0x0
  pushl $185
8010706c:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
80107071:	e9 46 f2 ff ff       	jmp    801062bc <alltraps>

80107076 <vector186>:
.globl vector186
vector186:
  pushl $0
80107076:	6a 00                	push   $0x0
  pushl $186
80107078:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
8010707d:	e9 3a f2 ff ff       	jmp    801062bc <alltraps>

80107082 <vector187>:
.globl vector187
vector187:
  pushl $0
80107082:	6a 00                	push   $0x0
  pushl $187
80107084:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80107089:	e9 2e f2 ff ff       	jmp    801062bc <alltraps>

8010708e <vector188>:
.globl vector188
vector188:
  pushl $0
8010708e:	6a 00                	push   $0x0
  pushl $188
80107090:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80107095:	e9 22 f2 ff ff       	jmp    801062bc <alltraps>

8010709a <vector189>:
.globl vector189
vector189:
  pushl $0
8010709a:	6a 00                	push   $0x0
  pushl $189
8010709c:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
801070a1:	e9 16 f2 ff ff       	jmp    801062bc <alltraps>

801070a6 <vector190>:
.globl vector190
vector190:
  pushl $0
801070a6:	6a 00                	push   $0x0
  pushl $190
801070a8:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
801070ad:	e9 0a f2 ff ff       	jmp    801062bc <alltraps>

801070b2 <vector191>:
.globl vector191
vector191:
  pushl $0
801070b2:	6a 00                	push   $0x0
  pushl $191
801070b4:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
801070b9:	e9 fe f1 ff ff       	jmp    801062bc <alltraps>

801070be <vector192>:
.globl vector192
vector192:
  pushl $0
801070be:	6a 00                	push   $0x0
  pushl $192
801070c0:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
801070c5:	e9 f2 f1 ff ff       	jmp    801062bc <alltraps>

801070ca <vector193>:
.globl vector193
vector193:
  pushl $0
801070ca:	6a 00                	push   $0x0
  pushl $193
801070cc:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
801070d1:	e9 e6 f1 ff ff       	jmp    801062bc <alltraps>

801070d6 <vector194>:
.globl vector194
vector194:
  pushl $0
801070d6:	6a 00                	push   $0x0
  pushl $194
801070d8:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
801070dd:	e9 da f1 ff ff       	jmp    801062bc <alltraps>

801070e2 <vector195>:
.globl vector195
vector195:
  pushl $0
801070e2:	6a 00                	push   $0x0
  pushl $195
801070e4:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
801070e9:	e9 ce f1 ff ff       	jmp    801062bc <alltraps>

801070ee <vector196>:
.globl vector196
vector196:
  pushl $0
801070ee:	6a 00                	push   $0x0
  pushl $196
801070f0:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
801070f5:	e9 c2 f1 ff ff       	jmp    801062bc <alltraps>

801070fa <vector197>:
.globl vector197
vector197:
  pushl $0
801070fa:	6a 00                	push   $0x0
  pushl $197
801070fc:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
80107101:	e9 b6 f1 ff ff       	jmp    801062bc <alltraps>

80107106 <vector198>:
.globl vector198
vector198:
  pushl $0
80107106:	6a 00                	push   $0x0
  pushl $198
80107108:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
8010710d:	e9 aa f1 ff ff       	jmp    801062bc <alltraps>

80107112 <vector199>:
.globl vector199
vector199:
  pushl $0
80107112:	6a 00                	push   $0x0
  pushl $199
80107114:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
80107119:	e9 9e f1 ff ff       	jmp    801062bc <alltraps>

8010711e <vector200>:
.globl vector200
vector200:
  pushl $0
8010711e:	6a 00                	push   $0x0
  pushl $200
80107120:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80107125:	e9 92 f1 ff ff       	jmp    801062bc <alltraps>

8010712a <vector201>:
.globl vector201
vector201:
  pushl $0
8010712a:	6a 00                	push   $0x0
  pushl $201
8010712c:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
80107131:	e9 86 f1 ff ff       	jmp    801062bc <alltraps>

80107136 <vector202>:
.globl vector202
vector202:
  pushl $0
80107136:	6a 00                	push   $0x0
  pushl $202
80107138:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
8010713d:	e9 7a f1 ff ff       	jmp    801062bc <alltraps>

80107142 <vector203>:
.globl vector203
vector203:
  pushl $0
80107142:	6a 00                	push   $0x0
  pushl $203
80107144:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80107149:	e9 6e f1 ff ff       	jmp    801062bc <alltraps>

8010714e <vector204>:
.globl vector204
vector204:
  pushl $0
8010714e:	6a 00                	push   $0x0
  pushl $204
80107150:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80107155:	e9 62 f1 ff ff       	jmp    801062bc <alltraps>

8010715a <vector205>:
.globl vector205
vector205:
  pushl $0
8010715a:	6a 00                	push   $0x0
  pushl $205
8010715c:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
80107161:	e9 56 f1 ff ff       	jmp    801062bc <alltraps>

80107166 <vector206>:
.globl vector206
vector206:
  pushl $0
80107166:	6a 00                	push   $0x0
  pushl $206
80107168:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
8010716d:	e9 4a f1 ff ff       	jmp    801062bc <alltraps>

80107172 <vector207>:
.globl vector207
vector207:
  pushl $0
80107172:	6a 00                	push   $0x0
  pushl $207
80107174:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80107179:	e9 3e f1 ff ff       	jmp    801062bc <alltraps>

8010717e <vector208>:
.globl vector208
vector208:
  pushl $0
8010717e:	6a 00                	push   $0x0
  pushl $208
80107180:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80107185:	e9 32 f1 ff ff       	jmp    801062bc <alltraps>

8010718a <vector209>:
.globl vector209
vector209:
  pushl $0
8010718a:	6a 00                	push   $0x0
  pushl $209
8010718c:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
80107191:	e9 26 f1 ff ff       	jmp    801062bc <alltraps>

80107196 <vector210>:
.globl vector210
vector210:
  pushl $0
80107196:	6a 00                	push   $0x0
  pushl $210
80107198:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
8010719d:	e9 1a f1 ff ff       	jmp    801062bc <alltraps>

801071a2 <vector211>:
.globl vector211
vector211:
  pushl $0
801071a2:	6a 00                	push   $0x0
  pushl $211
801071a4:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
801071a9:	e9 0e f1 ff ff       	jmp    801062bc <alltraps>

801071ae <vector212>:
.globl vector212
vector212:
  pushl $0
801071ae:	6a 00                	push   $0x0
  pushl $212
801071b0:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
801071b5:	e9 02 f1 ff ff       	jmp    801062bc <alltraps>

801071ba <vector213>:
.globl vector213
vector213:
  pushl $0
801071ba:	6a 00                	push   $0x0
  pushl $213
801071bc:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
801071c1:	e9 f6 f0 ff ff       	jmp    801062bc <alltraps>

801071c6 <vector214>:
.globl vector214
vector214:
  pushl $0
801071c6:	6a 00                	push   $0x0
  pushl $214
801071c8:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
801071cd:	e9 ea f0 ff ff       	jmp    801062bc <alltraps>

801071d2 <vector215>:
.globl vector215
vector215:
  pushl $0
801071d2:	6a 00                	push   $0x0
  pushl $215
801071d4:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
801071d9:	e9 de f0 ff ff       	jmp    801062bc <alltraps>

801071de <vector216>:
.globl vector216
vector216:
  pushl $0
801071de:	6a 00                	push   $0x0
  pushl $216
801071e0:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
801071e5:	e9 d2 f0 ff ff       	jmp    801062bc <alltraps>

801071ea <vector217>:
.globl vector217
vector217:
  pushl $0
801071ea:	6a 00                	push   $0x0
  pushl $217
801071ec:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
801071f1:	e9 c6 f0 ff ff       	jmp    801062bc <alltraps>

801071f6 <vector218>:
.globl vector218
vector218:
  pushl $0
801071f6:	6a 00                	push   $0x0
  pushl $218
801071f8:	68 da 00 00 00       	push   $0xda
  jmp alltraps
801071fd:	e9 ba f0 ff ff       	jmp    801062bc <alltraps>

80107202 <vector219>:
.globl vector219
vector219:
  pushl $0
80107202:	6a 00                	push   $0x0
  pushl $219
80107204:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
80107209:	e9 ae f0 ff ff       	jmp    801062bc <alltraps>

8010720e <vector220>:
.globl vector220
vector220:
  pushl $0
8010720e:	6a 00                	push   $0x0
  pushl $220
80107210:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80107215:	e9 a2 f0 ff ff       	jmp    801062bc <alltraps>

8010721a <vector221>:
.globl vector221
vector221:
  pushl $0
8010721a:	6a 00                	push   $0x0
  pushl $221
8010721c:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
80107221:	e9 96 f0 ff ff       	jmp    801062bc <alltraps>

80107226 <vector222>:
.globl vector222
vector222:
  pushl $0
80107226:	6a 00                	push   $0x0
  pushl $222
80107228:	68 de 00 00 00       	push   $0xde
  jmp alltraps
8010722d:	e9 8a f0 ff ff       	jmp    801062bc <alltraps>

80107232 <vector223>:
.globl vector223
vector223:
  pushl $0
80107232:	6a 00                	push   $0x0
  pushl $223
80107234:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80107239:	e9 7e f0 ff ff       	jmp    801062bc <alltraps>

8010723e <vector224>:
.globl vector224
vector224:
  pushl $0
8010723e:	6a 00                	push   $0x0
  pushl $224
80107240:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80107245:	e9 72 f0 ff ff       	jmp    801062bc <alltraps>

8010724a <vector225>:
.globl vector225
vector225:
  pushl $0
8010724a:	6a 00                	push   $0x0
  pushl $225
8010724c:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
80107251:	e9 66 f0 ff ff       	jmp    801062bc <alltraps>

80107256 <vector226>:
.globl vector226
vector226:
  pushl $0
80107256:	6a 00                	push   $0x0
  pushl $226
80107258:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
8010725d:	e9 5a f0 ff ff       	jmp    801062bc <alltraps>

80107262 <vector227>:
.globl vector227
vector227:
  pushl $0
80107262:	6a 00                	push   $0x0
  pushl $227
80107264:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80107269:	e9 4e f0 ff ff       	jmp    801062bc <alltraps>

8010726e <vector228>:
.globl vector228
vector228:
  pushl $0
8010726e:	6a 00                	push   $0x0
  pushl $228
80107270:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80107275:	e9 42 f0 ff ff       	jmp    801062bc <alltraps>

8010727a <vector229>:
.globl vector229
vector229:
  pushl $0
8010727a:	6a 00                	push   $0x0
  pushl $229
8010727c:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
80107281:	e9 36 f0 ff ff       	jmp    801062bc <alltraps>

80107286 <vector230>:
.globl vector230
vector230:
  pushl $0
80107286:	6a 00                	push   $0x0
  pushl $230
80107288:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
8010728d:	e9 2a f0 ff ff       	jmp    801062bc <alltraps>

80107292 <vector231>:
.globl vector231
vector231:
  pushl $0
80107292:	6a 00                	push   $0x0
  pushl $231
80107294:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80107299:	e9 1e f0 ff ff       	jmp    801062bc <alltraps>

8010729e <vector232>:
.globl vector232
vector232:
  pushl $0
8010729e:	6a 00                	push   $0x0
  pushl $232
801072a0:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
801072a5:	e9 12 f0 ff ff       	jmp    801062bc <alltraps>

801072aa <vector233>:
.globl vector233
vector233:
  pushl $0
801072aa:	6a 00                	push   $0x0
  pushl $233
801072ac:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
801072b1:	e9 06 f0 ff ff       	jmp    801062bc <alltraps>

801072b6 <vector234>:
.globl vector234
vector234:
  pushl $0
801072b6:	6a 00                	push   $0x0
  pushl $234
801072b8:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
801072bd:	e9 fa ef ff ff       	jmp    801062bc <alltraps>

801072c2 <vector235>:
.globl vector235
vector235:
  pushl $0
801072c2:	6a 00                	push   $0x0
  pushl $235
801072c4:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
801072c9:	e9 ee ef ff ff       	jmp    801062bc <alltraps>

801072ce <vector236>:
.globl vector236
vector236:
  pushl $0
801072ce:	6a 00                	push   $0x0
  pushl $236
801072d0:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
801072d5:	e9 e2 ef ff ff       	jmp    801062bc <alltraps>

801072da <vector237>:
.globl vector237
vector237:
  pushl $0
801072da:	6a 00                	push   $0x0
  pushl $237
801072dc:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
801072e1:	e9 d6 ef ff ff       	jmp    801062bc <alltraps>

801072e6 <vector238>:
.globl vector238
vector238:
  pushl $0
801072e6:	6a 00                	push   $0x0
  pushl $238
801072e8:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
801072ed:	e9 ca ef ff ff       	jmp    801062bc <alltraps>

801072f2 <vector239>:
.globl vector239
vector239:
  pushl $0
801072f2:	6a 00                	push   $0x0
  pushl $239
801072f4:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
801072f9:	e9 be ef ff ff       	jmp    801062bc <alltraps>

801072fe <vector240>:
.globl vector240
vector240:
  pushl $0
801072fe:	6a 00                	push   $0x0
  pushl $240
80107300:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
80107305:	e9 b2 ef ff ff       	jmp    801062bc <alltraps>

8010730a <vector241>:
.globl vector241
vector241:
  pushl $0
8010730a:	6a 00                	push   $0x0
  pushl $241
8010730c:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
80107311:	e9 a6 ef ff ff       	jmp    801062bc <alltraps>

80107316 <vector242>:
.globl vector242
vector242:
  pushl $0
80107316:	6a 00                	push   $0x0
  pushl $242
80107318:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
8010731d:	e9 9a ef ff ff       	jmp    801062bc <alltraps>

80107322 <vector243>:
.globl vector243
vector243:
  pushl $0
80107322:	6a 00                	push   $0x0
  pushl $243
80107324:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
80107329:	e9 8e ef ff ff       	jmp    801062bc <alltraps>

8010732e <vector244>:
.globl vector244
vector244:
  pushl $0
8010732e:	6a 00                	push   $0x0
  pushl $244
80107330:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
80107335:	e9 82 ef ff ff       	jmp    801062bc <alltraps>

8010733a <vector245>:
.globl vector245
vector245:
  pushl $0
8010733a:	6a 00                	push   $0x0
  pushl $245
8010733c:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
80107341:	e9 76 ef ff ff       	jmp    801062bc <alltraps>

80107346 <vector246>:
.globl vector246
vector246:
  pushl $0
80107346:	6a 00                	push   $0x0
  pushl $246
80107348:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
8010734d:	e9 6a ef ff ff       	jmp    801062bc <alltraps>

80107352 <vector247>:
.globl vector247
vector247:
  pushl $0
80107352:	6a 00                	push   $0x0
  pushl $247
80107354:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80107359:	e9 5e ef ff ff       	jmp    801062bc <alltraps>

8010735e <vector248>:
.globl vector248
vector248:
  pushl $0
8010735e:	6a 00                	push   $0x0
  pushl $248
80107360:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80107365:	e9 52 ef ff ff       	jmp    801062bc <alltraps>

8010736a <vector249>:
.globl vector249
vector249:
  pushl $0
8010736a:	6a 00                	push   $0x0
  pushl $249
8010736c:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
80107371:	e9 46 ef ff ff       	jmp    801062bc <alltraps>

80107376 <vector250>:
.globl vector250
vector250:
  pushl $0
80107376:	6a 00                	push   $0x0
  pushl $250
80107378:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
8010737d:	e9 3a ef ff ff       	jmp    801062bc <alltraps>

80107382 <vector251>:
.globl vector251
vector251:
  pushl $0
80107382:	6a 00                	push   $0x0
  pushl $251
80107384:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80107389:	e9 2e ef ff ff       	jmp    801062bc <alltraps>

8010738e <vector252>:
.globl vector252
vector252:
  pushl $0
8010738e:	6a 00                	push   $0x0
  pushl $252
80107390:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80107395:	e9 22 ef ff ff       	jmp    801062bc <alltraps>

8010739a <vector253>:
.globl vector253
vector253:
  pushl $0
8010739a:	6a 00                	push   $0x0
  pushl $253
8010739c:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
801073a1:	e9 16 ef ff ff       	jmp    801062bc <alltraps>

801073a6 <vector254>:
.globl vector254
vector254:
  pushl $0
801073a6:	6a 00                	push   $0x0
  pushl $254
801073a8:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
801073ad:	e9 0a ef ff ff       	jmp    801062bc <alltraps>

801073b2 <vector255>:
.globl vector255
vector255:
  pushl $0
801073b2:	6a 00                	push   $0x0
  pushl $255
801073b4:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
801073b9:	e9 fe ee ff ff       	jmp    801062bc <alltraps>
	...

801073c0 <lgdt>:

struct segdesc;

static inline void
lgdt(struct segdesc *p, int size)
{
801073c0:	55                   	push   %ebp
801073c1:	89 e5                	mov    %esp,%ebp
801073c3:	83 ec 10             	sub    $0x10,%esp
  volatile ushort pd[3];

  pd[0] = size-1;
801073c6:	8b 45 0c             	mov    0xc(%ebp),%eax
801073c9:	83 e8 01             	sub    $0x1,%eax
801073cc:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
801073d0:	8b 45 08             	mov    0x8(%ebp),%eax
801073d3:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
801073d7:	8b 45 08             	mov    0x8(%ebp),%eax
801073da:	c1 e8 10             	shr    $0x10,%eax
801073dd:	66 89 45 fe          	mov    %ax,-0x2(%ebp)

  asm volatile("lgdt (%0)" : : "r" (pd));
801073e1:	8d 45 fa             	lea    -0x6(%ebp),%eax
801073e4:	0f 01 10             	lgdtl  (%eax)
}
801073e7:	c9                   	leave  
801073e8:	c3                   	ret    

801073e9 <ltr>:
  asm volatile("lidt (%0)" : : "r" (pd));
}

static inline void
ltr(ushort sel)
{
801073e9:	55                   	push   %ebp
801073ea:	89 e5                	mov    %esp,%ebp
801073ec:	83 ec 04             	sub    $0x4,%esp
801073ef:	8b 45 08             	mov    0x8(%ebp),%eax
801073f2:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("ltr %0" : : "r" (sel));
801073f6:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
801073fa:	0f 00 d8             	ltr    %ax
}
801073fd:	c9                   	leave  
801073fe:	c3                   	ret    

801073ff <loadgs>:
  return eflags;
}

static inline void
loadgs(ushort v)
{
801073ff:	55                   	push   %ebp
80107400:	89 e5                	mov    %esp,%ebp
80107402:	83 ec 04             	sub    $0x4,%esp
80107405:	8b 45 08             	mov    0x8(%ebp),%eax
80107408:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  asm volatile("movw %0, %%gs" : : "r" (v));
8010740c:	0f b7 45 fc          	movzwl -0x4(%ebp),%eax
80107410:	8e e8                	mov    %eax,%gs
}
80107412:	c9                   	leave  
80107413:	c3                   	ret    

80107414 <lcr3>:
  return val;
}

static inline void
lcr3(uint val) 
{
80107414:	55                   	push   %ebp
80107415:	89 e5                	mov    %esp,%ebp
  asm volatile("movl %0,%%cr3" : : "r" (val));
80107417:	8b 45 08             	mov    0x8(%ebp),%eax
8010741a:	0f 22 d8             	mov    %eax,%cr3
}
8010741d:	5d                   	pop    %ebp
8010741e:	c3                   	ret    

8010741f <v2p>:
#define KERNBASE 0x80000000         // First kernel virtual address
#define KERNLINK (KERNBASE+EXTMEM)  // Address where kernel is linked

#ifndef __ASSEMBLER__

static inline uint v2p(void *a) { return ((uint) (a))  - KERNBASE; }
8010741f:	55                   	push   %ebp
80107420:	89 e5                	mov    %esp,%ebp
80107422:	8b 45 08             	mov    0x8(%ebp),%eax
80107425:	05 00 00 00 80       	add    $0x80000000,%eax
8010742a:	5d                   	pop    %ebp
8010742b:	c3                   	ret    

8010742c <p2v>:
static inline void *p2v(uint a) { return (void *) ((a) + KERNBASE); }
8010742c:	55                   	push   %ebp
8010742d:	89 e5                	mov    %esp,%ebp
8010742f:	8b 45 08             	mov    0x8(%ebp),%eax
80107432:	05 00 00 00 80       	add    $0x80000000,%eax
80107437:	5d                   	pop    %ebp
80107438:	c3                   	ret    

80107439 <seginit>:

// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
80107439:	55                   	push   %ebp
8010743a:	89 e5                	mov    %esp,%ebp
8010743c:	53                   	push   %ebx
8010743d:	83 ec 24             	sub    $0x24,%esp

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpunum()];
80107440:	e8 d8 ba ff ff       	call   80102f1d <cpunum>
80107445:	69 c0 bc 00 00 00    	imul   $0xbc,%eax,%eax
8010744b:	05 20 fb 10 80       	add    $0x8010fb20,%eax
80107450:	89 45 f4             	mov    %eax,-0xc(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80107453:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107456:	66 c7 40 78 ff ff    	movw   $0xffff,0x78(%eax)
8010745c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010745f:	66 c7 40 7a 00 00    	movw   $0x0,0x7a(%eax)
80107465:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107468:	c6 40 7c 00          	movb   $0x0,0x7c(%eax)
8010746c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010746f:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80107473:	83 e2 f0             	and    $0xfffffff0,%edx
80107476:	83 ca 0a             	or     $0xa,%edx
80107479:	88 50 7d             	mov    %dl,0x7d(%eax)
8010747c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010747f:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80107483:	83 ca 10             	or     $0x10,%edx
80107486:	88 50 7d             	mov    %dl,0x7d(%eax)
80107489:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010748c:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
80107490:	83 e2 9f             	and    $0xffffff9f,%edx
80107493:	88 50 7d             	mov    %dl,0x7d(%eax)
80107496:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107499:	0f b6 50 7d          	movzbl 0x7d(%eax),%edx
8010749d:	83 ca 80             	or     $0xffffff80,%edx
801074a0:	88 50 7d             	mov    %dl,0x7d(%eax)
801074a3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074a6:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801074aa:	83 ca 0f             	or     $0xf,%edx
801074ad:	88 50 7e             	mov    %dl,0x7e(%eax)
801074b0:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074b3:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801074b7:	83 e2 ef             	and    $0xffffffef,%edx
801074ba:	88 50 7e             	mov    %dl,0x7e(%eax)
801074bd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074c0:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801074c4:	83 e2 df             	and    $0xffffffdf,%edx
801074c7:	88 50 7e             	mov    %dl,0x7e(%eax)
801074ca:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074cd:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801074d1:	83 ca 40             	or     $0x40,%edx
801074d4:	88 50 7e             	mov    %dl,0x7e(%eax)
801074d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074da:	0f b6 50 7e          	movzbl 0x7e(%eax),%edx
801074de:	83 ca 80             	or     $0xffffff80,%edx
801074e1:	88 50 7e             	mov    %dl,0x7e(%eax)
801074e4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074e7:	c6 40 7f 00          	movb   $0x0,0x7f(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
801074eb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074ee:	66 c7 80 80 00 00 00 	movw   $0xffff,0x80(%eax)
801074f5:	ff ff 
801074f7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801074fa:	66 c7 80 82 00 00 00 	movw   $0x0,0x82(%eax)
80107501:	00 00 
80107503:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107506:	c6 80 84 00 00 00 00 	movb   $0x0,0x84(%eax)
8010750d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107510:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80107517:	83 e2 f0             	and    $0xfffffff0,%edx
8010751a:	83 ca 02             	or     $0x2,%edx
8010751d:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80107523:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107526:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
8010752d:	83 ca 10             	or     $0x10,%edx
80107530:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80107536:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107539:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80107540:	83 e2 9f             	and    $0xffffff9f,%edx
80107543:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
80107549:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010754c:	0f b6 90 85 00 00 00 	movzbl 0x85(%eax),%edx
80107553:	83 ca 80             	or     $0xffffff80,%edx
80107556:	88 90 85 00 00 00    	mov    %dl,0x85(%eax)
8010755c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010755f:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80107566:	83 ca 0f             	or     $0xf,%edx
80107569:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
8010756f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107572:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
80107579:	83 e2 ef             	and    $0xffffffef,%edx
8010757c:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80107582:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107585:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010758c:	83 e2 df             	and    $0xffffffdf,%edx
8010758f:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
80107595:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107598:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
8010759f:	83 ca 40             	or     $0x40,%edx
801075a2:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
801075a8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801075ab:	0f b6 90 86 00 00 00 	movzbl 0x86(%eax),%edx
801075b2:	83 ca 80             	or     $0xffffff80,%edx
801075b5:	88 90 86 00 00 00    	mov    %dl,0x86(%eax)
801075bb:	8b 45 f4             	mov    -0xc(%ebp),%eax
801075be:	c6 80 87 00 00 00 00 	movb   $0x0,0x87(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
801075c5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801075c8:	66 c7 80 90 00 00 00 	movw   $0xffff,0x90(%eax)
801075cf:	ff ff 
801075d1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801075d4:	66 c7 80 92 00 00 00 	movw   $0x0,0x92(%eax)
801075db:	00 00 
801075dd:	8b 45 f4             	mov    -0xc(%ebp),%eax
801075e0:	c6 80 94 00 00 00 00 	movb   $0x0,0x94(%eax)
801075e7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801075ea:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
801075f1:	83 e2 f0             	and    $0xfffffff0,%edx
801075f4:	83 ca 0a             	or     $0xa,%edx
801075f7:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
801075fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107600:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
80107607:	83 ca 10             	or     $0x10,%edx
8010760a:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80107610:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107613:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
8010761a:	83 ca 60             	or     $0x60,%edx
8010761d:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80107623:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107626:	0f b6 90 95 00 00 00 	movzbl 0x95(%eax),%edx
8010762d:	83 ca 80             	or     $0xffffff80,%edx
80107630:	88 90 95 00 00 00    	mov    %dl,0x95(%eax)
80107636:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107639:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107640:	83 ca 0f             	or     $0xf,%edx
80107643:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107649:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010764c:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107653:	83 e2 ef             	and    $0xffffffef,%edx
80107656:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010765c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010765f:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107666:	83 e2 df             	and    $0xffffffdf,%edx
80107669:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
8010766f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107672:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
80107679:	83 ca 40             	or     $0x40,%edx
8010767c:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107682:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107685:	0f b6 90 96 00 00 00 	movzbl 0x96(%eax),%edx
8010768c:	83 ca 80             	or     $0xffffff80,%edx
8010768f:	88 90 96 00 00 00    	mov    %dl,0x96(%eax)
80107695:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107698:	c6 80 97 00 00 00 00 	movb   $0x0,0x97(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
8010769f:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076a2:	66 c7 80 98 00 00 00 	movw   $0xffff,0x98(%eax)
801076a9:	ff ff 
801076ab:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076ae:	66 c7 80 9a 00 00 00 	movw   $0x0,0x9a(%eax)
801076b5:	00 00 
801076b7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076ba:	c6 80 9c 00 00 00 00 	movb   $0x0,0x9c(%eax)
801076c1:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076c4:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801076cb:	83 e2 f0             	and    $0xfffffff0,%edx
801076ce:	83 ca 02             	or     $0x2,%edx
801076d1:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801076d7:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076da:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801076e1:	83 ca 10             	or     $0x10,%edx
801076e4:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801076ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
801076ed:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801076f4:	83 ca 60             	or     $0x60,%edx
801076f7:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
801076fd:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107700:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
80107707:	83 ca 80             	or     $0xffffff80,%edx
8010770a:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
80107710:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107713:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010771a:	83 ca 0f             	or     $0xf,%edx
8010771d:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80107723:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107726:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
8010772d:	83 e2 ef             	and    $0xffffffef,%edx
80107730:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80107736:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107739:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80107740:	83 e2 df             	and    $0xffffffdf,%edx
80107743:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
80107749:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010774c:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80107753:	83 ca 40             	or     $0x40,%edx
80107756:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010775c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010775f:	0f b6 90 9e 00 00 00 	movzbl 0x9e(%eax),%edx
80107766:	83 ca 80             	or     $0xffffff80,%edx
80107769:	88 90 9e 00 00 00    	mov    %dl,0x9e(%eax)
8010776f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107772:	c6 80 9f 00 00 00 00 	movb   $0x0,0x9f(%eax)

  // Map cpu, and curproc
  c->gdt[SEG_KCPU] = SEG(STA_W, &c->cpu, 8, 0);
80107779:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010777c:	05 b4 00 00 00       	add    $0xb4,%eax
80107781:	89 c3                	mov    %eax,%ebx
80107783:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107786:	05 b4 00 00 00       	add    $0xb4,%eax
8010778b:	c1 e8 10             	shr    $0x10,%eax
8010778e:	89 c1                	mov    %eax,%ecx
80107790:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107793:	05 b4 00 00 00       	add    $0xb4,%eax
80107798:	c1 e8 18             	shr    $0x18,%eax
8010779b:	89 c2                	mov    %eax,%edx
8010779d:	8b 45 f4             	mov    -0xc(%ebp),%eax
801077a0:	66 c7 80 88 00 00 00 	movw   $0x0,0x88(%eax)
801077a7:	00 00 
801077a9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801077ac:	66 89 98 8a 00 00 00 	mov    %bx,0x8a(%eax)
801077b3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801077b6:	88 88 8c 00 00 00    	mov    %cl,0x8c(%eax)
801077bc:	8b 45 f4             	mov    -0xc(%ebp),%eax
801077bf:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801077c6:	83 e1 f0             	and    $0xfffffff0,%ecx
801077c9:	83 c9 02             	or     $0x2,%ecx
801077cc:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801077d2:	8b 45 f4             	mov    -0xc(%ebp),%eax
801077d5:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801077dc:	83 c9 10             	or     $0x10,%ecx
801077df:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801077e5:	8b 45 f4             	mov    -0xc(%ebp),%eax
801077e8:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
801077ef:	83 e1 9f             	and    $0xffffff9f,%ecx
801077f2:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
801077f8:	8b 45 f4             	mov    -0xc(%ebp),%eax
801077fb:	0f b6 88 8d 00 00 00 	movzbl 0x8d(%eax),%ecx
80107802:	83 c9 80             	or     $0xffffff80,%ecx
80107805:	88 88 8d 00 00 00    	mov    %cl,0x8d(%eax)
8010780b:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010780e:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80107815:	83 e1 f0             	and    $0xfffffff0,%ecx
80107818:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
8010781e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107821:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80107828:	83 e1 ef             	and    $0xffffffef,%ecx
8010782b:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80107831:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107834:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
8010783b:	83 e1 df             	and    $0xffffffdf,%ecx
8010783e:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80107844:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107847:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
8010784e:	83 c9 40             	or     $0x40,%ecx
80107851:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
80107857:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010785a:	0f b6 88 8e 00 00 00 	movzbl 0x8e(%eax),%ecx
80107861:	83 c9 80             	or     $0xffffff80,%ecx
80107864:	88 88 8e 00 00 00    	mov    %cl,0x8e(%eax)
8010786a:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010786d:	88 90 8f 00 00 00    	mov    %dl,0x8f(%eax)

  lgdt(c->gdt, sizeof(c->gdt));
80107873:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107876:	83 c0 70             	add    $0x70,%eax
80107879:	c7 44 24 04 38 00 00 	movl   $0x38,0x4(%esp)
80107880:	00 
80107881:	89 04 24             	mov    %eax,(%esp)
80107884:	e8 37 fb ff ff       	call   801073c0 <lgdt>
  loadgs(SEG_KCPU << 3);
80107889:	c7 04 24 18 00 00 00 	movl   $0x18,(%esp)
80107890:	e8 6a fb ff ff       	call   801073ff <loadgs>
  
  // Initialize cpu-local storage.
  cpu = c;
80107895:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107898:	65 a3 00 00 00 00    	mov    %eax,%gs:0x0
  proc = 0;
8010789e:	65 c7 05 04 00 00 00 	movl   $0x0,%gs:0x4
801078a5:	00 00 00 00 
}
801078a9:	83 c4 24             	add    $0x24,%esp
801078ac:	5b                   	pop    %ebx
801078ad:	5d                   	pop    %ebp
801078ae:	c3                   	ret    

801078af <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
801078af:	55                   	push   %ebp
801078b0:	89 e5                	mov    %esp,%ebp
801078b2:	83 ec 28             	sub    $0x28,%esp
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
801078b5:	8b 45 0c             	mov    0xc(%ebp),%eax
801078b8:	c1 e8 16             	shr    $0x16,%eax
801078bb:	c1 e0 02             	shl    $0x2,%eax
801078be:	03 45 08             	add    0x8(%ebp),%eax
801078c1:	89 45 f0             	mov    %eax,-0x10(%ebp)
  if(*pde & PTE_P){
801078c4:	8b 45 f0             	mov    -0x10(%ebp),%eax
801078c7:	8b 00                	mov    (%eax),%eax
801078c9:	83 e0 01             	and    $0x1,%eax
801078cc:	84 c0                	test   %al,%al
801078ce:	74 17                	je     801078e7 <walkpgdir+0x38>
    pgtab = (pte_t*)p2v(PTE_ADDR(*pde));
801078d0:	8b 45 f0             	mov    -0x10(%ebp),%eax
801078d3:	8b 00                	mov    (%eax),%eax
801078d5:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801078da:	89 04 24             	mov    %eax,(%esp)
801078dd:	e8 4a fb ff ff       	call   8010742c <p2v>
801078e2:	89 45 f4             	mov    %eax,-0xc(%ebp)
801078e5:	eb 4b                	jmp    80107932 <walkpgdir+0x83>
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
801078e7:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
801078eb:	74 0e                	je     801078fb <walkpgdir+0x4c>
801078ed:	e8 9d b2 ff ff       	call   80102b8f <kalloc>
801078f2:	89 45 f4             	mov    %eax,-0xc(%ebp)
801078f5:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
801078f9:	75 07                	jne    80107902 <walkpgdir+0x53>
      return 0;
801078fb:	b8 00 00 00 00       	mov    $0x0,%eax
80107900:	eb 41                	jmp    80107943 <walkpgdir+0x94>
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
80107902:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80107909:	00 
8010790a:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107911:	00 
80107912:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107915:	89 04 24             	mov    %eax,(%esp)
80107918:	e8 65 d5 ff ff       	call   80104e82 <memset>
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table 
    // entries, if necessary.
    *pde = v2p(pgtab) | PTE_P | PTE_W | PTE_U;
8010791d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107920:	89 04 24             	mov    %eax,(%esp)
80107923:	e8 f7 fa ff ff       	call   8010741f <v2p>
80107928:	89 c2                	mov    %eax,%edx
8010792a:	83 ca 07             	or     $0x7,%edx
8010792d:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107930:	89 10                	mov    %edx,(%eax)
  }
  return &pgtab[PTX(va)];
80107932:	8b 45 0c             	mov    0xc(%ebp),%eax
80107935:	c1 e8 0c             	shr    $0xc,%eax
80107938:	25 ff 03 00 00       	and    $0x3ff,%eax
8010793d:	c1 e0 02             	shl    $0x2,%eax
80107940:	03 45 f4             	add    -0xc(%ebp),%eax
}
80107943:	c9                   	leave  
80107944:	c3                   	ret    

80107945 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80107945:	55                   	push   %ebp
80107946:	89 e5                	mov    %esp,%ebp
80107948:	83 ec 28             	sub    $0x28,%esp
  char *a, *last;
  pte_t *pte;
  
  a = (char*)PGROUNDDOWN((uint)va);
8010794b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010794e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80107953:	89 45 f4             	mov    %eax,-0xc(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80107956:	8b 45 0c             	mov    0xc(%ebp),%eax
80107959:	03 45 10             	add    0x10(%ebp),%eax
8010795c:	83 e8 01             	sub    $0x1,%eax
8010795f:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80107964:	89 45 f0             	mov    %eax,-0x10(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80107967:	c7 44 24 08 01 00 00 	movl   $0x1,0x8(%esp)
8010796e:	00 
8010796f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107972:	89 44 24 04          	mov    %eax,0x4(%esp)
80107976:	8b 45 08             	mov    0x8(%ebp),%eax
80107979:	89 04 24             	mov    %eax,(%esp)
8010797c:	e8 2e ff ff ff       	call   801078af <walkpgdir>
80107981:	89 45 ec             	mov    %eax,-0x14(%ebp)
80107984:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80107988:	75 07                	jne    80107991 <mappages+0x4c>
      return -1;
8010798a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010798f:	eb 46                	jmp    801079d7 <mappages+0x92>
    if(*pte & PTE_P)
80107991:	8b 45 ec             	mov    -0x14(%ebp),%eax
80107994:	8b 00                	mov    (%eax),%eax
80107996:	83 e0 01             	and    $0x1,%eax
80107999:	84 c0                	test   %al,%al
8010799b:	74 0c                	je     801079a9 <mappages+0x64>
      panic("remap");
8010799d:	c7 04 24 bc 87 10 80 	movl   $0x801087bc,(%esp)
801079a4:	e8 94 8b ff ff       	call   8010053d <panic>
    *pte = pa | perm | PTE_P;
801079a9:	8b 45 18             	mov    0x18(%ebp),%eax
801079ac:	0b 45 14             	or     0x14(%ebp),%eax
801079af:	89 c2                	mov    %eax,%edx
801079b1:	83 ca 01             	or     $0x1,%edx
801079b4:	8b 45 ec             	mov    -0x14(%ebp),%eax
801079b7:	89 10                	mov    %edx,(%eax)
    if(a == last)
801079b9:	8b 45 f4             	mov    -0xc(%ebp),%eax
801079bc:	3b 45 f0             	cmp    -0x10(%ebp),%eax
801079bf:	74 10                	je     801079d1 <mappages+0x8c>
      break;
    a += PGSIZE;
801079c1:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
    pa += PGSIZE;
801079c8:	81 45 14 00 10 00 00 	addl   $0x1000,0x14(%ebp)
  }
801079cf:	eb 96                	jmp    80107967 <mappages+0x22>
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
801079d1:	90                   	nop
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
801079d2:	b8 00 00 00 00       	mov    $0x0,%eax
}
801079d7:	c9                   	leave  
801079d8:	c3                   	ret    

801079d9 <setupkvm>:
};

// Set up kernel part of a page table.
pde_t*
setupkvm()
{
801079d9:	55                   	push   %ebp
801079da:	89 e5                	mov    %esp,%ebp
801079dc:	53                   	push   %ebx
801079dd:	83 ec 34             	sub    $0x34,%esp
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
801079e0:	e8 aa b1 ff ff       	call   80102b8f <kalloc>
801079e5:	89 45 f0             	mov    %eax,-0x10(%ebp)
801079e8:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
801079ec:	75 0a                	jne    801079f8 <setupkvm+0x1f>
    return 0;
801079ee:	b8 00 00 00 00       	mov    $0x0,%eax
801079f3:	e9 98 00 00 00       	jmp    80107a90 <setupkvm+0xb7>
  memset(pgdir, 0, PGSIZE);
801079f8:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801079ff:	00 
80107a00:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107a07:	00 
80107a08:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107a0b:	89 04 24             	mov    %eax,(%esp)
80107a0e:	e8 6f d4 ff ff       	call   80104e82 <memset>
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
80107a13:	c7 04 24 00 00 00 0e 	movl   $0xe000000,(%esp)
80107a1a:	e8 0d fa ff ff       	call   8010742c <p2v>
80107a1f:	3d 00 00 00 fe       	cmp    $0xfe000000,%eax
80107a24:	76 0c                	jbe    80107a32 <setupkvm+0x59>
    panic("PHYSTOP too high");
80107a26:	c7 04 24 c2 87 10 80 	movl   $0x801087c2,(%esp)
80107a2d:	e8 0b 8b ff ff       	call   8010053d <panic>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80107a32:	c7 45 f4 a0 b4 10 80 	movl   $0x8010b4a0,-0xc(%ebp)
80107a39:	eb 49                	jmp    80107a84 <setupkvm+0xab>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
                (uint)k->phys_start, k->perm) < 0)
80107a3b:	8b 45 f4             	mov    -0xc(%ebp),%eax
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
80107a3e:	8b 48 0c             	mov    0xc(%eax),%ecx
                (uint)k->phys_start, k->perm) < 0)
80107a41:	8b 45 f4             	mov    -0xc(%ebp),%eax
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
80107a44:	8b 50 04             	mov    0x4(%eax),%edx
80107a47:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107a4a:	8b 58 08             	mov    0x8(%eax),%ebx
80107a4d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107a50:	8b 40 04             	mov    0x4(%eax),%eax
80107a53:	29 c3                	sub    %eax,%ebx
80107a55:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107a58:	8b 00                	mov    (%eax),%eax
80107a5a:	89 4c 24 10          	mov    %ecx,0x10(%esp)
80107a5e:	89 54 24 0c          	mov    %edx,0xc(%esp)
80107a62:	89 5c 24 08          	mov    %ebx,0x8(%esp)
80107a66:	89 44 24 04          	mov    %eax,0x4(%esp)
80107a6a:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107a6d:	89 04 24             	mov    %eax,(%esp)
80107a70:	e8 d0 fe ff ff       	call   80107945 <mappages>
80107a75:	85 c0                	test   %eax,%eax
80107a77:	79 07                	jns    80107a80 <setupkvm+0xa7>
                (uint)k->phys_start, k->perm) < 0)
      return 0;
80107a79:	b8 00 00 00 00       	mov    $0x0,%eax
80107a7e:	eb 10                	jmp    80107a90 <setupkvm+0xb7>
  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (p2v(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80107a80:	83 45 f4 10          	addl   $0x10,-0xc(%ebp)
80107a84:	81 7d f4 e0 b4 10 80 	cmpl   $0x8010b4e0,-0xc(%ebp)
80107a8b:	72 ae                	jb     80107a3b <setupkvm+0x62>
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start, 
                (uint)k->phys_start, k->perm) < 0)
      return 0;
  return pgdir;
80107a8d:	8b 45 f0             	mov    -0x10(%ebp),%eax
}
80107a90:	83 c4 34             	add    $0x34,%esp
80107a93:	5b                   	pop    %ebx
80107a94:	5d                   	pop    %ebp
80107a95:	c3                   	ret    

80107a96 <kvmalloc>:

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
80107a96:	55                   	push   %ebp
80107a97:	89 e5                	mov    %esp,%ebp
80107a99:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
80107a9c:	e8 38 ff ff ff       	call   801079d9 <setupkvm>
80107aa1:	a3 f8 28 11 80       	mov    %eax,0x801128f8
  switchkvm();
80107aa6:	e8 02 00 00 00       	call   80107aad <switchkvm>
}
80107aab:	c9                   	leave  
80107aac:	c3                   	ret    

80107aad <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
80107aad:	55                   	push   %ebp
80107aae:	89 e5                	mov    %esp,%ebp
80107ab0:	83 ec 04             	sub    $0x4,%esp
  lcr3(v2p(kpgdir));   // switch to the kernel page table
80107ab3:	a1 f8 28 11 80       	mov    0x801128f8,%eax
80107ab8:	89 04 24             	mov    %eax,(%esp)
80107abb:	e8 5f f9 ff ff       	call   8010741f <v2p>
80107ac0:	89 04 24             	mov    %eax,(%esp)
80107ac3:	e8 4c f9 ff ff       	call   80107414 <lcr3>
}
80107ac8:	c9                   	leave  
80107ac9:	c3                   	ret    

80107aca <switchuvm>:

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
80107aca:	55                   	push   %ebp
80107acb:	89 e5                	mov    %esp,%ebp
80107acd:	53                   	push   %ebx
80107ace:	83 ec 14             	sub    $0x14,%esp
  pushcli();
80107ad1:	e8 a5 d2 ff ff       	call   80104d7b <pushcli>
  cpu->gdt[SEG_TSS] = SEG16(STS_T32A, &cpu->ts, sizeof(cpu->ts)-1, 0);
80107ad6:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107adc:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80107ae3:	83 c2 08             	add    $0x8,%edx
80107ae6:	89 d3                	mov    %edx,%ebx
80107ae8:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80107aef:	83 c2 08             	add    $0x8,%edx
80107af2:	c1 ea 10             	shr    $0x10,%edx
80107af5:	89 d1                	mov    %edx,%ecx
80107af7:	65 8b 15 00 00 00 00 	mov    %gs:0x0,%edx
80107afe:	83 c2 08             	add    $0x8,%edx
80107b01:	c1 ea 18             	shr    $0x18,%edx
80107b04:	66 c7 80 a0 00 00 00 	movw   $0x67,0xa0(%eax)
80107b0b:	67 00 
80107b0d:	66 89 98 a2 00 00 00 	mov    %bx,0xa2(%eax)
80107b14:	88 88 a4 00 00 00    	mov    %cl,0xa4(%eax)
80107b1a:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80107b21:	83 e1 f0             	and    $0xfffffff0,%ecx
80107b24:	83 c9 09             	or     $0x9,%ecx
80107b27:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
80107b2d:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80107b34:	83 c9 10             	or     $0x10,%ecx
80107b37:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
80107b3d:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80107b44:	83 e1 9f             	and    $0xffffff9f,%ecx
80107b47:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
80107b4d:	0f b6 88 a5 00 00 00 	movzbl 0xa5(%eax),%ecx
80107b54:	83 c9 80             	or     $0xffffff80,%ecx
80107b57:	88 88 a5 00 00 00    	mov    %cl,0xa5(%eax)
80107b5d:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80107b64:	83 e1 f0             	and    $0xfffffff0,%ecx
80107b67:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80107b6d:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80107b74:	83 e1 ef             	and    $0xffffffef,%ecx
80107b77:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80107b7d:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80107b84:	83 e1 df             	and    $0xffffffdf,%ecx
80107b87:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80107b8d:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80107b94:	83 c9 40             	or     $0x40,%ecx
80107b97:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80107b9d:	0f b6 88 a6 00 00 00 	movzbl 0xa6(%eax),%ecx
80107ba4:	83 e1 7f             	and    $0x7f,%ecx
80107ba7:	88 88 a6 00 00 00    	mov    %cl,0xa6(%eax)
80107bad:	88 90 a7 00 00 00    	mov    %dl,0xa7(%eax)
  cpu->gdt[SEG_TSS].s = 0;
80107bb3:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107bb9:	0f b6 90 a5 00 00 00 	movzbl 0xa5(%eax),%edx
80107bc0:	83 e2 ef             	and    $0xffffffef,%edx
80107bc3:	88 90 a5 00 00 00    	mov    %dl,0xa5(%eax)
  cpu->ts.ss0 = SEG_KDATA << 3;
80107bc9:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107bcf:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  cpu->ts.esp0 = (uint)proc->kstack + KSTACKSIZE;
80107bd5:	65 a1 00 00 00 00    	mov    %gs:0x0,%eax
80107bdb:	65 8b 15 04 00 00 00 	mov    %gs:0x4,%edx
80107be2:	8b 52 08             	mov    0x8(%edx),%edx
80107be5:	81 c2 00 10 00 00    	add    $0x1000,%edx
80107beb:	89 50 0c             	mov    %edx,0xc(%eax)
  ltr(SEG_TSS << 3);
80107bee:	c7 04 24 30 00 00 00 	movl   $0x30,(%esp)
80107bf5:	e8 ef f7 ff ff       	call   801073e9 <ltr>
  if(p->pgdir == 0)
80107bfa:	8b 45 08             	mov    0x8(%ebp),%eax
80107bfd:	8b 40 04             	mov    0x4(%eax),%eax
80107c00:	85 c0                	test   %eax,%eax
80107c02:	75 0c                	jne    80107c10 <switchuvm+0x146>
    panic("switchuvm: no pgdir");
80107c04:	c7 04 24 d3 87 10 80 	movl   $0x801087d3,(%esp)
80107c0b:	e8 2d 89 ff ff       	call   8010053d <panic>
  lcr3(v2p(p->pgdir));  // switch to new address space
80107c10:	8b 45 08             	mov    0x8(%ebp),%eax
80107c13:	8b 40 04             	mov    0x4(%eax),%eax
80107c16:	89 04 24             	mov    %eax,(%esp)
80107c19:	e8 01 f8 ff ff       	call   8010741f <v2p>
80107c1e:	89 04 24             	mov    %eax,(%esp)
80107c21:	e8 ee f7 ff ff       	call   80107414 <lcr3>
  popcli();
80107c26:	e8 98 d1 ff ff       	call   80104dc3 <popcli>
}
80107c2b:	83 c4 14             	add    $0x14,%esp
80107c2e:	5b                   	pop    %ebx
80107c2f:	5d                   	pop    %ebp
80107c30:	c3                   	ret    

80107c31 <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
80107c31:	55                   	push   %ebp
80107c32:	89 e5                	mov    %esp,%ebp
80107c34:	83 ec 38             	sub    $0x38,%esp
  char *mem;
  
  if(sz >= PGSIZE)
80107c37:	81 7d 10 ff 0f 00 00 	cmpl   $0xfff,0x10(%ebp)
80107c3e:	76 0c                	jbe    80107c4c <inituvm+0x1b>
    panic("inituvm: more than a page");
80107c40:	c7 04 24 e7 87 10 80 	movl   $0x801087e7,(%esp)
80107c47:	e8 f1 88 ff ff       	call   8010053d <panic>
  mem = kalloc();
80107c4c:	e8 3e af ff ff       	call   80102b8f <kalloc>
80107c51:	89 45 f4             	mov    %eax,-0xc(%ebp)
  memset(mem, 0, PGSIZE);
80107c54:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80107c5b:	00 
80107c5c:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107c63:	00 
80107c64:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107c67:	89 04 24             	mov    %eax,(%esp)
80107c6a:	e8 13 d2 ff ff       	call   80104e82 <memset>
  mappages(pgdir, 0, PGSIZE, v2p(mem), PTE_W|PTE_U);
80107c6f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107c72:	89 04 24             	mov    %eax,(%esp)
80107c75:	e8 a5 f7 ff ff       	call   8010741f <v2p>
80107c7a:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
80107c81:	00 
80107c82:	89 44 24 0c          	mov    %eax,0xc(%esp)
80107c86:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80107c8d:	00 
80107c8e:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107c95:	00 
80107c96:	8b 45 08             	mov    0x8(%ebp),%eax
80107c99:	89 04 24             	mov    %eax,(%esp)
80107c9c:	e8 a4 fc ff ff       	call   80107945 <mappages>
  memmove(mem, init, sz);
80107ca1:	8b 45 10             	mov    0x10(%ebp),%eax
80107ca4:	89 44 24 08          	mov    %eax,0x8(%esp)
80107ca8:	8b 45 0c             	mov    0xc(%ebp),%eax
80107cab:	89 44 24 04          	mov    %eax,0x4(%esp)
80107caf:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107cb2:	89 04 24             	mov    %eax,(%esp)
80107cb5:	e8 9b d2 ff ff       	call   80104f55 <memmove>
}
80107cba:	c9                   	leave  
80107cbb:	c3                   	ret    

80107cbc <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
80107cbc:	55                   	push   %ebp
80107cbd:	89 e5                	mov    %esp,%ebp
80107cbf:	53                   	push   %ebx
80107cc0:	83 ec 24             	sub    $0x24,%esp
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
80107cc3:	8b 45 0c             	mov    0xc(%ebp),%eax
80107cc6:	25 ff 0f 00 00       	and    $0xfff,%eax
80107ccb:	85 c0                	test   %eax,%eax
80107ccd:	74 0c                	je     80107cdb <loaduvm+0x1f>
    panic("loaduvm: addr must be page aligned");
80107ccf:	c7 04 24 04 88 10 80 	movl   $0x80108804,(%esp)
80107cd6:	e8 62 88 ff ff       	call   8010053d <panic>
  for(i = 0; i < sz; i += PGSIZE){
80107cdb:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80107ce2:	e9 ad 00 00 00       	jmp    80107d94 <loaduvm+0xd8>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80107ce7:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107cea:	8b 55 0c             	mov    0xc(%ebp),%edx
80107ced:	01 d0                	add    %edx,%eax
80107cef:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80107cf6:	00 
80107cf7:	89 44 24 04          	mov    %eax,0x4(%esp)
80107cfb:	8b 45 08             	mov    0x8(%ebp),%eax
80107cfe:	89 04 24             	mov    %eax,(%esp)
80107d01:	e8 a9 fb ff ff       	call   801078af <walkpgdir>
80107d06:	89 45 ec             	mov    %eax,-0x14(%ebp)
80107d09:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80107d0d:	75 0c                	jne    80107d1b <loaduvm+0x5f>
      panic("loaduvm: address should exist");
80107d0f:	c7 04 24 27 88 10 80 	movl   $0x80108827,(%esp)
80107d16:	e8 22 88 ff ff       	call   8010053d <panic>
    pa = PTE_ADDR(*pte);
80107d1b:	8b 45 ec             	mov    -0x14(%ebp),%eax
80107d1e:	8b 00                	mov    (%eax),%eax
80107d20:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80107d25:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(sz - i < PGSIZE)
80107d28:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d2b:	8b 55 18             	mov    0x18(%ebp),%edx
80107d2e:	89 d1                	mov    %edx,%ecx
80107d30:	29 c1                	sub    %eax,%ecx
80107d32:	89 c8                	mov    %ecx,%eax
80107d34:	3d ff 0f 00 00       	cmp    $0xfff,%eax
80107d39:	77 11                	ja     80107d4c <loaduvm+0x90>
      n = sz - i;
80107d3b:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d3e:	8b 55 18             	mov    0x18(%ebp),%edx
80107d41:	89 d1                	mov    %edx,%ecx
80107d43:	29 c1                	sub    %eax,%ecx
80107d45:	89 c8                	mov    %ecx,%eax
80107d47:	89 45 f0             	mov    %eax,-0x10(%ebp)
80107d4a:	eb 07                	jmp    80107d53 <loaduvm+0x97>
    else
      n = PGSIZE;
80107d4c:	c7 45 f0 00 10 00 00 	movl   $0x1000,-0x10(%ebp)
    if(readi(ip, p2v(pa), offset+i, n) != n)
80107d53:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d56:	8b 55 14             	mov    0x14(%ebp),%edx
80107d59:	8d 1c 02             	lea    (%edx,%eax,1),%ebx
80107d5c:	8b 45 e8             	mov    -0x18(%ebp),%eax
80107d5f:	89 04 24             	mov    %eax,(%esp)
80107d62:	e8 c5 f6 ff ff       	call   8010742c <p2v>
80107d67:	8b 55 f0             	mov    -0x10(%ebp),%edx
80107d6a:	89 54 24 0c          	mov    %edx,0xc(%esp)
80107d6e:	89 5c 24 08          	mov    %ebx,0x8(%esp)
80107d72:	89 44 24 04          	mov    %eax,0x4(%esp)
80107d76:	8b 45 10             	mov    0x10(%ebp),%eax
80107d79:	89 04 24             	mov    %eax,(%esp)
80107d7c:	e8 6d a0 ff ff       	call   80101dee <readi>
80107d81:	3b 45 f0             	cmp    -0x10(%ebp),%eax
80107d84:	74 07                	je     80107d8d <loaduvm+0xd1>
      return -1;
80107d86:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107d8b:	eb 18                	jmp    80107da5 <loaduvm+0xe9>
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
80107d8d:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80107d94:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107d97:	3b 45 18             	cmp    0x18(%ebp),%eax
80107d9a:	0f 82 47 ff ff ff    	jb     80107ce7 <loaduvm+0x2b>
    else
      n = PGSIZE;
    if(readi(ip, p2v(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
80107da0:	b8 00 00 00 00       	mov    $0x0,%eax
}
80107da5:	83 c4 24             	add    $0x24,%esp
80107da8:	5b                   	pop    %ebx
80107da9:	5d                   	pop    %ebp
80107daa:	c3                   	ret    

80107dab <allocuvm>:

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80107dab:	55                   	push   %ebp
80107dac:	89 e5                	mov    %esp,%ebp
80107dae:	83 ec 38             	sub    $0x38,%esp
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
80107db1:	8b 45 10             	mov    0x10(%ebp),%eax
80107db4:	85 c0                	test   %eax,%eax
80107db6:	79 0a                	jns    80107dc2 <allocuvm+0x17>
    return 0;
80107db8:	b8 00 00 00 00       	mov    $0x0,%eax
80107dbd:	e9 c1 00 00 00       	jmp    80107e83 <allocuvm+0xd8>
  if(newsz < oldsz)
80107dc2:	8b 45 10             	mov    0x10(%ebp),%eax
80107dc5:	3b 45 0c             	cmp    0xc(%ebp),%eax
80107dc8:	73 08                	jae    80107dd2 <allocuvm+0x27>
    return oldsz;
80107dca:	8b 45 0c             	mov    0xc(%ebp),%eax
80107dcd:	e9 b1 00 00 00       	jmp    80107e83 <allocuvm+0xd8>

  a = PGROUNDUP(oldsz);
80107dd2:	8b 45 0c             	mov    0xc(%ebp),%eax
80107dd5:	05 ff 0f 00 00       	add    $0xfff,%eax
80107dda:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80107ddf:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a < newsz; a += PGSIZE){
80107de2:	e9 8d 00 00 00       	jmp    80107e74 <allocuvm+0xc9>
    mem = kalloc();
80107de7:	e8 a3 ad ff ff       	call   80102b8f <kalloc>
80107dec:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(mem == 0){
80107def:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107df3:	75 2c                	jne    80107e21 <allocuvm+0x76>
      cprintf("allocuvm out of memory\n");
80107df5:	c7 04 24 45 88 10 80 	movl   $0x80108845,(%esp)
80107dfc:	e8 a0 85 ff ff       	call   801003a1 <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
80107e01:	8b 45 0c             	mov    0xc(%ebp),%eax
80107e04:	89 44 24 08          	mov    %eax,0x8(%esp)
80107e08:	8b 45 10             	mov    0x10(%ebp),%eax
80107e0b:	89 44 24 04          	mov    %eax,0x4(%esp)
80107e0f:	8b 45 08             	mov    0x8(%ebp),%eax
80107e12:	89 04 24             	mov    %eax,(%esp)
80107e15:	e8 6b 00 00 00       	call   80107e85 <deallocuvm>
      return 0;
80107e1a:	b8 00 00 00 00       	mov    $0x0,%eax
80107e1f:	eb 62                	jmp    80107e83 <allocuvm+0xd8>
    }
    memset(mem, 0, PGSIZE);
80107e21:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80107e28:	00 
80107e29:	c7 44 24 04 00 00 00 	movl   $0x0,0x4(%esp)
80107e30:	00 
80107e31:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107e34:	89 04 24             	mov    %eax,(%esp)
80107e37:	e8 46 d0 ff ff       	call   80104e82 <memset>
    mappages(pgdir, (char*)a, PGSIZE, v2p(mem), PTE_W|PTE_U);
80107e3c:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107e3f:	89 04 24             	mov    %eax,(%esp)
80107e42:	e8 d8 f5 ff ff       	call   8010741f <v2p>
80107e47:	8b 55 f4             	mov    -0xc(%ebp),%edx
80107e4a:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
80107e51:	00 
80107e52:	89 44 24 0c          	mov    %eax,0xc(%esp)
80107e56:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
80107e5d:	00 
80107e5e:	89 54 24 04          	mov    %edx,0x4(%esp)
80107e62:	8b 45 08             	mov    0x8(%ebp),%eax
80107e65:	89 04 24             	mov    %eax,(%esp)
80107e68:	e8 d8 fa ff ff       	call   80107945 <mappages>
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
80107e6d:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80107e74:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107e77:	3b 45 10             	cmp    0x10(%ebp),%eax
80107e7a:	0f 82 67 ff ff ff    	jb     80107de7 <allocuvm+0x3c>
      return 0;
    }
    memset(mem, 0, PGSIZE);
    mappages(pgdir, (char*)a, PGSIZE, v2p(mem), PTE_W|PTE_U);
  }
  return newsz;
80107e80:	8b 45 10             	mov    0x10(%ebp),%eax
}
80107e83:	c9                   	leave  
80107e84:	c3                   	ret    

80107e85 <deallocuvm>:
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80107e85:	55                   	push   %ebp
80107e86:	89 e5                	mov    %esp,%ebp
80107e88:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
80107e8b:	8b 45 10             	mov    0x10(%ebp),%eax
80107e8e:	3b 45 0c             	cmp    0xc(%ebp),%eax
80107e91:	72 08                	jb     80107e9b <deallocuvm+0x16>
    return oldsz;
80107e93:	8b 45 0c             	mov    0xc(%ebp),%eax
80107e96:	e9 a4 00 00 00       	jmp    80107f3f <deallocuvm+0xba>

  a = PGROUNDUP(newsz);
80107e9b:	8b 45 10             	mov    0x10(%ebp),%eax
80107e9e:	05 ff 0f 00 00       	add    $0xfff,%eax
80107ea3:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80107ea8:	89 45 f4             	mov    %eax,-0xc(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80107eab:	e9 80 00 00 00       	jmp    80107f30 <deallocuvm+0xab>
    pte = walkpgdir(pgdir, (char*)a, 0);
80107eb0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107eb3:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80107eba:	00 
80107ebb:	89 44 24 04          	mov    %eax,0x4(%esp)
80107ebf:	8b 45 08             	mov    0x8(%ebp),%eax
80107ec2:	89 04 24             	mov    %eax,(%esp)
80107ec5:	e8 e5 f9 ff ff       	call   801078af <walkpgdir>
80107eca:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(!pte)
80107ecd:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80107ed1:	75 09                	jne    80107edc <deallocuvm+0x57>
      a += (NPTENTRIES - 1) * PGSIZE;
80107ed3:	81 45 f4 00 f0 3f 00 	addl   $0x3ff000,-0xc(%ebp)
80107eda:	eb 4d                	jmp    80107f29 <deallocuvm+0xa4>
    else if((*pte & PTE_P) != 0){
80107edc:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107edf:	8b 00                	mov    (%eax),%eax
80107ee1:	83 e0 01             	and    $0x1,%eax
80107ee4:	84 c0                	test   %al,%al
80107ee6:	74 41                	je     80107f29 <deallocuvm+0xa4>
      pa = PTE_ADDR(*pte);
80107ee8:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107eeb:	8b 00                	mov    (%eax),%eax
80107eed:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80107ef2:	89 45 ec             	mov    %eax,-0x14(%ebp)
      if(pa == 0)
80107ef5:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80107ef9:	75 0c                	jne    80107f07 <deallocuvm+0x82>
        panic("kfree");
80107efb:	c7 04 24 5d 88 10 80 	movl   $0x8010885d,(%esp)
80107f02:	e8 36 86 ff ff       	call   8010053d <panic>
      char *v = p2v(pa);
80107f07:	8b 45 ec             	mov    -0x14(%ebp),%eax
80107f0a:	89 04 24             	mov    %eax,(%esp)
80107f0d:	e8 1a f5 ff ff       	call   8010742c <p2v>
80107f12:	89 45 e8             	mov    %eax,-0x18(%ebp)
      kfree(v);
80107f15:	8b 45 e8             	mov    -0x18(%ebp),%eax
80107f18:	89 04 24             	mov    %eax,(%esp)
80107f1b:	e8 d6 ab ff ff       	call   80102af6 <kfree>
      *pte = 0;
80107f20:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107f23:	c7 00 00 00 00 00    	movl   $0x0,(%eax)

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
80107f29:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
80107f30:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f33:	3b 45 0c             	cmp    0xc(%ebp),%eax
80107f36:	0f 82 74 ff ff ff    	jb     80107eb0 <deallocuvm+0x2b>
      char *v = p2v(pa);
      kfree(v);
      *pte = 0;
    }
  }
  return newsz;
80107f3c:	8b 45 10             	mov    0x10(%ebp),%eax
}
80107f3f:	c9                   	leave  
80107f40:	c3                   	ret    

80107f41 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80107f41:	55                   	push   %ebp
80107f42:	89 e5                	mov    %esp,%ebp
80107f44:	83 ec 28             	sub    $0x28,%esp
  uint i;

  if(pgdir == 0)
80107f47:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80107f4b:	75 0c                	jne    80107f59 <freevm+0x18>
    panic("freevm: no pgdir");
80107f4d:	c7 04 24 63 88 10 80 	movl   $0x80108863,(%esp)
80107f54:	e8 e4 85 ff ff       	call   8010053d <panic>
  deallocuvm(pgdir, KERNBASE, 0);
80107f59:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80107f60:	00 
80107f61:	c7 44 24 04 00 00 00 	movl   $0x80000000,0x4(%esp)
80107f68:	80 
80107f69:	8b 45 08             	mov    0x8(%ebp),%eax
80107f6c:	89 04 24             	mov    %eax,(%esp)
80107f6f:	e8 11 ff ff ff       	call   80107e85 <deallocuvm>
  for(i = 0; i < NPDENTRIES; i++){
80107f74:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
80107f7b:	eb 3c                	jmp    80107fb9 <freevm+0x78>
    if(pgdir[i] & PTE_P){
80107f7d:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f80:	c1 e0 02             	shl    $0x2,%eax
80107f83:	03 45 08             	add    0x8(%ebp),%eax
80107f86:	8b 00                	mov    (%eax),%eax
80107f88:	83 e0 01             	and    $0x1,%eax
80107f8b:	84 c0                	test   %al,%al
80107f8d:	74 26                	je     80107fb5 <freevm+0x74>
      char * v = p2v(PTE_ADDR(pgdir[i]));
80107f8f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80107f92:	c1 e0 02             	shl    $0x2,%eax
80107f95:	03 45 08             	add    0x8(%ebp),%eax
80107f98:	8b 00                	mov    (%eax),%eax
80107f9a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80107f9f:	89 04 24             	mov    %eax,(%esp)
80107fa2:	e8 85 f4 ff ff       	call   8010742c <p2v>
80107fa7:	89 45 f0             	mov    %eax,-0x10(%ebp)
      kfree(v);
80107faa:	8b 45 f0             	mov    -0x10(%ebp),%eax
80107fad:	89 04 24             	mov    %eax,(%esp)
80107fb0:	e8 41 ab ff ff       	call   80102af6 <kfree>
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80107fb5:	83 45 f4 01          	addl   $0x1,-0xc(%ebp)
80107fb9:	81 7d f4 ff 03 00 00 	cmpl   $0x3ff,-0xc(%ebp)
80107fc0:	76 bb                	jbe    80107f7d <freevm+0x3c>
    if(pgdir[i] & PTE_P){
      char * v = p2v(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
80107fc2:	8b 45 08             	mov    0x8(%ebp),%eax
80107fc5:	89 04 24             	mov    %eax,(%esp)
80107fc8:	e8 29 ab ff ff       	call   80102af6 <kfree>
}
80107fcd:	c9                   	leave  
80107fce:	c3                   	ret    

80107fcf <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80107fcf:	55                   	push   %ebp
80107fd0:	89 e5                	mov    %esp,%ebp
80107fd2:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80107fd5:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80107fdc:	00 
80107fdd:	8b 45 0c             	mov    0xc(%ebp),%eax
80107fe0:	89 44 24 04          	mov    %eax,0x4(%esp)
80107fe4:	8b 45 08             	mov    0x8(%ebp),%eax
80107fe7:	89 04 24             	mov    %eax,(%esp)
80107fea:	e8 c0 f8 ff ff       	call   801078af <walkpgdir>
80107fef:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if(pte == 0)
80107ff2:	83 7d f4 00          	cmpl   $0x0,-0xc(%ebp)
80107ff6:	75 0c                	jne    80108004 <clearpteu+0x35>
    panic("clearpteu");
80107ff8:	c7 04 24 74 88 10 80 	movl   $0x80108874,(%esp)
80107fff:	e8 39 85 ff ff       	call   8010053d <panic>
  *pte &= ~PTE_U;
80108004:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108007:	8b 00                	mov    (%eax),%eax
80108009:	89 c2                	mov    %eax,%edx
8010800b:	83 e2 fb             	and    $0xfffffffb,%edx
8010800e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108011:	89 10                	mov    %edx,(%eax)
}
80108013:	c9                   	leave  
80108014:	c3                   	ret    

80108015 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80108015:	55                   	push   %ebp
80108016:	89 e5                	mov    %esp,%ebp
80108018:	83 ec 48             	sub    $0x48,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i;
  char *mem;

  if((d = setupkvm()) == 0)
8010801b:	e8 b9 f9 ff ff       	call   801079d9 <setupkvm>
80108020:	89 45 f0             	mov    %eax,-0x10(%ebp)
80108023:	83 7d f0 00          	cmpl   $0x0,-0x10(%ebp)
80108027:	75 0a                	jne    80108033 <copyuvm+0x1e>
    return 0;
80108029:	b8 00 00 00 00       	mov    $0x0,%eax
8010802e:	e9 f1 00 00 00       	jmp    80108124 <copyuvm+0x10f>
  for(i = 0; i < sz; i += PGSIZE){
80108033:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)
8010803a:	e9 c0 00 00 00       	jmp    801080ff <copyuvm+0xea>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
8010803f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108042:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108049:	00 
8010804a:	89 44 24 04          	mov    %eax,0x4(%esp)
8010804e:	8b 45 08             	mov    0x8(%ebp),%eax
80108051:	89 04 24             	mov    %eax,(%esp)
80108054:	e8 56 f8 ff ff       	call   801078af <walkpgdir>
80108059:	89 45 ec             	mov    %eax,-0x14(%ebp)
8010805c:	83 7d ec 00          	cmpl   $0x0,-0x14(%ebp)
80108060:	75 0c                	jne    8010806e <copyuvm+0x59>
      panic("copyuvm: pte should exist");
80108062:	c7 04 24 7e 88 10 80 	movl   $0x8010887e,(%esp)
80108069:	e8 cf 84 ff ff       	call   8010053d <panic>
    if(!(*pte & PTE_P))
8010806e:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108071:	8b 00                	mov    (%eax),%eax
80108073:	83 e0 01             	and    $0x1,%eax
80108076:	85 c0                	test   %eax,%eax
80108078:	75 0c                	jne    80108086 <copyuvm+0x71>
      panic("copyuvm: page not present");
8010807a:	c7 04 24 98 88 10 80 	movl   $0x80108898,(%esp)
80108081:	e8 b7 84 ff ff       	call   8010053d <panic>
    pa = PTE_ADDR(*pte);
80108086:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108089:	8b 00                	mov    (%eax),%eax
8010808b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108090:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if((mem = kalloc()) == 0)
80108093:	e8 f7 aa ff ff       	call   80102b8f <kalloc>
80108098:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010809b:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
8010809f:	74 6f                	je     80108110 <copyuvm+0xfb>
      goto bad;
    memmove(mem, (char*)p2v(pa), PGSIZE);
801080a1:	8b 45 e8             	mov    -0x18(%ebp),%eax
801080a4:	89 04 24             	mov    %eax,(%esp)
801080a7:	e8 80 f3 ff ff       	call   8010742c <p2v>
801080ac:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801080b3:	00 
801080b4:	89 44 24 04          	mov    %eax,0x4(%esp)
801080b8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801080bb:	89 04 24             	mov    %eax,(%esp)
801080be:	e8 92 ce ff ff       	call   80104f55 <memmove>
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), PTE_W|PTE_U) < 0)
801080c3:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801080c6:	89 04 24             	mov    %eax,(%esp)
801080c9:	e8 51 f3 ff ff       	call   8010741f <v2p>
801080ce:	8b 55 f4             	mov    -0xc(%ebp),%edx
801080d1:	c7 44 24 10 06 00 00 	movl   $0x6,0x10(%esp)
801080d8:	00 
801080d9:	89 44 24 0c          	mov    %eax,0xc(%esp)
801080dd:	c7 44 24 08 00 10 00 	movl   $0x1000,0x8(%esp)
801080e4:	00 
801080e5:	89 54 24 04          	mov    %edx,0x4(%esp)
801080e9:	8b 45 f0             	mov    -0x10(%ebp),%eax
801080ec:	89 04 24             	mov    %eax,(%esp)
801080ef:	e8 51 f8 ff ff       	call   80107945 <mappages>
801080f4:	85 c0                	test   %eax,%eax
801080f6:	78 1b                	js     80108113 <copyuvm+0xfe>
  uint pa, i;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
801080f8:	81 45 f4 00 10 00 00 	addl   $0x1000,-0xc(%ebp)
801080ff:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108102:	3b 45 0c             	cmp    0xc(%ebp),%eax
80108105:	0f 82 34 ff ff ff    	jb     8010803f <copyuvm+0x2a>
      goto bad;
    memmove(mem, (char*)p2v(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), PTE_W|PTE_U) < 0)
      goto bad;
  }
  return d;
8010810b:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010810e:	eb 14                	jmp    80108124 <copyuvm+0x10f>
      panic("copyuvm: pte should exist");
    if(!(*pte & PTE_P))
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
80108110:	90                   	nop
80108111:	eb 01                	jmp    80108114 <copyuvm+0xff>
    memmove(mem, (char*)p2v(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, v2p(mem), PTE_W|PTE_U) < 0)
      goto bad;
80108113:	90                   	nop
  }
  return d;

bad:
  freevm(d);
80108114:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108117:	89 04 24             	mov    %eax,(%esp)
8010811a:	e8 22 fe ff ff       	call   80107f41 <freevm>
  return 0;
8010811f:	b8 00 00 00 00       	mov    $0x0,%eax
}
80108124:	c9                   	leave  
80108125:	c3                   	ret    

80108126 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80108126:	55                   	push   %ebp
80108127:	89 e5                	mov    %esp,%ebp
80108129:	83 ec 28             	sub    $0x28,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
8010812c:	c7 44 24 08 00 00 00 	movl   $0x0,0x8(%esp)
80108133:	00 
80108134:	8b 45 0c             	mov    0xc(%ebp),%eax
80108137:	89 44 24 04          	mov    %eax,0x4(%esp)
8010813b:	8b 45 08             	mov    0x8(%ebp),%eax
8010813e:	89 04 24             	mov    %eax,(%esp)
80108141:	e8 69 f7 ff ff       	call   801078af <walkpgdir>
80108146:	89 45 f4             	mov    %eax,-0xc(%ebp)
  if((*pte & PTE_P) == 0)
80108149:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010814c:	8b 00                	mov    (%eax),%eax
8010814e:	83 e0 01             	and    $0x1,%eax
80108151:	85 c0                	test   %eax,%eax
80108153:	75 07                	jne    8010815c <uva2ka+0x36>
    return 0;
80108155:	b8 00 00 00 00       	mov    $0x0,%eax
8010815a:	eb 25                	jmp    80108181 <uva2ka+0x5b>
  if((*pte & PTE_U) == 0)
8010815c:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010815f:	8b 00                	mov    (%eax),%eax
80108161:	83 e0 04             	and    $0x4,%eax
80108164:	85 c0                	test   %eax,%eax
80108166:	75 07                	jne    8010816f <uva2ka+0x49>
    return 0;
80108168:	b8 00 00 00 00       	mov    $0x0,%eax
8010816d:	eb 12                	jmp    80108181 <uva2ka+0x5b>
  return (char*)p2v(PTE_ADDR(*pte));
8010816f:	8b 45 f4             	mov    -0xc(%ebp),%eax
80108172:	8b 00                	mov    (%eax),%eax
80108174:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80108179:	89 04 24             	mov    %eax,(%esp)
8010817c:	e8 ab f2 ff ff       	call   8010742c <p2v>
}
80108181:	c9                   	leave  
80108182:	c3                   	ret    

80108183 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80108183:	55                   	push   %ebp
80108184:	89 e5                	mov    %esp,%ebp
80108186:	83 ec 28             	sub    $0x28,%esp
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
80108189:	8b 45 10             	mov    0x10(%ebp),%eax
8010818c:	89 45 f4             	mov    %eax,-0xc(%ebp)
  while(len > 0){
8010818f:	e9 8b 00 00 00       	jmp    8010821f <copyout+0x9c>
    va0 = (uint)PGROUNDDOWN(va);
80108194:	8b 45 0c             	mov    0xc(%ebp),%eax
80108197:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010819c:	89 45 ec             	mov    %eax,-0x14(%ebp)
    pa0 = uva2ka(pgdir, (char*)va0);
8010819f:	8b 45 ec             	mov    -0x14(%ebp),%eax
801081a2:	89 44 24 04          	mov    %eax,0x4(%esp)
801081a6:	8b 45 08             	mov    0x8(%ebp),%eax
801081a9:	89 04 24             	mov    %eax,(%esp)
801081ac:	e8 75 ff ff ff       	call   80108126 <uva2ka>
801081b1:	89 45 e8             	mov    %eax,-0x18(%ebp)
    if(pa0 == 0)
801081b4:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
801081b8:	75 07                	jne    801081c1 <copyout+0x3e>
      return -1;
801081ba:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801081bf:	eb 6d                	jmp    8010822e <copyout+0xab>
    n = PGSIZE - (va - va0);
801081c1:	8b 45 0c             	mov    0xc(%ebp),%eax
801081c4:	8b 55 ec             	mov    -0x14(%ebp),%edx
801081c7:	89 d1                	mov    %edx,%ecx
801081c9:	29 c1                	sub    %eax,%ecx
801081cb:	89 c8                	mov    %ecx,%eax
801081cd:	05 00 10 00 00       	add    $0x1000,%eax
801081d2:	89 45 f0             	mov    %eax,-0x10(%ebp)
    if(n > len)
801081d5:	8b 45 f0             	mov    -0x10(%ebp),%eax
801081d8:	3b 45 14             	cmp    0x14(%ebp),%eax
801081db:	76 06                	jbe    801081e3 <copyout+0x60>
      n = len;
801081dd:	8b 45 14             	mov    0x14(%ebp),%eax
801081e0:	89 45 f0             	mov    %eax,-0x10(%ebp)
    memmove(pa0 + (va - va0), buf, n);
801081e3:	8b 45 ec             	mov    -0x14(%ebp),%eax
801081e6:	8b 55 0c             	mov    0xc(%ebp),%edx
801081e9:	89 d1                	mov    %edx,%ecx
801081eb:	29 c1                	sub    %eax,%ecx
801081ed:	89 c8                	mov    %ecx,%eax
801081ef:	03 45 e8             	add    -0x18(%ebp),%eax
801081f2:	8b 55 f0             	mov    -0x10(%ebp),%edx
801081f5:	89 54 24 08          	mov    %edx,0x8(%esp)
801081f9:	8b 55 f4             	mov    -0xc(%ebp),%edx
801081fc:	89 54 24 04          	mov    %edx,0x4(%esp)
80108200:	89 04 24             	mov    %eax,(%esp)
80108203:	e8 4d cd ff ff       	call   80104f55 <memmove>
    len -= n;
80108208:	8b 45 f0             	mov    -0x10(%ebp),%eax
8010820b:	29 45 14             	sub    %eax,0x14(%ebp)
    buf += n;
8010820e:	8b 45 f0             	mov    -0x10(%ebp),%eax
80108211:	01 45 f4             	add    %eax,-0xc(%ebp)
    va = va0 + PGSIZE;
80108214:	8b 45 ec             	mov    -0x14(%ebp),%eax
80108217:	05 00 10 00 00       	add    $0x1000,%eax
8010821c:	89 45 0c             	mov    %eax,0xc(%ebp)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
8010821f:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
80108223:	0f 85 6b ff ff ff    	jne    80108194 <copyout+0x11>
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
80108229:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010822e:	c9                   	leave  
8010822f:	c3                   	ret    
